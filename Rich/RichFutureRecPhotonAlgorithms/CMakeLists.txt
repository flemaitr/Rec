################################################################################
# Package: RichFutureRecPhotonAlgorithms
################################################################################
gaudi_subdir(RichFutureRecPhotonAlgorithms v1r0)

gaudi_depends_on_subdirs(Rich/RichFutureRecBase
                         Rich/RichFutureRecEvent
                         Rich/RichUtils
                         Rich/RichFutureUtils         
                         Kernel/VectorClass)

find_package(Boost)
find_package(GSL)
find_package(VDT)
find_package(Eigen)
find_package(ROOT)
find_package(Vc)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
                           ${EIGEN_INCLUDE_DIRS} ${Vc_INCLUDE_DIR})

gaudi_add_module(RichFutureRecPhotonAlgorithms
                 src/*.cpp
                 INCLUDE_DIRS Boost GSL VDT Eigen Kernel/VectorClass Rich/RichFutureRecBase Rich/RichFutureRecEvent Rich/RichUtils Rich/RichFutureUtils
                 LINK_LIBRARIES Boost GSL VDT VectorClassLib RichFutureRecBase RichFutureRecEvent RichUtils RichFutureUtils)

# Fixes for GCC7.
if( BINARY_TAG_COMP_NAME STREQUAL "gcc" AND BINARY_TAG_COMP_VERSION VERSION_GREATER "6.99")
  set_property(TARGET RichFutureRecPhotonAlgorithms APPEND PROPERTY COMPILE_FLAGS " -faligned-new ")
endif()
