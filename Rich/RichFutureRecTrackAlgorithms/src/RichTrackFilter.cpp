
// local
#include "RichTrackFilter.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

TrackFilter::TrackFilter( const std::string& name,
                          ISvcLocator* pSvcLocator )
  : MultiTransformer ( name, pSvcLocator,
                       { KeyValue{ "InTracksLocation",       LHCb::TrackLocation::Default } },
                       { KeyValue{ "OutLongTracksLocation",  LHCb::TrackLocation::Default+"RichLong" },
                         KeyValue{ "OutDownTracksLocation",  LHCb::TrackLocation::Default+"RichDown" },
                         KeyValue{ "OutUpTracksLocation",    LHCb::TrackLocation::Default+"RichUp"   } } )
{
  // Debug messages
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

OutData TrackFilter::operator()( const LHCb::Tracks& tracks ) const
{
  OutData filteredTks;
  auto & longTks = std::get<0>(filteredTks);
  auto & downTks = std::get<1>(filteredTks);
  auto & upTks   = std::get<2>(filteredTks);

  _ri_debug << "Found " << tracks.size() << " tracks" << endmsg;

  for ( const auto * tk : tracks )
  {
    _ri_verbo << " -> type " << tk->type() << endmsg;
    if      ( tk->type() == LHCb::Track::Types::Long       ) { longTks.insert( tk ); }
    else if ( tk->type() == LHCb::Track::Types::Downstream ) { downTks.insert( tk ); }
    else if ( tk->type() == LHCb::Track::Types::Upstream   ) { upTks  .insert( tk ); }
  }

  _ri_debug << "Selected " << longTks.size() << " Long, " << downTks.size()
            << " Downstream and " << upTks.size() << " Upstream tracks" << endmsg;

  return filteredTks;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackFilter )

//=============================================================================
