
#pragma once

// Gaudi
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecSpacePoints.h"

// Rec Utils
#include "RichRecUtils/RichRadCorrLocalPositions.h"

// Interfaces
#include "RichFutureInterfaces/IRichSmartIDTool.h"

namespace Rich
{
  namespace Future
  {

    // Use the functional framework
    using namespace Gaudi::Functional;

    namespace Rec
    {

      /** @class RayTraceTrackLocalPoints RichRayTraceTrackLocalPoints.h
       *
       *  Converts the ray traced track global PD positions to local coordinates.
       *
       *  @author Chris Jones
       *  @date   2016-09-30
       */

      class RayTraceTrackLocalPoints final :
        public Transformer< SegmentPanelSpacePoints::Vector( const SegmentPanelSpacePoints::Vector& ),
                            Traits::BaseClass_t<AlgBase> >
      {

      public:

        /// Standard constructor
        RayTraceTrackLocalPoints( const std::string& name,
                                   ISvcLocator* pSvcLocator );

        /// Initialization after creation
        StatusCode initialize() override;

      public:

        /// Functional operator
        SegmentPanelSpacePoints::Vector
          operator()( const SegmentPanelSpacePoints::Vector& gPoints ) const override;

      private:

        /// RichSmartID Tool
        ToolHandle<const ISmartIDTool> m_idTool
        { "Rich::Future::SmartIDTool/SmartIDTool:PUBLIC", this };

      };

    }
  }
}
