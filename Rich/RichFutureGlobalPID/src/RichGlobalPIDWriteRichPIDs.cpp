
// local
#include "RichGlobalPIDWriteRichPIDs.h"

using namespace Rich::Future::Rec;
using namespace Rich::Future::Rec::GlobalPID;

//=============================================================================

WriteRichPIDs::
WriteRichPIDs( const std::string& name, ISvcLocator* pSvcLocator )
  : Transformer( name, pSvcLocator,
                 { KeyValue{ "TracksLocation",              LHCb::TrackLocation::Default },
                   KeyValue{ "SummaryTracksLocation",       Summary::TESLocations::Tracks },
                   KeyValue{ "TrackPIDHyposInputLocation",  TrackPIDHyposLocation::Default },
                   KeyValue{ "TrackDLLsInputLocation",      TrackDLLsLocation::Default } },
                 { KeyValue{ "RichPIDsLocation",            LHCb::RichPIDLocation::Default } } )
{
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

LHCb::RichPIDs 
WriteRichPIDs::operator()( const InTracks& tracks,
                           const Summary::Track::Vector& gtracks,
                           const TrackPIDHypos& hypos,
                           const TrackDLLs::Vector& dlls ) const
{
  LHCb::RichPIDs rPIDs;
  rPIDs.reserve( tracks.size() );

  _ri_verbo << "Creating RichPIDs" << endmsg;

  // Loop over the zipped track data
  for ( const auto && data : Ranges::ConstZip(tracks,gtracks,hypos,dlls) )
  {
    // extract the data
    const auto * tk    = std::get<0>(data);
    const auto & gtk   = std::get<1>(data);
    const auto & bestH = std::get<2>(data);
    const auto & dlls  = std::get<3>(data);

    // Is this track active ? 
    if ( UNLIKELY(!gtk.active()) ) continue;

    // make a new RichPID and save, with same key as parent track
    auto * pid = new LHCb::RichPID();
    rPIDs.insert( pid, tk->key() );

    // Flag as an Offline GlobalPID result. Bit of a relic but useful
    // in comparisons to the 'current' stack.
    pid->setOfflineGlobal(true);

    // Set the track smart ref
    pid->setTrack( tk );

    // Set threshold info
    for ( const auto hypo : activeParticles() )
    {
      pid->setAboveThreshold( hypo, gtk.thresholds()[hypo] );
    }

    // radiator flags
    pid->setUsedAerogel  ( gtk.radiatorActive()[Rich::Aerogel]  );
    pid->setUsedRich1Gas ( gtk.radiatorActive()[Rich::Rich1Gas] );
    pid->setUsedRich2Gas ( gtk.radiatorActive()[Rich::Rich2Gas] );

    // Best PID value
    pid->setBestParticleID( bestH );

    // -------------------------------------------------------------------------------
    // Finalise delta LL values
    // -------------------------------------------------------------------------------
    // Use const cast hack to allow direct access to final vector, to avoid copying...
    auto & vDLLs = *( const_cast< std::vector<float>* >( &(pid->particleLLValues()) ) );
    assert( dlls.dataArray().size() == vDLLs.size() );
    //std::vector<float> vDLLs( dlls.dataArray().size(), 0.0 );
    std::copy( dlls.dataArray().begin(), dlls.dataArray().end(), vDLLs.begin() );
    // Get the pion DLL and ensure its >0
    auto pionDLL = vDLLs[Rich::Pion];
    if ( pionDLL < 0 ) { pionDLL = 0; }
    // sanity check on best ID
    const bool pidOK = ( vDLLs[bestH] <= 1e-10 );
    if ( UNLIKELY(!pidOK) )
    { 
      Warning("Track has non-zero best deltaLL value").ignore(); 
      _ri_debug << "Key = " << tk->key() << " BestID = " << bestH << " Raw DLLs = " << vDLLs << endmsg;
    }
    // Internally, the Global PID normalises the DLL values to the best hypothesis
    // and also works in "-loglikelihood" space.
    // For final storage, renormalise the DLLS w.r.t. the pion hypothesis and
    // invert the values
    for ( const auto hypo : activeParticles() )
    {
      if ( vDLLs[hypo] < 0 ) { vDLLs[hypo] = 0; }
      vDLLs[hypo] = (float) ( pionDLL - vDLLs[hypo] );
    } 
    // finally update DLL values in stored RichPID data object
    // Use move as no longer needed locally afterwards
    //pid->setParticleLLValues( std::move(vDLLs) );
    // -------------------------------------------------------------------------------
    
    // -------------------------------------------------------------------------------
    // Final checks
    // -------------------------------------------------------------------------------
    if ( !pid->isAboveThreshold(lightestActiveHypo()) )
    {
      warning() << "Lowest active mass hypothesis '" << lightestActiveHypo() 
                << "' is below threshold ..." << endmsg;
      warning() << *pid << endmsg;
    }
    // -------------------------------------------------------------------------------
    
    // print the final PID
    if ( !pidOK && !msgLevel(MSG::VERBOSE) ) { _ri_debug << "  " << *pid << endmsg; }
    else                                     { _ri_verbo << "  " << *pid << endmsg; }
  }
  
  // return the PID objects
  return rPIDs;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( WriteRichPIDs )

//=============================================================================
