
// local
#include "RichMCCherenkovResolution.h"

using namespace Rich::Future::Rec::MC::Moni;

//-----------------------------------------------------------------------------
// Implementation file for class : CherenkovResolution
//
// 2016-12-06 : Chris Jones
//-----------------------------------------------------------------------------

CherenkovResolution::
CherenkovResolution( const std::string& name, ISvcLocator* pSvcLocator )
  : Consumer( name, pSvcLocator,
              { KeyValue{ "SummaryTracksLocation", Summary::TESLocations::Tracks },
                KeyValue{ "TracksLocation",          LHCb::TrackLocation::Default },
                KeyValue{ "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default },
                KeyValue{ "RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default },
                KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
                KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                KeyValue{ "CherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
                KeyValue{ "CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default },
                KeyValue{ "CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default },
                KeyValue{ "TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles },
                KeyValue{ "RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default } } )
{
  // print some stats on the final plots
  //setProperty ( "HistoPrint", true ); 
}

//-----------------------------------------------------------------------------

StatusCode CherenkovResolution::prebookHistograms()
{
  using namespace Gaudi::Units;

  // Loop over radiators
  for ( const auto rad : Rich::radiators() )
  {

    // cache some numbers for later use
    m_pullPtotInc[rad] = nPullBins / ( m_PtotMax[rad] - m_PtotMin[rad] );

    if ( m_rads[rad] )
    {

      // Loop over all particle codes
      for ( const auto hypo : activeParticlesNoBT() )
      {
        // track plots
        richHisto1D( HID("expCKang",rad,hypo), "Expected CK angle",
                     m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D() );
        richHisto1D( HID("ckres",rad,hypo), "Calculated CKres",
                     m_ckResMin[rad], m_ckResMax[rad], nBins1D() );
        richProfile1D( HID("ckresVcktheta",rad,hypo), "Calculated CKres V CKangle",
                       m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D() );
        richProfile1D( HID("ckresVptot",rad,hypo), "Calculated CKres V ptot",
                       m_PtotMin[rad], m_PtotMax[rad], nBins1D() );
        // photon plots
        richProfile1D( HID("diffckVckang",rad,hypo),
                       "Rec-Exp CKtheta V CKtheta | MC true photons",
                       m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D() );
        richProfile1D( HID("diffckVPtot",rad,hypo),
                       "Rec-Exp CKtheta V ptot | MC true photons",
                       m_PtotMin[rad], m_PtotMax[rad], nBins1D() );
        richProfile1D( HID("fabsdiffckVckang",rad,hypo),
                       "fabs(Rec-Exp) CKtheta V CKtheta | MC true photons",
                       m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D() );
        richProfile1D( HID("fabsdiffckVPtot",rad,hypo),
                       "fabs(Rec-Exp) CKtheta V ptot | MC true photons",
                       m_PtotMin[rad], m_PtotMax[rad], nBins1D() );
        // pull plots
        richProfile1D( HID("ckPullVckang",rad,hypo),
                       "(Rec-Exp)/Res CKtheta V CKtheta | MC true photons",
                       m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D() );
        richProfile1D( HID("ckPullVPtot",rad,hypo),
                       "(Rec-Exp)/Res CKtheta V ptot | MC true photons",
                       m_PtotMin[rad], m_PtotMax[rad], nBins1D() );
        richProfile1D( HID("fabsCkPullVckang",rad,hypo),
                       "fabs((Rec-Exp)/Res) CKtheta V CKtheta | MC true photons",
                       m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D() );
        richProfile1D( HID("fabsCkPullVPtot",rad,hypo),
                       "fabs((Rec-Exp)/Res) CKtheta V ptot | MC true photons",
                       m_PtotMin[rad], m_PtotMax[rad], nBins1D() );
        // 1D plots binned in ptot
        for ( std::size_t i = 0; i < nPullBins; ++i )
        {
          // min/max Ptot for this bin 
          const auto binEdges = binMinMaxPtot(i,rad);
          // hist title string
          std::ostringstream title;
          title << "(Rec-Exp)/Res CKtheta | MC true photons | Bin " << i 
                << " Ptot " << binEdges.first << " to " << binEdges.second << " GeV";
          // book the histo
          richHisto1D( HID(binToID(i),rad,hypo), title.str(), -5, 5, nBins1D() );
        }

      }

    }
  }
 
  return StatusCode::SUCCESS;
}

//-----------------------------------------------------------------------------

void 
CherenkovResolution::operator()( const Summary::Track::Vector& sumTracks,
                                 const LHCb::Track::Selection& tracks,
                                 const SIMDPixelSummaries& pixels,
                                 const Rich::PDPixelCluster::Vector& clusters,
                                 const Relations::PhotonToParents::Vector& photToSegPix,
                                 const LHCb::RichTrackSegment::Vector& segments,
                                 const CherenkovAngles::Vector& expTkCKThetas,
                                 const CherenkovResolutions::Vector& ckResolutions,
                                 const SIMDCherenkovPhoton::Vector& photons,
                                 const Rich::Future::MC::Relations::TkToMCPRels& tkrels,
                                 const LHCb::MCRichDigitSummarys & digitSums ) const
{

  // Make a local MC helper object
  Helper mcHelper( tkrels, digitSums );

  // loop over the track info
  for ( const auto && data : Ranges::ConstZip(sumTracks,tracks) )
  {
    const auto & sumTk = std::get<0>(data);
    const auto & tk    = std::get<1>(data);

    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() )
    {
      // photon data
      const auto & phot = photons[photIn];
      const auto & rels = photToSegPix[photIn];

      // Get the SIMD summary pixel
      const auto & simdPix = pixels[rels.pixelIndex()];

      // the segment for this photon
      const auto & seg = segments[rels.segmentIndex()];

      // Radiator info
      const auto rad = seg.radiator();
      if ( !m_rads[rad] ) continue;

      // get the expected CK theta values for this segment
      const auto & expCKangles = expTkCKThetas[rels.segmentIndex()];

      // get the resolutions for this segment
      const auto & ckRes = ckResolutions[rels.segmentIndex()];

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // Get the MCParticles for this track
      const auto mcPs = mcHelper.mcParticles(*tk,true,0.5);

      // Weight per MCP
      const auto mcPW = ( !mcPs.empty() ? 1.0/(double)mcPs.size() : 1.0 );

      // loop over mass hypos
      for ( const auto hypo : activeParticlesNoBT() )
      {
        if ( expCKangles[hypo] > 0 )
        {
          richHisto1D( HID("expCKang",rad,hypo) )->fill( expCKangles[hypo] );
          richHisto1D( HID("ckres",rad,hypo) )->fill( ckRes[hypo] );
          richProfile1D( HID("ckresVcktheta",rad,hypo) )->fill( expCKangles[hypo], ckRes[hypo] );
          richProfile1D( HID("ckresVptot",rad,hypo) )->fill( pTot, ckRes[hypo] );
        }
      }

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i )
      {
        // Select valid entries
        if ( !phot.validityMask()[i] ) continue;

        // scalar cluster
        const auto & clus = clusters[ simdPix.scClusIndex()[i] ];

        // reconstructed theta
        const auto thetaRec = phot.CherenkovTheta()[i];
        
        // do we have an true MC Cherenkov photon
        const auto trueCKMCPs = mcHelper.trueCherenkovPhoton( *tk, rad, clus );
        
        // loop over MCPs
        for ( const auto mcP : mcPs )
        {

          // The True MCParticle type
          const auto pid = mcHelper.mcParticleType(mcP);
          // If MC type not known, skip
          if ( Rich::Unknown == pid ) continue; 
          // skip electrons which are reconstructed badly..
          if ( m_skipElectrons && Rich::Electron == pid ) continue; 
          
          // beta cut for true MC type
          const auto mcbeta = richPartProps()->beta( pTot, pid );
          if ( mcbeta >= m_minBeta[rad] && mcbeta <= m_maxBeta[rad] )
          {
            
            // true Cherenkov signal ?
            const bool trueCKSig = std::find( trueCKMCPs.begin(), 
                                              trueCKMCPs.end(), mcP ) != trueCKMCPs.end();

            // expected CK theta ( for true type )
            const auto thetaExp = expCKangles[pid];
            
            // delta theta
            const auto deltaTheta = thetaRec - thetaExp;
            
            // fill some plots for true photons only
            if ( trueCKSig )
            {
              richProfile1D( HID("diffckVckang",rad,pid) )->fill(thetaExp,deltaTheta,mcPW);
              richProfile1D( HID("diffckVPtot",rad,pid) )->fill(pTot,deltaTheta,mcPW);
              richProfile1D( HID("fabsdiffckVckang",rad,pid) )->fill(thetaExp,fabs(deltaTheta),mcPW);
              richProfile1D( HID("fabsdiffckVPtot",rad,pid) )->fill(pTot,fabs(deltaTheta),mcPW);

              // pulls
              if ( ckRes[pid] > 0 )
              {
                const auto pull = deltaTheta / ckRes[pid];
                richHisto1D  ( HID(binToID(pullBin(pTot,rad)),rad,pid) )->fill( pull, mcPW );
                richProfile1D( HID("ckPullVckang",    rad,pid) )->fill( thetaExp,       pull, mcPW );
                richProfile1D( HID("ckPullVPtot",     rad,pid) )->fill( pTot,           pull, mcPW );
                richProfile1D( HID("fabsCkPullVckang",rad,pid) )->fill( thetaExp, fabs(pull), mcPW );
                richProfile1D( HID("fabsCkPullVPtot", rad,pid) )->fill( pTot,     fabs(pull), mcPW );
              }

            }

          } // beta selection
          
        } // loop over associated MCPs
        
      } // scalar loop
      
    }
  }
  
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CherenkovResolution  )

//-----------------------------------------------------------------------------
