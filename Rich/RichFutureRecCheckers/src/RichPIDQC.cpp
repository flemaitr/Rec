
// local
#include "RichPIDQC.h"

using namespace Rich::Future::Rec::MC::Moni;

//-----------------------------------------------------------------------------
// Implementation file for class : PIDQC
//
// 2016-11-07 : Chris Jones
//-----------------------------------------------------------------------------

PIDQC::PIDQC( const std::string& name, ISvcLocator* pSvcLocator )
  : Consumer( name, pSvcLocator,
              { KeyValue{ "TracksLocation",    LHCb::TrackLocation::Default },
                KeyValue{ "RichPIDsLocation",  LHCb::RichPIDLocation::Default },
                KeyValue{ "TrackToMCParticlesRelations",
                          Rich::Future::MC::Relations::TrackToMCParticles } } )
{
  // Initialise
  for ( auto & i : m_sumTab ) { i.fill(0); }
  // debugging
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//-----------------------------------------------------------------------------

StatusCode PIDQC::initialize()
{
  auto sc = Consumer::initialize();
  if ( !sc ) return sc;

  // get tools
  return m_tkSel.retrieve();
}

//-----------------------------------------------------------------------------

void PIDQC::operator()( const LHCb::Track::Selection& tracks,
                        const LHCb::RichPIDs& pids,
                        const Rich::Future::MC::Relations::TkToMCPRels& rels ) const
{

  // track selector shortcut
  const auto tkSel = m_tkSel.get();

  // count total selected tracks
  const auto nTks = std::count_if( tracks.begin(), tracks.end(),
                                   [&tkSel]( const auto * tk )
                                   { return tk && tkSel->accept(*tk); } );

  // helper for the track -> MCP relations
  const Rich::Future::MC::Relations::TrackToMCParticle tkToMPCs(rels);

  // The lock. Eventually should isolate it more further down this scope ....
  std::lock_guard<std::mutex> lock(m_updateLock);

  // local counts
  unsigned int pidCount = 0;

  // Loop over all PID results
  for ( const auto pid : pids )
  {
    // Track for this PID
    const auto * tk = pid->track();

    _ri_debug << *pid << endmsg;

    // is track selected
    if ( !tkSel->accept(*tk) ) continue;
    // Is the track in the input list
    // (if not skip, means we are running on a reduced track list with selection cuts)
    if ( std::none_of( tracks.begin(), tracks.end(),
                       [&tk]( const auto * t ) { return t == tk; } ) ) continue;

    // Count PIDs and tracks
    ++pidCount;
    ++m_radCount[ Radiators(pid->usedAerogel(),pid->usedRich1Gas(),pid->usedRich2Gas()) ];

    // Get best reco PID
    auto bpid = pid->bestParticleID();
    // if below threshold, set as such
    if ( !pid->isAboveThreshold(bpid) ) { bpid = Rich::BelowThreshold; }

    // Get the MCParticle range for this track
    const auto mcPs = tkToMPCs.mcParticleRange(*tk);

    // Did we get any MCPs ?
    if ( mcPs.empty() )
    {
      // Fill once with MC type unknown (Below threshold)
      fillTable( bpid, Rich::BelowThreshold, 1.0 );
    }
    else
    {
      // Get the sum off all MCP weights
      const double wSum =
        std::accumulate( mcPs.begin(), mcPs.end(), 0.0,
                         []( const auto sum, const auto w )
                         { return sum + w.weight(); } );
      // fill once per associated MCP
      for ( const auto MC : mcPs )
      {
        // get the MCP pointer
        const LHCb::MCParticle * mcP = MC.to();
        // normalised weight for this MCP
        const auto mcPW = ( wSum > 0 ? MC.weight()/wSum : 1.0 );
        // MC Truth
        auto mcpid = tkToMPCs.mcParticleType( mcP );
        // If unknown, set to below threshold (i.e. ghost).
        if ( Rich::Unknown == mcpid        ) { mcpid = Rich::BelowThreshold; }
        // If a real type, but below threshold, set below threshold
        if ( !pid->isAboveThreshold(mcpid) ) { mcpid = Rich::BelowThreshold; }
        // fill table
        fillTable( bpid, mcpid, mcPW );
      }
    }

  } // pid loop

  // count events and tracks
  ++m_nEvents[0];
  if ( !pids.empty() ) ++m_nEvents[1];
  m_nTracks[0] += nTks;
  m_nTracks[1] += pidCount;

  plot1D( pidCount, "# PIDs per event", -0.5, 200.5, 201 );
  plot1D( (pids.empty() ? 0 : 1), "Event Success V Failures", -0.5, 1.5, 2 );
  if ( nTks > 0 )
  {
    plot1D( static_cast<double>(pidCount) / static_cast<double>(nTks),
            "Fraction of Tracks with PIDs", 0, 1, nBins1D() );
  }

}

//-----------------------------------------------------------------------------

StatusCode PIDQC::finalize()
{

  // compute efficiencies and purities
  double sumTot = 0;
  std::array<double,Rich::NParticleTypes> recTot, trueTot, trueTotExcludeX, eff, purity;
  recTot.fill(0);
  trueTot.fill(0);
  trueTotExcludeX.fill(0);
  eff.fill(0);
  purity.fill(0);
  for ( int iRec = 0; iRec<Rich::NParticleTypes; ++iRec )
  {
    for ( int iTrue = 0; iTrue<Rich::NParticleTypes; ++iTrue )
    {
      sumTot         += m_sumTab[iTrue][iRec];
      recTot[iRec]   += m_sumTab[iTrue][iRec];
      trueTot[iTrue] += m_sumTab[iTrue][iRec];
      if (iRec<Rich::NParticleTypes-1) trueTotExcludeX[iTrue] += m_sumTab[iTrue][iRec];
    }
  }
  if ( sumTot > 0.5 )
  {
    for ( int iRec = 0; iRec<Rich::NParticleTypes; ++iRec )
    {
      eff[iRec]    = ( trueTot[iRec]>0 ? 100*m_sumTab[iRec][iRec]/trueTot[iRec] : 0 );
      purity[iRec] = ( recTot[iRec]>0  ? 100*m_sumTab[iRec][iRec]/recTot[iRec]  : 0 );
    }

    // Kaon / Pion seperation
    const auto truePi = trueTotExcludeX[Rich::Pion];
    const auto trueKa = trueTotExcludeX[Rich::Kaon];
    std::array<double,2> kaonIDEff = {0,0};
    kaonIDEff[0] = ( trueKa>0 ? 100*( m_sumTab[Rich::Kaon][Rich::Kaon] +
                                      m_sumTab[Rich::Kaon][Rich::Proton] +
                                      m_sumTab[Rich::Kaon][Rich::Deuteron] ) / trueKa : 0 );
    kaonIDEff[1] = ( trueKa>0 ? std::sqrt(kaonIDEff[0]*(100.-kaonIDEff[0]) / trueKa ) : 0 );
    std::array<double,2> kaonMisIDEff = {0,0};
    kaonMisIDEff[0] = ( trueKa>0 ? 100*( m_sumTab[Rich::Kaon][Rich::Electron] +
                                         m_sumTab[Rich::Kaon][Rich::Muon] +
                                         m_sumTab[Rich::Kaon][Rich::Pion] ) / trueKa : 0 );
    kaonMisIDEff[1] = ( trueKa>0 ? std::sqrt(kaonMisIDEff[0]*(100.-kaonMisIDEff[0]) / trueKa ) : 0 );
    std::array<double,2> piIDEff = {0,0};
    piIDEff[0] = ( truePi>0 ? 100*( m_sumTab[Rich::Pion][Rich::Electron] +
                                    m_sumTab[Rich::Pion][Rich::Muon] +
                                    m_sumTab[Rich::Pion][Rich::Pion] ) / truePi : 0 );
    piIDEff[1] = ( truePi>0 ? std::sqrt(piIDEff[0]*(100.-piIDEff[0]) / truePi ) : 0 );
    std::array<double,2> piMisIDEff = {0,0};
    piMisIDEff[0] = ( truePi>0 ? 100*( m_sumTab[Rich::Pion][Rich::Kaon] +
                                       m_sumTab[Rich::Pion][Rich::Proton] +
                                       m_sumTab[Rich::Pion][Rich::Deuteron] ) / truePi : 0 );
    piMisIDEff[1] = ( truePi>0 ? std::sqrt(piMisIDEff[0]*(100.-piMisIDEff[0]) / truePi ) : 0 );

    // Scale entries to percent of total number of entries
    for ( auto & i : m_sumTab )
    {
      for ( auto & j : i ) { j = 100.0 * j / sumTot; }
    }

    // compute event and track PID success rates
    std::array<double,2> evPIDRate = {0,0};
    evPIDRate[0] = ( m_nEvents[0]>0 ? 100.*m_nEvents[1]/m_nEvents[0] : 100 );
    evPIDRate[1] = ( m_nEvents[0]>0 ? std::sqrt(evPIDRate[0]*(100.-evPIDRate[0])/m_nEvents[0]) : 100 );
    std::array<double,2> trPIDRate = {0,0};
    trPIDRate[0] = ( m_nTracks[0]>0 ? 100.*m_nTracks[1]/m_nTracks[0] : 100 );
    trPIDRate[1] = ( m_nTracks[0]>0 ? std::sqrt(trPIDRate[0]*(100.-trPIDRate[0])/m_nTracks[0]) : 100 );
    info() << "-------------+---------------------------------------------------------------+------------"
           << endmsg;
    info() << "   " << name()
           << " #Tracks = " << m_nTracks[0] << ", With RICH info = " << m_nTracks[1] << endmsg;
    info() << "-------------+---------------------------------------------------------------+------------"
           << endmsg
           << "   %total    |  Electron  Muon    Pion    Kaon   Proton  Deuteron   X  (MC)  |  %Purity"
           << endmsg
           << "-------------+---------------------------------------------------------------+------------"
           << endmsg
           << "             |                                                               |" << endmsg;
    const std::array<std::string,Rich::NParticleTypes> type
      = { " Electron    |", " Muon        |", " Pion        |",
          " Kaon        |", " Proton      |", " Deuteron    |",
          " X           |" };
    for ( int iRec = 0; iRec < Rich::NParticleTypes; ++iRec )
    {
      info() << type[iRec]
             << boost::format( " %8.3f%8.3f%8.3f%8.3f%8.3f%8.3f%8.3f      | %8.3f" ) %
        m_sumTab[0][iRec] % m_sumTab[1][iRec] %
        m_sumTab[2][iRec] % m_sumTab[3][iRec] %
        m_sumTab[4][iRec] % m_sumTab[5][iRec] %
        m_sumTab[6][iRec] % purity[iRec] << endmsg;
    }
    info() << "   (reco)    |                                                               |" << endmsg
           << "-------------+---------------------------------------------------------------+------------"
           << endmsg;
    info() << "   %Eff.     |" << boost::format( " %8.3f%8.3f%8.3f%8.3f%8.3f%8.3f%8.3f " ) %
      eff[0] % eff[1] % eff[2] % eff[3] % eff[4] % eff[5] % eff[6]
           << "     |" << endmsg;
    info() << "-------------+---------------------------------------------------------------+------------"
           << endmsg;

    info() << " % ID eff    |  K->K,Pr,D : "
           << boost::format( "%6.2f +-%6.2f   pi->e,m,pi : %6.2f +-%6.2f " ) %
      kaonIDEff[0] % kaonIDEff[1] % piIDEff[0] % piIDEff[1]
           << "  |" << endmsg;
    info() << " % MisID eff |  K->e,m,pi : "
           << boost::format( "%6.2f +-%6.2f   pi->K,Pr,D : %6.2f +-%6.2f " ) %
      kaonMisIDEff[0] % kaonMisIDEff[1] % piMisIDEff[0] % piMisIDEff[1]
           << "  |" << endmsg;
    info() << " % ID rate   |  Events    : "
           << boost::format( "%6.2f +-%6.2f   Tracks     : %6.2f +-%6.2f " ) %
      evPIDRate[0] % evPIDRate[1] % trPIDRate[0] % trPIDRate[1]
           << "  |" << endmsg;

    for ( const auto& RC : m_radCount )
    {
      const auto effR = ( m_nTracks[0]>0 ? 100.*((double)RC.second)/m_nTracks[0]    : 100.0 );
      const auto errR = ( m_nTracks[0]>0 ? std::sqrt(effR*(100.-effR)/m_nTracks[0]) : 100.0 );
      info() << "             |  -> With "
             << RC.first.radiators()
             << boost::format( "   : %6.2f +-%6.2f" ) % effR % errR
             << "   |" << endmsg;
    }

    info() << "-------------+---------------------------------------------------------------+------------"
           << endmsg;

  }
  else
  {
    warning() << "NO ENTRIES -> PID table skipped ..." << endmsg;
  }

  return Consumer::finalize();
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PIDQC )

//-----------------------------------------------------------------------------
