
#include "RichRecUtils/RichCKResolutionFitter.h"

// ROOT
#include <TMath.h>
#include <TFitResult.h>

using namespace Rich::Rec;

//=============================================================================

CKResolutionFitter::CKResolutionFitter() { }

//=============================================================================

CKResolutionFitter::FitResult 
CKResolutionFitter::fit( TH1 & hist, const Rich::RadiatorType rad ) const
{
  // returned object
  FitResult fitR;
  // which RICH ?
  const auto rIn = rIndex(rad);
  // run the fits in order, returning the first to succeed.
  for ( const auto & fitType : params().RichFitTypes[rIn] )
  {
    fitR = fitImp( hist, rad, fitType );
    if ( fitR.fitOK ) break;
  }
  // return
  return fitR;
}

//=============================================================================

CKResolutionFitter::FitResult
CKResolutionFitter::fitImp( TH1& hist, 
                            const Rich::RadiatorType rad,
                            const std::string& fitType ) const
{
  // Full fit result object
  FitResult fitRes;

  // used fit range
  auto & fitMin = fitRes.fitMin;
  auto & fitMax = fitRes.fitMax;

  // which RICH ?
  const auto rIn = rIndex(rad);

  // Firstly do a prefit

  // delta for the pre-fit
  const auto delta = params().RichStartDelta[rIn];

  // get the sclae of the histogram
  const auto yPeak = hist.GetBinContent( hist.GetMaximumBin() );

  // x value for peak
  auto xPeak = hist.GetBinCenter( hist.GetMaximumBin() );
  // if peak is too far off from expectation, just use 0.0 as starting point
  if ( fabs(xPeak) > delta/5.0 ) { xPeak = 0.0; }

  // range for the pre-fit
  fitMin = xPeak - delta;
  fitMax = xPeak + delta;

  // Do the pre-fit
  const std::string preFitFName = "Rad" + std::to_string(rad) + "PreFitF";
  TF1 preFitF( preFitFName.c_str(), "gaus", fitMin, fitMax );
  // Estimate overall scale from max bin
  preFitF.SetParameter( 0, yPeak );
  // Estimate peak from max bin
  preFitF.SetParameter( 1, xPeak );
  // rough guess at resolution. Does not need to be precise
  preFitF.SetParameter( 2, params().RichStartRes[rIn] );
  // run the pre-fit
  const auto preFitR = hist.Fit( &preFitF, "MQRS0" );
  
  // Fit result status
  auto & fitOK = fitRes.fitOK;
  fitOK = fitIsValid( preFitR );

  // Only proceed if prefit is OK
  if ( fitOK )
  {

    // The final fit function.
    auto & bestFitF = fitRes.overallFitFunc;
    // Set at first to the pre fit.
    bestFitF = preFitF;
    
    // Range for the final full fit, with background etc.
    fitMin = params().RichFitMin[rIn];
    fitMax = params().RichFitMax[rIn];

    // Fit Type
    if ( "FreeNPol" == fitType )
    {
      // Full free polynominal form for background

      // fit options
      const std::string minuitOpts = "MQRSE0";
      //const std::string minuitOpts = "MRSE0";

      // Polynominal degree for background
      const auto nPolFull = params().RichNPol[rIn];

      // full fit parameters
      unsigned int bestNPol = 0;

      // save the last fit function
      TF1 lastFitF = preFitF; 
      
      // Loop over poly orders
      for ( unsigned int nPol = 1; nPol < nPolFull+1; ++nPol )
      {
        const auto fFitFName = "Rad" + std::to_string(rad) + "fFitF" + std::to_string(nPol);
        const auto fFitFType = "gaus(0)+pol" + std::to_string(nPol) + "(3)" ;
        TF1 fFitF( fFitFName.c_str(), fFitFType.c_str(), fitMin, fitMax );
        
        const unsigned int nParamsToSet = ( nPol > 1 ? 3 + nPol : 3 );
        for ( unsigned int p = 0; p < nParamsToSet; ++p )
        {
          fFitF.SetParameter(p,lastFitF.GetParameter(p));
        }

        // set the gaussian parameter names
        fFitF.SetParName(0,"Gaus Const");
        fFitF.SetParName(1,"Gaus Mean");
        fFitF.SetParName(2,"Gaus Sigma");
        // Set the background parameter names
        for ( int i = 3; i < fFitF.GetNpar(); ++i )
        { fFitF.SetParName( i, ("Bkg Par "+std::to_string(i-3)).c_str() ); }
        
        // run the fit
        const auto fitR = hist.Fit( &fFitF, minuitOpts.c_str() );
        const auto validFit = fitIsValid( fitR );
        
        // save the last fit
        lastFitF = fFitF;
        
        // Fit OK?
        fitOK = validFit && fitIsOK(fFitF,rad);
        if ( fitOK )
        {
          bestFitF = fFitF;
          bestNPol = nPol;
        }
        else
        {
          if ( nPol == nPolFull )
          {
            // use last good fit
            const auto R = hist.Fit( &fFitF, minuitOpts.c_str() ); // CRJ : Why do I redo this fit...
            fitOK = ( R.Get() ? R->IsValid() : false );
          }
          if ( nPol > 1 ) break;
        }
        
      } // loop over pol fits

      // Setup the background function
      fitRes.bkgFitFunc = TF1( "Background", 
                               ("pol"+std::to_string(bestNPol)).c_str(),
                               fitMin, fitMax );

    }
    else if ( "FixedNPol" == fitType )
    {
      // Fixed free polynominal form for background

      // Minuit options
      const std::string minuitOpts = "MQRSE0";
      //const std::string minuitOpts = "MRSE0";
      //const std::string minuitOpts = "MRS0";

      const auto fFitFName = "Rad" + std::to_string(rad) + "fFitF";
      const std::string fFitFType = "gaus(0)+[3]*"+params().RichFixedNPolFunc(rad,4);
      TF1 fFitF( fFitFName.c_str(), fFitFType.c_str(), fitMin, fitMax );

      // Set starting gauss parameters
      for ( const int i : {0,1,2} )
      { fFitF.SetParameter( i, preFitF.GetParameter(i) ); }
      // Start background level at 0
      fFitF.SetParameter( 3, 0 );
      fFitF.SetParameter( 4, 1 );

      // set the signal parameter names
      fFitF.SetParName(0,"Gaus Const");
      fFitF.SetParName(1,"Gaus Mean");
      fFitF.SetParName(2,"Gaus Sigma");
      // background scale factor
      fFitF.SetParName(3,"Bkg Overall Scale");
      fFitF.SetParName(4,"Bkg Linear factor");

      // run the fit
      const auto fitR = hist.Fit( &fFitF, minuitOpts.c_str() );
      const auto validFit = fitIsValid( fitR );

      // Fit OK?
      fitOK = validFit && fitIsOK(fFitF,rad);
      if ( fitOK ) { bestFitF = fFitF; }

      // Setup the background function
      fitRes.bkgFitFunc = TF1( "Background", 
                               ("[0]*"+params().RichFixedNPolFunc(rad,1)).c_str(),
                               fitMin, fitMax );

    }
    else if ( "Hyperbolic" == fitType )
    {

      // Background : y = ( A - sqrt( B + C*(x-D)^2 ) )
      //            : C allowed to vary independently for x > D and x < D 

      // Minuit options
      const std::string minuitOpts = "MQRSE0";
      //const std::string minuitOpts = "MRSE0";

      // Full signal + Hyperbolic background function
      const auto fFitFName = "Rad" + std::to_string(rad) + "fFitF";
      const auto fFitFType = params().GaussHyperbolFitFunc(0);
      TF1 fFitF( fFitFName.c_str(), fFitFType.c_str(), fitMin, fitMax, 8 );

      // Set starting gauss parameters
      for ( const int i : {0,1,2} )
      { fFitF.SetParameter( i, preFitF.GetParameter(i) ); }

      // Start background level at 0
      for ( const int i : {3,4,5,6,7} )
      { fFitF.SetParameter( i, 0 ); }

      // set the signal parameter names
      fFitF.SetParName(0,"Gaus Const");
      fFitF.SetParName(1,"Gaus Mean");
      fFitF.SetParName(2,"Gaus Sigma");

      // background scale factor
      fFitF.SetParName(3,"Hyper A");
      fFitF.SetParName(4,"Hyper B");
      fFitF.SetParName(5,"Hyper C -ve");
      fFitF.SetParName(6,"Hyper C +ve");
      fFitF.SetParName(7,"Hyper D");

      // Side band fits to get C slope parameters
      TF1 sideBmF( (fFitFName+"SideBm").c_str(), "pol1", 
                   fitMin, fitMin + params().SideBandSize[rIn] );
      auto sideBm = hist.Fit( &sideBmF, minuitOpts.c_str() );
      TF1 sideBpF( (fFitFName+"SideBp").c_str(), "pol1", 
                   fitMax - params().SideBandSize[rIn], fitMax );
      auto sideBp = hist.Fit( &sideBpF, minuitOpts.c_str() );

      // Slope from linear y=mx+c fit equates to sqrt(C)
      const auto CmInit = std::pow( sideBmF.GetParameter(1), 2 );
      const auto CpInit = std::pow( sideBpF.GetParameter(1), 2 );

      // Fix some of the background terms
      fFitF.FixParameter(4,params().HyperInitB[rIn]);  // B
      fFitF.FixParameter(5,CmInit);                    // C -ve
      fFitF.FixParameter(6,CpInit);                    // C +ve
      fFitF.FixParameter(7,params().HyperInitD[rIn]);  // D
      
      // run the fit with all but the overall background scale set
      auto fitR = hist.Fit( &fFitF, minuitOpts.c_str() );
      const bool validFitA = fitIsValid(fitR);
      if ( validFitA ) { bestFitF = fFitF; }

      // Release C and refit
      fFitF.ReleaseParameter(5);
      fFitF.ReleaseParameter(6);
      fitR = hist.Fit( &fFitF, minuitOpts.c_str() );
      const bool validFitB = fitIsValid(fitR);
      if ( validFitB ) { bestFitF = fFitF; }

      // Release D and refit
      bool validFitC = false;
      //if ( validFitB )
      {
        fFitF.ReleaseParameter(7);
        fitR = hist.Fit( &fFitF, minuitOpts.c_str() );
        validFitC = fitIsValid(fitR) && fitIsOK(fFitF,rad);
        if ( validFitC ) { bestFitF = fFitF; }
      }

      // Release B and refit
      bool validFitD = false;
      //if ( validFitC )
      {
        fFitF.ReleaseParameter(4);
        fitR = hist.Fit( &fFitF, minuitOpts.c_str() );
        validFitD = fitIsValid(fitR) && fitIsOK(fFitF,rad);
        if ( validFitD ) { bestFitF = fFitF; }
      }

      // final OK status based on best fit
      fitOK = ( validFitD || validFitC );
      
      // Setup the background function 
      fitRes.bkgFitFunc = TF1( "Background", 
                               params().HyperbolFitFunc(0).c_str(),
                               fitMin, fitMax, 5 );

    }
    else
    {
      std::cerr << "Unknown fit type " << fitType << std::endl;
    }

    // Set the background function parameters
    for ( int i = 3; i < bestFitF.GetNpar(); ++i )
    {
      fitRes.bkgFitFunc.SetParameter( i-3, bestFitF.GetParameter(i) );
      fitRes.bkgFitFunc.SetParError ( i-3, bestFitF.GetParError(i)  );
      fitRes.bkgFitFunc.SetParName  ( i-3, bestFitF.GetParName(i)   );
    }
  
    // Final sanity check on the fit results
    // limits on fit sigma and mean, to detect complete failures such as when there
    // is no Cherenkov signal at all
    fitOK &= fitIsOK(bestFitF,rad);
    if ( fitOK )
    {
      fitRes.ckShift         = bestFitF.GetParameter(1);
      fitRes.ckShiftErr      = bestFitF.GetParError(1);
      fitRes.ckResolution    = bestFitF.GetParameter(2);
      fitRes.ckResolutionErr = bestFitF.GetParError(2);
    }

  } // pre fit OK

  // return final fit parameters
  return fitRes;
}

//=============================================================================

bool CKResolutionFitter::fitIsOK( const TF1& fFitF,
                                  const Rich::RadiatorType rad ) const
{
  // which RICH
  const auto rIn = rIndex(rad);
  // first check fit error on shift
  bool fitOK = fFitF.GetParError(1) < params().MaxShiftError[rIn];
  // now test the resolution error
  fitOK     &= fFitF.GetParError(2) < params().MaxSigmaError[rIn];
  // Final sanity check on the fit results to detect complete failures 
  // such as when there is no Cherenkov signal at all
  fitOK &= fabs( fFitF.GetParameter(1) ) < params().RichMaxMean[rIn];
  fitOK &= fabs( fFitF.GetParameter(2) ) < params().RichMaxSigma[rIn];
  // return the final status
  return fitOK;
}

//=============================================================================
