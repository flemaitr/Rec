from Gaudi.Configuration import *
import glob
from GaudiConf import IOHelper

# Check what is available
searchPaths = [
    "/usera/jonesc/NFS/data/Collision16/LHCb/DST/", # Cambridge
    "/home/chris/LHCb/Data/Collision16/LHCb/DST/"   # CRJ's CernVM
    ]

data = [ ]
for path in searchPaths :
    files = sorted(glob.glob(path+"*/*.dst"))
    data += [ "'PFN:"+file for file in files ]

IOHelper('ROOT').inputFiles( data, clear=True)
FileCatalog().Catalogs = [ 'xmlcatalog_file:out.xml' ]

from Configurables import LHCbApp

LHCbApp().DataType  = "2016"

#LHCbApp().CondDBtag = "cond-20170510"
