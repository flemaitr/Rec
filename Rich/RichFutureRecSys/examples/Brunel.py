
from Gaudi.Configuration import *
from Configurables import Brunel, LHCbApp

#############################################################################

def turnOffTrigger() :
    from Configurables import GaudiSequencer
    GaudiSequencer("DecodeTriggerSeq").Members = [ ]
appendPostConfigAction(turnOffTrigger)

Brunel().VetoHltErrorEvents = False
from Configurables import L0Conf
L0Conf().EnsureKnownTCK = False

#############################################################################

#Brunel.RichSequences = ["RICH"]

#from Configurables import RecMoniConf
#RecMoniConf().MoniSequence = richs
#Brunel().MCCheckSequence = richs+["PROTO"]

#RecMoniConf().MoniSequence = []
#Brunel().MCCheckSequence = []
#GaudiSequencer("MCLinksCaloSeq").Members = [ ]

#Brunel().Detectors = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Magnet', 'Tr']
#from Configurables import RecSysConf
#RecSysConf().Detectors = Brunel().Detectors
#Brunel().RecoSequence = ["Decoding","TrFast","TrBest","RICH"]

#############################################################################

# Timestamps in messages
#LHCbApp().TimeStamp = True

Brunel().OutputType = 'None'
#Brunel().OutputType = 'DST'
#Brunel().OutputType = 'LDST'

#importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")

Brunel().EvtMax     = 5000
Brunel().PrintFreq  = 100

#Brunel().Histograms = "Expert"
Brunel().Histograms = "None"

#ApplicationMgr().ExtSvc += [ "AuditorSvc" ]

##AuditorSvc().Auditors += [ "FPEAuditor" ]
## from Configurables import FPEAuditor
## #FPEAuditor().TrapOn = [ "DivByZero", "Overflow", "Underflow" ]
## #FPEAuditor().ActivateAt = ["Execute"]

#AuditorSvc().Auditors += [ "MemoryAuditor" ]

#Brunel().Monitors=["SC","FPE"]

#Brunel().OnlineMode = True

#Brunel().FilterTrackStates = False

msgSvc = getConfigurable("MessageSvc")
#msgSvc.setVerbose += [ "DeRichGasRadiator" ]
#msgSvc.setVerbose += [ "DeRichSystem" ]
#msgSvc.setVerbose += [ "DeRichHPD" ]
#msgSvc.OutputLevel = 1
msgSvc.Format = "% F%30W%S%7W%R%T %0W%M"
from Configurables import SequencerTimerTool
SequencerTimerTool("ToolSvc.SequencerTimerTool").NameSize = 30

#from Configurables import UpdateManagerSvc
#UpdateManagerSvc().OutputLevel = 1

#from Configurables import TrackBestTrackCreator, TrackVectorFitter
#TrackBestTrackCreator().addTool(TrackVectorFitter, 'Fitter')

#from Configurables import CondDB
#CondDB().IgnoreHeartBeat = True
#CondDB().EnableRunStampCheck = False
