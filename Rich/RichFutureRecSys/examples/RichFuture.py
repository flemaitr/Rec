
###############################################################
# Job options file
###############################################################

# --------------------------------------------------------------------------------------

from Gaudi.Configuration import *
from GaudiConfig.ControlFlow import seq
from Configurables import CondDB, LHCbApp, GaudiSequencer
import os

# --------------------------------------------------------------------------------------
# General configs

# histograms
#histos = "OfflineFull"
histos = "Expert"

# ROOT persistency
ApplicationMgr().HistogramPersistency = "ROOT"
from Configurables import RootHistCnv__PersSvc
RootHistCnv__PersSvc('RootHistCnv').ForceAlphaIds = True
myBuild = os.environ['User_release_area'].split('/')[-1]
myConf  = os.environ['CMTCONFIG']
rootFileBaseName = myBuild + "-" + myConf + "-" + histos
HistogramPersistencySvc().OutputFile = rootFileBaseName + "-Histos.root"

# Event numbers
nEvents                   = 1000
EventSelector().PrintFreq = 100
#LHCbApp().SkipEvents = 5416

# Just to initialise
CondDB()
LHCbApp()

# Timestamps in messages
#LHCbApp().TimeStamp = True

msgSvc = getConfigurable("MessageSvc")
#msgSvc.setVerbose += [ "DeRichGasRadiator" ]
#msgSvc.setVerbose += [ "DeRichSystem" ]
#msgSvc.setVerbose += [ "DeRichHPD" ]
msgSvc.Format = "% F%40W%S%7W%R%T %0W%M"
from Configurables import SequencerTimerTool
SequencerTimerTool("ToolSvc.SequencerTimerTool").NameSize = 40

# Auditors
#AuditorSvc().Auditors += [ "FPEAuditor" ]
#from Configurables import FPEAuditor
#FPEAuditor().ActivateAt = ["Execute"]
#AuditorSvc().Auditors += [ "NameAuditor" ]

# The overall sequence. Filled below.
all = GaudiSequencer( "All", MeasureTime = True )

# Finally set up the application
ApplicationMgr( TopAlg = [all],
                EvtMax = nEvents, # events to be processed
                ExtSvc = ['ToolSvc','AuditorSvc'],
                AuditAlgorithms = True )

# --------------------------------------------------------------------------------------

# Fetch required data from file
from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile
fetcher = FetchDataFromFile('FetchDSTData')
fetcher.DataKeys = ['Trigger/RawEvent','Rich/RawEvent','pRec/Track/Best']
all.Members += [ fetcher ]

# First various raw event decodings

from Configurables import createODIN
odinDecode = createODIN( "ODINFutureDecode" )
all.Members += [ odinDecode ]

from Configurables import Rich__Future__RawBankDecoder as RichDecoder
richDecode = RichDecoder( "RichFutureDecode" )
all.Members += [ richDecode ]

# Explicitly unpack the Tracks

#from Configurables import UnpackTrackFunctional
#tkUnpack = UnpackTrackFunctional("UnpackTracks")
from Configurables import UnpackTrack
tkUnpack = UnpackTrack("UnpackTracks")
all.Members += [ tkUnpack ]

# Filter the tracks by type

from Configurables import Rich__Future__Rec__TrackFilter as TrackFilter
tkFilt = TrackFilter("TrackTypeFilter")
all.Members += [ tkFilt ]

# Now get the RICH sequence

photoRecoType = "Quartic"
#photoRecoType = "FastQuartic"
#photoRecoType = "CKEstiFromRadius"

# Preload the geometry during initialise. Useful for timing studies.
preloadGeom = True
#preloadGeom = False

# Work around for some tracking tools the RICH uses that doesn't seem
# to be thread safe yet. Does not matter for normal execution, but if
# using Gaudi Hive set this to true. Changes how the RICH works to
# a not completely ideal mode, but avoids the problem.
#workAroundTrackTools = False
workAroundTrackTools = True

# Input tracks
tkLocs = { "Long" : tkFilt.OutLongTracksLocation,
           "Down" : tkFilt.OutDownTracksLocation,
           "Up"   : tkFilt.OutUpTracksLocation }
#tkLocs = { "All"  : "Rec/Track/Best" }
#tkLocs = { "Long" : tkFilt.OutLongTracksLocation }
#tkLocs = { "Up" : tkFilt.OutUpTracksLocation }

# Output PID
pidLocs =  { "Long" : "Rec/Rich/LongPIDs",
             "Down" : "Rec/Rich/DownPIDs",
             "Up"   : "Rec/Rich/UpPIDs" }
#pidLocs = { "All" : "Rec/Rich/PIDs" } 
#pidLocs = { "Long" : "Rec/Rich/LongPIDs" }
#pidLocs = { "Up" : "Rec/Rich/UpPIDs" }

# Merged output
finalPIDLoc = "Rec/Rich/PIDs"

# DataType
#dType = "2016"
dType = "Upgrade"

# Online Brunel mode.
#online = True
online = False

# --------------------------------------------------------------------------------------
# The reconstruction
from RichFutureRecSys.ConfiguredRichReco import RichRecoSequence
RichRec = RichRecoSequence( dataType              = dType,
                            onlineBrunelMode      = online,
                            photonReco            = photoRecoType,
                            preloadGeometry       = preloadGeom,
                            makeTkToolsThreadSafe = workAroundTrackTools,
                            inputTrackLocations   = tkLocs,
                            outputPIDLocations    = pidLocs,
                            mergedOutputPIDLocation = finalPIDLoc )

# The final sequence to run
all.Members += [ RichRec ]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Monitoring
from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecMonitors
RichMoni = RichRecMonitors( inputTrackLocations = tkLocs,
                            onlineBrunelMode    = online,
                            outputPIDLocations  = pidLocs,
                            histograms          = histos )
# Uncomment to enable monitoring
all.Members += [ RichMoni ]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# MC Checking
from Configurables import UnpackMCParticle, UnpackMCVertex
from Configurables import DataPacking__Unpack_LHCb__MCRichDigitSummaryPacker_ as RichSumUnPack
from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecCheckers
RichCheck = RichRecCheckers( inputTrackLocations = tkLocs,
                             onlineBrunelMode    = online,
                             outputPIDLocations  = pidLocs,
                             histograms          = histos )
# Uncomment to enable checking
#fetcher.DataKeys += ['pMC/Vertices','pMC/Particles']
#all.Members += [ UnpackMCVertex(), UnpackMCParticle(), RichSumUnPack("RichSumUnPack"), RichCheck ]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# ProtoParticle monitoring. For detailed PID monitoring.
protoMoniSeq = GaudiSequencer( "ProtoMonitoring", MeasureTime = True )
#all.Members += [ protoMoniSeq ] # Uncomment to enable Proto tuples.
# Remake the protos with the new Rich PID objects
from Configurables import ( UnpackProtoParticle,
                            ChargedProtoParticleAddRichInfo,
                            ChargedProtoCombineDLLsAlg )
protoMoniSeq.Members += [ UnpackProtoParticle(name       = "UnpackChargedProtos",
                                              OutputName = "/Event/Rec/ProtoP/Charged",
                                              InputName  = "/Event/pRec/ProtoP/Charged"),
                          ChargedProtoParticleAddRichInfo("ChargedProtoPAddRich"),
                          ChargedProtoCombineDLLsAlg("ChargedProtoPCombDLLs") ]
# monitoring config
from Configurables import GlobalRecoChecks
GlobalRecoChecks().Sequencer = protoMoniSeq
GlobalRecoChecks().ProtoTupleName = rootFileBaseName + "-ProtoTuple.root"
GlobalRecoChecks().ANNTupleName   = rootFileBaseName + "-ANNPIDTuple.root"
# Turn off ANNPID monitoring
GlobalRecoChecks().AddANNPIDInfo = False
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Example command lines
# --------------------------------------------------------------------------------------

# Normal running
# gaudirun.py -T ~/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,data/MCXDstUpgradeFiles.py} 2>&1 | tee ${User_release_area##/*/}-${CMTCONFIG}.log

# --------------------------------------------------------------------------------------
