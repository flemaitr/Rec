
// local
#include "RichPixelClusterLocalPositions.h"

using namespace Rich::Future::Rec;

//-----------------------------------------------------------------------------
// Implementation file for class : RichPixelClusterLocalPositions
//
// 2016-09-30 : Chris Jones
//-----------------------------------------------------------------------------

PixelClusterLocalPositions::PixelClusterLocalPositions( const std::string& name,
                                                        ISvcLocator* pSvcLocator )
  : Transformer ( name, pSvcLocator,
                  { KeyValue{ "RichPixelGlobalPositionsLocation",
                        SpacePointLocation::PixelsGlobal } },
                  { KeyValue{ "RichPixelLocalPositionsLocation",
                        SpacePointLocation::PixelsLocal } } )
{
  declareProperty( "SmartIDTool", m_idTool );
  //setProperty( "OutputLevel", MSG::DEBUG );
}

//=============================================================================

StatusCode PixelClusterLocalPositions::initialize()
{
  auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // load tools
  return m_idTool.retrieve();
}

//=============================================================================

SpacePointVector
PixelClusterLocalPositions::operator()( const SpacePointVector& gPoints ) const
{
  // the container to return
  SpacePointVector lPoints;
  lPoints.reserve( gPoints.size() );

  for ( const auto & gPos : gPoints )
  {
    lPoints.emplace_back( m_idTool.get()->globalToPDPanel(gPos) );
    _ri_debug << gPos << " -> " << lPoints.back() << endmsg;
  }

  // return the final space points
  return lPoints;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PixelClusterLocalPositions )

//=============================================================================
