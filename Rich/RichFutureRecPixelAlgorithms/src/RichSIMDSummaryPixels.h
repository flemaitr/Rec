
#pragma once

// STL
#include <algorithm>

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Rec Event
#include "RichFutureRecEvent/RichRecSpacePoints.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

// RichDet
#include "RichDet/DeRichPD.h"

// Utils
#include "RichUtils/ZipRange.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      // Use the functional framework
      using namespace Gaudi::Functional;

      /** @class SIMDSummaryPixels RichSIMDSummaryPixels.h
       *
       *  Forms SIMD summary objects for the pixel information
       *
       *  @author Chris Jones
       *  @date   2017-10-16
       */
      class SIMDSummaryPixels final :
        public Transformer< SIMDPixelSummaries( const Rich::PDPixelCluster::Vector&,
                                                const SpacePointVector&,
                                                const SpacePointVector& ),
                            Traits::BaseClass_t<AlgBase> >
      {

      public:

        /// Standard constructor
        SIMDSummaryPixels( const std::string& name,
                           ISvcLocator* pSvcLocator );

      public:

        /// Operator for each space point
        SIMDPixelSummaries operator()( const Rich::PDPixelCluster::Vector& clusters,
                                       const SpacePointVector& gPoints,
                                       const SpacePointVector& lPoints ) const override;

      };

    }
  }
}
