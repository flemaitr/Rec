
#pragma once

// STL
#include <algorithm>

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"

// Rec Event
#include "RichFutureRecEvent/RichRecSpacePoints.h"

// interfaces
#include "RichFutureInterfaces/IRichSmartIDTool.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      // Use the functional framework
      using namespace Gaudi::Functional;

      /** @class PixelClusterGlobalPositions RichPixelClusterGlobalPositions.h
       *
       *  Computes the global space points for the given pixel clusters.
       *
       *  @author Chris Jones
       *  @date   2016-09-30
       */
      class PixelClusterGlobalPositions final :
        public Transformer< SpacePointVector( const Rich::PDPixelCluster::Vector& ),
                            Traits::BaseClass_t<AlgBase> >
      {

      public:

        /// Standard constructor
        PixelClusterGlobalPositions( const std::string& name, ISvcLocator* pSvcLocator );

        /// Initialization of the tool after creation
        StatusCode initialize() override;

      public:

        /// Algorithm execution via transform
        SpacePointVector operator()( const Rich::PDPixelCluster::Vector& clusters ) const override;

      private:

        /// RichSmartID Tool
        ToolHandle<const ISmartIDTool> m_idTool
        { "Rich::Future::SmartIDTool/SmartIDTool:PUBLIC", this };

      };

    }
  }
}
