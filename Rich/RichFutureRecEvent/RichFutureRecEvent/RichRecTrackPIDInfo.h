
#pragma once

// STL
#include <string>

// Kernel
#include "Kernel/FastAllocVector.h"

// Utils
#include "RichFutureUtils/RichHypoData.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      /// Type for track PID values
      using TrackPIDHypos = LHCb::STL::Vector<Rich::ParticleIDType>;

      /// TES locations for track PID values
      namespace TrackPIDHyposLocation
      {
        /// Default TES location for track PID values
        static const std::string Default = "Rec/RichFuture/TrackHypos/Default";
        /// Default TES location for track PID values produced by the GlobalPID
        static const std::string Global  = "Rec/RichFuture/TrackHypos/Global";
      }

      /// Type for track hypothesis DLL values
      using TrackDLLs = Rich::Future::HypoData<double>;

      /// TES locations for track PID values
      namespace TrackDLLsLocation
      {
        /// Default TES location for track PID values
        static const std::string Default = "Rec/RichFuture/TrackDLLs/Default";
        /// Default TES location for track PID values produced by the GlobalPID
        static const std::string Global  = "Rec/RichFuture/TrackDLLs/Global";
      }

    }
  }
}
