#ifndef RICHFUTURERECALGORITHMS_MERGERICHPIDS_H 
#define RICHFUTURERECALGORITHMS_MERGERICHPIDS_H 1

// STL
#include <memory>

// from Gaudi
#include "GaudiAlg/MergingTransformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event
#include "Event/RichPID.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      // Use the functional framework
      using namespace Gaudi::Functional;

      /** @class MergeRichPIDs MergeRichPIDs.h
       *
       *  Merges RichPID objects from multiple containers into one.
       *
       *  @author Chris Jones
       *  @date   2016-11-05
       */

      class MergeRichPIDs final :
        public MergingTransformer< LHCb::RichPIDs( const vector_of_const_<LHCb::RichPIDs>& ),
                                   Traits::BaseClass_t<AlgBase> >
      {

      public:

        /// Standard constructor
        MergeRichPIDs( const std::string& name, ISvcLocator* pSvcLocator );

      public:

        /// Functional operator
        LHCb::RichPIDs operator()( const vector_of_const_<LHCb::RichPIDs>& inPIDs ) const override;

      private:

        /// Should the original RichPID be preserved in the cloned container ?
        Gaudi::Property<bool> m_keepPIDKey { this, "PreserveOriginalKeys", true };

      };

    }
  }
}

#endif // RICHFUTURERECALGORITHMS_MERGERICHPIDS_H
