
// local
#include "RichPixelClusters.h"

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------
// Implementation file for class : PixelClusters
//
// 2016-12-06 : Chris Jones
//-----------------------------------------------------------------------------

PixelClusters::PixelClusters( const std::string& name, ISvcLocator* pSvcLocator )
  : Consumer( name, pSvcLocator,
              KeyValue{ "RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default } )
{ }

//-----------------------------------------------------------------------------

StatusCode PixelClusters::prebookHistograms()
{
  StatusCode sc = StatusCode::SUCCESS;
  for ( const auto rich : Rich::detectors() )
  {
    if ( sc ) sc = StatusCode{richHisto1D( Rich::HistogramID( "clusterSize", rich ),
                                           "Pixel Cluster Sizes", -0.5, 100.5, 101 ) != nullptr};
  }
  return sc;
}

//-----------------------------------------------------------------------------

void PixelClusters::operator()( const Rich::PDPixelCluster::Vector& clusters ) const
{
  // the lock
  std::lock_guard<std::mutex> lock(m_updateLock);

  for ( const auto & cluster : clusters )
  {
    if ( UNLIKELY( cluster.empty() ) )
    {
      Warning("Empty cluster !",StatusCode::SUCCESS).ignore();
    }
    else
    {
      richHisto1D( HID("clusterSize",cluster.rich()) ) -> fill( cluster.size() );
    }
  }

}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PixelClusters )

//-----------------------------------------------------------------------------
