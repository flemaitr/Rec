
#pragma once

// STD
#include <mutex>

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichUtils/RichDAQDefinitions.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {
      namespace Moni
      {
        
        // Use the functional framework
        using namespace Gaudi::Functional;
        
        /** @class DetectorHits RichDetectorHits.h
         *
         *  Monitors the data sizes in the RICH detectors.
         *
         *  @author Chris Jones
         *  @date   2016-12-06
         */
        
        class DetectorHits final
          : public Consumer< void( const Rich::Future::DAQ::L1Map& ),
                             Traits::BaseClass_t<HistoAlgBase> >
        {
        
        public:
          
          /// Standard constructor
          DetectorHits( const std::string& name, ISvcLocator* pSvcLocator );

        public:
          
          /// Functional operator
          void operator()( const Rich::Future::DAQ::L1Map& data ) const override;

        protected:
          
          /// Pre-Book all histograms
          StatusCode prebookHistograms() override;

        private:

          /// Maximum pixels
          Gaudi::Property<unsigned int> m_maxPixels { this, "MaxPixels", 10000u };

        private:

          /// mutex lock
          mutable std::mutex m_updateLock;
          
        };
      
      }
    }
  }
}
