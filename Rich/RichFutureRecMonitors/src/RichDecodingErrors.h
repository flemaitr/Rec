
#pragma once

// STD
#include <sstream>
#include <map>

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichUtils/RichDAQDefinitions.h"

// Event model
#include "Event/ODIN.h"

// RichDet
#include "RichDet/DeRichSystem.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {
      namespace Moni
      {
        
        // Use the functional framework
        using namespace Gaudi::Functional;
        
        /** @class DecodingErrors RichDecodingErrors.h
         *
         *  Monitors for errors in the RICH decoding.
         *
         *  @author Chris Jones
         *  @date   2016-12-06
         */
        
        class DecodingErrors final
          : public Consumer< void( const Rich::Future::DAQ::L1Map&, const LHCb::ODIN& ),
                             Traits::BaseClass_t<HistoAlgBase> >
        {
        
        public:
          
          /// Standard constructor
          DecodingErrors( const std::string& name, ISvcLocator* pSvcLocator );

          /// Initialize
          StatusCode initialize() override;

        public:
          
          /// Functional operator
          void operator()( const Rich::Future::DAQ::L1Map& data, 
                           const LHCb::ODIN& odin ) const override;

        protected:
          
          /// Pre-Book all histograms
          StatusCode prebookHistograms() override;

        private:

          /// Get histo labels
          const BinLabels & labels();

          /// Make plots for given L1 board
          void makePlots( const Rich::Future::DAQ::IngressMap & inMap,
                          const LHCb::ODIN & odin,
                          const int l1ID ) const;

          /// Get the 1D histogram
          AIDA::IProfile1D * getHisto( const int l1ID ) const;

          /// Fill the plots for the given L1ID and error code
          void fillPlots( const Rich::DAQ::Level1CopyNumber& copyN,
                          const int errorCode,
                          const bool error,
                          AIDA::IProfile1D * h1D,
                          AIDA::IHistogram2D * h2D ) const;
          
        private:

          /// Pointer to RICH system detector element
          const DeRichSystem * m_richSys = nullptr;

          /// Map of histograms for each L1 board
          std::map<Rich::DAQ::Level1HardwareID,AIDA::IProfile1D*> m_l1Histos;

          /// The combined histo
          AIDA::IProfile1D * m_l1CombH = nullptr;

          /// The decoding errors by board
          AIDA::IHistogram2D * m_2dErrors = nullptr;
          
        };
      
      }
    }
  }
}
