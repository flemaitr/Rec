// LHCb
#include "Event/Track.h"
#include "Event/Node.h"
#include "Event/VPLightCluster.h"

// Local
#include "VPTrackMonitor.h"

// Event/LinkerEvent
#include "Linker/LinkedTo.h"
// Event/MCEvent
#include "Event/MCHit.h"
#include "Event/MCParticle.h"

DECLARE_COMPONENT( VPTrackMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPTrackMonitor::VPTrackMonitor(const std::string& name,
                               ISvcLocator* pSvcLocator)
    : GaudiTupleAlg(name, pSvcLocator) {
  declareProperty("TrackContainer",
                  m_trackLocation = LHCb::TrackLocation::Default);
  declareProperty("ClusterContainer",
                  m_clusterLocation = LHCb::VPClusterLocation::Light);
  declareProperty("LinkedHitsLocation",
                   m_linkedHitsLocation = LHCb::VPClusterLocation::Light + "2MCHits");
}

//=============================================================================
// Destructor
//=============================================================================
VPTrackMonitor::~VPTrackMonitor() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode VPTrackMonitor::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize();
  if (sc.isFailure()) return sc;


  m_det = getDetIfExists<DeVP>(DeVPLocation::Default);
  if (!m_det) {
    return Error("No detector element at " + DeVPLocation::Default);
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode VPTrackMonitor::execute() {

  // Get tracks
  const LHCb::Tracks* tracks = getIfExists<LHCb::Tracks>(m_trackLocation);
  if (!tracks) {
    return Error("No tracks at " + m_trackLocation);
  }
  const Gaudi::XYZPoint origin(0., 0., 0.);

  const LHCb::VPLightClusters* clusters = getIfExists<LHCb::VPLightClusters>(m_clusterLocation);
  if (!clusters) {
    return Error("No clusters at " + m_clusterLocation);
  }


  auto links = LinkedTo<LHCb::MCHit, LHCb::VPLightCluster>(evtSvc(), msgSvc(),
  m_linkedHitsLocation);

  size_t tracknumber=0;


  for (const LHCb::Track* track : *tracks) { //start of track-loop
    // Skip tracks which are not VELO tracks and not long tracks.
    const bool bwd = track->checkFlag(LHCb::Track::Flags::Backward);
    const auto type = track->type();
    if (type != LHCb::Track::Types::Velo && type != LHCb::Track::Types::Long && !bwd) {
      continue;
    }
    const bool fitted = track->checkFitStatus(LHCb::Track::FitStatus::Fitted);
    if (!fitted) continue;
    const double chi2NDOF = track->chi2PerDoF();
    const double probChi2 = track->probChi2();
    const double ghostProb = track->ghostProbability();
    const double phi = track->phi();
    const double eta = track->pseudoRapidity();

    const double p = track->p();
    const double pt = track->pt();

    unsigned int nClusters = track->nodes().size();
    size_t nodenumber=0;
    size_t velonodenumber=0;

    for (const LHCb::Node* node : track->nodes()) {
      if (dynamic_cast<const LHCb::FitNode&>(*node).hasMeasurement() &&
        node->type() == LHCb::Node::HitOnTrack &&
        node->measurement().type() == LHCb::Measurement::VP) {
        velonodenumber++;
      }
    }

    for (const LHCb::Node* node : track->nodes()) { // start cluster loop
      const LHCb::FitNode& fitnode = dynamic_cast<const LHCb::FitNode&>(*node);
      if (!fitnode.hasMeasurement()) continue;
      if (node->type() != LHCb::Node::HitOnTrack) continue;
      // Skip non-VP measurements.
      if ((node->measurement()).type() != LHCb::Measurement::VP) continue;
      // Get the sensor.
      const LHCb::LHCbID id = (node->measurement()).lhcbID();

      const LHCb::VPChannelID vpx_id = id.vpID();
      const LHCb::VPLightCluster* node_cluster = nullptr;

      for(auto& cluster : *clusters){
        if(cluster.second.channelID()==vpx_id){
          node_cluster=&cluster.second;
          break;
        }
      }
      if(!node_cluster){
        return Error("fail to find cluster");
      }

      const size_t number_pixels = node_cluster->pixels().size();

      const DeVPSensor* sens = m_det->sensorOfChannel(id.vpID());

      const double x = node_cluster->x();
      const double y = node_cluster->y();
      const double z = node_cluster->z();

      // Get MC hit for this cluster and plot residuals
      const LHCb::MCHit* hit = links.first(node_cluster->channelID());
      if (!hit) continue;
      // Get true track direction for this hit
      const double yangle = atan(hit->dydz()) / Gaudi::Units::degree;
      const double xangle = atan(hit->dxdz()) / Gaudi::Units::degree;
      const double theta = sqrt(xangle * xangle + yangle * yangle);
      // Get hit position.
      const Gaudi::XYZPoint mchitPoint = hit->midPoint();

      // Calculate the truth-residuals.
      const double dx = x - mchitPoint.x();
      const double dy = y - mchitPoint.y();
      const double dz = z - mchitPoint.z();
      const double d3 = sqrt(dx * dx + dy * dy + dz * dz);


      const auto corner = sens->localToGlobal(origin);
      const auto cluPos = node->state().position();
      const auto residual = getResidual(cluPos, sens, fitnode);

      Tuple theTuple = nTuple("VPTrackMonitor", "");
      theTuple->column("resX", residual.x());
      theTuple->column("resY", residual.y());
      theTuple->column("resZ",residual.z());
      theTuple->column("nodeResidual", node->residual());

      //GLOBAL:
      theTuple->column("clusX", cluPos.x());
      theTuple->column("clusY", cluPos.y());
      theTuple->column("clusZ", cluPos.z());

      theTuple->column("node_X", x);
      theTuple->column("node_Y", y);
      theTuple->column("node_Z", z);
      theTuple->column("number_pixels",number_pixels);
      theTuple->column("bwd", bwd);

      const LHCb::State state = fitnode.unbiasedState();

      theTuple->column("unbiasedNode_X", state.x());
      theTuple->column("unbiasedNode_Y", state.y());
      theTuple->column("unbiasedNode_Z", state.z());

      const Gaudi::XYZPoint clusGlobal(cluPos.x(),cluPos.y(), cluPos.z());
      const auto clusLocal = sens->globalToLocal(clusGlobal);
      theTuple->column("clusXLocal", clusLocal.x());
      theTuple->column("clusYLocal", clusLocal.y());
      theTuple->column("clusZLocal", clusLocal.z());

      const Gaudi::XYZPoint nodeGlobal(x, y, z);
      const auto nodeLocal = sens->globalToLocal(nodeGlobal);

      theTuple->column("node_XLocal", nodeLocal.x());
      theTuple->column("node_YLocal", nodeLocal.y());
      theTuple->column("node_ZLocal", nodeLocal.z());

      const Gaudi::XYZPoint node_unbiasedGlobal(state.x(), state.y(), state.z());
      const auto node_unbiasedLocal = sens->globalToLocal(node_unbiasedGlobal);
      theTuple->column("node_XunbiasedLocal", node_unbiasedLocal.x());
      theTuple->column("node_YunbiasedLocal", node_unbiasedLocal.y());
      theTuple->column("node_ZunbiasedLocal", node_unbiasedLocal.z());

      theTuple->column("mchitX", mchitPoint.x()); //MC truth particle position x
      theTuple->column("mchitY", mchitPoint.y()); //MC truth particle position y
      theTuple->column("mchitZ", mchitPoint.z());//MC truth particle position z

      theTuple->column("sensEdgeX", corner.x());
      theTuple->column("sensEdgeY", corner.y());
      theTuple->column("Error", node->errMeasure());

      theTuple->column("module", sens->module());
      theTuple->column("station", sens->station());
      theTuple->column("isRight", sens->isRight());
      theTuple->column("isLeft", sens->isLeft());

      theTuple->column("phi", phi);
      theTuple->column("eta", eta);
      theTuple->column("theta", theta);

      theTuple->column("xangle", xangle);
      theTuple->column("yangle", yangle);

      theTuple->column("chi2PerDoF", chi2NDOF);
      theTuple->column("probChi2", probChi2);
      theTuple->column("ghostProb", ghostProb);

      theTuple->column("nClusters", nClusters);
      theTuple->column("TrackType", type);

      theTuple->column("truth_resX", dx);
      theTuple->column("truth_resY", dy);
      theTuple->column("truth_resZ", dz);
      theTuple->column("truth_res3", d3);
      theTuple->column("p", p);
      theTuple->column("pt", pt);
      theTuple->column("nodenumber", nodenumber);
      theTuple->column("tracknumber",tracknumber);
      theTuple->column("velonodenumber", velonodenumber);

      theTuple->write();

      nodenumber++;
    }

    tracknumber++;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Calculate the residuals.
//=============================================================================
Gaudi::XYZVector VPTrackMonitor::getResidual(const Gaudi::XYZPoint& clusterGlobal,
                                             const DeVPSensor* sens,
                                             const LHCb::FitNode& fitnode) const {
  if (msgLevel(MSG::DEBUG)) debug() << "Calculate unbiased residuals" << endmsg;
  const LHCb::State state = fitnode.unbiasedState();
  const Gaudi::XYZPoint trackInterceptGlobal(state.x(), state.y(), state.z());
  const auto trackInterceptLocal = sens->globalToLocal(trackInterceptGlobal);
  const auto clusterLocal = sens->globalToLocal(clusterGlobal);
  const double resx = trackInterceptLocal.x() - clusterLocal.x(); //comparing track_x in local with cluster_x_local
  const double resy = trackInterceptLocal.y() - clusterLocal.y(); //unbiased
  const double resz = trackInterceptLocal.z() - clusterLocal.z();
  return Gaudi::XYZVector(resx, resy, resz);
}
