#ifndef PARAMETERIZEDKALMANFIT_H 
#define PARAMETERIZEDKALMANFIT_H 1

// Include files
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "GaudiKernel/ToolHandle.h" 
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"

#include "Kernel/ILHCbMagnetSvc.h"

#include "GaudiKernel/System.h"

#include "Event/Track.h"

#include "TrackInterfaces/IMeasurementProvider.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "KalmanParametrizations.h"
#include "ParameterizedKalmanFit_Methods.h"

#include <TTree.h>

using namespace ParKalman;

/** @class ParameterizedKalmanFit ParameterizedKalmanFit.h
 *
 *  Algorithm to perfrom a simplified Kalman filter based on parameterized extrapolations
 *
 *  Parameters:
 *  - UseUTHits:             Use hits in the UT in the fit
 *  - UseTHits:              Use hits in the SciFi in the fit
 *  - MaxNumOutlier:         Maximal number of outliers that should be removed
 *  - UseForwMomEstiamte:    Use the momentum estaimte of an forward iteration 
 *  - UseForwChi2Estiamte:   Use the chi2 of an forward estimate
 *  - UseOneParameterSet:    Use the parameters for magnet polarity down also for mag up
 *                           (some signs are reversed)
 *  - ParamFileLocation:     Location of the parameter files:
 *                           Has to contain MagUp/ and MagDown/
 *
 *  @author Simon Stemmle
 *  @date   2017-10-26
 */

class ParameterizedKalmanFit : public Gaudi::Functional::Transformer<
                                       LHCb::Tracks(const LHCb::Tracks&)> { 
public: 
  /// Standard constructor
  ParameterizedKalmanFit( const std::string& name, ISvcLocator* pSvcLocator );
  
  /// Algorithm initialization
  StatusCode initialize() override;   

  /// Algorithm execution
  LHCb::Tracks operator()(const LHCb::Tracks&) const override;

protected:

private:
  Gaudi::Property<bool>        m_UseUT                 {this, "UseUTHits"          , true };

  Gaudi::Property<bool>        m_UseT                  {this, "UseTHits"           , true };

  Gaudi::Property<int>         m_MaxNoutlier           {this, "MaxNumOutlier"      , 1    };
  
  Gaudi::Property<bool>        m_UseForwardMomEstimate {this, "UseForwMomEstimate" , true };
  
  Gaudi::Property<bool>        m_UseForwardChi2Estimate{this, "UseForwChi2Estimate", true };
  
  Gaudi::Property<bool>        m_UseOneParameterSet    {this, "UseOneParameterSet" , false};
  
  Gaudi::Property<std::string> m_ParamFileLocation     {this, "ParamFileLocation"  ,
                                                          System::getEnv("PARAMFILESROOT")
                                                            +"/data/ParametrizedKalmanFit"};
    
  //cache information for the smoother step
  bool m_do_smoother = true;

  //Parametrizations objects for each polarity
  KalmanParametrizations m_ParExtrUp   = KalmanParametrizations();
  KalmanParametrizations m_ParExtrDown = KalmanParametrizations();
  
  //#####
  //Tools
  //#####
  //measuremant provider for the different detectors
  ToolHandle<IMeasurementProvider>  m_measProviderV  = 
                 {this, "MeasProvider_V", "MeasurementProviderT<MeasurementProviderTypes::VP>/VPMeasurementProvider"};
  ToolHandle<IMeasurementProvider>  m_measProviderUT = 
                 {this, "MeasProvider_UT","MeasurementProviderT<MeasurementProviderTypes::UT>/UTMeasurementProvider"}; 
  ToolHandle<IMeasurementProvider>  m_measProviderT  = 
                 {this, "MeasProvider_T", "FTMeasurementProvider/mMeasProvT"};

  //extrapolators for the extrapolation to the beam pipe
  ToolHandle<ITrackExtrapolator> m_extrapolator_toPV = {"TrackMasterExtrapolator/extr", this};

  //Magnet field service in order to load the correct parameters
  ILHCbMagnetSvc* m_magFieldSvc = nullptr;

  //#################
  //1. Main method
  //#################

  /// Runs the Kalman filter on a track
  StatusCode fit(trackInfo &tI) const;

  //##########################################################################################
  // Methods for the Kalman filter that are not implemented in ParameterizedKalmanFit_Methods
  //##########################################################################################
  
  /// General method for predicting to a hit
  bool PredictState(int forward, int nHit, Gaudi::Vector5 &x, Gaudi::SymMatrix5x5 &C, double &lastz,
                    trackInfo &tI) const;
  
};

#endif //PARAMETERIZEDKALMANFIT_H
