// Include files
// -------------
// Event model
#include "TrackKernel/TrackTraj.h"
#include "Event/Measurement.h"
#include "Event/TrackFitResult.h"

// local
#include "MeasurementProvider.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : MeasurementProvider
//
// 2005-04-14 : Jose Angel Hernando Morata
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( MeasurementProvider )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MeasurementProvider::MeasurementProvider( const std::string& type,
                                          const std::string& name,
                                          const IInterface* parent )
: base_class ( type, name , parent )
{
  declareProperty( "VeloRProvider", m_veloRProvider ) ;
  declareProperty( "VeloPhiProvider", m_veloPhiProvider ) ;
  declareProperty( "VPProvider", m_vpProvider ) ;
  declareProperty( "TTProvider", m_ttProvider ) ;
  declareProperty( "UTProvider", m_utProvider ) ;
  declareProperty( "ITProvider", m_itProvider ) ;
  declareProperty( "OTProvider", m_otProvider ) ;
  declareProperty( "FTProvider", m_ftProvider ) ;
  declareProperty( "MuonProvider", m_muonProvider ) ;
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode MeasurementProvider::initialize()
{
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << "MeasurementProvider::initialize()" << endmsg;
  StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure()) return sc;  // error already reported by base class

  m_providermap.fill(nullptr) ;

  if(!m_ignoreVelo) {
    sc = m_veloRProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::VeloR] = &(*m_veloRProvider) ;

    sc = m_veloPhiProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::VeloPhi] = &(*m_veloPhiProvider) ;
  } else {
    m_veloRProvider.disable();
    m_veloPhiProvider.disable();
  }

  if(!m_ignoreVP) {
    sc = m_vpProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::VP] = &(*m_vpProvider) ;
  } else {
    m_vpProvider.disable();
  }

  if(!m_ignoreTT) {
    sc = m_ttProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::TT] = &(*m_ttProvider) ;
  } else {
    m_ttProvider.disable();
  }

  if(!m_ignoreUT) {
    sc = m_utProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::UT] = &(*m_utProvider) ;
  } else {
    m_utProvider.disable();
  }

  if(!m_ignoreIT) {
    sc = m_itProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::IT] = &(*m_itProvider) ;
  } else {
    m_itProvider.disable();
  }

  if(!m_ignoreOT) {
    sc = m_otProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::OT] = &(*m_otProvider) ;
  } else {
    m_otProvider.disable();
  }

  if(!m_ignoreFT) {
    sc = m_ftProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::FT] = &(*m_ftProvider) ;
  } else {
    m_ftProvider.disable();
  }

  if(!m_ignoreMuon) {
    sc = m_muonProvider.retrieve() ;
    if (sc.isFailure()) return sc;
    m_providermap[LHCb::Measurement::Muon] = &(*m_muonProvider) ;
  } else {
    m_muonProvider.disable();
  }
  return sc ;
}


StatusCode MeasurementProvider::finalize()
{
  StatusCode sc;
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << "In MeasurementProvider::finalize. Releasing tool handles." << endmsg ;
  // make sure to release all toolhandles
  if(!m_ignoreVelo) {
    sc = m_veloRProvider.release() ;
    if (sc.isFailure()) return sc;
    sc = m_veloPhiProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  if(!m_ignoreVP) {
    sc = m_vpProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  if(!m_ignoreTT) {
    sc = m_ttProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  if(!m_ignoreUT) {
    sc = m_utProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  if(!m_ignoreIT) {
    sc = m_itProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  if(!m_ignoreOT) {
    sc = m_otProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  if(!m_ignoreFT) {
    sc = m_ftProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  if(!m_ignoreMuon) {
    sc = m_muonProvider.release() ;
    if (sc.isFailure()) return sc;
  }
  sc = GaudiTool::finalize() ;
  return sc ;
}


//=============================================================================
// Load all the Measurements from the list of LHCbIDs on the input Track
//=============================================================================
StatusCode MeasurementProvider::load( Track& track ) const
{
  // make sure we have a place to store the result
  if( !track.fitResult()) track.setFitResult( new LHCb::TrackFitResult() ) ;

  std::vector<LHCbID> newids ; newids.reserve(track.lhcbIDs().size() );
  for ( const auto& id : track.lhcbIDs() ) {
    // First look if the Measurement corresponding to this LHCbID
    // is already in the Track, i.e. whether it has already been loaded!
    if ( track.fitResult()->measurement( id ) ) {
      Warning("Found measurements already loaded on track!",StatusCode::SUCCESS,0).ignore() ;
      if( msgLevel( MSG::DEBUG ) || msgLevel( MSG::VERBOSE ) )
        debug() << "Measurement had already been loaded for the LHCbID"
                << " channelID, detectorType = "
                << id.channelID() << " , " << id.detectorType()
                << "  -> Measurement loading skipped for this LHCbID!"
                << endmsg;
    } else newids.push_back( id ) ;
  }

  // create a reference trajectory
  LHCb::TrackTraj reftraj(track) ;

  // create all measurements for selected IDs
  LHCb::TrackFitResult::MeasurementContainer newmeasurements ;
  newmeasurements.reserve(newids.size());

  addToMeasurements(newids,newmeasurements,reftraj) ;

  // remove all zeros, just in case.
  auto newend = std::remove_if(newmeasurements.begin(),newmeasurements.end(),
                               [](const Measurement* m) { return m==nullptr; } );
  if(newend != newmeasurements.end()) {
    Warning("Some measurement pointers are zero: ",StatusCode::SUCCESS,0).ignore() ;
    if( msgLevel( MSG::DEBUG ) )
      debug() << "Some measurement pointers are zero: " << int(newmeasurements.end() - newend) << endmsg ;
    newmeasurements.erase(newend,newmeasurements.end()) ;
  }

  // add the measurements to the track
  track.fitResult()->addToMeasurements( newmeasurements ) ;

  // Update the status flag of the Track
  track.setPatRecStatus( Track::PatRecStatus::PatRecMeas );

  return StatusCode::SUCCESS;
}

namespace {
LHCb::Measurement::Type measurementtype(const LHCb::LHCbID& id)
{
  switch( id.detectorType() ) {
  case LHCb::LHCbID::Velo: return id.isVeloR() ? LHCb::Measurement::VeloR
                                               : LHCb::Measurement::VeloPhi ;
  case LHCb::LHCbID::VP:   return LHCb::Measurement::VP      ;
  case LHCb::LHCbID::TT:   return LHCb::Measurement::TT      ;
  case LHCb::LHCbID::UT:   return LHCb::Measurement::UT      ;
  case LHCb::LHCbID::IT:   return LHCb::Measurement::IT      ;
  case LHCb::LHCbID::OT:   return LHCb::Measurement::OT      ;
  case LHCb::LHCbID::FT:   return LHCb::Measurement::FT      ;
  case LHCb::LHCbID::Muon: return LHCb::Measurement::Muon    ;
  default:                 return LHCb::Measurement::Unknown ;
  }
}
}

//=============================================================================
// Construct a Measurement of the type of the input LHCbID
//=============================================================================

Measurement* MeasurementProvider::measurement ( const LHCbID& id, bool localY ) const
{
  const IMeasurementProvider* provider = m_providermap[measurementtype( id )] ;
  return provider ? provider->measurement(id, localY) : nullptr ;
}

Measurement* MeasurementProvider::measurement ( const LHCbID& id, const LHCb::ZTrajectory<double>& state, bool localY ) const
{
  const IMeasurementProvider* provider = m_providermap[measurementtype( id )] ;
  return provider ? provider->measurement(id,state, localY) : nullptr ;
}

//-----------------------------------------------------------------------------
/// Create a list of measurements from a list of LHCbIDs
//-----------------------------------------------------------------------------
void MeasurementProvider::addToMeasurements( LHCb::span<LHCb::LHCbID> ids,
                                             std::vector<LHCb::Measurement*>& measurements,
                                             const LHCb::ZTrajectory<double>& tracktraj ) const
{
  // dispatch the measurements according to their type.
  for ( unsigned i=0; i<m_providermap.size(); ++i ) {
    const auto* provider = m_providermap[i];
    if (!provider) continue;
    auto pivot = std::stable_partition( ids.begin(), ids.end(),
                                        [&](const LHCb::LHCbID& id)
                                        { return measurementtype(id) == i; } );
    auto n = std::distance( ids.begin(), pivot );
    provider->addToMeasurements( ids.first(n), measurements, tracktraj );
    ids = ids.subspan(n);
  }
}

//=============================================================================
