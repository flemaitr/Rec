//-----------------------------------------------------------------------------
/** @file VeloTrackSelector.cpp
 *
 *  Implementation file for reconstruction tool : VeloTrackSelector
 *
 *  @author M.Needham Matt.Needham@cern.ch
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   30/12/2005
 */
//-----------------------------------------------------------------------------

// STL
#include <string>

#include "TrackSelector.h"

class VeloTrackSelector : public TrackSelector
{

public:

  /// constructor
  using TrackSelector::TrackSelector;

  /** Returns if the given track is selected or not
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return boolean indicating if the track is selected or not
   *  @retval true  Track is selected
   *  @retval false Track is rejected
   */
  bool accept ( const LHCb::Track & aTrack ) const override;

private:
  Gaudi::Property<size_t> m_minHitsASide { this,  "MinHitsASide" , 0 } ;
  Gaudi::Property<size_t> m_minHitsCSide { this,  "MinHitsCSide" , 0 } ;
};

DECLARE_COMPONENT( VeloTrackSelector )

//-----------------------------------------------------------------------------

bool VeloTrackSelector::accept ( const LHCb::Track& aTrack ) const
{
  size_t numHits[2] = {0,0} ;
  for( auto it = aTrack.lhcbIDs().begin() ;
       it != aTrack.lhcbIDs().end(); ++it )
    if( it->isVelo() ) {
      LHCb::VeloChannelID veloid = it->veloID() ;
      unsigned int side    = (veloid.sensor()%2) ;
      ++numHits[side] ;
    }
  return
    numHits[0] >= m_minHitsASide &&
    numHits[1] >= m_minHitsCSide &&
    TrackSelector::accept(aTrack) ;
}

