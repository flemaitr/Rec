#ifndef TRACKTOOLS_MEASUREMENTPROVIDER_H
#define TRACKTOOLS_MEASUREMENTPROVIDER_H

// Include files
// -------------
#include <array>
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/ToolHandle.h"

// from LHCbKernel
#include "Kernel/LHCbID.h"

// from TrackInterfaces
#include "TrackInterfaces/IMeasurementProvider.h"
#include "TrackInterfaces/ISTClusterPosition.h"

// from TrackEvent
#include "Event/Track.h"

class MeasurementProvider : public extends<GaudiTool, IMeasurementProvider> {
public:
  /** standard tool constructor */
  MeasurementProvider( const std::string& type, const std::string& name, const IInterface* parent);

  /** initialize tool */
  StatusCode initialize() override;

  /** finalize tool */
  StatusCode finalize() override;

  /** See interface class */
  StatusCode load( LHCb::Track& track ) const override;

  /** See interface class */
  virtual LHCb::Measurement* measurement( const LHCb::LHCbID& id, bool localY=false) const override;

  /** See interface class */
  virtual LHCb::Measurement* measurement( const LHCb::LHCbID& id, const LHCb::ZTrajectory<double>& ref,
                                          bool localY=false) const override;

  /** See interface class */
  virtual void addToMeasurements( LHCb::span<LHCb::LHCbID> ids,
                                  std::vector<LHCb::Measurement*>& measurements,
                                  const LHCb::ZTrajectory<double>& reftraj) const override;

private:
  // Handles to actual measurement providers
  ToolHandle<IMeasurementProvider> m_veloRProvider =   { "MeasurementProviderT<MeasurementProviderTypes::VeloR>/VeloRMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_veloPhiProvider = { "MeasurementProviderT<MeasurementProviderTypes::VeloPhi>/VeloPhiMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_vpProvider =      { "MeasurementProviderT<MeasurementProviderTypes::VP>/VPMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_ttProvider =      { "MeasurementProviderT<MeasurementProviderTypes::TT>/TTMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_utProvider =      { "MeasurementProviderT<MeasurementProviderTypes::UT>/UTMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_itProvider =      { "MeasurementProviderT<MeasurementProviderTypes::IT>/ITMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_otProvider =      { "OTMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_ftProvider =      { "FTMeasurementProvider", this };
  ToolHandle<IMeasurementProvider> m_muonProvider =    { "MuonMeasurementProvider", this };

  Gaudi::Property<bool> m_ignoreVelo{ this, "IgnoreVelo", false };                                           ///< Ignore Velo hits
  Gaudi::Property<bool> m_ignoreVP{ this, "IgnoreVP", true }; // VP does not exist in default detector   ///< Ignore VP hits
  Gaudi::Property<bool> m_ignoreTT{ this, "IgnoreTT", false };                                           ///< Ignore TT hits
  Gaudi::Property<bool> m_ignoreUT{ this, "IgnoreUT", true };                                            ///< Ignore UT hits
  Gaudi::Property<bool> m_ignoreIT{ this, "IgnoreIT", false };                                           ///< Ignore IT hits
  Gaudi::Property<bool> m_ignoreOT{ this, "IgnoreOT", false };                                           ///< Ignore OT hits
  Gaudi::Property<bool> m_ignoreFT{ this, "IgnoreFT", true  };                                           ///< Ignore FT hits
  Gaudi::Property<bool> m_ignoreMuon{ this, "IgnoreMuon", false };                                           ///< Ignore Muon hits
  Gaudi::Property<bool> m_initializeReference{ this, "InitializeReference", true } ;///< Initialize measurement reference vector with closest state on track

  std::array<const IMeasurementProvider*,LHCb::Measurement::UTLite+1> m_providermap ;
};
#endif // TRACKTOOLS_MEASUREMENTPROVIDER_H
