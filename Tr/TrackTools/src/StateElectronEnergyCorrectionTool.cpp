// Include files
// -------------
#include <cmath>

// from DetDesc
#include "DetDesc/Material.h"

// local
#include "StateElectronEnergyCorrectionTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : StateElectronEnergyCorrectionTool
//
// 2006-08-18 : Eduardo Rodrigues
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( StateElectronEnergyCorrectionTool )

//=============================================================================
// Correct a State for electron dE/dx energy losses
//=============================================================================
void StateElectronEnergyCorrectionTool::correctState( LHCb::State& state,
                                                      const Material* material,
                                                      ranges::v3::any& /*cache*/,
                                                      double wallThickness,
                                                      bool upstream,
                                                      double  ) const
{
  //hard energy loss for electrons
  double t = wallThickness / material -> radiationLength()
    * sqrt( 1. + std::pow(state.tx(),2) + std::pow(state.ty(),2) );
  if ( ! upstream ) t *= -1.;

  // protect against t too big
  if ( fabs(t) > m_maxRadLength )  t = std::copysign( 1.0, t ) * m_maxRadLength;

  // apply correction
  Gaudi::TrackVector&    tX = state.stateVector();
  Gaudi::TrackSymMatrix& tC = state.covariance();

  tC(4,4) += std::pow(tX[4],2) * ( exp(-t*log(3.0)/log(2.0))-exp(-2.0*t) );
  tX[4]   *= exp(-t);
}


//=============================================================================
