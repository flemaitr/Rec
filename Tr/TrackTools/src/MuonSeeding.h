#ifndef MUONSEEDING_H
#define MUONSEEDING_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "MuonInterfaces/MuonTrack.h"
#include "MuonInterfaces/MuonHit.h"
#include "MuonInterfaces/IMuonTrackMomRec.h"
#include "MuonInterfaces/IMuonTrackRec.h"
#include "Event/Track.h"
#include "Event/Particle.h"

#include "Event/State.h"
#include "Event/RecVertex.h"

#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "Kernel/LHCbID.h"
#include "GaudiKernel/Chrono.h"


/** @class MuonSeeding MuonSeeding.h
 *
 * \brief  Make a MuonSeeding: Get muon standalone tracks
 *
 * Parameters:
 * - ToolName: Name for the tool that makes muon standalone track.
 * - Extrapolator: Name for the track extrapolator.
 * - FillMuonStubInfo: Fill parameters of muon stub in info fields of track;
 * - Output: The location the tracks should be written to.
 *
 *  @author Michel De Cian
 *  @date   2010-09-20
 */

class MuonSeeding final : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  MuonSeeding( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:

  // -- Methods
  StatusCode fillPVs(std::vector<double>& PVPos);
  StatusCode iterateToPV(LHCb::Track* track, LHCb::State& muonState, LHCb::State& veloState,
      const std::vector<double>& PVPos, double qOverP);

  // -- Properties
  Gaudi::Property<bool>         m_fitTracks         { this, "FitTracks",        true };
  Gaudi::Property<std::string>  m_outputLoc         { this, "Output",           "Rec/MuonSeeding/Tracks" };

  // -- Tools
  ToolHandle<IMuonTrackRec>       m_trackTool { "MuonNNetRec/MuonRecTool", this };
  ToolHandle<ITrackFitter>        m_trackFitter { "TrackMasterFitter/Fitter", this };
  ToolHandle<IMuonTrackMomRec>    m_momentumTool { "MuonTrackMomRec/MuonMomTool", this };
  ToolHandle<ITrackExtrapolator>  m_extrapolator { "TrackMasterExtrapolator/Extrapolator", this };
};
#endif // GETMUONTRACK_H
