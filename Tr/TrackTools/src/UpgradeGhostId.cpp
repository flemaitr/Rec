// Include files
#include "Kernel/HitPattern.h"
#include "Event/FTCluster.h"
#include "Event/Particle.h"
#include "Event/STCluster.h"
#include "Event/Track.h"
#include "Event/TrackFitResult.h"
#include "Event/VPLightCluster.h"
#include "Event/GhostTrackInfo.h"
#include "TMVA/TMVAUp_1_MLP_1.class.C"
#include "TMVA/TMVAUp_3_MLP_3.class.C"
#include "TMVA/TMVAUp_4_MLP_4.class.C"
#include "TMVA/TMVAUp_5_MLP_5.class.C"
#include "TMVA/TMVAUp_6_MLP_6.class.C"


#include "TMath.h"

// local
#include "UpgradeGhostId.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UpgradeGhostId
//
// 2014-12-30 : Paul Seyfert
// following an earlier version by Angelo Di Canto
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( UpgradeGhostId )

//=============================================================================
//

namespace {
 static const int largestChannelIDype = 1+std::max(LHCb::LHCbID::UT,std::max(LHCb::LHCbID::VP,LHCb::LHCbID::FT));
 static const int largestTrackTypes = 1+LHCb::Track::Types::Ttrack;}


StatusCode UpgradeGhostId::finalize()
{

  delete m_expectedHits;
  delete m_readers[LHCb::Track::Types::Velo];
  delete m_readers[LHCb::Track::Types::Upstream];
  delete m_readers[LHCb::Track::Types::Downstream];
  delete m_readers[LHCb::Track::Types::Long];
  delete m_readers[LHCb::Track::Types::Ttrack];
  return GaudiTool::finalize();
}

StatusCode UpgradeGhostId::initialize()
{
  if( !GaudiTool::initialize() ) return StatusCode::FAILURE;

  if (largestTrackTypes<=std::max(LHCb::Track::Types::Ttrack,std::max(
     std::max(LHCb::Track::Types::Velo,LHCb::Track::Types::Upstream),
     std::max(LHCb::Track::Types::Ttrack,LHCb::Track::Types::Downstream))))
    return Warning("ARRAY SIZE SET WRONG (largestTrackTypes is smaller than enum LHCb::Track::Types",StatusCode::FAILURE);

  m_vpExpectation = tool<IVPExpectation>("VPExpectation");
  for (auto entry : m_expectorNames) {
    m_Expectations.push_back(tool<IHitExpectation>(entry));
  }
  m_expectedHits = new std::vector<int>(m_expectorNames.size(),0);


  m_readers = std::vector<IClassifierReader*> (largestTrackTypes,NULL);
  std::vector<std::string> names = veloVars();
  m_readers[LHCb::Track::Types::Velo] = new ReadMLP_1(names);
  names = upstreamVars();
  m_readers[LHCb::Track::Types::Upstream] = new ReadMLP_4(names);
  names = downstreamVars();
  m_readers[LHCb::Track::Types::Downstream] = new ReadMLP_5(names);
  names = longVars();
  m_readers[LHCb::Track::Types::Long] = new ReadMLP_3(names);
  names = ttrackVars();
  m_readers[LHCb::Track::Types::Ttrack] = new ReadMLP_6(names);

  return StatusCode::SUCCESS;
}

StatusCode UpgradeGhostId::countHits() {
  const LHCb::VPLightClusters* vpCont = NULL;
  const LHCb::STClusters* utCont = NULL;
  vpCont = getIfExists<LHCb::VPLightClusters>(LHCb::VPClusterLocation::Light);
  utCont = getIfExists<LHCb::STClusters>(LHCb::STClusterLocation::UTClusters);
  if (vpCont) m_veloHits = vpCont->size();
  if (utCont) m_utHits = utCont->size();
  if (!((vpCont)&&(utCont))) {
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;

}

namespace {
  inline std::vector<double> subdetectorhits(const LHCb::Track& aTrack) {

    std::vector<double> returnvalue = std::vector<double>(largestChannelIDype,0.);
    returnvalue[LHCb::LHCbID::UT] = 0.;
    returnvalue[LHCb::LHCbID::FT] = 0.;
    returnvalue[LHCb::LHCbID::VP] = 0.;
    for (auto lhcbid : aTrack.lhcbIDs()) {
      if (lhcbid.detectorType()>=returnvalue.size()) {continue;} // may be a hit in a non-tracking detector
      (returnvalue[lhcbid.detectorType()]) += 1.;
    }
    return returnvalue;
  }
}


//=============================================================================
StatusCode UpgradeGhostId::execute(LHCb::Track& aTrack) const
{
  std::vector<double> obsarray = subdetectorhits(aTrack);
  const LHCb::TrackFitResult* fit = aTrack.fitResult();

  std::vector<double> variables; variables.reserve(18);
  //if (LHCb::Track::Types::Velo == tracktype || LHCb::Track::Types::Long == tracktype || LHCb::Track::Types::Upstream == tracktype) {
  if (aTrack.hasVelo()) {
    variables.push_back(obsarray[LHCb::LHCbID::VP] );
    variables.push_back(m_vpExpectation->nExpected(aTrack));
    variables.push_back(aTrack.info(LHCb::Track::AdditionalInfo::FitVeloChi2, -999));
    variables.push_back(aTrack.info(LHCb::Track::AdditionalInfo::FitVeloNDoF, -999));
  }
  //if (LHCb::Track::Types::Downstream == tracktype || LHCb::Track::Types::Long == tracktype || LHCb::Track::Types::Ttrack == tracktype) {
  if (aTrack.hasT()) {
    variables.push_back(obsarray[LHCb::LHCbID::FT] );
    variables.push_back(m_Expectations[1]->nExpected(aTrack));
    variables.push_back(aTrack.info(LHCb::Track::AdditionalInfo::FitTChi2, -999));
    variables.push_back(aTrack.info(LHCb::Track::AdditionalInfo::FitTNDoF, -999));
  }
  //if (LHCb::Track::Types::Downstream == tracktype || LHCb::Track::Types::Long == tracktype || LHCb::Track::Types::Upstream == tracktype) {
  if (aTrack.hasUT()) { // includes longtracks w/o ut hits
    variables.push_back(obsarray[LHCb::LHCbID::UT] );
    variables.push_back(m_Expectations[0]->nExpected(aTrack));
  }
  if (LHCb::Track::Types::Long == aTrack.type()) {
    variables.push_back(aTrack.info(LHCb::Track::AdditionalInfo::FitMatchChi2, -999));
  }
  if (aTrack.hasUT()) {
    variables.push_back((fit->nMeasurements(LHCb::Measurement::UT) - fit->nActiveMeasurements(LHCb::Measurement::UT)));// "UpgradeGhostInfo_UToutlier",'F'
  }
  variables.push_back(m_veloHits);
  variables.push_back(  m_utHits);
  variables.push_back(aTrack.chi2());
  if (LHCb::Track::Types::Long != aTrack.type() && LHCb::Track::Types::Downstream != aTrack.type()) {
    variables.push_back(aTrack.nDoF());
  }
  if (LHCb::Track::Types::Velo != aTrack.type()) {
    variables.push_back(aTrack.pt());
  }
  variables.push_back(aTrack.pseudoRapidity());

  //float netresponse = m_readers[aTrack.type()]->GetRarity(variables); // TODO rarity would be nice, see https://sft.its.cern.ch/jira/browse/ROOT-7050
  float netresponse = m_readers[aTrack.type()]->GetMvaValue(variables);

  aTrack.setGhostProbability(1.-netresponse);

  return StatusCode::SUCCESS;
}

std::vector<std::string> UpgradeGhostId::veloVars() const {
  std::vector<std::string> retval;
  retval.push_back("UpgradeGhostInfo_obsVP");
  retval.push_back("UpgradeGhostInfo_expVP");
  retval.push_back("UpgradeGhostInfo_FitVeloChi2");
  retval.push_back("UpgradeGhostInfo_FitVeloNDoF");
  retval.push_back("UpgradeGhostInfo_veloHits");
  retval.push_back("UpgradeGhostInfo_utHits");
  retval.push_back("TRACK_CHI2");
  retval.push_back("TRACK_NDOF");
  retval.push_back("TRACK_ETA");
  return retval;
}


std::vector<std::string> UpgradeGhostId::upstreamVars() const {
  std::vector<std::string> retval;
  retval.push_back("UpgradeGhostInfo_obsVP");
  retval.push_back("UpgradeGhostInfo_expVP");
  retval.push_back("UpgradeGhostInfo_FitVeloChi2");
  retval.push_back("UpgradeGhostInfo_FitVeloNDoF");
  retval.push_back("UpgradeGhostInfo_obsUT");
  retval.push_back("UpgradeGhostInfo_expUTHitExpectation");
  retval.push_back("UpgradeGhostInfo_UToutlier");
  retval.push_back("UpgradeGhostInfo_veloHits");
  retval.push_back("UpgradeGhostInfo_utHits");
  retval.push_back("TRACK_CHI2");
  retval.push_back("TRACK_NDOF");
  retval.push_back("TRACK_PT");
  retval.push_back("TRACK_ETA");
  return retval;
}



std::vector<std::string> UpgradeGhostId::downstreamVars() const {
  std::vector<std::string> retval;
  retval.push_back("UpgradeGhostInfo_obsFT");
  retval.push_back("UpgradeGhostInfo_expFTHitExpectation");
  retval.push_back("UpgradeGhostInfo_FitTChi2");
  retval.push_back("UpgradeGhostInfo_FitTNDoF");
  retval.push_back("UpgradeGhostInfo_obsUT");
  retval.push_back("UpgradeGhostInfo_expUTHitExpectation");
  retval.push_back("UpgradeGhostInfo_UToutlier");
  retval.push_back("UpgradeGhostInfo_veloHits");
  retval.push_back("UpgradeGhostInfo_utHits");
  retval.push_back("TRACK_CHI2");
  retval.push_back("TRACK_PT");
  retval.push_back("TRACK_ETA");
  return retval;
}



std::vector<std::string> UpgradeGhostId::longVars() const {
  std::vector<std::string> retval;
  retval.push_back("UpgradeGhostInfo_obsVP");
  retval.push_back("UpgradeGhostInfo_expVP");
  retval.push_back("UpgradeGhostInfo_FitVeloChi2");
  retval.push_back("UpgradeGhostInfo_FitVeloNDoF");
  retval.push_back("UpgradeGhostInfo_obsFT");
  retval.push_back("UpgradeGhostInfo_expFTHitExpectation");
  retval.push_back("UpgradeGhostInfo_FitTChi2");
  retval.push_back("UpgradeGhostInfo_FitTNDoF");
  retval.push_back("UpgradeGhostInfo_obsUT");
  retval.push_back("UpgradeGhostInfo_expUTHitExpectation");
  retval.push_back("UpgradeGhostInfo_FitMatchChi2");
  retval.push_back("UpgradeGhostInfo_UToutlier");
  retval.push_back("UpgradeGhostInfo_veloHits");
  retval.push_back("UpgradeGhostInfo_utHits");
  retval.push_back("TRACK_CHI2");
  retval.push_back("TRACK_PT");
  retval.push_back("TRACK_ETA");
  return retval;
}



std::vector<std::string> UpgradeGhostId::ttrackVars() const {
  std::vector<std::string> retval;
  retval.push_back("UpgradeGhostInfo_obsFT");
  retval.push_back("UpgradeGhostInfo_expFTHitExpectation");
  retval.push_back("UpgradeGhostInfo_FitTChi2");
  retval.push_back("UpgradeGhostInfo_FitTNDoF");
  retval.push_back("UpgradeGhostInfo_veloHits");
  retval.push_back("UpgradeGhostInfo_utHits");
  retval.push_back("TRACK_CHI2");
  retval.push_back("TRACK_NDOF");
  retval.push_back("TRACK_PT");
  retval.push_back("TRACK_ETA");
  return retval;
}
