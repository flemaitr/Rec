// from GaudiKernel
#include "GaudiKernel/SystemOfUnits.h"

// Event
#include "Event/Track.h"
#include "Event/VeloCluster.h"
#include "Event/VeloMeasurement.h"
#include "Event/VeloLiteMeasurement.h"
#include "Event/STCluster.h"
#include "Event/STMeasurement.h"

// local
#include "TrackLikelihood.h"

#include <algorithm>

#include "Kernel/LHCbID.h"

#include "gsl/gsl_math.h"
#include "gsl/gsl_rng.h"
#include "gsl/gsl_randist.h"
#include "gsl/gsl_cdf.h"

#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/IVeloExpectation.h"

using namespace LHCb;
using namespace Gaudi;

namespace {
    double binomialTerm(const unsigned int found,
                        const unsigned int expected,
                        const double eff) {

      unsigned int n = expected;
      unsigned int r = found;
      if ( r > n ) r = n;
      double fnum = 1.;
      double fden = 1.;
      for ( unsigned int i = 0; i < n-r; ++i ) {
        fnum *= double( n-i );
        fden *= double( i+1 );
      }

      return log( fnum/fden) + r*log(eff) + (n-r)*log(1 - eff);
    }
}
DECLARE_COMPONENT( TrackLikelihood )

//=============================================================================
//
//=============================================================================

StatusCode TrackLikelihood::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure()) return sc;

  if (m_useVelo) m_veloExpectation = tool<IVeloExpectation>("VeloExpectation");
  if (m_useTT)   m_ttExpectation = tool<IHitExpectation>("TTHitExpectation");
  if (m_useUT)   m_utExpectation = tool<IHitExpectation>("UTHitExpectation");
  if (m_useIT)   m_itExpectation = tool<IHitExpectation>("ITHitExpectation");
  if (m_useOT)   m_otExpectation = tool<IHitExpectation>("OTHitExpectation");

  return StatusCode::SUCCESS;
}

float TrackLikelihood::operator()(const LHCb::Track& aTrack) const
{
  // chi^2 contribution
  double lik = ( aTrack.probChi2() < 1e-50 ? -150.0
                                           : log(aTrack.probChi2())/m_chiWeight);

  if (m_useVelo && aTrack.hasVelo()) lik += addVelo(aTrack);
  if (m_useTT && aTrack.hasTT()) lik += addTT(aTrack);
  if (m_useUT && aTrack.hasUT()) lik += addUT(aTrack);

 if (aTrack.hasT()){
   if (m_useOT) lik += addOT(aTrack);
   if (m_useIT) lik += addIT(aTrack);
  }

  // return the likelihood value
  return lik;
}

double TrackLikelihood::addVelo(const LHCb::Track& aTrack) const{

  double lik = 0.0;

  // pick up the LHCbIDs + measurements
  const std::vector<LHCb::LHCbID>& ids = aTrack.lhcbIDs();

  // get the number of expected hits in the velo
  auto nVelo = std::accumulate( ids.begin(), ids.end(), std::array<unsigned,2>{0u,0u},
                                [](std::array<unsigned, 2> N, const LHCbID& id) {
                                    if (id.isVelo()) ++N[id.veloID().isRType()];
                                    return N;
                                } );

  // get the number of hits
  IVeloExpectation::Info expectedVelo = m_veloExpectation->expectedInfo(aTrack);
  lik += binomialTerm(nVelo[1], expectedVelo.nR,   m_veloREff);
  lik += binomialTerm(nVelo[0], expectedVelo.nPhi, m_veloPhiEff);

  // loop and count # with high threshold in R and phi
  unsigned int nHigh = 0;
  unsigned int nTot = 0;
  for (const LHCb::Measurement* hit : aTrack.measurements() ) {
    switch ( hit->type() ) {
      case Measurement::VeloR:
      case Measurement::VeloPhi: {
          ++nTot;
          auto veloMeas = dynamic_cast<const LHCb::VeloMeasurement*>(hit);
          assert (veloMeas!=nullptr );
          if (veloMeas->cluster()->highThreshold()) ++nHigh;
          break;
      }
      case Measurement::VeloLiteR:
      case Measurement::VeloLitePhi: {
          ++nTot;
          auto veloLiteMeas = dynamic_cast<const LHCb::VeloLiteMeasurement*>(hit);
          assert(veloLiteMeas!=nullptr);
          if (veloLiteMeas->highThreshold()) ++nHigh;
          break;
      }
      default: /* empty on purpose */ ;
    }
  }

  // add term to lik
  double prob1 = gsl_ran_binomial_pdf(nHigh, m_veloHighEff1, nTot);
  double prob2 = gsl_ran_binomial_pdf(nHigh, m_veloHighEff2, nTot);
  lik += log(std::max(prob1,prob2));

  return lik;
}

double TrackLikelihood::addTT(const LHCb::Track& aTrack) const{

  double lik = 0.0;

   // get the number of expected hits in TT
  const unsigned int nExpectedTT = m_ttExpectation->nExpected(aTrack);
  if (nExpectedTT > 2) {

    // pick up the LHCbIDs + measurements
    const std::vector<LHCb::LHCbID>& ids = aTrack.lhcbIDs();
    const unsigned int nTTHits = std::count_if(ids.begin(), ids.end(),[](const LHCb::LHCbID& id) { return id.isTT(); } );
    lik += binomialTerm(nTTHits, nExpectedTT ,m_ttEff);

    // spillover information for TT
    // loop and count # with high threshold
    unsigned int nHigh = 0;
    for (const LHCb::Measurement* hit : aTrack.measurements() ) {
      if (!hit->checkType(Measurement::TT) ) continue;
      const LHCb::STMeasurement* stMeas = dynamic_cast<const LHCb::STMeasurement*>(hit);
      if (stMeas->cluster()->highThreshold()) ++nHigh;
    }

    double spillprob = gsl_ran_binomial_pdf(nHigh, m_ttHighEff, nTTHits);
    lik += log(spillprob);
  }

  return lik;
}

double TrackLikelihood::addUT(const LHCb::Track& aTrack) const{

  double lik = 0.0;

   // get the number of expected hits in UT
  const unsigned int nExpectedUT = m_utExpectation->nExpected(aTrack);
  if (nExpectedUT > 2) {

    const std::vector<LHCb::LHCbID>& ids = aTrack.lhcbIDs();
    const unsigned int nUTHits = std::count_if(ids.begin(), ids.end(),[](const LHCb::LHCbID& id) { return id.isUT(); } );
    lik += binomialTerm(nUTHits, nExpectedUT ,m_utEff);

    // spillover information for UT
    // loop and count # with high threshold
    unsigned int nHigh = 0;
    for (const LHCb::Measurement* hit :  aTrack.measurements() ) {
      if (!hit->checkType(Measurement::UT)) continue;
      const LHCb::STMeasurement* stMeas = dynamic_cast<const LHCb::STMeasurement*>(hit);
      if (stMeas->cluster()->highThreshold()) ++nHigh;
    }

    double spillprob = gsl_ran_binomial_pdf(nHigh, m_utHighEff, nUTHits);
    lik += log(spillprob);
  }
  return lik;
}

double TrackLikelihood::addOT(const LHCb::Track& aTrack) const{

  double lik = 0.0;

  // pick up the LHCbIDs
  const std::vector<LHCb::LHCbID>& ids = aTrack.lhcbIDs();
  auto nOT = std::count_if(ids.begin(), ids.end(),
                           [](const LHCb::LHCbID& id) { return id.isOT(); } );
  if (nOT > 0u) {
    IHitExpectation::Info otInfo = m_otExpectation->expectation(aTrack);
    lik += binomialTerm(nOT,otInfo.nExpected, m_otEff);
    lik += otInfo.likelihood;
  }
  return lik;
}

double TrackLikelihood::addIT(const LHCb::Track& aTrack) const{

  double lik = 0.0;

  // pick up the LHCbIDs + measurements
  // IT
  const std::vector<LHCb::LHCbID>& ids = aTrack.lhcbIDs();
  auto nIT = std::count_if(ids.begin(), ids.end(),
               [](const LHCb::LHCbID& id) { return id.isIT(); } );
  if (nIT > 0u) {
    unsigned int nITexpec = m_itExpectation->nExpected(aTrack);
    lik += binomialTerm(nIT, nITexpec, m_itEff);

    // spillover information for IT
    // loop and count # with high threshold
    unsigned int nHigh = 0;
    for (const LHCb::Measurement* hit : aTrack.measurements() ) {
      if (!hit->checkType(Measurement::IT)) continue;
      const LHCb::STMeasurement* stMeas = dynamic_cast<const LHCb::STMeasurement*>(hit);
      if (stMeas->cluster()->highThreshold()) ++nHigh;
    }

    double spillprob = gsl_ran_binomial_pdf(nHigh, m_itHighEff, nIT);
    lik += log(spillprob);
  }
  return lik;
}
