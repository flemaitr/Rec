#ifndef PATPV_PATPV3D_H
#define PATPV_PATPV3D_H 1

// Include files
// -------------
// From Gaudi
#include "GaudiAlg/Transformer.h"
// Interfaces
#include "TrackInterfaces/IPVOfflineTool.h"
// Local
#include "Event/PrimaryVertex.h"
#include "Event/RecVertex.h"

/** @class PatPV3D PatPV3D.h
 *  Algorithm to find the primary vertices at the HLT.
 *
 *  @author Eduardo Rodrigues
 *  @author Sebastien Ponce
 */

//-----------------------------------------------------------------------------

typedef Gaudi::Functional::MultiTransformerFilter<
    std::tuple<LHCb::RecVertices,LHCb::PrimaryVertices>(const LHCb::Tracks&)
  > PatPV3DBaseClass;

class PatPV3D : public PatPV3DBaseClass {
public:
  /// Standard constructor
  PatPV3D(const std::string& name, ISvcLocator* pSvcLocator);

  /// initialize
  StatusCode initialize() override;

  /// Algorithm execution
  std::tuple<bool, LHCb::RecVertices,LHCb::PrimaryVertices> operator()(const LHCb::Tracks&) const override;

private:
  bool m_refitpv ;                      /// Flag to refit PVs when converting to type PrimaryVertex
  IPVOfflineTool* m_pvsfit = nullptr;   // PV fitting tool

};
#endif // PATPV_PATPV3D_H
