#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE test_traj

#include <boost/test/unit_test.hpp>
#include "TrackKernel/StateZTraj.h"
#include "Kernel/VectorConfiguration.h"

namespace {
  using vectype = Vec4d;
  constexpr int W = 4;
  static_assert(W > 0, " number of elements should be > 0");

  constexpr std::array<double, W> x  { -36.1555, -1.01366, 787.603, -5.59514 };
  constexpr std::array<double, W> y  { -273.555, -7.13817, -1087.02, -39.4269 };
  constexpr std::array<double, W> tx { 0.000175838, 0.000175838, 0.000179528, 0.000175838 };
  constexpr std::array<double, W> ty { -0.0128336, -0.0166972, 0.213602, -0.0165059 };
  constexpr std::array<double, W> qop { -0.116527, -0.116762, -0.111832, -0.117043 };
  constexpr std::array<double, W> z  { 2320.28, 36.919, 9403, 313.081 };

  // AOS
  std::array<LHCb::StateZTraj<double>, W> aost {{
      { x[0], y[0], tx[0], ty[0], qop[0], z[0] },
      { x[1], y[1], tx[1], ty[1], qop[1], z[1] },
      { x[2], y[2], tx[2], ty[2], qop[2], z[2] },
      { x[3], y[3], tx[3], ty[3], qop[3], z[3] }
    }};

  // SOA with VCL
  LHCb::StateZTraj<vectype> soat {
    { x[0], x[1], x[2], x[3] },
    { y[0], y[1], y[2], y[3] },
    { tx[0], tx[1], tx[2], tx[3] },
    { ty[0], ty[1], ty[2], ty[3] },
    { qop[0], qop[1], qop[2], qop[3] },
    { z[0], z[1], z[2], z[3] },
      };
}

BOOST_AUTO_TEST_CASE( CHECK_OMEGA_Y )
{
  double pz = 2103.28564;
  auto soa_oy = soat.omegay(pz);

  for( int i = 0; i < W; ++i ) {
    auto aos_oy = aost[i].omegay( pz );
    BOOST_TEST_MESSAGE(" traj " << i);
    BOOST_CHECK_MESSAGE(aos_oy == soa_oy[i], aos_oy << " == " << soa_oy[i]);
  }
}

BOOST_AUTO_TEST_CASE( CHECK_OMEGA_X )
{
  double pz = 5413.28564;
  auto soa_ox = soat.omegax(pz);

  for( int i = 0; i < W; ++i ) {
    auto aos_ox = aost[i].omegax( pz );
    BOOST_TEST_MESSAGE(" traj " << i);
    BOOST_CHECK_MESSAGE(aos_ox == soa_ox[i], aos_ox << " == " << soa_ox[i]);
  }
}

BOOST_AUTO_TEST_CASE( CHECK_ATT )
{
  for( int i = 0; i < W; ++i ) {
    BOOST_TEST_MESSAGE(" traj " << i);
    BOOST_CHECK_MESSAGE(aost[i].m_z == soat.m_z[i], aost[i].m_z << "==" << soat.m_z[i]);
    BOOST_CHECK_MESSAGE(aost[i].m_qOverP == soat.m_qOverP[i], aost[i].m_qOverP << "==" << soat.m_qOverP[i]);

    for(int k = 0; k < 3; ++k) {
      BOOST_CHECK_MESSAGE(aost[i].m_cx[k] == soat.m_cx[k][i], aost[i].m_cx[k] << "==" << soat.m_cx[k][i]);
      BOOST_CHECK_MESSAGE(aost[i].m_cy[k] == soat.m_cy[k][i], aost[i].m_cy[k] << "==" << soat.m_cy[k][i]);
    }
  }
}

BOOST_AUTO_TEST_CASE( CHECK_POSITION )
{
  double pz = 4453.28;
  auto soa_pos = soat.position(pz);

  for( int i = 0; i < W; ++i ) {
    auto aos_pos = aost[i].position(pz);
    BOOST_TEST_MESSAGE(" traj " << i);
    BOOST_CHECK_MESSAGE(aos_pos.X() == soa_pos.X()[i],          \
                        "X: " << aos_pos.X() << " == " << soa_pos.X()[i]);
    BOOST_CHECK_MESSAGE(aos_pos.Y() == soa_pos.Y()[i],          \
                        "Y: " << aos_pos.Y() << " == " << soa_pos.Y()[i]);
    BOOST_CHECK_MESSAGE(aos_pos.Z() == soa_pos.Z()[i],          \
                        "Z: " << aos_pos.Z() << " == " << soa_pos.Z()[i]);
  }
}

BOOST_AUTO_TEST_CASE( CHECK_MU_ESTIMATE )
{
  //Point pp { 2.9, 5.18, 9.2 };
  auto soa_mue = soat.muEstimate( { 2.9, 5.18, 9.2 } );

  for( int i = 0; i < W; ++i ) {
    auto aos_mue = aost[i].muEstimate( { 2.9, 5.18, 9.2 } );
    BOOST_TEST_MESSAGE(" traj " << i);
    BOOST_CHECK_MESSAGE(aos_mue == soa_mue[i], aos_mue << " == " << soa_mue[i]);
  }
}

BOOST_AUTO_TEST_CASE( CHECK_DERIVATIVE )
{
  double pz = 3564.28;
  auto soa_deriv = soat.derivative(pz);

  for( int i = 0; i < W; ++i ) {
    auto aos_deriv = aost[i].derivative(pz);
    BOOST_TEST_MESSAGE(" traj " << i);

    for(int j = 2; j <= 4; ++j) {
      BOOST_CHECK_MESSAGE(aos_deriv(0, j) == soa_deriv(0, j)[i],        \
                          "0:" << j << " " << aos_deriv(0, j) << " == " << soa_deriv(0, j)[i]);
      BOOST_CHECK_MESSAGE(aos_deriv(1, j) == soa_deriv(1, j)[i],        \
                          "1:" << j << " " << aos_deriv(1, j) << " == " << soa_deriv(1, j)[i]);
    }
  }
}
