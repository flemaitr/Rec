#ifndef _ITrackCaloMatch_H
#define _ITrackCaloMatch_H

#include "GaudiKernel/IAlgTool.h"


/** @class ITrackCaloMatch
 *
 *  interface for getting energy deposited in calos associated to track
 *  returned value is the appropriately weighted sum of ecal, hcal and preshower
 *  zero indicates no energy found
 *
 *  @author M.Needham
 *  @date   31/05/2005
 */

namespace LHCb{
 class Track;
}

struct ITrackCaloMatch: extend_interfaces<IAlgTool> {

  DeclareInterfaceID(ITrackCaloMatch, 1, 0 );

  /// the method
  virtual double energy(const LHCb::Track& aTrack) const = 0;

};

#endif
