################################################################################
# Package: TrackAssociators
################################################################################
gaudi_subdir(TrackAssociators v2r12p1)

gaudi_depends_on_subdirs(Event/DigiEvent
                         Event/LinkerEvent
                         Event/MCEvent
                         GaudiAlg
                         Kernel/LHCbKernel
                         Tr/TrackFitEvent)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(TrackAssociators
                 src/*.cpp
                 INCLUDE_DIRS Event/DigiEvent
                 LINK_LIBRARIES LinkerEvent MCEvent GaudiAlgLib LHCbKernel TrackFitEvent)

