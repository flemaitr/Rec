// track interfaces
#include "Event/Track.h"

#include "TrackEraseExtraInfo.h"


using namespace LHCb;

DECLARE_COMPONENT( TrackEraseExtraInfo )
//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackEraseExtraInfo::TrackEraseExtraInfo(const std::string& name,
					 ISvcLocator* pSvcLocator):
  GaudiAlgorithm(name, pSvcLocator)
{
  declareProperty("InputLocation",  m_inputLocation  = TrackLocation::Default);
  declareProperty("ErasableInfo",   m_erasableInfo   = { LHCb::Track::AdditionalInfo::PatQuality,
                                                         LHCb::Track::AdditionalInfo::Cand1stQPat,
                                                         LHCb::Track::AdditionalInfo::Cand2ndQPat,
                                                         LHCb::Track::AdditionalInfo::Cand1stChi2Mat,
                                                         LHCb::Track::AdditionalInfo::Cand2ndChi2Mat,
                                                         LHCb::Track::AdditionalInfo::MatchChi2,
                                                         LHCb::Track::AdditionalInfo::TsaLikelihood,
                                                         LHCb::Track::AdditionalInfo::nPRVeloRZExpect,
                                                         LHCb::Track::AdditionalInfo::nPRVelo3DExpect } );
  declareProperty("PrintExtraInfo", m_printExtraInfo = false);
}
//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackEraseExtraInfo::initialize() {

  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( UNLIKELY(msgLevel(MSG::DEBUG)) ) debug() << "==> initialize" << endmsg;

  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode TrackEraseExtraInfo::execute(){

  Tracks* inCont = getIfExists<Tracks>(m_inputLocation);
  if( inCont == nullptr){
    return Warning( "Input container "+m_inputLocation+" does not exist", StatusCode::SUCCESS, 20 );
  }

  // -- Print extra info which is on track
  if( UNLIKELY( m_printExtraInfo )){
    for( LHCb::Track* track : *inCont){
      info() << "ExtraInfo for track: " << track->type() << " : " << track->key() << endmsg;
      const LHCb::Track::ExtraInfo extraInfo = track->extraInfo();
      for ( const auto& ei : extraInfo ) {
        const LHCb::Track::AdditionalInfo addInfo =
          static_cast<LHCb::Track::AdditionalInfo>(ei.first);
        info() << " " << addInfo << "=" << ei.second << endmsg;
      }
    }
  }

  for (auto& track : *inCont) {
    for(int i : m_erasableInfo) track->eraseInfo( i );
  }

  return StatusCode::SUCCESS;
}
