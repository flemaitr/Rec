/** @file TrackBestTrackCreator.cpp
 *
 * @author Sebastien Ponce
 *
 * @date 2016-09-20
 *
 * This code was extracted from TrackBestTrackCreator while converted to the
 * functionnal approach. Originally TrackBestTrackCreator had a boolean property
 * named SplitByType and triggering the split if set. After the conversion
 * the splitting has been separated into this algorithm that should be used
 * right after TrackBestTrackCreator is splitting should be done
 */

// Include files
// -------------
// from STD
#include <algorithm>
#include <vector>
#include <unordered_map>

// TrackEvent
#include "Event/Track.h"

// from Gaudi
#include "GaudiAlg/SplittingTransformer.h"
#include "GaudiKernel/Property.h"

/** @brief splits tracks by type
 *
 * @author Sebastien Ponce
 *
 * @date 2016-09-20
 *
 * This code was extracted from TrackBestTrackCreator while converted to the
 * functionnal approach. Originally TrackBestTrackCreator had a boolean property
 * named SplitByType and triggering the split if set. After the conversion
 * the splitting has been separated into this algorithm that should be used
 * right after TrackBestTrackCreator is splitting should be done
 */
class TrackBestTrackSplitter : public Gaudi::Functional::SplittingTransformer<
  std::vector<LHCb::Tracks>(const LHCb::Tracks&)> {

public:
  /// Standard constructor
  TrackBestTrackSplitter(const std::string& name, ISvcLocator* pSvcLocator);

  StatusCode initialize() override;

  std::vector<LHCb::Tracks> operator()(const LHCb::Tracks&) const override;

private:
  // output Track container paths, by type
  std::vector<std::string> m_tracksOutContainerTypes;
  std::vector<int> m_supportedTrackTypes;
};

//-----------------------------------------------------------------------------
// Implementation file for class : TrackBestTrackSplitter
//
// 2016-09-20 Sebastien Ponce
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( TrackBestTrackSplitter )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackBestTrackSplitter::TrackBestTrackSplitter( const std::string& name,
                                              ISvcLocator* pSvcLocator) :
SplittingTransformer(name, pSvcLocator,
                     KeyValue{"TracksInContainerLoc", LHCb::TrackLocation::Default},
                     KeyValues{"TracksOutContainerLocs", {""}}), // fail if not filled in config
  m_supportedTrackTypes({1, 3, 4, 5, 6})
{
  declareProperty("TracksOutContainerTypes", m_tracksOutContainerTypes);
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackBestTrackSplitter::initialize() {
  if (m_tracksOutContainerTypes.size() != outputLocationSize()) {
    return Warning("Failed to initialize TrackBestTrackSplitter as TracksOutContainerLocs and TracksOutContainerTypes properties are inconsistent (different length)",
                   StatusCode::FAILURE);
  }

  unsigned int n = 0;
  for (auto itT = m_tracksOutContainerTypes.begin();
       itT != m_tracksOutContainerTypes.end();
       n++, itT++) {
    if (msgLevel(MSG::DEBUG)) {
      debug() << "Splitting types up into:" << endmsg;
      debug() << "  " << *itT << " -> " << outputLocation(n) << endmsg;
    }
    if (std::none_of(m_supportedTrackTypes.begin(),m_supportedTrackTypes.end(),
                     [itT](int i){return *itT==LHCb::Track::TypesToString(i);})){
      warning() << *itT << " does not correspond to a supported track type."
                << endmsg;
    }
  }
  // Print out the user-defined settings
  // -----------------------------------
  if (msgLevel(MSG::DEBUG)) {
    std::string outLocString;
    for(n = 0; n < outputLocationSize(); n++) {
      outLocString += outputLocation(n);
      if(n != outputLocationSize()-1) {
        outLocString += ", ";
      }
    }
    debug() << endmsg
            << "============ TrackBestTrackSplitter Settings ===========" << endmsg
            << "TracksInContainerLoc : " << getProperty( "TracksInContainerLoc").toString() << endmsg
            << "TrackOutContainerLocs  : " << outLocString << endmsg
            << "TrackOutContainerTypes  : " << getProperty( "TracksOutContainerTypes").toString() << endmsg
            << "=======================================================" << endmsg
            << endmsg;
  }

  return StatusCode::SUCCESS;
}


//=============================================================================
// operator()
//=============================================================================
std::vector<LHCb::Tracks>
TrackBestTrackSplitter::operator()(const LHCb::Tracks& ins) const {
  /// build reverse mapping of types to position in output vector
  std::unordered_map<int, int> type2index;
  int index = 0;
  for (auto it : m_tracksOutContainerTypes) {
    auto type = LHCb::Track::TypesToType(it);
    if (type != LHCb::Track::TypeUnknown) {
      type2index[type] = index;
    }
    index++;
  }
  // split tracks based on time
  std::vector<LHCb::Tracks> result;
  result.resize(m_tracksOutContainerTypes.size());
  counter("# executions")++;
  for (auto& tr: ins) {
    if (std::find_if(m_tracksOutContainerTypes.begin(),
                     m_tracksOutContainerTypes.end(),
                     [tr](std::string stype){
                       return tr->type() == LHCb::Track::TypesToType(stype);
                     }) != m_tracksOutContainerTypes.end()) {
      result[type2index[tr->type()]].add(tr);
      counter(std::string("# ") + LHCb::Track::TypesToString(tr->type())
            + " tracks split off")++;
    } else {
      if (msgLevel(MSG::DEBUG)) {
        debug() << "No outputlocation for track type "<< tr->type()<<" was specified!" << endmsg;
      }
    }
  }
  return result;
}
