/** @class TTrackFromLong TTrackFromLong.h
 *
 *  Fake a T seed from a long track
 *
 *  @author M.Needham
 *  @date   30/05/2006
 */

#include <string>
#include "Event/Track.h"
#include "GaudiKernel/SystemOfUnits.h"
using namespace Gaudi::Units;
#include "GaudiAlg/ScalarTransformer.h"

namespace {
  auto isT = [](const LHCb::LHCbID& id) { return id.isOT() || id.isIT(); };
}

struct TTrackFromLong : Gaudi::Functional::ScalarTransformer< TTrackFromLong, LHCb::Tracks(const LHCb::Tracks&) >
{
  TTrackFromLong(const std::string& name, ISvcLocator* pSvcLocator):
    ScalarTransformer( name, pSvcLocator,
                       { KeyValue( "inputLocation", LHCb::TrackLocation::Forward) },
                       KeyValue( "outputLocation", LHCb::TrackLocation::Seed)  )
  { }

  using ScalarTransformer::operator();
  boost::optional<LHCb::Track> operator()(const LHCb::Track& trk) const // Note: this is NOT a virtual function!!
  {
     auto count = std::count_if( begin(trk.lhcbIDs()), end(trk.lhcbIDs()), isT );
     if (count<5) return boost::none;

     LHCb::Track seed;
     seed.setType( LHCb::Track::Types::Ttrack );
     seed.setHistory( LHCb::Track::History::PatSeeding);
     const auto& lastState = trk.closestState(9000.*mm);
     LHCb::State tState;
     tState.setLocation( LHCb::State::AtT );
     tState.setState(lastState.stateVector());
     tState.setZ(lastState.z());
     tState.setCovariance(lastState.covariance());
     seed.addToStates(tState);
     seed.setPatRecStatus(LHCb::Track::PatRecStatus::PatRecIDs);
     for (const auto& id : trk.lhcbIDs() ) {
       if (isT(id)) seed.addToLhcbIDs(id);
     }
     return seed;
  };
};

DECLARE_COMPONENT( TTrackFromLong )
