
#include <memory>

void RichCorrelations()
{

  auto c = std::make_unique<TCanvas>( "RichCor", "RichCor", 1400, 1000 );

  TTree tree;
  
  tree.AddFriend("hlt2 = ChargedProtoTuple/protoPtuple",
                 "/usera/jonesc/LHCbCMake/Brunel/output/future/Quartic100R1R2-AllHypos/protoparticles.tuples.root");

  //tree.AddFriend("hlt1 = ChargedProtoTuple/protoPtuple", "/usera/jonesc/LHCbCMake/Brunel/output/future/FastQuartic50R1R2-AllHypos-NoUnAmBigPhot-NoBeamPipeCheck-1It/protoparticles.tuples.root");
  //tree.AddFriend("hlt1 = ChargedProtoTuple/protoPtuple", "/usera/jonesc/LHCbCMake/Brunel/output/future/CKEsti50R1R2-KaPi/protoparticles.tuples.root");
  tree.AddFriend("hlt1 = ChargedProtoTuple/protoPtuple", "/usera/jonesc/LHCbCMake/Brunel/output/future/CKEsti50R1R2-AllHypos/protoparticles.tuples.root");

  tree.Draw("hlt2.TrackP:hlt1.TrackP","hlt2.TrackP<100000","zcol");
  c->SaveAs( "P-Correlations.pdf" );

  auto sel = TCut("hlt2.RichAbovePiThres>0 && hlt1.RichAbovePiThres>0 && hlt2.TrackP<100000 && hlt2.TrackP>3000");

  auto trueK = TCut("abs(hlt2.MCParticleType) == 321");

  auto truePi = TCut("abs(hlt2.MCParticleType) == 211");

  tree.Draw( "hlt2.RichDLLk:hlt1.RichDLLk",sel && trueK,"zcol");
  c->SaveAs( "TrueK-RichDLLk-2dCorrelations.pdf" );

  tree.Draw( "hlt2.RichDLLk-hlt1.RichDLLk",sel && trueK );
  c->SaveAs( "TrueK-RichDLLk-1dCorrelations.pdf" );

  tree.Draw( "hlt1.RichDLLk", sel );
  c->SaveAs( "All-RichDLLk-HLT1.pdf" );

  tree.Draw( "hlt2.RichDLLk", sel );
  c->SaveAs( "All-RichDLLk-HLT2.pdf" );

  tree.Draw( "hlt1.RichDLLk", sel && trueK );
  c->SaveAs( "TrueK-RichDLLk-HLT1.pdf" );

  tree.Draw( "hlt2.RichDLLk", sel && trueK );
  c->SaveAs( "TrueK-RichDLLk-HLT2.pdf" );

  tree.Draw( "hlt1.RichDLLk", sel && truePi );
  c->SaveAs( "TruePi-RichDLLk-HLT1.pdf" );

  tree.Draw( "hlt2.RichDLLk", sel && truePi );
  c->SaveAs( "TruePi-RichDLLk-HLT2.pdf" );

  tree.Draw( "hlt1.RichDLLk", sel && trueK && TCut("hlt2.RichDLLk>-2") );
  c->SaveAs( "TrueK-RichDLLk-HLT1-HLT2gt-2.pdf" );

  tree.Draw( "hlt1.RichDLLk", sel && trueK && TCut("hlt2.RichDLLk>0") );
  c->SaveAs( "TrueK-RichDLLk-HLT1-HLT2gt0.pdf" );

  tree.Draw( "hlt1.RichDLLk", sel && trueK && TCut("hlt2.RichDLLk>2") );
  c->SaveAs( "TrueK-RichDLLk-HLT1-HLT2gt2.pdf" );

  tree.Draw( "hlt1.RichDLLk", sel && trueK && TCut("hlt2.RichDLLk>4") );
  c->SaveAs( "TrueK-RichDLLk-HLT1-HLT2gt4.pdf" );

  tree.Draw( "hlt1.RichDLLk", sel && trueK && TCut("hlt2.RichDLLk>10") );
  c->SaveAs( "TrueK-RichDLLk-HLT1-HLT2gt10.pdf" );

}
