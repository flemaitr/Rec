#include "Event/LumiCounters.h"

#include "DumpLumiEvents.h"

#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/bzip2.hpp>
#include <sstream>

DECLARE_COMPONENT( DumpLumiEvents )
namespace {
inline bool hasEnding(const std::string& s, const std::string& ending) {
    return (s.length() >= ending.length()) &&
           (0 == s.compare(s.size() - ending.size(), ending.size(), ending));
}

inline double round(double x, double factor) {
  return int(x * factor) / factor;
}

}


std::ostream& DumpLumiEvents::outputOdin(std::ostream& s, const LHCb::ODIN& odin) const {
  return s << odin.version() << ","
           << odin.runNumber() << ","
           << odin.calibrationStep() << ","
           << odin.eventType() << ","
           << odin.orbitNumber() << ","
           << odin.eventNumber() << ","
           << odin.gpsTime() << ","
           << odin.errorBits() << ","
           << odin.detectorStatus() << ","
           << odin.bunchCurrent() << ","
           << (int) (odin.bunchCrossingType()) << ","
           << odin.forceBit() << ","
           << (int) (odin.calibrationType()) << ","
           << (int) (odin.triggerType()) << ","
           << odin.timeAlignmentEventWindow() << ","
           << odin.bunchId() << ","
           << ";";
}

std::ostream& DumpLumiEvents::outputLumiSummary(std::ostream& s, const LHCb::HltLumiSummary& hltLumiSummary) const {
  for (const auto& si : hltLumiSummary.extraInfo() ) s << si.first << "," << si.second << ",";
  return s << ";";
}

std::ostream& DumpLumiEvents::outputVertices(std::ostream& s, const LHCb::RecVertices& recVertices) const {
  for (const auto& vx : recVertices ) {
      s << round(vx->position().x(), 1e5) << ","
        << round(vx->position().y(), 1e5) << ","
        << round(vx->position().z(), 1e5) << ","
        << vx->tracks().size() << ","
        << round(vx->chi2(), 1e2) << ',';
  }
  return s << ";";
}

StatusCode DumpLumiEvents::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if (sc.isFailure())
    return sc; // error printed already by GaudiAlgorithm
  if (msgLevel(MSG::DEBUG))
    debug() << "==> Initialize" << endmsg;

  auto compressed = hasEnding(m_outputFileName, ".bz2");

  // Open output file
  std::ios::openmode mode = std::ios::out | std::ios::trunc;
  if (compressed) mode |= std::ios::binary;
  m_outputFile.open(m_outputFileName.value(), mode);
  if (m_outputFile.fail())
    return Error("Could not open output file", StatusCode::FAILURE);

  // Create filtering stream with or without compression
  boost::iostreams::filtering_ostream* fs = new boost::iostreams::filtering_ostream();
  if (compressed) fs->push(boost::iostreams::bzip2_compressor());
  fs->push(m_outputFile);
  m_outputStream =  fs;

  // Setup intermediate stream
  if (m_sort) {
    if (m_intermediateStreamFileName.empty())
      m_intermediateStream = new std::stringstream();
    else {
      m_intermediateStream = new std::fstream(m_intermediateStreamFileName,
          std::fstream::in | std::fstream::out | std::fstream::trunc);
      if (m_intermediateStream->fail())
        return Error("Could not open file for the intermediate stream.", StatusCode::FAILURE);
    }

    m_keyLocationPairs.reserve(m_nEventsHint);
    info() << "Reserved capacity for " << m_nEventsHint.value() << " elements in key-location pairs vector." << endmsg;
  } else {
    m_intermediateStream = m_outputStream; //FIXME: TODO: KABOOM! both m_intermediateStream and m_outputStream will get deleted in finalize....
  }

  return StatusCode::SUCCESS;
}

StatusCode DumpLumiEvents::execute() {
  if (msgLevel(MSG::DEBUG))
    debug() << "==> Execute" << endmsg;



  if (!exist<LHCb::ODIN>(LHCb::ODINLocation::Default))
    return Error("ODIN cannot be loaded", StatusCode::FAILURE);
  if (!exist<LHCb::HltLumiSummary>(LHCb::HltLumiSummaryLocation::Default))
    return Error("LumiSummary cannot be loaded", StatusCode::FAILURE);
  auto dumpVertices = !m_recVerticesLocation.empty();
  if (dumpVertices && !exist<LHCb::RecVertices>(m_recVerticesLocation))
    return Error("RecVertices cannot be loaded", StatusCode::FAILURE);

  LHCb::ODIN* odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
  LHCb::HltLumiSummary* hltLumiSummary = get<LHCb::HltLumiSummary>(LHCb::HltLumiSummaryLocation::Default);
  LHCb::RecVertices* recVertices = dumpVertices?get<LHCb::RecVertices>(m_recVerticesLocation):nullptr;

  // interested only in events with random counter different from 0
  unsigned int random = hltLumiSummary->info(LHCb::LumiCounters::Random, 0);
  if (random != 0) {
    if (LIKELY(m_sort))
      m_keyLocationPairs.emplace_back(odin->gpsTime(), m_intermediateStream->tellp());

    outputOdin(*m_intermediateStream, *odin);
    outputLumiSummary(*m_intermediateStream, *hltLumiSummary);
    if (dumpVertices) outputVertices(*m_intermediateStream, *recVertices);
    (*m_intermediateStream) << '\n';

    if (UNLIKELY(msgLevel(MSG::DEBUG))) {
      std::ostringstream oss;
      outputOdin(oss, *odin);
      outputLumiSummary(oss, *hltLumiSummary);
      if (dumpVertices) outputVertices(oss, *recVertices);
      debug() << oss.str() << endmsg;
    }
  }

  setFilterPassed(random != 0);
  return StatusCode::SUCCESS;
}

StatusCode DumpLumiEvents::finalize() {
  if (msgLevel(MSG::DEBUG))
    debug() << "==> Finalize" << endmsg;

  if (m_sort) {
    std::sort(m_keyLocationPairs.begin(), m_keyLocationPairs.end() );

    std::istream* is = dynamic_cast<std::iostream*>(m_intermediateStream);

    for (const auto& klp :  m_keyLocationPairs) {
      is->seekg(klp.location);
      is->get(*(m_outputStream->rdbuf()));
      m_outputStream->put('\n');
      if (msgLevel(MSG::DEBUG)) {
        is->seekg(klp.location);
        is->get(*(debug().stream().rdbuf()));
        debug() << endmsg;
      }
    }

    delete m_intermediateStream;
  }

  delete m_outputStream; // closes m_outputFile too

  return GaudiAlgorithm::finalize(); // must be called after all other actions
}
