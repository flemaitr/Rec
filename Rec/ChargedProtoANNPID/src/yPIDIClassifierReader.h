#ifndef yPIDIClassifierReader__def
#define yPIDIClassifierReader__def
  /** @class yPIDIClassifierReader yPIDImpFactory.h
   *
   *  Interface class for all yPID solutions
   */



  class yPIDIClassifierReader
  {
  public:
    /// Destructor
    virtual ~yPIDIClassifierReader() = default;
    /// return classifier response
    virtual double GetMvaValue( const std::vector<double>& inputValues ) const = 0;
    /// returns classifier status
    bool IsStatusClean() const { return fStatusIsClean; }
  protected:
    bool fStatusIsClean{true}; ///< Status flag
  };
#endif
