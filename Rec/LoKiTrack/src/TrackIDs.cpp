// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <algorithm>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/ToStream.h"
#include "Kernel/STLExtensions.h"
// ============================================================================
// local
// ============================================================================
#include "LoKi/TrackIDs.h"
// ============================================================================
// ============================================================================
/** @file
 *  Implementation file for class : TrackIDs
 *  Collection of functors that deals with LHCbIDs for Tracks
 *  (on request from Wouter Hulsbergen)
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date   2009-12-17
 */
// ============================================================================
namespace
{
  static_assert( std::numeric_limits<unsigned int>::is_integer    &&
                 std::numeric_limits<unsigned int>::is_specialized,
                 "" ) ;


  static const auto s_countIDs_tbl = LHCb::make_array(
                  std::make_pair( &LHCb::LHCbID::isVelo,       "isVelo"       ),
                  std::make_pair( &LHCb::LHCbID::isVeloR,      "isVeloR"      ),
                  std::make_pair( &LHCb::LHCbID::isVeloPhi,    "isVeloPhi"    ),
                  std::make_pair( &LHCb::LHCbID::isVeloPileUp, "isVeloPileUp" ),
                  std::make_pair( &LHCb::LHCbID::isVP,         "isVP"         ),
                  std::make_pair( &LHCb::LHCbID::isTT,         "isTT"         ),
                  std::make_pair( &LHCb::LHCbID::isIT,         "isIT"         ),
                  std::make_pair( &LHCb::LHCbID::isST,         "isST"         ),
                  std::make_pair( &LHCb::LHCbID::isOT,         "isOT"         ),
                  std::make_pair( &LHCb::LHCbID::isRich,       "isRich"       ),
                  std::make_pair( &LHCb::LHCbID::isCalo,       "isCalo"       ),
                  std::make_pair( &LHCb::LHCbID::isMuon,       "isMuon"       ),
                  // Backwards compatability
                  std::make_pair( &LHCb::LHCbID::isVP,         "isVeloPix"    )
             );
  static const auto s_countOTIDs_tbl = LHCb::make_array(
                  std::make_pair( &LHCb::OTChannelID::geometry,                "geometry"                    ),
                  std::make_pair( &LHCb::OTChannelID::sequentialUniqueLayer,   "sequentialUniqueLayer"       ),
                  std::make_pair( &LHCb::OTChannelID::sequentialUniqueQuarter, "sequentialUniqueQuarter"     ),
                  std::make_pair( &LHCb::OTChannelID::sequentialUniqueModule,  "sequentialUniqueModule"      ),
                  std::make_pair( &LHCb::OTChannelID::sequentialUniqueOtis,    "sequentialUniqueOtis"        ),
                  std::make_pair( &LHCb::OTChannelID::sequentialUniqueStraw,   "sequentialUniqueStraw"       ),
                  std::make_pair( &LHCb::OTChannelID::channelID,               "channelID"                   ),
                  std::make_pair( &LHCb::OTChannelID::channelID,               "channel"                     ),
                  std::make_pair( &LHCb::OTChannelID::tdcTime,                 "tdcTime"                     ),
                  std::make_pair( &LHCb::OTChannelID::module,                  "module"                      ),
                  std::make_pair( &LHCb::OTChannelID::quarter,                 "quarter"                     ),
                  std::make_pair( &LHCb::OTChannelID::layer,                   "layer"                       ),
                  std::make_pair( &LHCb::OTChannelID::straw,                   "straw"                       ),
                  std::make_pair( &LHCb::OTChannelID::station,                 "station"                     ),
                  std::make_pair( &LHCb::OTChannelID::uniqueModule,            "uniqueModule"                ),
                  std::make_pair( &LHCb::OTChannelID::uniqueQuarter,           "uniqueQuarter"               ),
                  std::make_pair( &LHCb::OTChannelID::uniqueLayer,             "uniqueLayer"                 ),
                  std::make_pair( &LHCb::OTChannelID::uniqueStraw,             "uniqueStraw"                 ) );

  static const auto s_countSTIDs_tbl = LHCb::make_array(
                  std::make_pair( &LHCb::STChannelID::strip,           "strip"            ),
                  std::make_pair( &LHCb::STChannelID::sector,          "sector"           ),
                  std::make_pair( &LHCb::STChannelID::detRegion,       "detRegion"        ),
                  std::make_pair( &LHCb::STChannelID::detRegion,       "region"           ),
                  std::make_pair( &LHCb::STChannelID::layer,           "layer"            ),
                  std::make_pair( &LHCb::STChannelID::station,         "station"          ),
                  std::make_pair( &LHCb::STChannelID::uniqueLayer,     "uniqueLayer"      ),
                  std::make_pair( &LHCb::STChannelID::uniqueDetRegion, "uniqueDetRegion"  ),
                  std::make_pair( &LHCb::STChannelID::uniqueDetRegion, "uniqueRegion"     ),
                  std::make_pair( &LHCb::STChannelID::uniqueSector,    "uniqueSector"     ),
                  std::make_pair( &LHCb::STChannelID::type,            "type"             ),
                  std::make_pair( &LHCb::STChannelID::channelID,       "channelID"        ),
                  std::make_pair( &LHCb::STChannelID::channelID,       "channel"          ) );

  static const auto s_countVeloIDs_tbl2 = LHCb::make_array(
                  std::make_pair( &LHCb::VeloChannelID::channelID,  "channelID" ),
                  std::make_pair( &LHCb::VeloChannelID::channelID,  "channel" ),
                  std::make_pair( &LHCb::VeloChannelID::strip,      "strip"     ),
                  std::make_pair( &LHCb::VeloChannelID::sensor,     "sensor"    ) );

  static const auto s_countVeloIDs_tbl1 = LHCb::make_array(
                  std::make_pair( &LHCb::VeloChannelID::isPileUp,   "isPileUp" ),
                  std::make_pair( &LHCb::VeloChannelID::isRType,    "isRType"  ),
                  std::make_pair( &LHCb::VeloChannelID::isPhiType,  "isPhiType") );

  static const auto s_countVeloIDs_tbl3 = LHCb::make_array(
                  std::make_pair( "isPileUp",  &LHCb::VeloChannelID::isPileUp  ),
                  std::make_pair( "pileUp",    &LHCb::VeloChannelID::isPileUp  ),
                  std::make_pair( "PileUp",    &LHCb::VeloChannelID::isPileUp  ),
                  std::make_pair( "isRType",   &LHCb::VeloChannelID::isRType   ),
                  std::make_pair( "RType",     &LHCb::VeloChannelID::isRType   ),
                  std::make_pair( "rType",     &LHCb::VeloChannelID::isRType   ),
                  std::make_pair( "R",         &LHCb::VeloChannelID::isRType   ),
                  std::make_pair( "r",         &LHCb::VeloChannelID::isRType   ),
                  std::make_pair( "isPhiType", &LHCb::VeloChannelID::isPhiType ),
                  std::make_pair( "PhiType",   &LHCb::VeloChannelID::isPhiType ),
                  std::make_pair( "phiType",   &LHCb::VeloChannelID::isPhiType ),
                  std::make_pair( "Phi",       &LHCb::VeloChannelID::isPhiType ),
                  std::make_pair( "phi",       &LHCb::VeloChannelID::isPhiType ) );

}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountIDs::CountIDs ( LoKi::Track::CountIDs::PMF pmf )
: m_pmf  ( pmf  )
{
  auto i = std::find_if( s_countIDs_tbl.begin(), s_countIDs_tbl.end(),
                         [&](const auto& p) { return p.first==pmf; } );
  if ( i != s_countIDs_tbl.end() ) m_nick = i->second;

  Assert ( 0 != m_pmf , "Invalid LHCb::LHCbID member function!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountIDs::CountIDs
( const std::string& nick )
  : LoKi::AuxFunBase ( std::tie ( nick ) )
  , m_nick ( nick )
{
  auto i = std::find_if( s_countIDs_tbl.begin(), s_countIDs_tbl.end(),
                         [&](const auto& p) { return p.second==m_nick; } );
  if ( i != s_countIDs_tbl.end() ) m_pmf = i->first;
  if ( !m_pmf ) Exception ( "Invalid LHCb::LHCbID member function: '" + m_nick + "'" ) ;

}
// ============================================================================
// MANDATORY: clone method ("virtual consttructor")
// ============================================================================
LoKi::Track::CountIDs*
LoKi::Track::CountIDs::clone() const
{ return  new LoKi::Track::CountIDs ( *this ) ; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::CountIDs::result_type
LoKi::Track::CountIDs::operator() ( LoKi::Track::CountIDs::argument t ) const
{
  //
  if ( UNLIKELY( !t ) ) {
    Error ("LHCb::Track* points to NULL, return -1") ;
    return -1 ;
  }
  //
  const auto&  lhcbids = t->lhcbIDs() ;
  //
  return std::count_if ( lhcbids.begin() , lhcbids.end() ,
                         [&](const LHCb::LHCbID& id) { return (id.*m_pmf)(); } );
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Track::CountIDs::fillStream( std::ostream& s ) const
{ return s << " TrIDC( '" << m_nick << "' ) " ; }

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountOTIDs::CountOTIDs ( LoKi::Track::CountOTIDs::PMF pmf ,
                                      const unsigned int            i   )
: m_pmf   ( pmf   )
, m_uints ( 1 , i )
{
  setNick ( m_pmf ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::OTchannelID member function!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountOTIDs::CountOTIDs ( LoKi::Track::CountOTIDs::PMF     pmf ,
                                      const std::vector<unsigned int>& i   )
: m_pmf   ( pmf   )
, m_uints ( i     )
{
  setNick ( m_pmf ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::OTchannelID member function!" ) ;
  Assert ( !m_uints.empty() , "Empty vector of values is specified!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountOTIDs::CountOTIDs ( const unsigned int           imin ,
                                      LoKi::Track::CountOTIDs::PMF pmf  ,
                                      const unsigned int           imax )
: m_pmf   ( pmf   )
, m_imin  ( imin )
, m_imax  ( imax )
{
  setNick ( m_pmf ) ;
  Assert ( 0 != m_pmf    , "Invalid LHCb::OTChannelID member function!" ) ;
  Assert ( imin <= imax  , "Invalid range of values" ) ;
}
// ============================================================================
// set nick
// ============================================================================
void LoKi::Track::CountOTIDs::setNick ( LoKi::Track::CountOTIDs::PMF pmf )
{
  auto i = std::find_if( s_countOTIDs_tbl.begin(), s_countOTIDs_tbl.end(),
                         [&](const auto& p) { return p.first == pmf; } );
  if ( i !=  s_countOTIDs_tbl.end() ) m_nick = i->second;
  else Exception ( "Invalid LHCb::OTChannelID member function!") ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountOTIDs::CountOTIDs ( const std::string&               nick ,
                                      const std::vector<unsigned int>& i    )
: LoKi::AuxFunBase ( std::tie ( nick , i ) )
, m_uints ( i     )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::OTChannelID member function: '" + m_nick + "'" ) ;
  Assert ( !m_uints.empty() , "Empty vector of values is specified!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountOTIDs::CountOTIDs ( const std::string&  nick ,
                                      const unsigned int  i    )
: LoKi::AuxFunBase ( std::tie ( nick , i ) )
, m_uints ( 1 , i )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::OTChannelID member function: '" + m_nick + "'" ) ;
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountOTIDs::CountOTIDs ( const unsigned int  imin ,
                                      const std::string&  nick ,
                                      const unsigned int  imax )
: LoKi::AuxFunBase ( std::tie ( imin , nick , imax  ) )
, m_imin  ( imin  )
, m_imax  ( imax  )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf   , "Invalid LHCb::OTChannelID member function: '" + m_nick + "'" ) ;
  Assert ( imin <= imax  , "Invalid range of values" ) ;
}
// ============================================================================
// set PMF properly
// ============================================================================
void LoKi::Track::CountOTIDs::setPmf ( const std::string& nick )
{
  auto i = std::find_if(s_countOTIDs_tbl.begin(), s_countOTIDs_tbl.end(),
                        [&](const auto& p) { return p.second == nick; } );
  if ( i!=s_countOTIDs_tbl.end() ) m_pmf = i->first;
  else Exception ( "Invalid LHCb::OTChannelID member function '" + nick + "'") ;
}
// ============================================================================
// MANDATORY: clone method ('virtual constructor')
// ============================================================================
LoKi::Track::CountOTIDs*
LoKi::Track::CountOTIDs::clone() const
{ return new LoKi::Track::CountOTIDs ( *this ) ; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::CountOTIDs::result_type
LoKi::Track::CountOTIDs::operator()( LoKi::Track::CountOTIDs::argument t ) const
{
  //
  if ( UNLIKELY(!t) ) {
    Error ("LHCb::Track* points to NULL, return -1") ;
    return -1 ;
  }
  //
  const auto& lhcbids = t->lhcbIDs() ;
  //
  size_t res = 0 ;
  //
  for ( auto id = lhcbids.begin() ; lhcbids.end() != id ; ++id )
  {
    if ( ! id->isOT() ) { continue ; }
    //
    const unsigned int r = (id->otID().*m_pmf) () ;
    if      ( m_uints.empty()     ) { if ( m_imin <= r && r <= m_imax ) { ++res ; } }
    else if ( 1 == m_uints.size() ) { if ( m_uints.front() == r       ) { ++res ; } }
    else {
      if  ( m_uints.end() != std::find ( m_uints.begin() , m_uints.end() , r ) ) { ++res ; }
    }
  }
  //
  return res;
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Track::CountOTIDs::fillStream( std::ostream& s ) const
{
  s << " TrOTIDC( " ;
  //
  if      ( m_uints.empty()     ) { s << m_imin << " ,'" << m_nick << "', "<< m_imax ; }
  else if ( 1 == m_uints.size() ) { s << "'" << m_nick << "', " << m_uints.front()     ; }
  else {
    s << "'" << m_nick << "', " ;
    Gaudi::Utils::toStream ( m_uints , s ) ;
  }
  //
  return s << " ) " ;
}



// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountSTIDs::CountSTIDs ( LoKi::Track::CountSTIDs::PMF pmf ,
                                      const unsigned int            i   )
: m_pmf   ( pmf   )
, m_uints ( 1 , i )
{
  setNick ( m_pmf ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::STChannelID member function!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountSTIDs::CountSTIDs ( LoKi::Track::CountSTIDs::PMF    pmf ,
                                      const std::vector<unsigned int>& i   )
: m_pmf   ( pmf   )
, m_uints ( i     )
{
  setNick ( m_pmf ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::STChannelID member function!" ) ;
  Assert ( !m_uints.empty() , "Empty vector of values is specified!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountSTIDs::CountSTIDs ( const unsigned int            imin ,
                                      LoKi::Track::CountSTIDs::PMF pmf  ,
                                      const unsigned int            imax )
: m_pmf   ( pmf   )
, m_imin  ( imin )
, m_imax  ( imax )
{
  setNick ( m_pmf ) ;
  Assert ( 0 != m_pmf    , "Invalid LHCb::STChannelID member function!" ) ;
  Assert ( imin <= imax  , "Invalid range of values" ) ;
}
// ============================================================================
// set nick
// ============================================================================
void LoKi::Track::CountSTIDs::setNick ( LoKi::Track::CountSTIDs::PMF pmf )
{
  auto i = std::find_if( s_countSTIDs_tbl.begin(), s_countSTIDs_tbl.end(),
                         [&](const auto& p) { return p.first == pmf; } );
  if ( i != s_countSTIDs_tbl.end() ) m_nick = i->second ;
  else Exception ( "Invalid LHCb::STChannelID member function!") ;
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountSTIDs::CountSTIDs ( const std::string&               nick ,
                                      const std::vector<unsigned int>& i    )
: LoKi::AuxFunBase ( std::tie ( nick , i  ) )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::STChannelID member function: '" + m_nick + "'" ) ;
  Assert ( !m_uints.empty() , "Empty vector of values is specified!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountSTIDs::CountSTIDs ( const std::string&  nick ,
                                      const unsigned int  i    )
: LoKi::AuxFunBase ( std::tie ( nick , i  ) )
, m_uints ( 1 , i )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf , "Invalid LHCb::STChannelID member function: '" + m_nick + "'" ) ;
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountSTIDs::CountSTIDs ( const unsigned int  imin ,
                                      const std::string&  nick ,
                                      const unsigned int  imax )
: LoKi::AuxFunBase ( std::tie ( imin, nick , imax ) )
, m_imin  ( imin  )
, m_imax  ( imax  )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf   , "Invalid LHCb::OTChannelID member function: '" + m_nick + "'" ) ;
  Assert ( imin <= imax  , "Invalid range of values" ) ;
}
// ============================================================================
// set PMF properly
// ============================================================================
void LoKi::Track::CountSTIDs::setPmf ( const std::string& nick )
{
  auto i = std::find_if( s_countSTIDs_tbl.begin(), s_countSTIDs_tbl.end(),
                         [&](const auto& p) { return p.second == nick; } );
  if ( i != s_countSTIDs_tbl.end() ) m_pmf = i->first;
  else Exception ( "Invalid LHCb::STChannelID member function '" + nick + "'");
}


// ============================================================================
// MANDATORY: clone method ('virtual constructor')
// ============================================================================
LoKi::Track::CountSTIDs*
LoKi::Track::CountSTIDs::clone() const
{ return new LoKi::Track::CountSTIDs ( *this ) ; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::CountSTIDs::result_type
LoKi::Track::CountSTIDs::operator()
  ( LoKi::Track::CountSTIDs::argument t ) const
{
  //
  if ( UNLIKELY(!t) ) {
    Error ("LHCb::Track* points to NULL, return -1") ;
    return -1 ;
  }
  //
  const auto& lhcbids = t->lhcbIDs() ;
  //
  size_t res = 0 ;
  //
  for ( auto id = lhcbids.begin() ; lhcbids.end() != id ; ++id )
  {
    if ( ! id->isST() ) { continue ; }
    //
    const unsigned int r = (id->stID().*m_pmf) () ;
    if      ( m_uints.empty()     ) { if ( m_imin <= r && r <= m_imax ) { ++res ; } }
    else if ( 1 == m_uints.size() ) { if ( m_uints.front() == r       ) { ++res ; } }
    else
    {
      if  ( m_uints.end() != std::find ( m_uints.begin() , m_uints.end() , r ) ) { ++res ; }
    }
  }
  //
  return res;
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Track::CountSTIDs::fillStream ( std::ostream& s ) const
{
  s << " TrSTIDC( " ;
  //
  if      ( m_uints.empty()     ) { s << m_imin << " ,'" << m_nick << "', "<< m_imax ; }
  else if ( 1 == m_uints.size() ) { s << "'" << m_nick << "', " << m_uints.front()     ; }
  else {
    s << "'" << m_nick << "', " ;
    Gaudi::Utils::toStream ( m_uints , s ) ;
  }
  //
  return s << " ) " ;
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs ( LoKi::Track::CountVeloIDs::PMF2 pmf ,
                                          const unsigned int               i   )
: m_pmf2  ( pmf   )
, m_uints ( 1 , i )
{
  setNick ( m_pmf2 ) ;
  Assert ( 0 != m_pmf2 , "Invalid LHCb::VeloChannelID member function!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs ( LoKi::Track::CountVeloIDs::PMF2 pmf ,
                                          const std::vector<unsigned int>& i   )
: m_pmf2  ( pmf   )
, m_uints ( i     )
{
  setNick ( m_pmf2 ) ;
  Assert ( 0 != m_pmf2 , "Invalid LHCb::VeloChannelID member function!" ) ;
  Assert ( !m_uints.empty() , "Empty vector of values is specified!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs ( const unsigned int               imin ,
                                          LoKi::Track::CountVeloIDs::PMF2 pmf  ,
                                          const unsigned int               imax )
: m_pmf2  ( pmf   )
, m_imin  ( imin )
, m_imax  ( imax )
{
  setNick ( m_pmf2 ) ;
  Assert ( 0 != m_pmf2 , "Invalid LHCb::OTChannelID member function!" ) ;
  Assert ( imin <= imax  , "Invalid range of values" ) ;
}
// ============================================================================
// set nick
// ============================================================================
void LoKi::Track::CountVeloIDs::setNick ( LoKi::Track::CountVeloIDs::PMF2 pmf )
{
  auto i = std::find_if( s_countVeloIDs_tbl2.begin(), s_countVeloIDs_tbl2.end(),
                         [&](const auto& p) { return p.first == pmf; } );
  if ( i != s_countVeloIDs_tbl2.end() ) m_nick = i->second;
  else Exception ( "Invalid LHCb::VeloChannelID member function!") ;
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs ( const std::string&               nick ,
                                          const std::vector<unsigned int>& i    )
: LoKi::AuxFunBase ( std::tie ( nick , i ) )
, m_uints ( i     )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf2 , "Invalid LHCb::VeloChannelID member function: '" + m_nick + "'" ) ;
  Assert ( !m_uints.empty() , "Empty vector of values is specified!" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs( const std::string&  nick ,
                                         const unsigned int  i    )
: LoKi::AuxFunBase ( std::tie ( nick , i ) )
, m_uints ( 1 , i )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf2 , "Invalid LHCb::VeloChannelID member function: '" + m_nick + "'" ) ;
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs ( const unsigned int  imin ,
                                          const std::string&  nick ,
                                          const unsigned int  imax )
: LoKi::AuxFunBase ( std::tie ( imin , nick , imax ) )
, m_imin  ( imin  )
, m_imax  ( imax  )
, m_nick  ( nick  )
{
  setPmf ( m_nick ) ;
  Assert ( 0 != m_pmf2  , "Invalid LHCb::VeloChannelID member function: '" + m_nick + "'" ) ;
  Assert ( imin <= imax  , "Invalid range of values" ) ;
}
// ============================================================================
// set PMF properly
// ============================================================================
void LoKi::Track::CountVeloIDs::setPmf ( const std::string& nick )
{
  auto i = std::find_if( s_countVeloIDs_tbl2.begin(), s_countVeloIDs_tbl2.end(),
                         [&](const auto& p) { return p.second == nick; } );
  if ( i != s_countVeloIDs_tbl2.end() ) m_pmf2 = i->first;
  else Exception ( "Invalid LHCb::VeloChannelID member function '" + nick + "'") ;
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs ( LoKi::Track::CountVeloIDs::PMF1 pmf )
: m_pmf1  ( pmf   )
{
  auto i = std::find_if( s_countVeloIDs_tbl1.begin(), s_countVeloIDs_tbl1.end(),
                         [&](const auto& p) { return p.first == m_pmf1; } );
  if ( i != s_countVeloIDs_tbl1.end() ) m_nick = i->second;
  else Exception("Invalid LHCb::VeloChannelID memebr function") ;
  Assert ( 0 != m_pmf1  , "Invalid LHCb::VeloChannelID member function:" ) ;
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVeloIDs::CountVeloIDs ( const std::string& nick  )
: LoKi::AuxFunBase ( std::tie ( nick ) )
, m_nick  ( nick )
{
  auto i = std::find_if( s_countVeloIDs_tbl3.begin(), s_countVeloIDs_tbl3.end(),
                         [&](const auto& p) { return p.first == m_nick; } );
  if ( i != s_countVeloIDs_tbl3.end() ) m_pmf1 = i->second;
  Assert ( 0 != m_pmf1  , "Invalid LHCb::VeloChannelID member function '" + m_nick + "'" ) ;
}
// ============================================================================
// MANDATORY: clone method ('virtual constructor')
// ============================================================================
LoKi::Track::CountVeloIDs*
LoKi::Track::CountVeloIDs::clone() const
{ return new LoKi::Track::CountVeloIDs ( *this ) ; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::CountVeloIDs::result_type
LoKi::Track::CountVeloIDs::operator() ( LoKi::Track::CountVeloIDs::argument t ) const
{
  //
  //
  if ( UNLIKELY(!t) ) {
    Error ("LHCb::Track* points to NULL, return -1") ;
    return -1 ;
  }
  //
  const auto& lhcbids = t->lhcbIDs() ;
  //
  size_t res = 0 ;
  //
  for ( auto id = lhcbids.begin() ; lhcbids.end() != id ; ++id )
  {
    if ( ! id->isVelo() ) { continue ; }
    //
    const LHCb::VeloChannelID velo = id->veloID() ;
    //
    if ( m_pmf1 ) { if ( (velo.*m_pmf1)() ) { ++res ; } }
    else {
      const unsigned int r = (velo.*m_pmf2) () ;
      if      ( m_uints.empty()     ) { if ( m_imin <= r && r <= m_imax ) { ++res ; } }
      else if ( 1 == m_uints.size() ) { if ( m_uints.front() == r       ) { ++res ; } }
      else {
        if  ( m_uints.end() != std::find ( m_uints.begin() , m_uints.end() , r ) ) { ++res ; }
      }
    }
  }
  //
  return res;
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Track::CountVeloIDs::fillStream ( std::ostream& s ) const
{
  s << " TrVELOIDC( " ;
  //
  if ( m_pmf1 ) { return s << "'" << m_nick << "' ) " ; }
  //
  if      ( m_uints.empty()     ) { s << m_imin << " ,'" << m_nick << "', "<< m_imax ; }
  else if ( 1 == m_uints.size() ) { s << "'" << m_nick << "', " << m_uints.front()     ; }
  else {
    s << "'" << m_nick << "', " ;
    Gaudi::Utils::toStream ( m_uints , s ) ;
  }
  //
  return s << " ) " ;
}


// ============================================================================
// The END
// ============================================================================
