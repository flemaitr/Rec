#ifndef PRFTHITHANDLER_H
#define PRFTHITHANDLER_H 1

// Include files
#include "PrKernel/PrHit.h"
#include "PrKernel/PrFTInfo.h"
#include "PrKernel/PrHitZone.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/ObjectContainerBase.h"

#include "GaudiKernel/Range.h"

#include "TfKernel/IndexedHitContainer1D.h"


/** @class PrFTHitHandler PrFTHitHandler.h
 *  Handler of Sci-Fit hits, the object is stored in the transient event store and
 *  each algorithm reads the object from there without calling the HitManagers (tools)
 *  @author Renato Quagliani
 *  @author Sebastien Ponce
 */

template<class Hit>
class PrFTHitHandler final {
public:
   // Constants you need to change if  Sci-Fi number of layers is going to change.
  enum Numbers{
    NFTZones = 24, NFTXLayers = 6, NFTUVLayers = 6
  };
  
  using HitContainer = IndexedHitContainer1D<Numbers::NFTZones, Hit>;
  
  /// Type for range of PrFT Hits within a container
  using HitRange = typename HitContainer::HitRange;
  using HitIter = typename std::vector<Hit>::const_iterator;
  
  PrFTHitHandler() = default;
  PrFTHitHandler(const PrFTHitHandler& other) = default;
  PrFTHitHandler(PrFTHitHandler&& other) = default;
  PrFTHitHandler& operator=(const PrFTHitHandler& other) = default;
  PrFTHitHandler& operator=(PrFTHitHandler&& other) = default;
  ~PrFTHitHandler() = default;
  
  PrFTHitHandler(typename HitContainer::Hits&& hits, typename HitContainer::Offsets&& offsets)
    : m_hits{std::forward<typename HitContainer::Hits>(hits),
      std::forward<typename HitContainer::Offsets>(offsets)}
  {
    
  }

  // Constructor with the capacity of the HitContainer
  PrFTHitHandler(int size) : m_hits(size) {};

  
  template<typename ... Args>
  void addHitInZone(unsigned int lay, Args&& ... args)
  {
    m_hits.addHit(  std::forward_as_tuple( std::forward<Args>(args)... ), lay);
  }
  
  bool is_sorted()
  {
    return m_hits.is_sorted(compX());
  }

  void setOffsets()
  {
    m_hits.setOffsets();
  }
  
  /// Return the current number of zones
  auto isEmpty(unsigned int lay) const{
    return m_hits.empty( lay );
  }
  
   // Hit handling/ sorting etc...
  class compX{
   public:
    inline bool operator()(      float lv,       float rv) const { return lv < rv;}
    inline bool operator()(const Hit& lhs,       float rv) const { return (*this)(lhs.x(),  rv);}
    inline bool operator()(      float lv, const Hit& rhs) const { return (*this)(lv,       rhs.x());}
    inline bool operator()(const Hit& lhs, const Hit& rhs) const { return (*this)(lhs.x(),  rhs.x());}
    inline bool operator()(const Hit* lhs,       float rv) const { return (*this)(lhs->x(), rv);}
    inline bool operator()(      float lv, const Hit* rhs) const { return (*this)(lv,       rhs->x());}
    inline bool operator()(const Hit* lhs, const Hit* rhs) const { return (*this)(lhs->x(), rhs->x());}
  };
  class compXreverse{
  public:
    inline bool operator()(      float lv,       float rv) const { return lv > rv;}
    inline bool operator()(const Hit& lhs,       float rv) const { return (*this)(lhs.x(),  rv);}
    inline bool operator()(      float lv, const Hit& rhs) const { return (*this)(lv,       rhs.x());}
    inline bool operator()(const Hit& lhs, const Hit& rhs) const { return (*this)(lhs.x(),  rhs.x());}
    inline bool operator()(const Hit* lhs,       float rv) const { return (*this)(lhs->x(), rv);}
    inline bool operator()(      float lv, const Hit* rhs) const { return (*this)(lv,       rhs->x());}
    inline bool operator()(const Hit* lhs, const Hit* rhs) const { return (*this)(lhs->x(), rhs->x());}
  };
  
  template <class ForwardIt, class T>
    ForwardIt linear_lower_bound(ForwardIt first, ForwardIt last, T value) const{
    while (first != last && *first < value) ++first;
    return first;
  }
  template <class ForwardIt, class T>
    ForwardIt linear_lower_bound(ForwardIt first, ForwardIt last, T value) {
    while (first != last && *first < value) ++first;
    return first;
  }
  
  template <class ForwardIt, class T, class Compare>
    ForwardIt linear_lower_bound(ForwardIt first, ForwardIt last, T value, Compare comp) const{
    while (first != last && comp(*first, value)) ++first;
    return first;
  }
  template <class ForwardIt, class T, class Compare>
    ForwardIt linear_lower_bound(ForwardIt first, ForwardIt last, T value, Compare comp) {
    while (first != last && comp(*first, value)) ++first;
    return first;
  }
  
  template <class ForwardIt, class T>
    ForwardIt linear_upper_bound(ForwardIt first, ForwardIt last, T value){
      while (first != last && !(value < *first)) ++first;
      return first;
  }
  template <class ForwardIt, class T>
    ForwardIt linear_upper_bound(ForwardIt first, ForwardIt last, T value)const{
    while (first != last && !(value < *first)) ++first;
    return first;
  }
  template <class ForwardIt, class T, class Compare>
    ForwardIt linear_upper_bound(ForwardIt first, ForwardIt last, T value, Compare comp){
    while(first != last && !comp(value, *first)) ++first;
    return first;
  }
  
  template <class ForwardIt, class T, class Compare>
    ForwardIt linear_upper_bound(ForwardIt first, ForwardIt last, T value, Compare comp)const{
    while(first != last && !comp(value, *first)) ++first;
    return first;
  }
  
  //search lowerBound given a layer in log time
  HitIter getIterator_lowerBound(unsigned int lay, float xMin) const{
    return std::lower_bound( getIterator_Begin(lay), getIterator_End(lay), xMin ,
                             [](const Hit& a, const float testval) -> bool { return a.x() < testval;} );
  }
  
  //search upperBound given a layer in log timing
  HitIter getIterator_upperBound(unsigned int lay, float xMax) const{
    return std::upper_bound( getIterator_Begin(lay), getIterator_End(lay), xMax ,
                               [](const float testval, const Hit& a) -> bool { return a.x() > testval ; });
  }
  
  //search lowerBound from known position to another known position ( log time)
  HitIter get_lowerBound_log(HitIter& given_it, HitIter& end, float xMin) const{
    return std::lower_bound(given_it, end, xMin, compX());
  }
  //search upperBound from a known position to another known position (log time)
  HitIter get_upperBound_log(HitIter& given_it, HitIter& end,float xMax) const{
    return std::upper_bound( given_it, end, xMax, compX());
  }
  //search lowerBound from a known position to another known position (linear time)
  HitIter get_lowerBound_lin(HitIter& given_it, HitIter&end, float xMin) const{
    return linear_lower_bound( given_it , end, xMin, compX());
  }
  //search upperBound from known position to another known position (linear time)
  HitIter get_upperBound_lin(HitIter& given_it, HitIter& end, float xMax) const{
    return linear_upper_bound( given_it, end, xMax, compX());
  }
  //search with STL method a lowerBound in low/high range starting from high
  HitIter get_lowerBound_log_reverse( HitIter& low, HitIter& high, float xMin) const{
    std::reverse_iterator<HitIter> revBegin(high);
    std::reverse_iterator<HitIter> revEnd(low);
    std::reverse_iterator<HitIter> revLower = std::upper_bound( revBegin, revEnd, xMin, compXreverse());
    return revLower.base();
  }
  //search with STL method an upper bond in low/high range starting from high
  HitIter get_upperBound_log_reverse( HitIter& low, HitIter& high, float xMax) const{
    std::reverse_iterator<HitIter> revBegin(high);
    std::reverse_iterator<HitIter> revEnd(low);
    std::reverse_iterator<HitIter> revUpper = std::lower_bound( revBegin, revEnd, xMax, compXreverse());
    return revUpper.base();
  }
  
  //search linearly a lowerBound in low/high range starting from high
  HitIter get_lowerBound_lin_reverse( HitIter&low, HitIter& high, float xMin) const{
    std::reverse_iterator<HitIter> revBegin(high);
    std::reverse_iterator<HitIter> revEnd(low);
    std::reverse_iterator<HitIter> revLower = linear_upper_bound( revBegin, revEnd, xMin, compXreverse());
    return revLower.base();
  }
  
  //search linearly an upperBound in low/high range starting from high
  HitIter get_upperBound_lin_reverse( HitIter& low, HitIter& high, float xMax) const{
    std::reverse_iterator<HitIter> revBegin(high);
    std::reverse_iterator<HitIter> revEnd(low);
    std::reverse_iterator<HitIter> revUpper = linear_lower_bound( revBegin, revEnd, xMax, compXreverse());
    return revUpper.base();
  }
  
  const HitContainer& hits() const
  {
    return m_hits;
  }
  
  inline HitRange hits(const unsigned int layer) const
  {
    return m_hits.range ( layer );
  }
  
  Hit& hit(size_t index)
  {
    return m_hits.hit(index);
  }
  
  const Hit& hit(size_t index) const
  {
    return m_hits.hit(index);
  }
  
  HitIter getIterator_Begin(unsigned int lay) const
  {
    auto range = m_hits.range ( lay );
    
    return range.begin();
  }
  
  HitIter getIterator_End(unsigned int lay) const
  {
    auto range = m_hits.range ( lay ); 
    return range.end();
  }
  
 private:
  
  HitContainer m_hits;
  
};

#endif // PRFTHITHANDLER_H
