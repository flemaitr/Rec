#ifndef PRFORWARDTRACKING_H
#define PRFORWARDTRACKING_H 1

// Include files
// from Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "PrKernel/PrFTHitHandler.h"
#include "GaudiAlg/ISequencerTimerTool.h"

#include "PrKernel/IPrDebugTool.h"
#include "PrKernel/PrHitManager.h"

#include "IPrForwardTool.h"

/** @class PrForwardTracking PrForwardTracking.h
 *
 *  \brief Main algorithm for the Forward tracking of the upgrade
 *  \brief The main work is done in PrForwardTool
 *
 *   Parameters:
 * - InputName: Name of the input container
 * - OutputName: Name of the output container
 * - HitManagerName: Name of the hit manager
 * - ForwardToolName: Name of the tool that does the actual forward tracking
 * - DeltaQuality: Quality criterion to reject duplicates
 * - DebugToolName: Name of the debug tool
 * - WantedKey: Key of the MCParticle one wants to look at
 * - VeloKey: Key of the Velo track one wants to look at
 * - TimingMeasurement: Print table with timing measurements
 *
 *
 *  @author Olivier Callot
 *  @date   2012-03-20
 *  @author Michel De Cian
 *  @date   2014-03-12 Changes with make code more standard and faster
 */

class PrForwardTracking : public Gaudi::Functional::Transformer<LHCb::Tracks(const PrFTHitHandler<PrHit>&, const LHCb::Tracks&)> {
public:
  /// Standard constructor
  PrForwardTracking( const std::string& name, ISvcLocator* pSvcLocator );

  /// initialization
  StatusCode initialize() override;
  /// main call
  LHCb::Tracks operator()(const PrFTHitHandler<PrHit>&, const LHCb::Tracks&) const override;

private:

  Gaudi::Property<std::string>    m_forwardToolName{this, "ForwardToolName",  "PrForwardTool" };
  Gaudi::Property<float>          m_deltaQuality   {this, "DeltaQuality",      0.1            };
  // Parameters for debugging
  Gaudi::Property<std::string>    m_debugToolName  {this, "DebugToolName",    ""              };
  Gaudi::Property<int>            m_wantedKey      {this, "WantedKey",        -100            };
  Gaudi::Property<int>            m_veloKey        {this, "VeloKey",          -100            };
  Gaudi::Property<bool>           m_doTiming       {this, "TimingMeasurement", false          };

  
  /** Check if track can be forwarded
   *  @brief Check if track can be forwarded
   *  @param track Velo track that might be forwarded
   *  @return bool
   */
  bool acceptTrack(const LHCb::Track* track ) const {
    return !(track->checkFlag( LHCb::Track::Invalid) ) && !(track->checkFlag( LHCb::Track::Backward) );
  }

  IPrForwardTool* m_forwardTool = nullptr;
  
  //== Debugging controls
  IPrDebugTool*  m_debugTool = nullptr;
  ISequencerTimerTool* m_timerTool = nullptr;
  int            m_timeTotal;
  int            m_timePrepare;
  int            m_timeExtend;
  int            m_timeFinal;
};
#endif // PRFORWARDTRACKING_H
