#ifndef PRADDUTHITSTOOL_H
#define PRADDUTHITSTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AnyDataHandle.h"

#include "Event/State.h"
#include "Event/Track.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "IPrAddUTHitsTool.h" // Interface

#include "PrKernel/UTHitHandler.h"
/*
 * @class PrAddUTHitsTool PrAddUTHitsTool.h
 *
 * \brief  Adds UT hits to long tracks, see note LHCb-INT-2010-20 for TT version
 *
 * Parameters:
 * - ZUTField: Z-Position of the kink for the state extrapolation
 * - ZMSPoint: Z-Position of the multiple scattering point
 * - UTParam: Parameter of the slope of the state extrapolation
 * - MaxChi2Tol: Offset of the chi2 cut
 * - MaxChi2Slope: Slope of the chi2 cut
 * - MaxChi2POffset: Momentum offest of the chi2 cut
 * - YTolSlope: Offest of the y-tolerance cut
 * - XTol: Offest of the x-window cut
 * - XTolSlope: Slope of the x-window cut
 * - MajAxProj: Major axis of the ellipse for the cut on the projection
 * - MinAxProj: Minor axis of the ellipse for the cut on the projection
 * - ZUTProj: Z-Position which state has to be closest to
 *
 *
 *  @author Michel De Cian
 *  @date   2016-05-11
 *
*/

class PrAddUTHitsTool : public extends<GaudiTool, IPrAddUTHitsTool>
{
public:
  /// Standard constructor
  PrAddUTHitsTool( const std::string& type, const std::string& name, const IInterface* parent );

  ~PrAddUTHitsTool() override; ///< Destructor

  StatusCode initialize() override;

  /** @brief Add UT clusters to matched tracks. This calls returnUTHits internally
      @param track Track to add the UT hits to
  */
  StatusCode addUTHits( LHCb::Track& track ) const override;

  /** Return UT hits without adding them.
      @param state State closest to UT for extrapolation (normally Velo state)
      @param ttHits Container to fill UT hits in
      @param finalChi2 internal chi2 of the UT hit adding
      @param p momentum estimate. If none given, the one from the state will be taken
  */
  UT::Mut::Hits returnUTHits( LHCb::State& state, double& finalChi2, double p = 0 ) const override;

private:
  AnyDataHandle<UT::HitHandler> m_HitHandler{UT::Info::HitLocation, Gaudi::DataHandle::Reader, this};

  Gaudi::Property<double> p_zUTField{this, "ZUTField", 1740. * Gaudi::Units::mm};
  Gaudi::Property<double> p_zMSPoint{this, "ZMSPoint", 400. * Gaudi::Units::mm};
  Gaudi::Property<double> p_utParam{this, "UTParam", 29.};
  Gaudi::Property<double> p_zUTProj{this, "ZUTProj", 2500. * Gaudi::Units::mm};
  Gaudi::Property<double> p_maxChi2Tol{this, "MaxChi2Tol", 2.0};
  Gaudi::Property<double> p_maxChi2Slope{this, "MaxChi2Slope", 25000};
  Gaudi::Property<double> p_maxChi2POffset{this, "MaxChi2POffset", 100};
  Gaudi::Property<double> p_yTolSlope{this, "YTolSlope", 20000.};
  Gaudi::Property<double> p_xTol{this, "XTol", 1.0};
  Gaudi::Property<double> p_xTolSlope{this, "XTolSlope", 30000.0};
  Gaudi::Property<double> p_majAxProj{this, "MajAxProj", 20.0 * Gaudi::Units::mm};
  Gaudi::Property<double> p_minAxProj{this, "MinAxProj", 2.0 * Gaudi::Units::mm};

  UT::Mut::Hits selectHits( const LHCb::State& state, const double p) const;
  void calculateChi2( double& chi2, const double& bestChi2, double& finalDist, const double& p,
                      UT::Mut::Hits& goodUT ) const;
  void printInfo( double dist, double chi2, const LHCb::State& state, const UT::Mut::Hits& goodUT ) const;

  double m_invMajAxProj2;

  ILHCbMagnetSvc* m_magFieldSvc;
};

#endif // PRADDUTHITSTOOL_H
