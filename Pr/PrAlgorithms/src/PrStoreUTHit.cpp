#include "PrStoreUTHit.h"

#include "GaudiKernel/IRegistry.h"
#include "STDet/DeSTDetector.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrStoreUTHit
//
// 2016-11-15 : Renato Quagliani, Christoph Hasse
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrStoreUTHit )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrStoreUTHit::PrStoreUTHit( const std::string& name,
                            ISvcLocator* pSvcLocator)
: Transformer ( name , pSvcLocator ,
                KeyValue{ "InputLocation",  UT::Info::ClusLocation},
                KeyValue{ "UTHitsLocation", UT::Info::HitLocation}
                ){}
//=============================================================================
// Initialization
//=============================================================================
StatusCode PrStoreUTHit::initialize() {
  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  //TODO : alignment need the updateSvc for detector ( UT experts needed )
  m_utDet = getDet<DeSTDetector>(DeSTDetLocation::UT);
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
UT::HitHandler PrStoreUTHit::operator()(const UTLiteClusters &clus) const {
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  UT::HitHandler hitHandler(clus.size());
  StoreUTHits(hitHandler, clus);
  return hitHandler;
}


//========== Store UT Hits in TES method =======
void PrStoreUTHit::StoreUTHits(UT::HitHandler& hitHandler, const UTLiteClusters &clus)const{
  //info()<<"UTDet "<<m_utDet<<endmsg;
  for( auto&cluster : clus){
    //find the sector associated to the cluster channelID and use it to create the Hit
    auto aSector = m_utDet->findSector( cluster.channelID());
    hitHandler.AddHit(aSector, cluster);
  }
  //--- sort the hits in each station/layer by XAtYEq0 (as needed by the consumers )
  hitHandler.sortByXAtYEq0();

  // Set the offsets. Required for further use of HitContainer.
  // To be called after addHit and sort.
  hitHandler.setOffsets();

}
StatusCode PrStoreUTHit::BuildGeometry(){
  //--- do we need to cache some geometry information to produce the hits or objects which are geometry dependent? (maybe the Sectors? )
  return StatusCode::SUCCESS;
}
