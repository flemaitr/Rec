#include "PrStoreFTHit.h"

#include <boost/numeric/conversion/cast.hpp>

//-----------------------------------------------------------------------------
// Implementation file for class : PrStoreFTHit
// This algorithms is expected to store the hits in TES together with Geometry
// information ( should be fixed ?) . Geometry information are encoded in the
// PrFTHiHandler::m_zones private variable
//
// 2016-07-07 : Renato Quagliani
//-----------------------------------------------------------------------------
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrStoreFTHit )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrStoreFTHit::PrStoreFTHit(const std::string& name,
                                 ISvcLocator* pSvcLocator) :
Transformer(name, pSvcLocator,
            KeyValue{"InputLocation", LHCb::FTLiteClusterLocation::Default},
            KeyValue{"FTHitsLocation", PrFTInfo::FTHitsLocation}) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrStoreFTHit::initialize() {
  // parent initialization
  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  // Load detector and GeometryBuild is SUPER-time consuming, so this is stored
  // as a condition in the detector store and updated only when the geometry changes
  // This is possible thanks to the UpdateManager service that allows to register
  // a condition and the method to be called when it changes
  // TODO move this to Condition Handles once these ones they are available

  // register a derived condition object for the zone cache
  detSvc()->registerObject(PrFTInfo::FTCondLocation, new DataObject());
  m_zoneHandler = new PrFTZoneHandler();
  detSvc()->registerObject(PrFTInfo::FTZonesLocation, m_zoneHandler);
  // make sure the detector element is updated before the algorithm is executed
  registerCondition(DeFTDetectorLocation::Default, m_ftDet, &PrStoreFTHit::buildGeometry);
  // make sure the derived condition is updated when the conditions of this algorithm are
  updMgrSvc()->registerCondition(m_zoneHandler, this);
  // This is needed to work around a missing update of the pointer when loading the detector element
  // This is a bug a priori specific to detector elements
  updMgrSvc()->update(m_zoneHandler);

  // TODO: this should be ~80 micron; get this from a tool
  m_clusResolution = {0.05, 0.08, 0.11, 0.14, 0.17, 0.20, 0.23, 0.26, 0.29};

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
PrFTHitHandler<PrHit> PrStoreFTHit::operator()(const FTLiteClusters &clusters) const {
  if (msgLevel(MSG::DEBUG)) {
    debug() << "==> Execute" << endmsg;
    debug()<<  "Detector version used: "<< m_ftDet->version()<<endmsg;
  }
  // create a hitHandler to be returned and stored in the TES
  PrFTHitHandler<PrHit> hitHandler( clusters.size() );
  // Store Hits
  storeHits(hitHandler, clusters);
  // return
  return hitHandler;
}


//=============================================================================
// buildGeometry
//=============================================================================
StatusCode PrStoreFTHit::buildGeometry(){
  DetectorSegment segUp;
  DetectorSegment segDown;
  info() << "FtDEt = " << m_ftDet << endmsg;
  if( m_ftDet->version() < 61 ) {
    error() << "This version requires FTDet v6.1 or higher" << endmsg;
    return StatusCode::FAILURE;
  }
  for( auto station : m_ftDet->stations() ) {
    for( auto layer : station->layers() ) {
      int id = 4*(station->stationID() - 1) + layer->layerID();

      DetectorSegment seg(0, layer->globalZ(), layer->dxdy(), layer->dzdy(), 0., 0.);
      float xmax = 0.5*layer->sizeX();
      float ymax = 0.5*layer->sizeY();

      //The setGeometry defines the z at y=0, the dxDy and the dzDy, as well as the isX properties of the zone.
      //This is important, since these are used in the following.
      //They are set once for each zone in this method.
      m_zoneHandler->MakeZone( 2*id+1, seg, -xmax, xmax, -25., ymax ); // Small overlap (25 mm) for stereo layers
      m_zoneHandler->MakeZone( 2*id  , seg, -xmax, xmax, -ymax, 25. ); // Small overlap (25 mm) for stereo layers

      //----> Debug zones
      if( msgLevel(MSG::DEBUG)){
        debug() << "Layer " << id << " z " << m_zoneHandler->zone(2*id).z()
                << " angle " << m_zoneHandler->zone(2*id).dxDy() << endmsg;
      }
      //----> Debug zones
    }
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// StoreHits
//=============================================================================
void PrStoreFTHit::storeHits(PrFTHitHandler<PrHit> &hitHandler, const FTLiteClusters &clusters) const
{
  if ( msgLevel( MSG::DEBUG) ) debug() << "Retrieved " << clusters.size() << " clusters" << endmsg;

  const DeFTMat* mat = nullptr;
  unsigned int prevMatID = 99999999;
  float dxdy(0.0), dzdy(0.0), globaldy(0.0);
  for ( const auto& clus : clusters ) {
    if( clus.channelID().uniqueMat() != prevMatID ) {
      mat       = m_ftDet->findMat( clus.channelID() );
      prevMatID = mat->elementID().uniqueMat();
      dxdy      = mat->dxdy();
      dzdy      = mat->dzdy();
      globaldy  = mat->globaldy();
    }

    int fraction = clus.fractionBit();
    LHCb::FTChannelID id = clus.channelID();

    int lay  = id.uniqueLayer()-4;
    int zone = int(id.isTop());
    int code = 2*lay + zone;

    auto endPoint = mat->endPoint(id, fraction);
    float x0     = endPoint.x()-dxdy*endPoint.y();
    float z0     = endPoint.z()-dzdy*endPoint.y();
    float yMin   = endPoint.y();
    float yMax   = yMin + globaldy;
    if( id.isBottom() ) std::swap( yMin, yMax );

    assert( clus.pseudoSize() < 9 && "Pseudosize of cluster is > 8. Out of range.");
    float errX = m_clusResolution[clus.pseudoSize()];

    hitHandler.addHitInZone( code, LHCb::LHCbID( id ), x0, z0, dxdy, dzdy, yMin, yMax, errX , zone, lay );
    if ( msgLevel(MSG::VERBOSE) )
      verbose() << " .. hit " << id << " code=" << code << " x=" << x0 << " z=" << z0 << endmsg;
  }

  // Prepare the hit container after sorting
  hitHandler.setOffsets();

  // Verify that the hits are sorted
  assert( hitHandler.is_sorted() &&
          "FT hits must be properly sorted for the pattern recognition "
          "Lower by X for each zone" );

  if (UNLIKELY(msgLevel(MSG::VERBOSE))) {
    const auto& container = hitHandler.hits();
    const auto& offsets = container.offsets();
    for (size_t i = 0; i < container.size(); ++i) {
      unsigned id = 2*container.hit(i).planeCode() + container.hit(i).zone();
      verbose() << std::setw(6) << std::right << i << " [" << offsets[id].first << ";" << offsets[id].second << "] "
                << std::setw(6) << std::right << id
                << std::setw(10) << std::right << container.hit(i).x() << endmsg;
    }
  }
}
