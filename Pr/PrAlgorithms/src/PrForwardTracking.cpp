// Include files

// from Gaudi
#include "Event/Track.h"

// local
#include "PrForwardTracking.h"
#include "PrKernel/PrFTInfo.h"

#include <cassert>

//-----------------------------------------------------------------------------
// Implementation file for class : PrForwardTracking
//
// 2012-03-20 : Olivier Callot
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrForwardTracking )

namespace {
  template <typename T>
  struct counting_inserter {
    int count = 0;
    counting_inserter& operator++() { ++count; return *this; }
    counting_inserter& operator*() { return *this; }
    counting_inserter& operator=(const T&) { return *this; }
  };
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrForwardTracking::PrForwardTracking(const std::string& name,
                                     ISvcLocator* pSvcLocator) :
  Transformer(name, pSvcLocator,
              { KeyValue{"FTHitsLocation", PrFTInfo::FTHitsLocation},
                KeyValue{"InputName", LHCb::TrackLocation::Velo} },
              KeyValue{"OutputName", LHCb::TrackLocation::Forward})
{ }

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrForwardTracking::initialize() {
  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  //only init after build geometry!
  m_forwardTool = tool<IPrForwardTool>( m_forwardToolName, this );

  
  m_debugTool   = 0;
  if ( "" != m_debugToolName ) {
    m_debugTool = tool<IPrDebugTool>( m_debugToolName );
  } else {
    m_wantedKey = -100;  // no debug
  }
  m_forwardTool->setDebugParams( m_debugTool, m_wantedKey, m_veloKey );
  

  if ( m_doTiming) {
    m_timerTool = tool<ISequencerTimerTool>( "SequencerTimerTool/Timer", this );
    m_timeTotal   = m_timerTool->addTimer( "PrForward total" );
    m_timerTool->increaseIndent();
    m_timePrepare = m_timerTool->addTimer( "PrForward prepare" );
    m_timeExtend  = m_timerTool->addTimer( "PrForward extend" );
    m_timeFinal   = m_timerTool->addTimer( "PrForward final" );
    m_timerTool->decreaseIndent();
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::Tracks PrForwardTracking::operator()(const PrFTHitHandler<PrHit>& prFTHitHandler,
                                           const LHCb::Tracks& velo) const {
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  if ( m_doTiming ) {
    m_timerTool->start( m_timeTotal );
    m_timerTool->start( m_timePrepare );
  }

  LHCb::Tracks result;

  if (velo.empty()) {
    return result;
  }

  //== If needed, debug the cluster associated to the requested MC particle.
  if ( 0 <= m_wantedKey ) {
    info() << "--- Looking for MCParticle " << m_wantedKey << endmsg;
    for ( unsigned int zone = 0; PrFTInfo::nbZones() > zone; ++zone ) {
      auto it    = prFTHitHandler.getIterator_Begin(zone);
      auto itEnd = prFTHitHandler.getIterator_End(zone);
      for (;it<itEnd;++it){
        if ( m_forwardTool->matchKey( *it ) ) m_forwardTool->printHit( *it, " " );
      }
    }
  }

  //============================================================
  //== Main processing: Extend selected tracks
  //============================================================
  if ( m_doTiming ) {
    m_timerTool->stop( m_timePrepare );
    m_timerTool->start( m_timeExtend );
  }

  // -- Loop over all Velo input tracks and try to find extension in the T-stations
  // -- This is the main 'work' of the forward tracking.
  m_forwardTool->extendTrack(velo, result , prFTHitHandler);

  //============================================================
  //== Final processing: filtering of duplicates,...
  //============================================================
  if ( m_doTiming ) {
    m_timerTool->stop( m_timeExtend );
    m_timerTool->start( m_timeFinal );
  }

  // -- Sort the tracks according to their x-position of the state in the T-stations
  // -- in order to make the final loop faster.
  std::sort( result.begin(), result.end(),
             [](const LHCb::Track* track1, const LHCb::Track* track2){
               return track1->stateAt( LHCb::State::AtT )->x() <  track2->stateAt( LHCb::State::AtT )->x();
             });

  auto notFT = [](const LHCb::LHCbID& id) { return !id.isFT(); };

  for ( LHCb::Tracks::iterator itT1 = result.begin(); result.end() != itT1; ++itT1 ) {
    LHCb::State* state1 = (*itT1)->stateAt( LHCb::State::AtT );

    for ( LHCb::Tracks::iterator itT2 = itT1+1; result.end() != itT2; ++itT2 ) {
      LHCb::State* state2 = (*itT2)->stateAt( LHCb::State::AtT );
      double dx = state1->x() - state2->x();
      if ( std::fabs(dx) > 50. ) break; // The distance only gets larger, as the vectors are sorted
      double dy = state1->y() - state2->y();
      if ( std::fabs(dy) > 100. ) continue;

      // -- Find the beginning of the FT LHCbIDs
      const auto& id1 = (*itT1)->lhcbIDs();
      const auto& id2 = (*itT2)->lhcbIDs();

      // -- Velo track has at least three hits, so advance by three
      // checking whether we have the correct minimal number of velo hits
      assert( std::distance(begin(id1), end(id1)) > 3 && "must have at least three velo hits");
      assert( std::distance(begin(id2), end(id2)) > 3 && "must have at least three velo hits");
      assert( std::all_of(begin(id1), begin(id1) + 3, notFT) && "first three hits should not be FT, as there should be at least 3 velo hits");
      assert( std::all_of(begin(id2), begin(id2) + 3, notFT) && "first three hits should not be FT, as there should be at least 3 velo hits");

      auto begin1 = std::partition_point(begin(id1) + 3, end(id1), notFT);
      auto begin2 = std::partition_point(begin(id2) + 3, end(id2), notFT);

      // -- This get the cardinal of intersections between the two vectors
      int nCommon = std::set_intersection(begin1, id1.end(),
                                          begin2, id2.end(),
                                          counting_inserter<LHCb::LHCbID>{}).count;

      int n1 = std::distance( begin1, id1.end() );
      int n2 = std::distance( begin2, id2.end() );

      if ( nCommon > .4 * (n1 + n2 ) ) {
        float q1 = (*itT1)->info( LHCb::Track::AdditionalInfo::PatQuality, 0. );
        float q2 = (*itT2)->info( LHCb::Track::AdditionalInfo::PatQuality, 0. );
        if ( msgLevel( MSG::DEBUG ) ) debug() << (*itT1)->key() << " (q=" << q1 << ") and "
                << (*itT2)->key() << " (q=" << q2 << ") share "
                << nCommon << " FT hits" << endmsg;
        if ( q1 + m_deltaQuality < q2 ) {
          if ( msgLevel( MSG::DEBUG ) ) debug() << "Erase " << (*itT2)->key() << endmsg;
          result.erase( itT2 );
          itT2 = itT1;
        } else if ( q2 + m_deltaQuality < q1 ) {
          if ( msgLevel( MSG::DEBUG ) ) debug() << "Erase " << (*itT1)->key() << endmsg;
          result.erase( itT1 );
          itT1 = result.begin();
          itT2 = itT1;
        }
      }
    }
  }

  if ( m_doTiming ) {
    m_timerTool->stop( m_timeFinal );
    m_timerTool->stop( m_timeTotal );
  }
  return result;
}
