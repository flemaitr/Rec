// *********************************************************************************
// ************************ Introduction to Forward Tracking **********************
// *********************************************************************************
//
//  A detailed introduction in Forward tracking (with real pictures!) can be
//  found here:
//  (2002) http://cds.cern.ch/record/684710/files/lhcb-2002-008.pdf
//  (2007) http://cds.cern.ch/record/1033584/files/lhcb-2007-015.pdf
//  (2014) http://cds.cern.ch/record/1641927/files/LHCb-PUB-2014-001.pdf
//
// *** Short Introduction in geometry:
//
// The SciFi Tracker Detector, or simple Fibre Tracker (FT) consits out of 3 stations.
// Each station consists out of 4 planes/layers. Thus there are in total 12 layers,
// in which a particle can leave a hit. The reasonable maximum number of hits a track
// can have is thus also 12 (sometimes 2 hits per layer are picked up).
//
// Each layer consists out of several Fibre mats. A fibre has a diameter of below a mm.(FIXME)
// Several fibres are glued alongside each other to form a mat.
// A Scintilating Fibre produces light, if a particle traverses. This light is then
// detected on the outside of the Fibre mat.
//
// Looking from the collision point, one (X-)layer looks like the following:
//
//                    y       6m
//                    ^  ||||||||||||| Upper side
//                    |  ||||||||||||| 2.5m
//                    |  |||||||||||||
//                   -|--||||||o||||||----> -x
//                       |||||||||||||
//                       ||||||||||||| Lower side
//                       ||||||||||||| 2.5m
//
// All fibres are aranged parallel to the y-axis. There are three different
// kinds of layers, denoted by X,U,V. The U/V layers are rotated with respect to
// the X-layers by +/- 5 degrees, to also get a handle of the y position of the
// particle. As due to the magnetic field particles are only deflected in
// x-direction, this configuration offers the best resolution.
// The layer structure in the FT is XUVX-XUVX-XUVX.
//
// The detector is divided into an upeer and a lower side (>/< y=0). As particles
// are only deflected in x direction there are only very(!) few particles that go
// from the lower to the upper side, or vice versa. The reconstruction algorithm
// can therefore be split into two independent steps: First track reconstruction
// for tracks in the upper side, and afterwards for tracks in the lower side.
//
// Due to construction issues this is NOT true for U/V layers. In these layers the
// complete(!) fibre modules are rotated, producing a zic-zac pattern at y=0, also
// called  "the triangles". Therefore for U/V layers it must be explicetly also
// searched for these hit on the "other side", if the track is close to y=0.
// Sketch (rotation exagerated!):
//                                          _.*
//     y ^   _.*                         _.*
//       | .*._      Upper side       _.*._
//       |     *._                 _.*     *._
//       |--------*._           _.*           *._----------------> x
//       |           *._     _.*                 *._     _.*
//                      *._.*       Lower side      *._.*
//
//
//
//
//
//       Zone ordering defined on PrKernel/PrFTInfo.h
//
//     y ^
//       |    1  3  5  7     9 11 13 15    17 19 21 23
//       |    |  |  |  |     |  |  |  |     |  |  |  |
//       |    x  u  v  x     x  u  v  x     x  u  v  x   <-- type of layer
//       |    |  |  |  |     |  |  |  |     |  |  |  |
//       |------------------------------------------------> z
//       |    |  |  |  |     |  |  |  |     |  |  |  |
//       |    |  |  |  |     |  |  |  |     |  |  |  |
//       |    0  2  4  6     8 10 12 14    16 18 20 22
//
//
// *** Short introduction in the Forward Tracking algorithm
//
// The track reconstruction is seperated into several steps:
//
// 1) Using only X-hits
//    1.1) Preselection: collectAllXHits()
//    1.2) Hough Transformation: xAtRef_SamePlaneHits()
//    1.3) Cluster search: selectXCandidates()
//    1.4) Linear and than Cubic Fit of X-Projection
// 2) Introducing U/V hits or also called stereo hits
//    2.1) Preselection: collectStereoHits
//    2.2) Cluster search: selectStereoHits
//    2.3) Fit Y-Projection
// 3) Using all (U+V+X) hits
//    3.1) Fitting X-Projection
//    3.2) calculating track quality with a Neural Net
//    3.3) final clone+ghost killing
//
// *****************************************************************

// Include files
// from Gaudi
#include "Event/StateParameters.h"
// local
#include "PrKernel/PrFTInfo.h"
#include "PrForwardTool.h"

#include <algorithm>

#include <Vc/Vc>

//-----------------------------------------------------------------------------
// Implementation file for class : PrForwardTool
//
// 2012-03-20 : Olivier Callot
// 2013-03-15 : Thomas Nikodem
// 2015-02-13 : Sevda Esen [additional search in the triangles by Marian Stahl]
// 2016-03-09 : Thomas Nikodem [complete restructuring]
//-----------------------------------------------------------------------------

namespace {
  using std::string;
  using std::vector;

  //Input variable names for TMVA NeuralNet
  const vector<string> mlpInputVars {{"nPlanes"}, {"dSlope"}, {"dp"}, {"slope2"}, {"dby"}, {"dbx"}, {"day"}};

  //original PrGeometry Params
  constexpr auto zMagnetParams = LHCb::make_array( 5212.38f, 406.609f, -1102.35f, -498.039f );
  constexpr auto xParams = LHCb::make_array( 18.6195f, -5.55793f );
  constexpr auto byParams = LHCb::make_array( -0.667996f );
  constexpr auto cyParams = LHCb::make_array( -3.68424e-05f );
  constexpr auto momentumParams = LHCb::make_array( 1.21014f, 0.637339f, -0.200292f, 0.632298f, 3.23793f, -27.0259f );
  //  constexpr auto m_covarianceValues = LHCb::make_array( 4.0f,
  //                                                        400.0f, // ErrX = 2mm
  //                                                        4.e-6f, // ErrSlX = 2 mrad
  //                                                        1.e-4f, // ErrSlY = 10 mrad
  //                                                        0.1f);  // errQQoverP = 10% of qOverP
  constexpr float zReference = 8520.f;
  constexpr int numberallXHitsAfterCuts = 2500;
}

// Declaration of the Tool Factory
DECLARE_COMPONENT( PrForwardTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  PrForwardTool::PrForwardTool( const std::string& type,
                                const std::string& name,
                                const IInterface* parent )
: base_class ( type, name , parent ),
  m_MLPReader_1st{mlpInputVars},
  m_MLPReader_2nd{mlpInputVars}
{
}
//=========================================================================
//  initialize
//=========================================================================
StatusCode PrForwardTool::initialize ( ) {
  StatusCode sc = GaudiTool::initialize();
  if ( !sc ) return sc;

  m_geoTool = tool<PrGeometryTool>("PrGeometryTool");

  if ( "" != m_addUTHitsToolName  ) {
    m_addUTHitsTool = tool<IPrAddUTHitsTool>( m_addUTHitsToolName, this );
  }

  //check options
  if(m_maxChi2StereoLinear <= m_maxChi2Stereo){
    error() << "Error: m_maxChi2StereoLinear must be chosen larger than m_maxChi2Stereo" << endmsg;
    return StatusCode::FAILURE;
  }

  // Zones cache, retrieved from the detector store
  registerCondition<PrForwardTool>(PrFTInfo::FTZonesLocation, m_zoneHandler);

  return StatusCode::SUCCESS;
}

//=========================================================================
//  Main method: Process a track
//=========================================================================
void PrForwardTool::extendTrack (const LHCb::Tracks& velo, LHCb::Tracks& result, const PrFTHitHandler<PrHit>& FTHitHandler) const {

  std::vector<PrForwardTrack> tracks; tracks.reserve(velo.size());

  //in the worst(!) case preslection <<< HERE!
  for(const auto& track: velo) {
    if(    not ( track->checkFlag( LHCb::Track::Invalid) ) && not (track->checkFlag( LHCb::Track::Backward) ) // accept track
        && not ( m_preselection && track->pt() < m_preselectionPT ) // preselect the VeloUT tracks
        && not ( std::fabs(track->closestState(StateParameters::ZEndVelo).qOverP()) > 0.001 ) //very low momentum tracks P < 1GeV of no interest
      ) {
      tracks.emplace_back(track);
    }
  }

  //First loop Hough Cluster search
  PrParameters pars{m_minXHits, m_maxXWindow, m_maxXWindowSlope, m_maxXGap, 4u};
  PrParameters pars2ndLoop{m_minXHits2nd, m_maxXWindow2nd, m_maxXWindowSlope2nd, m_maxXGap2nd, 4u};

  // -- This does not change throughout the code
  const float magScaleFactor = m_geoTool->magscalefactor();

  PrForwardTracks trackCandidates;
  PrForwardTracks trackCandidates2ndLoop;
  
  std::vector<ModPrHit> allXHitsUpper;
  std::vector<ModPrHit> allXHitsLower;
  allXHitsUpper.reserve(numberallXHitsAfterCuts);
  allXHitsLower.reserve(numberallXHitsAfterCuts);

  int nbTracks = tracks.size();

  for(int i = 0; i < nbTracks; ++i) {
    const auto& track = tracks[i];
    allXHitsUpper.clear();
    allXHitsLower.clear();
    trackCandidates.clear();
    trackCandidates2ndLoop.clear();

    //calculate y of velo at zref to decide on which side to search
    const float yAtRef = track.seed().y(zReference);

    // -- it happens rarely that we have to collect in both halves
    if(yAtRef>-5.f){
      collectAllXHits<PrHitZone::Side::Upper>(allXHitsUpper, track, FTHitHandler, pars, magScaleFactor);
      selectXCandidates<PrHitZone::Side::Upper>(trackCandidates, track, allXHitsUpper, FTHitHandler, pars);
    }
    if(yAtRef< 5.f){
      collectAllXHits<PrHitZone::Side::Lower>(allXHitsLower, track, FTHitHandler, pars, magScaleFactor);
      selectXCandidates<PrHitZone::Side::Lower>(trackCandidates, track, allXHitsLower, FTHitHandler, pars);
    }

    // -- > Debug --------
    if ( msgLevel(MSG::DEBUG)) {
      info() << endmsg
             << "=============== Selected " << trackCandidates.size() << " candidates in 1st loop."
             << endmsg
             << endmsg;
      int nValidTracks = 0;
      for ( const auto& trackcand : trackCandidates ){
        if ( trackcand.valid() ) printTrack( trackcand );
        nValidTracks++;
      }
      info () << "  valid tracks :   " << nValidTracks << endmsg;
    }
    // -- < Debug --------

    //Stereo hit search and full Fit
    selectFullCandidates(trackCandidates, FTHitHandler, pars, magScaleFactor);

    // erase tracks not valid
    trackCandidates.erase(std::remove_if(trackCandidates.begin(),
                                         trackCandidates.end(),
                                         [](const auto& track) { return not track.valid(); }),
                          trackCandidates.end());

    // -- > Debug --------
    if ( msgLevel(MSG::DEBUG) ) {
      auto velot = std::next(velo.begin(), i);
      info() << "********** final list of 1st loop candidates, Velo = " << (*velot)->key() << " ********" << endmsg;
      for ( const auto& trackcand : trackCandidates ){
        printTrack( trackcand );
      }
    }
    // -- < Debug --------

    //check tracks, if no OK track is found start second Loop search
    bool ok = std::any_of(trackCandidates.begin(), trackCandidates.end(),
                          [](const auto& track) {
                            return track.hits().size() > 10;
                          });

    if(!ok && m_secondLoop){
      //Second loop Hough Cluster search
      if(yAtRef>-5.f)
        selectXCandidates<PrHitZone::Side::Upper>(trackCandidates2ndLoop, track, allXHitsUpper,
                                                  FTHitHandler, pars2ndLoop);
      if(yAtRef< 5.f)
        selectXCandidates<PrHitZone::Side::Lower>(trackCandidates2ndLoop, track, allXHitsLower,
                                                  FTHitHandler, pars2ndLoop);
      selectFullCandidates(trackCandidates2ndLoop, FTHitHandler, pars2ndLoop, magScaleFactor);
      trackCandidates.insert(std::end(trackCandidates),
                             std::begin(trackCandidates2ndLoop),
                             std::remove_if(trackCandidates2ndLoop.begin(),
                                            trackCandidates2ndLoop.end(),
                                            [](const auto& track) { return not track.valid(); }));

      ok = not trackCandidates.empty();
    }

    //clone+ghost killing after merging
    if(ok || !m_secondLoop){
      std::sort(trackCandidates.begin(), trackCandidates.end(), PrForwardTrack::LowerByQuality() );
      float minQuality = m_maxQuality;
      for ( auto& track : trackCandidates ){
        if(track.quality() + m_deltaQuality < minQuality) minQuality = track.quality() + m_deltaQuality;
        if(track.quality() > minQuality) track.setValid(false);
      }
      makeLHCbTracks(trackCandidates, result);
    }
  }
}


//=========================================================================
//  Create Full candidates out of xCandidates
//  Searching for stereo hits
//  Fit of all hits
//  save everything in track candidate folder
//=========================================================================
void PrForwardTool::selectFullCandidates(PrForwardTracks& trackCandidates, const PrFTHitHandler<PrHit>& FTHitHandler,
                                         PrParameters& pars, const float magScaleFactor) const {

  int nbOK = 0;
  PrPlaneCounter pc;
  vector<float> mlpInput(7, 0.);

  for ( auto& cand : trackCandidates ){
    if ( !cand.valid() ) continue;
    cand.setValid( false ); //set only true after track passed everything

    //at least 4 stereo hits OR  minTotalHits - found xHits (WATCH unsigned numbers!)
    pars.minStereoHits = 4u;
    if(cand.hits().size() + pars.minStereoHits < m_minTotalHits) pars.minStereoHits = m_minTotalHits - cand.hits().size();

    // search for hits in U/V layers
    std::vector<ModPrHit> stereoHits = collectStereoHits(cand, FTHitHandler);
    if(stereoHits.size() < pars.minStereoHits) continue;
    std::sort(stereoHits.begin(), stereoHits.end());

    // select best U/V hits
    if ( !selectStereoHits(cand, FTHitHandler, stereoHits, pars) ) continue;

    //reset pc to count ALL hits
    pc.clear();
    pc.set(cand.hits());

    //make a fit of ALL hits
    if(!fitXProjection(cand, pars, pc))continue;

    //check in empty x layers for hits
    auto checked_empty = (cand.y(zReference) < 0.f) ?
        addHitsOnEmptyXLayers<PrHitZone::Side::Lower>(cand, true, FTHitHandler, pars, pc)
      : addHitsOnEmptyXLayers<PrHitZone::Side::Upper>(cand, true, FTHitHandler, pars, pc);

    if(not checked_empty) continue;

    //track has enough hits, calcualte quality and save if good enough
    if(pc.nbDifferent() >= m_minTotalHits){

      const float qOverP  = calcqOverP(cand, magScaleFactor);

      //orig params before fitting , TODO faster if only calc once?? mem usage?
      const float xAtRef = cand.x(zReference );
      float dSlope  = ( cand.seed().x(zReference) - xAtRef ) / ( zReference - zMagnetParams[0]);
      const float zMagSlope = zMagnetParams[2] * cand.seed().tx2 +  zMagnetParams[3] * cand.seed().ty2;
      const float zMag    = zMagnetParams[0] + zMagnetParams[1] *  dSlope * dSlope  + zMagSlope;
      const float xMag    = cand.seed().x( zMag );
      const float slopeT  = ( xAtRef - xMag ) / ( zReference - zMag );
      dSlope        = slopeT - cand.seed().tx;
      const float dyCoef  = dSlope * dSlope * cand.seed().ty;

      float bx = slopeT;
      float ay = cand.seed().y( zReference );
      float by = cand.seed().ty + dyCoef * byParams[0];

      //ay,by,bx params
      const auto yPars = cand.getYParams();
      const float ay1  = yPars.get(0);
      const float by1  = yPars.get(1);
      const auto xPars = cand.getXParams();
      const float bx1  = xPars.get(1);

      mlpInput[0] = pc.nbDifferent();
      mlpInput[1] = qOverP;
      mlpInput[2] = cand.seed().qOverP - qOverP; //veloUT - scifi
      if(std::fabs(cand.seed().qOverP) < 1e-9f) mlpInput[2] = 0.f; //no momentum estiamte
      mlpInput[3] = cand.seed().slope2;
      mlpInput[4] = by - by1;
      mlpInput[5] = bx - bx1;
      mlpInput[6] = ay - ay1;

      float quality = 0.f;
      /// WARNING: if the NN classes straight out of TMVA are used, put a mutex here!
      if(pars.minXHits > 4) quality = m_MLPReader_1st.GetMvaValue(mlpInput); //1st loop NN
      else                  quality = m_MLPReader_2nd.GetMvaValue(mlpInput); //2nd loop NN

      quality = 1.f-quality; //backward compability

      if(quality < m_maxQuality){
        cand.setValid( true );
        cand.setQuality( quality );
        cand.setQoP( qOverP );
        // -- > Debug --------
        if (msgLevel(MSG::DEBUG)) {
          info() << "*** Accepted as track " << nbOK << " ***" << endmsg;
          printTrack( cand );
        }
        // -- < Debug --------
        ++nbOK;
      }
    }
  }
}

//find matching stereo hit if available, also searching in triangle region
template<>
inline bool
PrForwardTool::matchStereoHitWithTriangle<PrHitZone::Side::Upper>(std::vector<PrHit>::const_iterator& itUV2,
                                                                  const std::vector<PrHit>::const_iterator& itUV2end,
                                                                  const float yInZone,
                                                                  const float xMinUV, const float xMaxUV) const
{
  //search for opposite side UV hit
  //test lower layer, thus only ymax
  for ( ; itUV2end != itUV2 && (*itUV2).x() < xMaxUV; ++itUV2 ) {
    if(   ((*itUV2).x() > xMinUV) &&   (*itUV2).yMax() > yInZone - m_yTolUVSearch ){
      return true;
    }
  }
  return false;
}

//find matching stereo hit if available, also searching in triangle region
template<>
inline bool
PrForwardTool::matchStereoHitWithTriangle<PrHitZone::Side::Lower>(std::vector<PrHit>::const_iterator& itUV2,
                                                                  const std::vector<PrHit>::const_iterator& itUV2end,
                                                                  const float yInZone,
                                                                  const float xMinUV, const float xMaxUV) const
{
  //search for opposite side UV hit
  //test upper layer, thus only ymin
  for ( ; itUV2end != itUV2 && (*itUV2).x() < xMaxUV; ++itUV2 ) {
    if(  ((*itUV2).x() > xMinUV) && (*itUV2).yMin() < yInZone + m_yTolUVSearch  ){
      return true;
    }
  }
  return false;
}

//=========================================================================
//  Collect all X hits, within a window defined by the minimum Pt.
//  Better restrictions possible, if we use the momentum of the input track.
//  Ask for the presence of a stereo hit in the same biLayer compatible.
//  This reduces the efficiency. X-alone hits to be re-added later in the processing
//=========================================================================
template<PrHitZone::Side SIDE>
void PrForwardTool::collectAllXHits (std::vector<ModPrHit>& allXHits, const PrForwardTrack& track,
                                     const PrFTHitHandler<PrHit>& FTHitHandler,
                                     const PrParameters& pars, const float magScaleFactor) const {

  //TODO improve check if hits can be found at all
  const VeloSeed& seed = track.seed();

  //== Compute the size of the search window in the reference plane
  float dxRef = calcDxRef(seed, m_minPT);
  //dxRef *= 1.10; //== 10% tolerance
  dxRef *= 0.9; //make windows a bit too small REALLY?
  const float zMag = zMagnet(track);

  // -- Compute some thing we need later on
  const float q = seed.qOverP>0.f ? 1.f :-1.f;
  const float dir = q*magScaleFactor*(-1.f);
  
  // -- This is all for the treatment of "wrong sign tracks", ie tracks that got the wrong sign assigned in VeloUT
  // -- Only do this once (outside the loop)
  const float pt = track.track()->pt();
  const bool wSignTreatment = m_useWrongSignWindow && pt>m_wrongSignPT;
  
  float dxRefWS = 0.0;
  if( wSignTreatment ){
    dxRefWS = calcDxRef(seed, m_wrongSignPT);
    dxRefWS *= 0.9; //make windows a bit too small - FIXME check effect of this, seems wrong
  }

  // ----------------------------------

  std::array<int, 7> iZoneEnd; //6 x planes
  iZoneEnd[0] = 0;
  int cptZone = 1;

  for(unsigned int iZone = 0; iZone < PrFTInfo::NFTXLayers; iZone++) {

    const unsigned int zoneNumber   = PrFTZoneHandler::getXZone<SIDE>(iZone);
    const auto& zone = m_zoneHandler->zone(zoneNumber);

    const float zZone   = zone.z();
    const float xInZone = seed.x(zZone);
    const float yInZone = seed.y(zZone);
    //TODO do this check more clever at the beginning?!
    if ( !zone.isInside( xInZone, yInZone ) ) continue;
    
    const float xTol  = ( zZone < zReference ) ? dxRef * zZone / zReference :  dxRef * (zZone - zMag) / ( zReference - zMag );
    float xMin        = xInZone - xTol;
    float xMax        = xInZone + xTol;

    // -- Use momentum estimate from VeloUT tracks
    if( m_useMomentumEstimate && 0.f != track.seed().qOverP ){

      //TODO tune this window
      // -- Extra window to catch wrong sign tracks
      float xTolWS = 0.0;
      if( wSignTreatment ){
        xTolWS  = ( zZone < zReference ) ? dxRefWS * zZone / zReference :  dxRefWS * (zZone - zMag) / ( zReference - zMag );
      }
      
      if(dir > 0){
        xMin = xInZone - xTolWS;
      }else{
        xMax = xInZone + xTolWS;
      }
    }
    // --

    const unsigned int uvZoneNumber = PrFTZoneHandler::getUVZone<SIDE>(iZone);
    const unsigned int triangleZone = PrFTZoneHandler::getTriangleZone<SIDE>(iZone);

    // -- Use search to find the lower bound of the range of x values
    auto itH   = FTHitHandler.getIterator_lowerBound(zoneNumber, xMin);
    const auto itEnd = FTHitHandler.getIterator_lowerBound( zoneNumber,xMax );

    // range of xHits of current window ([xMin, xMax]) on the current zone
    const range_of_const_<PrHit> xHits{ itH, itEnd };
    if(UNLIKELY(xHits.empty())) continue; //otherwise crash in calculating xMinUV

    //MatchStereoHits
    const auto& zoneUv      = m_zoneHandler->zone(uvZoneNumber);
    const float xInUv       = seed.x(zoneUv.z());
    const float zRatio      = ( zoneUv.z() - zMag ) / ( zZone - zMag );
    const float dx          = yInZone * zoneUv.dxDy();//x correction from rotation by stereo angle
    const float xCentral    = xInZone + dx;
    float xPredUv = xInUv + ( itH->x() - xInZone) * zRatio - dx;//predicted hit in UV-layer
    float maxDx   = m_tolYCollectX + ( std::fabs( itH->x() -xCentral ) + std::fabs( yInZone ) ) * m_tolYSlopeCollectX;
    float xMinUV  = xPredUv - maxDx;
    auto itUV1    = FTHitHandler.getIterator_lowerBound(uvZoneNumber, xMinUV);
    const auto itUV1end = FTHitHandler.getIterator_End( uvZoneNumber );


    const float xPredUVProto =  xInUv - xInZone * zRatio - dx;
    const float maxDxProto   =  m_tolYCollectX + std::abs( yInZone ) * m_tolYSlopeCollectX;

    if(std::fabs(yInZone) > m_tolYTriangleSearch){ //cuts very slightly into distribution, 100% save cut is ~50
      //no triangle search necessary!
      for (auto& xHit : xHits) { //loop over all xHits in a layer between xMin and xMax
        const float xPredUv = xPredUVProto + xHit.x()* zRatio;//predicted hit in UV-layer
        const float maxDx   = maxDxProto   + std::fabs( xHit.x() -xCentral )* m_tolYSlopeCollectX;
        const float xMinUV  = xPredUv - maxDx;
        const float xMaxUV  = xPredUv + maxDx;
        if( matchStereoHit(itUV1, itUV1end, xMinUV, xMaxUV) ){
          allXHits.emplace_back(std::addressof(xHit), 0.f, xHit.planeCode(), 0);
        }
      }
    }else{
      //triangle search
      auto itUV2    = FTHitHandler.getIterator_lowerBound(triangleZone, xMinUV);
      const auto itUV2end = FTHitHandler.getIterator_End( triangleZone );
      for (auto& xHit : xHits) { //loop over all xHits in a layer between xMin and xMax
        const float xPredUv = xPredUVProto + xHit.x()* zRatio;//predicted hit in UV-layer
        const float maxDx   = maxDxProto   + std::fabs( xHit.x() -xCentral )* m_tolYSlopeCollectX;
        const float xMinUV  = xPredUv - maxDx;
        const float xMaxUV  = xPredUv + maxDx;
        if( matchStereoHit(itUV1, itUV1end, xMinUV, xMaxUV)
            || matchStereoHitWithTriangle<SIDE>(itUV2, itUV2end, yInZone, xMinUV, xMaxUV) ) {
          allXHits.emplace_back(std::addressof(xHit), 0.f, xHit.planeCode(), 0);
        }
      }
    }
    const int iStart = iZoneEnd[cptZone-1];
    const int iEnd = allXHits.size();
    iZoneEnd[cptZone++] = iEnd;
    if( LIKELY(!(iStart == iEnd))){
      xAtRef_SamePlaneHits(track, allXHits.begin() + iStart , allXHits.begin() + iEnd); //calc xRef for all hits on same layer
    }
  }

  if(LIKELY(cptZone == 7)){
    merge6Sorted( allXHits, iZoneEnd);
  }else{
    // default sort
    std::sort( allXHits.begin(), allXHits.end());
  }

  // -- > Debug --------
  if ( msgLevel(MSG::DEBUG) ) {
    const float xWindow = pars.maxXWindow + fabs(seed.x(zReference)) * pars.maxXWindowSlope;
    info() << "**** Processing Velo track " << track.track()->key() << " zone "
           << " Selected " << allXHits.size() << " hits, window size " << xWindow << endmsg;
  }
  // -- < Debug --------

}

//=========================================================================
//  Select the zones in the allXHits array where we can have a track
//=========================================================================
template<PrHitZone::Side SIDE>
void PrForwardTool::selectXCandidates(PrForwardTracks& trackCandidates, const PrForwardTrack& seed,
                                      std::vector<ModPrHit>& allXHits,
                                      const PrFTHitHandler<PrHit>& FTHitHandler, const PrParameters& pars) const{
  if ( allXHits.size() < pars.minXHits ) return;
  const auto itEnd = std::end(allXHits);
  const float xStraight = seed.x(zReference);
  auto it1 = std::begin(allXHits);
  auto it2 = it1;

  //Parameters for X-hit only fit, thus do not require stereo hits
  PrParameters xFitPars{pars};
  xFitPars.minStereoHits = 0;

  PrPlaneCounter pc;
  PrLineFitter lineFitter;
  PrHits coordToFit;
  coordToFit.reserve(16);
  std::array< std::vector<ModPrHit>, 12> otherHits;


  while( true ) {
    //find next unused Hits
    while (it1+ pars.minXHits - 1 < itEnd && !it1->isValid()) ++it1;
    it2 = it1 + pars.minXHits; //it2 pointing at last+1 element, like list.end()
    while (it2 <= itEnd && !(it2 - 1)->isValid()) ++it2;
    if (it2 > itEnd) break; //Minimum is after the end!

    //define search window for Cluster
    //TODO better xWindow calculation?? how to tune this???
    const float xWindow = pars.maxXWindow + (fabs(it1->coord)
                                             + fabs(it1->coord - xStraight)) * pars.maxXWindowSlope;

    //If window is to small, go one step right
    if (((it2 - 1)->coord - it1->coord) > xWindow) {
      ++it1;
      continue;
    }

    //Cluster candidate found, now count planes
    pc.clear();
    for (auto itH = it1; itH != it2; ++itH) {
      if (itH->isValid()) pc.addHit(*itH);
    }

    //Improve cluster (at the moment only add hits to the right)
    auto itLast = it2 - 1;
    while (it2 < itEnd) {
      //if last/first hit isUsed, skip this
      if (!it2->isValid()) {
        ++it2;
        continue;
      }
      //now  the first and last+1 hit exist and are not used!

      //Add next hit,
      // if there is only a small gap between the hits
      //    or inside window and plane is still empty
      if ( ( it2->coord < itLast->coord + pars.maxXGap )
           || ( (it2->coord - it1->coord < xWindow) && (pc.nbInPlane(*it2) == 0) ) ) {
        pc.addHit(*it2);
        itLast = it2;
        ++it2;
        continue;
      }
      //Found nothing to improve
      break;
    }

    //if not enough different planes, start again from the very beginning with next right hit
    if (pc.nbDifferent() < pars.minXHits) {
      ++it1;
      continue;
    }

    //====================================================================
    //  Now we have a (rather) clean candidate, do best hit selection
    //  Two possibilities:
    //  1) If there are enough planes with only one hit, do a straight
    //      line fit through these and select good matching others
    //  2) Do some magic
    //====================================================================

    coordToFit.clear();
    coordToFit.reserve(16);
    float xAtRef = 0.;
    const unsigned int nbSingle = pc.nbSingle();

    if ( nbSingle >= m_minSingleHits && nbSingle != pc.nbDifferent() ) {
      //1) we have enough single planes (thus two) to make a straight line fit

      lineFitter.reset( zReference, &coordToFit );
      for(int i=0; i < 12; i++) otherHits[i].clear();

      // -- > Debug --------
      if ( msgLevel(MSG::DEBUG) ){
        info() << "--- " << nbSingle << " planes with a single hit. Select best chi2 in other planes ---" << endmsg;
        for (auto itH = it1; it2 > itH; ++itH ) {
          if ( !itH->isValid() ) continue;
          printHit( *(itH->hit) );
        }
      }
      // -- < Debug --------

      //seperate single and double hits
      for(auto itH = it1; it2 > itH; ++itH ){
        if( !itH->isValid() ) continue;
        if( pc.nbInPlane(*itH) == 1 ){
          lineFitter.addHit(*itH);
        }else{
          otherHits[itH->planeCode].push_back(*itH);
        }
      }
      lineFitter.solve();

      //select best other hits (only best other hit is enough!)
      for(int i = 0; i < 12; i++){  //12 layers
        if(otherHits[i].empty()) continue;

        float bestChi2 = 1e9f;

        const ModPrHit* best = &(otherHits[i][0]);
        for( const auto& hit : otherHits[i] ){
          const float chi2 = lineFitter.chi2( hit );
          if( chi2 < bestChi2 ){
            bestChi2 = chi2;
            best = &hit;
          }
        }
        lineFitter.addHit( *best );
        lineFitter.solve();
      }
      xAtRef = lineFitter.coordAtRef();

    }else{
      // 2) Try to find a small distance containing at least 5(4) different planes
      //    Most of the time do nothing

      const unsigned int nPlanes =  std::min(pc.nbDifferent(),uint{5});
      std::vector<ModPrHit>::iterator itWindowStart = it1;
      std::vector<ModPrHit>::iterator itWindowEnd   = it1 + nPlanes; //pointing at last+1
      //Hit is used, go to next unused one
      while( itWindowEnd<=it2  &&  !(itWindowEnd-1)->isValid()) ++itWindowEnd;
      if( itWindowEnd > it2) continue; //start from very beginning

      float minInterval = 1.e9f;
      std::vector<ModPrHit>::iterator best     = itWindowStart;
      std::vector<ModPrHit>::iterator bestEnd  = itWindowEnd;

      PrPlaneCounter lpc;
      for (auto itH = itWindowStart; itH != itWindowEnd; ++itH) {
        if (itH->isValid()) lpc.addHit(*itH);
      }

      while ( itWindowEnd <= it2 ) {
        if ( lpc.nbDifferent() >= nPlanes ) {
          //have nPlanes, check x distance
          const float dist = (itWindowEnd-1)->coord - itWindowStart->coord;
          if ( dist < minInterval ) {
            minInterval = dist;
            best    = itWindowStart;
            bestEnd = itWindowEnd;
          }
        } else {
          //too few planes, add one hit
          ++itWindowEnd;
          while( itWindowEnd<=it2  &&  !(itWindowEnd-1)->isValid()) ++itWindowEnd;
          if( itWindowEnd > it2) break;
          lpc.addHit( *(itWindowEnd-1) );
          continue;
        }
        //move on to the right
        lpc.removeHit( *itWindowStart );
        ++itWindowStart;
        while( itWindowStart<itWindowEnd && !itWindowStart->isValid() ) ++itWindowStart;
        //last hit guaranteed to be not used. Therefore there is always at least one hit to go to. No additional if required.
      }

      //TODO tune minInterval cut value
      if ( minInterval < 1.f ) {
        it1 = best;
        it2 = bestEnd;
      }

      //Fill coords and compute average x at reference
      for ( std::vector<ModPrHit>::iterator itH = it1; it2 != itH; ++itH ) {
        if (itH->isValid()) {
          coordToFit.push_back( itH->hit );
          xAtRef += itH->coord;
        }
      }
      xAtRef /= ((float)coordToFit.size());
    }

    //=== We have a candidate :)

    //overwriting is faster than resetting, attention: values which are not overwritten do not make sense!!
    pc.clear();
    pc.set(coordToFit); //too difficult to keep track of add and delete, just do it again..
    //only unused(!) hits in coordToFit now

    bool ok = pc.nbDifferent() > 3;
    PrForwardTrack track(seed);
    if(ok){
      track.replaceHits(std::move(coordToFit));
      setTrackParameters(track, xAtRef);
      fastLinearFit(track, pars, pc);
      addHitsOnEmptyXLayers<SIDE>(track, false, FTHitHandler, pars, pc);
      ok = pc.nbDifferent() > 3;
    }
    //== Fit and remove hits...
    if (ok) ok = fitXProjection(track, xFitPars, pc);
    if (ok) ok = track.chi2PerDoF() < m_maxChi2PerDoF;
    if (ok) ok = addHitsOnEmptyXLayers<SIDE>(track, true, FTHitHandler, xFitPars, pc);
    if (ok) {
      //set ModPrHits used , challenge: we don't have the link any more!
      //Do we really need isUsed in Forward? We can otherwise speed up the search quite a lot!
      // --> we need it for the second loop
      setHitsUsed(it1, itEnd, track, xFitPars);
      trackCandidates.emplace_back(std::move(track));
      // -- > Debug --------
      if ( msgLevel(MSG::DEBUG) ) {
        info() << "=== Storing track candidate " << trackCandidates.size() << endmsg;
        printTrack(trackCandidates.back());
        info() << endmsg;
      }
      // -- < Debug --------
    }
    //next one
    ++it1;
  }
}
//=========================================================================
//  Fit a linear form, remove the external worst as long as chi2 is big...
//=========================================================================
void PrForwardTool::fastLinearFit(PrForwardTrack& track, const PrParameters& pars, PrPlaneCounter& pc) const {

  bool fit = true;
  while (fit) {
    //== Fit a line
    float s0   = 0.;
    float sz   = 0.;
    float sz2  = 0.;
    float sd   = 0.;
    float sdz  = 0.;

    for (const auto& hit : track.hits() ){
      const float zHit = hit->z();
      const float d = hit->distanceXHit(track.x(zHit));
      const float w = hit->w();
      const float z = zHit - zReference;
      s0   += w;
      sz   += w * z;
      sz2  += w * z * z;
      sd   += w * d;
      sdz  += w * d * z;
    }
    float den = (sz*sz-s0*sz2);
    if( !(std::fabs(den) > 1e-5))return;
    const float da  = (sdz * sz - sd * sz2) / den;
    const float db  = (sd *  sz - s0 * sdz) / den;
    track.addXParams<2>( {da, db} );
    fit = false;

    // -- > Debug --------
    if ( msgLevel(MSG::DEBUG) ) {
      info() << "Linear fit, current status : " << endmsg;
      printTrack( track );
    }
    // -- < Debug --------

    if ( track.hits().size() < pars.minXHits ) return;

    PrHits::iterator worst = track.hits().end();
    float maxChi2 = 0.f;
    const bool notMultiple = pc.nbDifferent() == track.hits().size();
    //TODO how many multiple hits do we normaly have?
    //how often do we do the right thing here?
    //delete two hits at same time?

    for ( auto itH = std::begin(track.hits()); std::end(track.hits()) != itH; ++itH ) {
      float chi2 = track.chi2XHit( **itH );
      if ( chi2 > maxChi2 && ( notMultiple || pc.nbInPlane( (*itH)->planeCode() ) > 1 ) ) {
        maxChi2 = chi2;
        worst   = itH;
      }
    }

    //== Remove grossly out hit, or worst in multiple layers

    if ( maxChi2 > m_maxChi2LinearFit || ( !notMultiple && maxChi2 > 4.f ) ) {

      // -- > Debug --------
      if ( msgLevel(MSG::DEBUG) ) {
        info() << "Remove hit ";
        printHit( **worst );
      }
      // -- < Debug --------

      pc.removeHit( **worst );
      //removing hit from track list, no need to keep track of isUsed
      std::iter_swap(worst,track.hits().end()-1); //faster than just erase, order does not matter
      track.hits().pop_back();
      fit = true;
    }
  }
}
//=========================================================================
//  Fit the X projection of a track, return OK if fit sucecssfull
//=========================================================================
bool PrForwardTool::fitXProjection(PrForwardTrack& track, const PrParameters& pars, PrPlaneCounter& pc) const {

  // -- > Debug --------
  if ( msgLevel(MSG::DEBUG) ) {
    info() << "  -- Entering fitXProjection with:" << endmsg;
    printTrack(track);
  }
  // -- < Debug --------

  if ( pc.nbDifferent() < pars.minXHits ) {

    // -- > Debug --------
    if ( msgLevel(MSG::DEBUG) ) info() << "  == Not enough layers ( "<< pc.nbDifferent() << " ) with hits" << endmsg;
    // -- < Debug --------

    return false;
  }

  bool doFit = true;
  while ( doFit ) {
    //== Fit a cubic
    float s0   = 0.f;
    float sz   = 0.f;
    float sz2  = 0.f;
    float sz3  = 0.f;
    float sz4  = 0.f;
    float sd   = 0.f;
    float sdz  = 0.f;
    float sdz2 = 0.f;

    for (const auto& hit : track.hits() ) {
      float d = track.distance( *hit );
      float w = hit->w();
      float z = .001f * ( hit->z() - zReference );
      s0   += w;
      sz   += w * z;
      sz2  += w * z * z;
      sz3  += w * z * z * z;
      sz4  += w * z * z * z * z;
      sd   += w * d;
      sdz  += w * d * z;
      sdz2 += w * d * z * z;
    }
    const float b1 = sz  * sz  - s0  * sz2;
    const float c1 = sz2 * sz  - s0  * sz3;
    const float d1 = sd  * sz  - s0  * sdz;
    const float b2 = sz2 * sz2 - sz * sz3;
    const float c2 = sz3 * sz2 - sz * sz4;
    const float d2 = sdz * sz2 - sz * sdz2;
    const float den = (b1 * c2 - b2 * c1 );
    if(!(std::fabs(den) > 1e-5)) return false;
    const float db  = (d1 * c2 - d2 * c1 ) / den;
    const float dc  = (d2 * b1 - d1 * b2 ) / den;
    const float da  = ( sd - db * sz - dc * sz2) / s0;
    track.addXParams<3>( {da, db*1.e-3f, dc*1.e-6f} );

    float maxChi2 = 0.f;
    float totChi2 = 0.f;
    //int   nDoF = -3; // fitted 3 parameters
    int  nDoF = -3;
    const bool notMultiple = pc.nbDifferent() == track.hits().size();

    const auto itEnd = std::end(track.hits());
    auto worst = itEnd;
    for ( auto itH = std::begin(track.hits()); itEnd != itH; ++itH ) {
      float chi2 = track.chi2( **itH );
      totChi2 += chi2;
      ++nDoF;
      if ( chi2 > maxChi2 && ( notMultiple || pc.nbInPlane( (*itH)->planeCode() ) > 1 ) ) {
        maxChi2 = chi2;
        worst   = itH;
      }
    }
    if ( nDoF < 1 )return false;
    track.setChi2nDof( {totChi2, (float)nDoF} );

    // -- > Debug --------
    if ( msgLevel(MSG::DEBUG) ) {
      info() << "  -- In fitXProjection, maxChi2 = " << maxChi2 << " totCHi2/nDof " << totChi2/nDoF << endmsg;
      printTrack( track );
    }
    // -- < Debug --------

    if ( worst == itEnd ) {
      return true;
    }
    doFit = false;
    if ( totChi2/nDoF > m_maxChi2PerDoF  ||
         maxChi2 > m_maxChi2XProjection ) {
      pc.removeHit( **worst ); //only valid "unused" hits in track.hits()
      std::iter_swap(worst,track.hits().end()-1); //faster than just erase, order does not matter
      track.hits().pop_back();

      if (pc.nbDifferent() < pars.minXHits + pars.minStereoHits) {

        // -- > Debug --------
        if ( msgLevel(MSG::DEBUG) ) {
          info() << "  == Not enough layers with hits" << endmsg;
          printTrack( track );
        }
        // -- < Debug --------

        return false;
      }
      doFit = true;
    }
  }

  // -- > Debug --------
  if ( msgLevel(MSG::DEBUG) ) {
    info() << "  -- End fitXProjection -- " << endmsg;
    printTrack( track );
  }
  // -- < Debug --------

  return true;
}

//=========================================================================
//  Fit the X projection of a track, return OK if fit sucecssfull
//=========================================================================
bool PrForwardTool::fitYProjection(PrForwardTrack& track, std::vector<const PrHit*>& stereoHits,
                                   const PrParameters& pars, PrPlaneCounter& pc) const {

  // -- > Debug --------
  if ( msgLevel(MSG::DEBUG) ) {
    info() << "  -- Entering fitYProjection with:" << endmsg;
    printTrack( track );
  }
  // -- < Debug --------

  if ( pc.nbDifferent() < pars.minStereoHits ) {
    // -- > Debug --------
    if ( msgLevel(MSG::DEBUG) ) info() << "  == Not enough layers ( "<< pc.nbDifferent() << " ) with hits" << endmsg;
    // -- < Debug --------
    return false;
  }

  float maxChi2 = 1.e9f;
  bool parabola = false; //first linear than parabola

  //== Fit a line
  const float tolYMag = m_tolYMag + m_tolYMagSlope * fabs(track.xStraight(zReference) - track.seed().x(zReference));
  const float wMag   = 1./(tolYMag * tolYMag );

  bool doFit = true;
  while ( doFit ) {

    //Use position in magnet as constrain in fit
    //although bevause wMag is quite small only little influence...
    float zMag  = zMagnet(track);
    const float dyMag = track.yStraight( zMag ) - track.seed().y( zMag );
    zMag -= zReference;
    float s0   = wMag;
    float sz   = wMag * zMag;
    float sz2  = wMag * zMag * zMag;
    float sd   = wMag * dyMag;
    float sdz  = wMag * dyMag * zMag;

    std::vector<const PrHit*>::const_iterator itEnd = stereoHits.end();

    if ( parabola ) {
      float sz2m = 0.;
      float sz3  = 0.;
      float sz4  = 0.;
      float sdz2 = 0.;

      for ( const PrHit* hit : stereoHits ){
        const float d = - track.distance( *hit ) / hit->dxDy(); //TODO multiplication much faster than division!
        const float w = hit->w();
        const float z = hit->z() - zReference;
        s0   += w;
        sz   += w * z;
        sz2m += w * z * z;
        sz2  += w * z * z;
        sz3  += w * z * z * z;
        sz4  += w * z * z * z * z;
        sd   += w * d;
        sdz  += w * d * z;
        sdz2 += w * d * z * z;
      }
      const float b1 = sz  * sz   - s0  * sz2;
      const float c1 = sz2m* sz   - s0  * sz3;
      const float d1 = sd  * sz   - s0  * sdz;
      const float b2 = sz2 * sz2m - sz * sz3;
      const float c2 = sz3 * sz2m - sz * sz4;
      const float d2 = sdz * sz2m - sz * sdz2;
      const float den = (b1 * c2 - b2 * c1 );
      if(!(std::fabs(den) > 1e-5))return false;

      const float db  = (d1 * c2 - d2 * c1 ) / den;
      const float dc  = (d2 * b1 - d1 * b2 ) / den;
      const float da  = ( sd - db * sz - dc * sz2 ) / s0;
      track.addYParams<3>( {da, db, dc} );

      // -- > Debug --------
      if (  msgLevel(MSG::DEBUG) ){
        info() << "fitYProjection Parabolic Fit da: " << da << " db: " << db  << " dc: " << dc << endmsg;
      }
      // -- < Debug --------
    } else {

      for ( const auto& hit : stereoHits ){
        const float d = - track.distance( *hit ) / hit->dxDy();
        const float w = hit->w();
        const float z = hit->z() - zReference;
        s0   += w;
        sz   += w * z;
        sz2  += w * z * z;
        sd   += w * d;
        sdz  += w * d * z;
      }
      const float den = (s0 * sz2 - sz * sz );
      if(!(std::fabs(den) > 1e-5))return false;
      const float da  = (sd * sz2 - sdz * sz ) / den;
      const float db  = (sdz * s0 - sd  * sz ) / den;
      track.addYParams<2>( {da, db} );

      // -- > Debug --------
      if (  msgLevel(MSG::DEBUG) ){
        info() << "fitYProjection Linear Fit da: " << da << " db: " << db  << endmsg;
      }
      // -- < Debug --------
    }//fit end, now doing outlier removal

    std::vector<const PrHit*>::iterator worst = std::end(stereoHits);
    maxChi2 = 0.;
    for ( std::vector<const PrHit*>::iterator itH = std::begin(stereoHits); itEnd != itH; ++itH ) {
      const float chi2 = track.chi2( **itH );
      if ( chi2 > maxChi2 ) {
        maxChi2 = chi2;
        worst   = itH;
      }
    }

    if ( maxChi2 < m_maxChi2StereoLinear && !parabola ) {
      parabola = true;
      maxChi2 = 1.e9f;
      continue;
    }

    if ( maxChi2 > m_maxChi2Stereo ) {
      pc.removeHit( **worst ); //stereo hits never 'used'
      if ( pc.nbDifferent() < pars.minStereoHits ) {

        // -- > Debug --------
        if ( msgLevel(MSG::DEBUG) ) info() << "-- not enough different planes after removing worst: " << pc.nbDifferent()
                                           << " for " << pars.minStereoHits << " --" << endmsg;
        // -- < Debug --------

        return false;
      }
      stereoHits.erase( worst );
      continue;
    }

    break;
  }

  return true;
}
//=========================================================================
//  Add hits on empty X layers, and refit if something was added
//=========================================================================
template<PrHitZone::Side SIDE>
bool PrForwardTool::addHitsOnEmptyXLayers(PrForwardTrack& track, bool fullFit,
                                          const PrFTHitHandler<PrHit>& FTHitHandler,
                                          const PrParameters& pars, PrPlaneCounter& pc) const {

  //is there an empty plane? otherwise skip here!
  if( pc.nbDifferent() > 11) return true;

  bool  added = false;
  const auto& xPars = track.getXParams();
  const float x1 = xPars.get(0);
  const float xStraight = track.seed().x(zReference);
  const float xWindow = pars.maxXWindow + ( fabs( x1 ) + fabs( x1 - xStraight ) ) * pars.maxXWindowSlope;

  for ( unsigned int iZone = 0; iZone < PrFTInfo::NFTXLayers; iZone++) {
    const unsigned int zoneNumber = PrFTZoneHandler::getXZone<SIDE>(iZone);
    if (pc.nbInPlane(zoneNumber / 2) != 0) continue;

    const float zZone =  m_zoneHandler->zone(zoneNumber).z();
    const float xPred  = track.x( zZone );
    const float minX = xPred - xWindow;
    const float maxX = xPred + xWindow;
    float bestChi2 = 1.e9f;
    const PrHit* best = nullptr;

    // -- Use a search to find the lower bound of the range of x values
    auto itH   = FTHitHandler.getIterator_lowerBound(zoneNumber, minX);
    const auto itEnd = FTHitHandler.getIterator_End(zoneNumber);
    for ( ; itEnd != itH; ++itH ) {
      if( itH->x() > maxX ) break;
      const float d = itH->distanceXHit(xPred); //fast distance good enough at this point (?!)
      const float chi2 = d*d * itH->w();
      if ( chi2 < bestChi2 ) {
        bestChi2 = chi2;
        best = &*itH;
      }
    }
    if ( nullptr != best ) {

      // -- > Debug --------
      if ( msgLevel(MSG::DEBUG) ) {
        info() << format( "AddHitOnEmptyXLayer:    chi2%8.2f", bestChi2 );
        printHit( *best, " ");
      }
      // -- < Debug --------

      track.addHit(*best);
      pc.addHit(*best);
      added = true;
    }
  }
  if ( !added ) return true;
  if ( fullFit ) {
    return fitXProjection(track, pars, pc);
  }
  fastLinearFit( track, pars, pc );
  return true;
}
//=========================================================================
//  Add hits on empty stereo layers, and refit if something was added
//=========================================================================
bool PrForwardTool::addHitsOnEmptyStereoLayers ( PrForwardTrack& track, std::vector<const PrHit*>& stereoHits,
                                                 const PrFTHitHandler<PrHit>& FTHitHandler, const PrParameters& pars,
                                                 PrPlaneCounter& pc) const {

  //at this point pc is counting only stereo HITS!
  if(pc.nbDifferent()  > 5) return true;

  bool added = false;
  for ( unsigned int zoneNumber = 0; PrFTInfo::nbZones() > zoneNumber; zoneNumber += 1 ) {
    if ( pc.nbInPlane( zoneNumber/2 ) != 0 ) continue; //there is already one hit
    const auto& zone = m_zoneHandler->zone(zoneNumber); //maybe we want it outside to be faster?
    if ( zone.isX() ) continue;  // exclude X zones

    float zZone = zone.z();
    float yZone = track.y( zZone );
    zZone = zone.z( yZone );  // Correct for dzDy
    yZone = track.y( zZone );
    const float xPred  = track.x( zZone );

    const bool triangleSearch = std::fabs(yZone) < m_tolYTriangleSearch;
    if(!triangleSearch && (2.f*float(((zoneNumber%2)==0))-1.f) * yZone > 0.f) continue;

    //only version without triangle search!
    const float dxTol = m_tolY + m_tolYSlope * ( fabs( xPred - track.seed().x(zZone)) + fabs(yZone) );
    // -- Use a binary search to find the lower bound of the range of x values
    // -- This takes the y value into account
    auto itH   = FTHitHandler.getIterator_lowerBound(zoneNumber, -dxTol - yZone * zone.dxDy() + xPred);
    const auto itEnd = FTHitHandler.getIterator_End( zoneNumber );

    const PrHit* best = nullptr;
    float bestChi2 = m_maxChi2Stereo;
    if(triangleSearch){
      for ( ; itEnd != itH; ++itH ) {
        const float dx = itH->x( yZone ) - xPred ;
        if ( dx >  dxTol ) break;
        if( yZone > itH->yMax() + m_yTolUVSearch)continue;
        if( yZone < itH->yMin() - m_yTolUVSearch)continue;
        const float d = itH->distance(xPred,yZone);
        const float chi2 = d*d * itH->w();
        if ( chi2 < bestChi2 ) {
          bestChi2 = chi2;
          best = &*itH;
        }
      }
    }else{
      //no triangle search, thus no min max check
      for ( ; itEnd != itH; ++itH ) {
        const float dx = itH->x( yZone ) - xPred ;
        if ( dx >  dxTol ) break;
        const float d = itH->distance(xPred,yZone);
        const float chi2 = d*d * itH->w();
        if ( chi2 < bestChi2 ) {
          bestChi2 = chi2;
          best = &*itH;
        }
      }
    }


    if ( nullptr != best ) {
      // -- > Debug --------
      if ( msgLevel(MSG::DEBUG) ) {
        info() << format( "AddHitOnEmptyStereoLayer:    chi2%8.2f", bestChi2 );
        printHit( *best, " ");
        info() << "zZone: " << zZone << " xpred: " << xPred << " dxTol " << dxTol << endmsg;
      }
      // -- < Debug --------
      stereoHits.push_back(best);
      pc.addHit(*best);
      added = true;
    }
  }
  if ( !added ) return true;
  return fitYProjection( track, stereoHits, pars, pc );
}

//=========================================================================
//  Collect all hits in the stereo planes compatible with the track
//=========================================================================
std::vector<ModPrHit> PrForwardTool::collectStereoHits(PrForwardTrack& track,
                                                       const PrFTHitHandler<PrHit>& FTHitHandler) const {
  std::vector<ModPrHit> stereoHits;
  stereoHits.reserve(PrFTInfo::nbZones());

  // -- > Debug --------
  if ( msgLevel(MSG::DEBUG) ) info() << "== Collecte stereo hits. wanted ones: " << endmsg;
  // -- < Debug --------

  for ( const auto& zoneNumber : PrFTInfo::stereoZones ) {
    const auto& zone = m_zoneHandler->zone(zoneNumber);
    float zZone = zone.z();
    const float yZone = track.y( zZone );
    zZone = zone.z( yZone );  // Correct for dzDy
    const float xPred  = track.x( zZone );

    const bool triangleSearch = std::fabs(yZone) < m_tolYTriangleSearch;
    if(!triangleSearch && (2.f*float((zoneNumber%2)==0)-1.f) * yZone > 0.f) continue;

    //float dxDySign = 1.f - 2.f *(float)(zone.dxDy()<0); // same as ? zone.dxDy()<0 : -1 : +1 , but faster??!!
    const float dxDySign = zone.dxDy()<0?-1.f:1.f;
    const float dxTol = m_tolY + m_tolYSlope * (std::fabs(xPred - track.seed().x(zZone)) + std::fabs(yZone));

    // -- Use a binary search to find the lower bound of the range of x values
    // -- This takes the y value into account
    auto itH   = FTHitHandler.getIterator_lowerBound(zoneNumber, -dxTol - yZone * zone.dxDy() + xPred);
    const auto itEnd = FTHitHandler.getIterator_End( zoneNumber );
    if(triangleSearch){
      for ( ; itEnd != itH; ++itH ) {
        const float dx = itH->x( yZone ) - xPred ;
        if ( dx >  dxTol ) break;
        if( yZone > itH->yMax() + m_yTolUVSearch)continue;
        if( yZone < itH->yMin() - m_yTolUVSearch)continue;
        stereoHits.emplace_back(&*itH, dx*dxDySign, itH->planeCode(), 0);
      }
    }else{ //no triangle search, thus no min max check
      for ( ; itEnd != itH; ++itH ) {
        const float dx = itH->x( yZone ) - xPred ;
        if ( dx >  dxTol ) break;
        stereoHits.emplace_back(&*itH, dx*dxDySign, itH->planeCode(), 0);
      }
    }
  }
  
  return stereoHits;
}
//=========================================================================
//  Fit the stereo hits
//=========================================================================
bool PrForwardTool::selectStereoHits( PrForwardTrack& track, const PrFTHitHandler<PrHit>& FTHitHandler,
                                      const std::vector<ModPrHit>& allStereoHits,
                                      const PrParameters& pars) const {
  //why do we rely on xRef? --> coord is NOT xRef for stereo HITS!

  std::vector<const PrHit*> bestStereoHits;
  trackPars<3> originalYParams(track.getYParams());
  trackPars<3> bestYParams;
  float bestMeanDy       = 1e9f;
  auto minStereoHits = pars.minStereoHits;

  auto beginRange = std::begin(allStereoHits) - 1;
  if(minStereoHits > allStereoHits.size()) return false; //otherwise crash if minHits is too large
  auto endLoop = std::end(allStereoHits) - pars.minStereoHits;
  PrPlaneCounter pc;
  while ( beginRange < endLoop ) {
    ++beginRange;

    // -- > Debug --------
    if ( msgLevel(MSG::DEBUG) ) {
      info() << " stereo start at ";
      printHit( *(beginRange->hit) );
    }
    // -- > Debug --------

    pc.clear(); // counting now stereo hits
    auto endRange = beginRange;
    float sumCoord = 0.;
    while( pc.nbDifferent() < minStereoHits ||
           endRange->coord < (endRange-1)->coord + m_minYGap ) {
      pc.addHit( *endRange );
      sumCoord += endRange->coord;
      ++endRange;
      if ( endRange == allStereoHits.end() ) break;
    }

    //clean cluster
    while( true ) {
      const float averageCoord = sumCoord / float(endRange-beginRange);

      // remove first if not single and farest from mean
      if ( pc.nbInPlane( *beginRange ) > 1 &&
           ((averageCoord - beginRange->coord) > 1.0f * ((endRange-1)->coord - averageCoord)) ) { //tune this value has only little effect?!
        pc.removeHit( *beginRange );
        sumCoord -= (beginRange++)->coord;
        continue;
      }

      if(endRange == allStereoHits.end()) break; //already at end, cluster cannot be expanded anymore

      //add next, if it decreases the range size and is empty
      if ( (pc.nbInPlane(*endRange) == 0)
           &&(averageCoord - beginRange->coord > endRange->coord - averageCoord )) {
        pc.addHit( *endRange );
        sumCoord += (endRange++)->coord;
        continue;
      }

      break;
    }

    // -- > Debug --------
    if ( msgLevel(MSG::DEBUG) ) {
      info() << "Selected stereo range from " << endmsg;
      printHit( *(beginRange->hit)  );
      printHit( *((endRange-1)->hit) );
    }
    // -- < Debug --------

    //Now we have a candidate, lets fit him

    //track = original; //only yparams are changed
    track.setYParams(originalYParams);
    std::vector<const PrHit*> trackStereoHits;
    trackStereoHits.reserve(std::distance(beginRange, endRange));
    std::transform(beginRange, endRange, std::back_inserter(trackStereoHits),
                   [](const ModPrHit& hit) { return hit.hit; });

    //fit Y Projection of track using stereo hits
    if(!fitYProjection(track, trackStereoHits, pars, pc))continue;

    if(!addHitsOnEmptyStereoLayers(track, trackStereoHits, FTHitHandler, pars, pc))continue;

    if(trackStereoHits.size() < bestStereoHits.size()) continue; //number of hits most important selection criteria!

    //== Calculate  dy chi2 /ndf
    float meanDy = 0.;
    for ( const auto& hit : trackStereoHits ){
      const float d = track.distance( *hit ) / hit->dxDy();
      meanDy += d*d;
    }
    meanDy /=  float(trackStereoHits.size()-1);

    if ( trackStereoHits.size() > bestStereoHits.size() || meanDy < bestMeanDy  ){
      // if same number of hits take smaller chi2
      // -- > Debug --------
      if ( msgLevel(MSG::DEBUG) ) {
        info() << "************ Store candidate, nStereo " << trackStereoHits.size() << " meanDy " << meanDy << endmsg;
      }
      // -- < Debug --------
      bestYParams = track.getYParams();
      bestMeanDy     = meanDy;
      bestStereoHits = std::move(trackStereoHits);
    }

  }
  if ( bestStereoHits.size() > 0 ) {
    track.setYParams(bestYParams); //only y params have been modified
    track.addHits( bestStereoHits );
    return true;
  }
  return false;
}

//=========================================================================
//  Convert the local track to the LHCb representation
//=========================================================================
void PrForwardTool::makeLHCbTracks ( PrForwardTracks& trackCandidates, LHCb::Tracks& result )const {

  auto validEnd = std::remove_if(trackCandidates.begin(), trackCandidates.end(),
                                  [](const auto& track) { return not track.valid(); });
  const range_of_const_<PrForwardTrack> validTracks{ trackCandidates.begin(), validEnd };

  for (const auto& cand : validTracks ){
    LHCb::Track* tmp = cand.track()->clone();
    tmp->setType( LHCb::Track::Types::Long );
    tmp->setHistory( LHCb::Track::History::PrForward );
    tmp->addToAncestors( cand.track() );

    const double qOverP  = cand.getQoP();
    const double errQop2 = 0.1 * 0.1 * qOverP * qOverP;

    for ( const auto& state : tmp->states() ){
      state->setQOverP( qOverP );
      state->setErrQOverP2( errQop2 );
    }

    LHCb::State tState;
    const double z = StateParameters::ZEndT;
    tState.setLocation( LHCb::State::AtT );
    tState.setState( cand.x( z ), cand.y( z ), z, cand.xSlope( z ), cand.ySlope( z ), qOverP );

    //== overestimated covariance matrix, as input to the Kalman fit

    tState.setCovariance( m_geoTool->covariance( qOverP ) );
    tmp->addToStates( tState );

    //== LHCb ids.

    tmp->setPatRecStatus( LHCb::Track::PatRecStatus::PatRecIDs );

    for (const auto& hit : cand.hits() ){
      tmp->addToLhcbIDs( hit->id() );
    }
    tmp->setChi2PerDoF( cand.chi2PerDoF() );
    tmp->setNDoF(       cand.nDoF() );
    tmp->addInfo( LHCb::Track::AdditionalInfo::PatQuality, cand.quality() );

    //ADD UT hits on track
    if ( nullptr != m_addUTHitsTool  &&  !m_useMomentumEstimate) { //FIXME switch of if veloUT tracks as input
      StatusCode sc = m_addUTHitsTool->addUTHits( *tmp );
      if (sc.isFailure()){
        if(  msgLevel(MSG::DEBUG) )
          debug()<<" Failure in adding UT hits to track"<<endmsg;
      }
    }

    result.insert( tmp );

    // -- > Debug --------
    if ( msgLevel(MSG::DEBUG) ) info() << "Store track " << (*(result.end()-1))->key() << " quality " << (*(result.end()-1))->info( LHCb::Track::AdditionalInfo::PatQuality, 0. ) << endmsg;
    // -- < Debug --------
  }

}

//=========================================================================
//  Merge the 6 layers of x hits for the hough trafo
//=========================================================================
inline
void PrForwardTool::merge6Sorted(std::vector<ModPrHit>& allXHits, const std::array<int, 7>& boundaries) const {
  std::array<std::vector<ModPrHit>::iterator, 7> offset;

  std::transform( std::begin(boundaries), std::end(boundaries),
                  std::begin(offset),
                  [start = std::begin(allXHits)](int b)
                  { return start + b; } );

  std::inplace_merge(offset[0], offset[1], offset[2]);
  std::inplace_merge(offset[2], offset[3], offset[4]);
  std::inplace_merge(offset[0], offset[2], offset[4]);
  std::inplace_merge(offset[4], offset[5], offset[6]);
  std::inplace_merge(offset[0], offset[4], offset[6]);
}

void PrForwardTool::setHitsUsed(std::vector<ModPrHit>::iterator it, const std::vector<ModPrHit>::iterator& itEnd,
                                const PrForwardTrack& track, const PrParameters& pars) const{
  //Hits before it1 are not checked. Thus this method does not work perfect. However, it seems good enough :)

  const auto& xPars = track.getXParams();
  const float x1 = xPars.get(0);

  while(it < itEnd ){
    if(!it->isValid()){
      ++it;
      continue;
    }
    if( it->coord > (x1 + 2 * pars.maxXWindow)) break;
    //search hit in track
    for(const PrHit* trackHit : track.hits()){
      if( it->hit == trackHit){
        it->setInvalid(); // coord == max means the same as hit is deleted
        break;
      }
    }
    ++it;
  }
}

// ############ original PrGeometryTool #######################3

//=========================================================================
//  Compute the x projection at the reference plane
//=========================================================================
inline
void PrForwardTool::xAtRef_SamePlaneHits(const PrForwardTrack& track,
                                         std::vector<ModPrHit>::iterator itH, 
                                         const std::vector<ModPrHit>::iterator itEnd) const {
  //calculate xref for this plane
  const float zHit    = itH->hit->z(); //all hits in same layer
  //const float yHit    = track.yFromVelo( zHit );
  const float xFromVelo_Hit = track.seed().x(zHit);
  const float zMagSlope = zMagnetParams[2] * track.seed().tx2 +  zMagnetParams[3] * track.seed().ty2;

  const float tx = track.seed().tx;
  const float dSlopeDivPart = 1.f / ( zHit - zMagnetParams[0]);
  const float dz      = 1.e-3f * ( zHit - zReference );

  // Vector of float
  Vc::float_v xHits;

  while( itEnd>itH ){
    std::vector<ModPrHit>::iterator itH2 = itH;
    for(unsigned int i = 0; i < Vc::float_v::Size; ++i, ++itH2) {
      xHits[i] = itH2<itEnd?itH2->hit->x():0.f;
    }

    const Vc::float_v dSlope  = ( xFromVelo_Hit - xHits ) * dSlopeDivPart;
    const Vc::float_v zMag    = zMagnetParams[0] + zMagnetParams[1] *  dSlope * dSlope  + zMagSlope;
    const Vc::float_v xMag    = xFromVelo_Hit + tx * (zMag - zHit);
    const Vc::float_v dxCoef  = dz * dz * ( xParams[0] + dz * xParams[1] ) * dSlope;
    const Vc::float_v ratio   = (  zReference - zMag ) / ( zHit - zMag );
    const Vc::float_v x       = xMag + ratio * (xHits + dxCoef  - xMag);
    xHits = x;

    for(unsigned int i = 0; i < Vc::float_v::Size && itH != itEnd; ++i, ++itH) {
      itH->coord = xHits[i];
    }
  }

}

//=========================================================================
//  Set the parameters of the track, from the (average) x at reference
//=========================================================================
void PrForwardTool::setTrackParameters ( PrForwardTrack& track, const float xAtRef) const {

  float dSlope  = (track.seed().x(zReference) - xAtRef) / (zReference - zMagnetParams[0]);
  const float zMagSlope = zMagnetParams[2] * track.seed().tx2 +  zMagnetParams[3] * track.seed().ty2;
  const float zMag    = zMagnetParams[0] + zMagnetParams[1] *  dSlope * dSlope  + zMagSlope;
  assert(zMag != zReference && "zMag can not be equal to zReference");
  const float xMag    = track.seed().x( zMag );
  const float slopeT  = (xAtRef - xMag) / (zReference - zMag);
  dSlope              = slopeT - track.seed().tx;
  const float dyCoef  = dSlope * dSlope * track.seed().ty;

  track.setParams( {xAtRef,
                    slopeT,
                    1.e-6f * xParams[0] * dSlope,
                    1.e-9f * xParams[1] * dSlope,
                    track.seed().y(zReference),
                    track.seed().ty + dyCoef * byParams[0],
                    dyCoef * cyParams[0] });
}

//=========================================================================
//  Returns the best momentum estimate
//=========================================================================
inline
float PrForwardTool::calcqOverP ( const PrForwardTrack& track, const float magScaleFactor) const {

  float qop{1.0f/Gaudi::Units::GeV} ;
  if( std::abs(magScaleFactor) > 1e-6f ) {
    const float bx   = track.xSlope( zReference );
    const float bx2  = bx * bx;
    const float coef = ( momentumParams[0] +
                         momentumParams[1] * bx2 +
                         momentumParams[2] * bx2 * bx2 +
                         momentumParams[3] * bx * track.seed().tx +
                         momentumParams[4] * track.seed().ty2 +
                         momentumParams[5] * track.seed().ty2 * track.seed().ty2 );
    const float proj = sqrt( ( 1.f + track.seed().slope2 ) / ( 1.f + track.seed().tx2 ) );
    qop = ( track.seed().tx - bx ) / ( coef * Gaudi::Units::GeV * proj * magScaleFactor) ;
  }
  return qop ;

}

//=========================================================================
//  Returns estimate of magnet kick position
//=========================================================================
inline
float PrForwardTool::zMagnet(const PrForwardTrack& track) const {
  return ( zMagnetParams[0] +
           zMagnetParams[2] * track.seed().tx2 +
           zMagnetParams[3] * track.seed().ty2 );
}
