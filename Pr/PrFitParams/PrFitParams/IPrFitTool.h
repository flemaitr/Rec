#ifndef IPRFITTOOL_H 
#define IPRFITTOOL_H 1

// Include files
// from boost
#include <boost/optional.hpp>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "PrFitParams/LinParFit.h"

struct IPrFitTool : extend_interfaces<IAlgTool>
{
  DeclareInterfaceID (IPrFitTool, 1, 0);

  enum class XY { X, Y };

  virtual boost::optional<std::tuple<double, double>>
      fitLine(const std::vector<Gaudi::XYZPoint>& hit, XY mode, double z0) const =0;
  
  virtual boost::optional<std::tuple<double, double, double>>
      fitParabola(const std::vector<Gaudi::XYZPoint>& hit, XY mode, double z0) const =0;
  
  virtual boost::optional<std::tuple<double, double, double, double>>
      fitCubic(const std::vector<Gaudi::XYZPoint>& hit, XY mode, double z0) const =0;


};
#endif // IPRFITTOOL_H
