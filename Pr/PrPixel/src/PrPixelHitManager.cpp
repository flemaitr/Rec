#include <cmath>

// LHCb
#include "Event/RawEvent.h"
#include "Event/VPCluster.h"
// Local
#include "PrPixelHitManager.h"

// ranges
#include <range/v3/algorithm.hpp>
#include <range/v3/view.hpp>

namespace {
   using namespace PrPixel;
   using namespace ranges;
}

DECLARE_COMPONENT( PrPixelHitManager )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrPixelHitManager::PrPixelHitManager(const std::string &type,
                                     const std::string &name,
                                     const IInterface *parent) :
GaudiTool(type, name, parent) {
  declareInterface<PrPixelHitManager>(this);
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrPixelHitManager::initialize() {

  StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure()) return sc;
  m_isDebug = msgLevel(MSG::DEBUG);
  if (m_isDebug) debug() << "==> Initialize" << endmsg;
  // Get detector element.
  m_vp = getDet<DeVP>(DeVPLocation::Default);
  // Make sure we are up-to-date on populated VELO stations
  registerCondition(m_vp->sensors().front()->geometry(),
                    &PrPixelHitManager::rebuildGeometry);
  // First update
  sc = updMgrSvc()->update(this);
  if (!sc.isSuccess()) {
    return Error("Failed to update station structure.");
  }

  // zero out all buffers
  // note: gcc needs -O3 before it converts std::array::fill(0)
  //       into a call to memset ;-(
  std::memset(m_sp_patterns.data(),0,sizeof(m_sp_patterns));
  std::memset(m_sp_sizes.data(),0,sizeof(m_sp_sizes));
  std::memset(m_sp_fx.data(),0,sizeof(m_sp_fx));
  std::memset(m_sp_fy.data(),0,sizeof(m_sp_fy));

  // create pattern buffer for isolated SPs
  cacheSPPatterns();

  return StatusCode::SUCCESS;
}

//=========================================================================
// Cache Super Pixel cluster patterns.
//=========================================================================
void PrPixelHitManager::cacheSPPatterns() {

  // create a cache for all super pixel cluster patterns.
  // this is an unoptimized 8-way flood fill on the 8 pixels
  // in the super pixel.
  // no point in optimizing as this is called once in
  // initialize() and only takes about 20 us.

  // define deltas to 8-connectivity neighbours
  const int dx[] = {-1, 0, 1, -1, 0, 1, -1, 1};
  const int dy[] = {-1, -1, -1, 1, 1, 1, 0, 0};

  // clustering buffer for isolated superpixels.
  unsigned char sp_buffer[8];

  // SP index buffer and its size for single SP clustering
  unsigned char sp_idx[8];

  // stack and stack pointer for single SP clustering
  unsigned char sp_stack[8];
  unsigned char sp_stack_ptr = 0;

  // loop over all possible SP patterns
  for (unsigned int sp = 0; sp < 256; ++sp) {
    unsigned char sp_idx_size = 0;
    for (unsigned int shift = 0; shift < 8; ++shift) {
      const unsigned char p = sp & (1 << shift);
      sp_buffer[shift] = p;
      if (p) {
        sp_idx[sp_idx_size++] = shift;
      }
    }

    // loop over pixels in this SP and use them as
    // cluster seeds.
    // note that there are at most two clusters
    // in a single super pixel!
    unsigned char clu_idx = 0;
    for (unsigned int ip = 0; ip < sp_idx_size; ++ip) {
      unsigned char idx = sp_idx[ip];

      if (0 == sp_buffer[idx]) {
        continue;
      }  // pixel is used

      sp_stack_ptr = 0;
      sp_stack[sp_stack_ptr++] = idx;
      sp_buffer[idx] = 0;
      unsigned char x = 0;
      unsigned char y = 0;
      unsigned char n = 0;

      while (sp_stack_ptr) {
        idx = sp_stack[--sp_stack_ptr];
        const unsigned char row = idx % 4;
        const unsigned char col = idx / 4;
        x += col;
        y += row;
        ++n;

        for (unsigned int ni = 0; ni < 8; ++ni) {
          const char ncol = col + dx[ni];
          if (ncol < 0 || ncol > 1) continue;
          const char nrow = row + dy[ni];
          if (nrow < 0 || nrow > 3) continue;
          const unsigned char nidx = (ncol << 2) | nrow;
          if (0 == sp_buffer[nidx]) continue;
          sp_stack[sp_stack_ptr++] = nidx;
          sp_buffer[nidx] = 0;
        }
      }

      const uint32_t cx = x / n;
      const uint32_t cy = y / n;
      const float fx = x / static_cast<float>(n) - cx;
      const float fy = y / static_cast<float>(n) - cy;

      m_sp_sizes[sp] |= n << (4 * clu_idx);

      // store the centroid pixel
      m_sp_patterns[sp] |= ((cx << 2) | cy) << 4 * clu_idx;

      // set the two cluster flag if this is the second cluster
      m_sp_patterns[sp] |= clu_idx << 3;

      // set the pixel fractions
      m_sp_fx[2 * sp + clu_idx] = fx;
      m_sp_fy[2 * sp + clu_idx] = fy;

      // increment cluster count. note that this can only become 0 or 1!
      ++clu_idx;
    }
  }

  if (m_isDebug) {
    debug() << "==== SP Patterns Start ====" << endmsg;
    for (unsigned int i = 0; i < 256; ++i) {
      unsigned char pattern = m_sp_patterns[i];
      debug() << "-- pattern " << i << " --" << endmsg;
      for (int shift = 3; shift >= 0; --shift) {
        debug() << ((i >> shift) & 1) << (i >> (shift + 4) & 1) << endmsg;
      }
      debug() << (pattern & 3) << " " << ((pattern >> 2) & 1);
      if (pattern & 8) {
        debug() << "\t" << ((pattern >> 4) & 3) << " " << ((pattern >> 6) & 1)
                << endmsg;
      } else {
        debug() << "\tn/a" << endmsg;
      }
    }
    debug() << "==== SP Patterns End   ====" << endmsg;
  }
  return;
}

//============================================================================
// Rebuild the geometry (in case something changes in the Velo during the run)
//============================================================================
StatusCode PrPixelHitManager::rebuildGeometry() {

  // Delete the existing modules.
  m_modules.clear();
  m_firstModule = 999;
  m_lastModule = 0;

  int previousLeft = -1;
  int previousRight = -1;
  const auto sensors = m_vp->sensors();
  // get pointers to local x coordinates and pitches
  m_local_x = sensors.front()->xLocal().data();
  m_x_pitch = sensors.front()->xPitch().data();
  m_pixel_size = sensors.front()->pixelSize(LHCb::VPChannelID(0, 0, 0, 0)).second;

  float ltg_rot_components[9];
  for (auto sensor : sensors) {
    // TODO:
    // if (!sensor->isReadOut()) continue;

    // get the local to global transformation matrix and
    // store it in a flat float array of sixe 12.
    Gaudi::Rotation3D ltg_rot;
    Gaudi::TranslationXYZ ltg_trans;
    sensor->geometry()->toGlobalMatrix().GetDecomposition(ltg_rot, ltg_trans);
    ltg_rot.GetComponents(ltg_rot_components);
    unsigned idx = 16 * sensor->sensorNumber();
    m_ltg[idx++] = ltg_rot_components[0];
    m_ltg[idx++] = ltg_rot_components[1];
    m_ltg[idx++] = ltg_rot_components[2];
    m_ltg[idx++] = ltg_rot_components[3];
    m_ltg[idx++] = ltg_rot_components[4];
    m_ltg[idx++] = ltg_rot_components[5];
    m_ltg[idx++] = ltg_rot_components[6];
    m_ltg[idx++] = ltg_rot_components[7];
    m_ltg[idx++] = ltg_rot_components[8];
    m_ltg[idx++] = ltg_trans.X();
    m_ltg[idx++] = ltg_trans.Y();
    m_ltg[idx++] = ltg_trans.Z();

    // Get the number of the module this sensor is on.
    const unsigned int number = sensor->module();
    if (number < m_modules.size()) {
      // Check if this module has already been setup.
      if (m_modules[number]) continue;
    } else {
      m_modules.resize(number + 1, 0);
    }

    // Create a new module and add it to the list.
    m_module_pool.emplace_back(number, sensor->isRight());
    PrPixelModule *module = &m_module_pool.back();
    module->setZ(sensor->z());
    if (sensor->isRight()) {
      module->setPrevious(previousRight);
      previousRight = number;
    } else {
      module->setPrevious(previousLeft);
      previousLeft = number;
    }
    m_modules[number] = module;
    if (m_firstModule > number) m_firstModule = number;
    if (m_lastModule < number) m_lastModule = number;
  }
  // the module pool might have been resized -- make sure
  // all module pointers are valid.
  for (unsigned int i = 0; i < m_module_pool.size(); ++i) {
    PrPixelModule *module = &m_module_pool[i];
    m_modules[module->number()] = module;
  }
  if (msgLevel(MSG::DEBUG)) {
    debug() << "Found modules from " << m_firstModule << " to " << m_lastModule
            << endmsg;
    for (const auto& module: m_modules) {
      if (module) {
        debug() << "  Module " << module->number() << " prev "
                << module->previous() << endmsg;
      }
    }
  }

  return StatusCode::SUCCESS;
}

//=========================================================================
// Build PrPixelHits from raw bank.
//=========================================================================
bool PrPixelHitManager::buildHitsFromRawBank( const LHCb::RawEvent& rawEvent,
                                              std::vector<PrPixelModuleHits>& modulehits, LHCb::VPLightClusters& clusters,
                                              bool storecluster ) const
{
  auto& tBanks = rawEvent.banks(LHCb::RawBank::VP);
  if ( UNLIKELY( tBanks.empty() ) ) return true;

  const unsigned int version = (*tBanks.begin())->version();
  if ( UNLIKELY( version != 2 ) ) {
    warning() << "Unsupported raw bank version (" << version << ")" << endmsg;
    return false;
  }

  // WARNING:
  // This is a rather long function. Please refrain from breaking this
  // up into smaller functions as this will severely impact the
  // timing performance. And yes, this has been measured. Just don't.

  // Clustering buffers
  std::array<unsigned char, VP::NPixelsPerSensor> buffer{};
  std::vector<uint32_t> pixel_idx;
  std::vector<uint32_t> stack;

  // reserve a minimal stack
  stack.reserve(64);

  for( auto& mhits : modulehits) {
    mhits.getXVec().reserve(200);
    mhits.getYVec().reserve(200);
    mhits.getZVec().reserve(200);
    mhits.getIDVec().reserve(200);
  }

  std::vector<float> xFractions; xFractions.reserve(200);
  std::vector<float> yFractions; yFractions.reserve(200);
  // ChannelIDs for offline mode
  std::vector<std::vector<LHCb::VPChannelID>> channelIDs;

  // Loop over VP RawBanks
  for (auto iterBank = tBanks.begin(); iterBank != tBanks.end(); ++iterBank) {

    xFractions.clear();
    yFractions.clear();
    channelIDs.clear();

    const unsigned int sensor = (*iterBank)->sourceID();
    const unsigned int module = sensor / VP::NSensorsPerModule;
    auto& hitsXVec = modulehits[module].getXVec();
    auto& hitsYVec = modulehits[module].getYVec();
    auto& hitsZVec = modulehits[module].getZVec();
    auto& hitsIDVec = modulehits[module].getIDVec();
    const float *ltg = m_ltg + 16 * sensor;

    // reset and then fill the super pixel buffer for a sensor
    // memset(m_sp_buffer,0,256*256*3*sizeof(unsigned char));
    // the memset is too slow here. the occupancy is so low that
    // resetting a few elements is *much* faster.
    const unsigned int nrc = pixel_idx.size();
    for (unsigned int irc = 0; irc < nrc; ++irc) {
      buffer[pixel_idx[irc]] = 0;
    }
    pixel_idx.clear();

    const uint32_t *bank = (*iterBank)->data();
    const uint32_t nsp = *bank++;

    for (unsigned int i = 0; i < nsp; ++i) {
      const uint32_t sp_word = *bank++;
      uint8_t sp = sp_word & 0xFFU;

      if (0 == sp) continue;  // protect against zero super pixels.

      const uint32_t sp_addr = (sp_word & 0x007FFF00U) >> 8;
      const uint32_t sp_row = sp_addr & 0x3FU;
      const uint32_t sp_col = (sp_addr >> 6);
      const uint32_t no_sp_neighbours = sp_word & 0x80000000U;

      // if a super pixel is isolated the clustering boils
      // down to a simple pattern look up.
      // don't do this if we run in offline mode where we want to record all
      // contributing channels; in that scenario a few more us are negligible
      // compared to the complication of keeping track of all contributing
      // channel IDs.
      if (m_trigger && no_sp_neighbours) {
        const int sp_size = m_sp_sizes[sp];
        const uint32_t idx = m_sp_patterns[sp];
        const uint32_t chip = sp_col / (CHIP_COLUMNS / 2);

        if ((sp_size & 0x0F) <= m_maxClusterSize) {
          // there is always at least one cluster in the super
          // pixel. look up the pattern and add it.
          const uint32_t row = idx & 0x03U;
          const uint32_t col = (idx >> 2) & 1;
          const uint32_t cx = sp_col * 2 + col;
          const uint32_t cy = sp_row * 4 + row;

          LHCb::VPChannelID cid(sensor, chip, cx % CHIP_COLUMNS, cy);

          const float fx = m_sp_fx[sp * 2];
          const float fy = m_sp_fy[sp * 2];
          const float local_x = m_local_x[cx] + fx * m_x_pitch[cx];
          const float local_y = (cy + 0.5 + fy) * m_pixel_size;

          //gx
          hitsXVec.emplace_back(ltg[0] * local_x + ltg[1] * local_y + ltg[9]);
          //gy
          hitsYVec.emplace_back(ltg[3] * local_x + ltg[4] * local_y + ltg[10]);
          //gz
          hitsZVec.emplace_back(ltg[6] * local_x + ltg[7] * local_y + ltg[11]);

          hitsIDVec.emplace_back(cid);
          xFractions.push_back(fx);
          yFractions.push_back(fy);
        }

        // if there is a second cluster for this pattern
        // add it as well.
        if ((idx & 8) && (((sp_size >> 4) & 0x0F) <= m_maxClusterSize)) {
          const uint32_t row = (idx >> 4) & 3;
          const uint32_t col = (idx >> 6) & 1;
          const uint32_t cx = sp_col * 2 + col;
          const uint32_t cy = sp_row * 4 + row;

          LHCb::VPChannelID cid(sensor, chip, cx % CHIP_COLUMNS, cy);

          const float fx = m_sp_fx[sp * 2 + 1];
          const float fy = m_sp_fy[sp * 2 + 1];
          const float local_x = m_local_x[cx] + fx * m_x_pitch[cx];
          const float local_y = (cy + 0.5 + fy) * m_pixel_size;

          //gx
          hitsXVec.emplace_back(ltg[0] * local_x + ltg[1] * local_y + ltg[9]);
          //gy
          hitsYVec.emplace_back(ltg[3] * local_x + ltg[4] * local_y + ltg[10]);
          //gz
          hitsZVec.emplace_back(ltg[6] * local_x + ltg[7] * local_y + ltg[11]);

          hitsIDVec.emplace_back(cid);
          xFractions.push_back(fx);
          yFractions.push_back(fy);
        }

        continue;  // move on to next super pixel
      }

      // this one is not isolated or we are targeting clusters; record all
      // pixels.
      for (uint32_t shift = 0; shift < 8; ++shift) {
        const uint8_t pixel = sp & 1;
        if (pixel) {
          const uint32_t row = sp_row * 4 + shift % 4;
          const uint32_t col = sp_col * 2 + shift / 4;
          const uint32_t idx = (col << 8) | row;
          buffer[idx] = pixel;
          pixel_idx.push_back(idx);
        }
        sp = sp >> 1;
        if (0 == sp) break;
      }
    }  // loop over super pixels in raw bank

    // the sensor buffer is filled, perform the clustering on
    // clusters that span several super pixels.
    const unsigned int nidx = pixel_idx.size();
    for (unsigned int irc = 0; irc < nidx; ++irc) {

      const uint32_t idx = pixel_idx[irc];
      const uint8_t pixel = buffer[idx];

      if (0 == pixel) continue;  // pixel is used in another cluster

      // 8-way row scan optimized seeded flood fill from here.
      stack.clear();

      // mark seed as used
      buffer[idx] = 0;

      // initialize sums
      unsigned int x = 0;
      unsigned int y = 0;
      unsigned int n = 0;

      // push seed on stack
      stack.push_back(idx);

      // as long as the stack is not exhausted:
      // - pop the stack and add popped pixel to cluster
      // - scan the row to left and right, adding set pixels
      //   to the cluster and push set pixels above and below
      //   on the stack (and delete both from the pixel buffer).
      while (!stack.empty()) {


        // pop pixel from stack and add it to cluster
        const uint32_t idx = stack.back();
        stack.pop_back();
        const uint32_t row = idx & 0xFFU;
        const uint32_t col = (idx >> 8) & 0x3FFU;
        x += col;
        y += row;
        ++n;

        if (!m_trigger) {
          const uint32_t chip = col / CHIP_COLUMNS;
          LHCb::VPChannelID cid(sensor, chip, col % CHIP_COLUMNS, row);
          if( n == 1){
            channelIDs.push_back({{cid}});
          }else{
            channelIDs.back().push_back( cid);
          }
        }

        // check up and down
        uint32_t u_idx = idx + 1;
        if (row < VP::NRows - 1 && buffer[u_idx]) {
          buffer[u_idx] = 0;
          stack.push_back(u_idx);
        }
        uint32_t d_idx = idx - 1;
        if (row > 0 && buffer[d_idx]) {
          buffer[d_idx] = 0;
          stack.push_back(d_idx);
        }

        // scan row to the right
        for (unsigned int c = col + 1; c < VP::NSensorColumns; ++c) {
          const uint32_t nidx = (c << 8) | row;
          // check up and down
          u_idx = nidx + 1;
          if (row < VP::NRows - 1 && buffer[u_idx]) {
            buffer[u_idx] = 0;
            stack.push_back(u_idx);
          }
          d_idx = nidx - 1;
          if (row > 0 && buffer[d_idx]) {
            buffer[d_idx] = 0;
            stack.push_back(d_idx);
          }
          // add set pixel to cluster or stop scanning
          if (buffer[nidx]) {
            buffer[nidx] = 0;
            x += c;
            y += row;
            ++n;
            if (!m_trigger) {
              const uint32_t chip = c / CHIP_COLUMNS;
              LHCb::VPChannelID cid(sensor, chip, c % CHIP_COLUMNS, row);
              channelIDs.back().push_back(cid);
            }
          } else {
            break;
          }
        }

        // scan row to the left
        for (int c = col - 1; c >= 0; --c) {
          const uint32_t nidx = (c << 8) | row;
          // check up and down
          u_idx = nidx + 1;
          if (row < VP::NRows - 1 && buffer[u_idx]) {
            buffer[u_idx] = 0;
            stack.push_back(u_idx);
          }
          d_idx = nidx - 1;
          if (row > 0 && buffer[d_idx]) {
            buffer[d_idx] = 0;
            stack.push_back(d_idx);
          }
          // add set pixel to cluster or stop scanning
          if (buffer[nidx]) {
            buffer[nidx] = 0;
            x += c;
            y += row;
            ++n;
            if (!m_trigger) {
              const uint32_t chip = c / CHIP_COLUMNS;
              LHCb::VPChannelID cid(sensor, chip, c % CHIP_COLUMNS, row);
              channelIDs.back().push_back(cid);
            }
          } else {
            break;
          }
        }
      }  // while the stack is not empty

      // we are done with this cluster, calculate
      // centroid pixel coordinate and fractions.
      if (n <= m_maxClusterSize) {
        // if the pixel is smaller than the max cluster size, store it for the tracking
        const unsigned int cx = x / n;
        const unsigned int cy = y / n;
        const float fx = x / static_cast<float>(n) - cx;
        const float fy = y / static_cast<float>(n) - cy;

        xFractions.push_back(fx);
        yFractions.push_back(fy);

        // store target (3D point for tracking)
        const uint32_t chip = cx / CHIP_COLUMNS;
        LHCb::VPChannelID cid(sensor, chip, cx % CHIP_COLUMNS, cy);

        const float local_x = m_local_x[cx] + fx * m_x_pitch[cx];
        const float local_y = (cy + 0.5 + fy) * m_pixel_size;
        //gx
        hitsXVec.emplace_back(ltg[0] * local_x + ltg[1] * local_y + ltg[9]);
        //gy
        hitsYVec.emplace_back(ltg[3] * local_x + ltg[4] * local_y + ltg[10]);
        //gz
        hitsZVec.emplace_back(ltg[6] * local_x + ltg[7] * local_y + ltg[11]);

        hitsIDVec.emplace_back(cid);
      } else if (!m_trigger) {
        // Offline mode, store all 3D points
        const unsigned int cx = x / n;
        const unsigned int cy = y / n;
        const float fx = x / static_cast<float>(n) - cx;
        const float fy = y / static_cast<float>(n) - cy;

        // store target (cluster and 3D point for tracking)
        const uint32_t chip = cx / CHIP_COLUMNS;
        LHCb::VPChannelID cid(sensor, chip, cx % CHIP_COLUMNS, cy);
        const float local_x = m_local_x[cx] + fx * m_x_pitch[cx];
        const float local_y = (cy + 0.5 + fy) * m_pixel_size;
        const float gx = ltg[0] * local_x + ltg[1] * local_y + ltg[9];
        const float gy = ltg[3] * local_x + ltg[4] * local_y + ltg[10];
        const float gz = ltg[6] * local_x + ltg[7] * local_y + ltg[11];
        channelIDs.pop_back();
        clusters.emplace( std::piecewise_construct, std::make_tuple(cid.channelID()),
                          std::forward_as_tuple( fx, fy, gx, gy, gz, cid.channelID() ) );
      }
    }  // loop over all potential seed pixels
    if(storecluster){
      if(m_trigger){
        storeTriggerClusters( modulehits[module], xFractions, yFractions, clusters );
      }else{
        storeOfflineClusters( modulehits[module], xFractions, yFractions, channelIDs, clusters );
      }
    }
  }    // loop over all banks

  return true;
}

//=========================================================================
// Store clusters.
//=========================================================================
void PrPixelHitManager::storeOfflineClusters( PrPixelModuleHits& mhits, std::vector<float>& xFractions,
                                              std::vector<float>& yFractions,
                                              std::vector<std::vector<LHCb::VPChannelID>> channelIDs,
                                              LHCb::VPLightClusters& clusters ) const
{

  auto& hitsXVec = mhits.getXVec();
  auto& hitsYVec = mhits.getYVec();
  auto& hitsZVec = mhits.getZVec();
  auto& hitsIDVec = mhits.getIDVec();

  const unsigned offset = hitsXVec.size() - xFractions.size();
  unsigned channel;
  for ( size_t i = 0; i < xFractions.size(); ++i ) {
    //const LHCb::VPChannelID cid {hitsIDVec[offset+i].vpID()};
    channel = hitsIDVec[offset+i].vpID().channelID();
    // It is possible that two clusters have the same centroid
    // channel ID. This is extremely rare but we have to protect
    // against it. First come, first serve.
    // TODO: carefully study whether these rare cases can create
    // a significant systematic. It's very doubtful, but still...
    //if ( UNLIKELY( clusters.object( cid ) != nullptr ) ) {
      //warning() << "Duplicate VP channel ID in clustering (offline): " << cid << endmsg;
      //continue;
    //}

    clusters.emplace( std::piecewise_construct, std::make_tuple(channel),
                      std::forward_as_tuple( xFractions[i], yFractions[i], hitsXVec[offset + i], hitsYVec[offset + i],
                                             hitsZVec[offset + i], channel, std::move(channelIDs[i]) ) );
  }
}

void PrPixelHitManager::storeTriggerClusters( PrPixelModuleHits& mhits, std::vector<float>& xFractions,
                                              std::vector<float>& yFractions, LHCb::VPLightClusters& clusters ) const
{

  // In trigger configuration, only  clusters for 3D points are created
  // that were handed to the pixel tracking. The list of contributing channel
  // IDs for each cluster is not available in this scenario. We will make one
  // entry in the list which is the centroid pixel channel ID. It is possible
  // that this refers to a pixel that has not fired. Hence reliable MC matching
  // is not possible in trigger configuration.
  auto& hitsXVec = mhits.getXVec();
  auto& hitsYVec = mhits.getYVec();
  auto& hitsZVec = mhits.getZVec();
  auto& hitsIDVec = mhits.getIDVec();

  const int offset = hitsXVec.size() - xFractions.size();
  unsigned channel;
  for ( size_t i = 0; i < xFractions.size(); ++i ) {
    //const LHCb::VPChannelID cid {hitsIDVec[offset+i].vpID()};
    channel = hitsIDVec[offset+i].vpID().channelID();

    // It is possible that two clusters have the same centroid
    // channel ID. This is extremely rare but we have to protect
    // against it. First come, first serve.
    // TODO: carefully study whether these rare cases can create
    // a significant systematic. It's very doubtful, but still...
    //if ( UNLIKELY( clusters.object( cid ) != nullptr ) ) {
      //warning() << "Duplicate VP channel ID in clustering (trigger): " << cid << endmsg;
      //continue;
    //}

    clusters.emplace( std::piecewise_construct, std::make_tuple(channel),
                      std::forward_as_tuple( xFractions[i], yFractions[i], hitsXVec[offset + i], hitsYVec[offset + i],
                                             hitsZVec[offset + i], channel ) );
  }
}

//=========================================================================
StatusCode PrPixelHitManager::process(const LHCb::RawEvent& rawEvent,
                                      std::vector<PrPixelModuleHits>& modulehits,
                                      LHCb::VPLightClusters& clusters,
                                      bool storecluster) const {

  // Storage for extra hits that don't go into clusters for the tracking
  if (!buildHitsFromRawBank(rawEvent, modulehits, clusters, storecluster)) {
    return Error("Cannot retrieve/decode raw bank.", StatusCode::SUCCESS);
  }

  // sort the hits in each module container
  // FIXME currently sorting the flags as well but they are all 0...
  //auto comp = []( const std::tuple<float, float, float, LHCb::LHCbID>& h1,
                   //const std::tuple<float, float, float, LHCb::LHCbID>& h2 ) {
    //printf("%3.10g < %3.10g %i\n", std::get<0>( h1 ), std::get<0>( h2 ), (std::get<0>( h1 ) < std::get<0>( h2 )));
    //return std::get<0>( h1 ) < std::get<0>( h2 );
  //};

  for( auto & mhits : modulehits){
    auto& hitsXVec = mhits.getXVec();
    auto& hitsYVec = mhits.getYVec();
    auto& hitsZVec = mhits.getZVec();
    auto& hitsIDVec = mhits.getIDVec();

    //for (size_t i =0 ; i<hitsXVec.size(); ++i) info() << format("x %3.10g y%3.10g ", hitsXVec[i] ,hitsYVec[i]) << endmsg;

    auto perm = sort_permutation( hitsXVec, []( const float& a, const float& b ) {
      //printf( "%3.10g < %3.10g %i\n", a, b, ( a <b ) );
      return a < b;
    } );
    apply_permutation_in_place(hitsXVec, perm);
    apply_permutation_in_place(hitsYVec, perm);
    apply_permutation_in_place(hitsZVec, perm);
    apply_permutation_in_place(hitsIDVec, perm);

    //auto zipped = view::zip(hitsXVec, hitsYVec, hitsZVec, hitsIDVec);
    //ranges::sort(zipped, comp);
    mhits.createIsUsedVec();
  }

  return StatusCode::SUCCESS;
}
