// LHCb
#include "Event/Track.h"
#include "Event/StateParameters.h"
#include "Kernel/VPConstants.h"
// Local
#include "PrPixelTracking.h"
#include <range/v3/algorithm.hpp>
#include <range/v3/view.hpp>

DECLARE_COMPONENT(PrPixelTracking)
namespace{
  using namespace ranges;
}
//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrPixelTracking::PrPixelTracking(const std::string &name,
                                 ISvcLocator *pSvcLocator) :
MultiTransformer(name , pSvcLocator,
                 KeyValue{"RawEventLocations",
                          Gaudi::Functional::concat_alternatives(LHCb::RawEventLocation::Velo,
                                                                 LHCb::RawEventLocation::Default,
                                                                 LHCb::RawEventLocation::Other)},
                 {KeyValue{"OutputTracksName", LHCb::TrackLocation::Velo},
                  KeyValue{"ClusterLocation", LHCb::VPClusterLocation::Light}})
{
  // use the square of the scatter to avoid calls to sqrt()
  m_maxScatter.declareUpdateHandler([this](Property&) {
      m_maxScatterSq = std::pow( m_maxScatter.value(), 2 );
      }).useUpdateHandler();
  // Note that we need to explicitly call `useUpdateHandler` to make sure the derived value is consistent
}


//=============================================================================
// Initialization
//=============================================================================
StatusCode PrPixelTracking::initialize() {

  StatusCode sc = MultiTransformer::initialize();
  if (sc.isFailure()) return sc;
  // Setup the hit manager.
  m_hitManager->setMaxClusterSize(m_maxClusterSize);
  m_hitManager->setTrigger(m_trigger);

  // always set Histo top dir if we are debugging histos
#ifdef DEBUG_HISTO
    setHistoTopDir("VP/");
#endif

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<LHCb::Tracks, LHCb::VPLightClusters> PrPixelTracking::operator()(const LHCb::RawEvent& rawEvent) const {
  // Build hits from clusters and sort them by global x within each module.

  // create hits and clusters
  std::vector<PrPixelModuleHits> modulehits(m_hitManager->lastModule()+1);
  LHCb::VPLightClusters clusters;
  clusters.reserve(5000);
  StatusCode sc = m_hitManager->process(rawEvent, modulehits, clusters, m_storecluster);
  if (UNLIKELY(sc.isFailure())) {
    throw GaudiException("Failure in getting hits!", "PrPixelTracking" , sc);
  }


  //info() << "printing hits of " << modulehits.size() << " modules" << endmsg;
  //for (auto& m : modulehits){
    //info() << "printing next module with: " << m.size() << " hits" << endmsg;
    //for (size_t hit = 0; hit < m.size(); ++hit) info() << m.getX(hit) << " " << m.getY(hit) << " " << m.getZ(hit) << " " << m.getID(hit) <<  endmsg;
  //}

  // Search for tracks by finding a pair of hits, then extrapolating.
  LHCb::Tracks outputTracks;
  searchByPair(modulehits, outputTracks);

#ifdef DEBUG_HISTO
    monitor(modulehits, outputTracks, clusters);
#endif
  //int i = 0;
    //for ( auto tr : outputTracks ){
      //info() << "Track: " << ++i << endmsg;
      //for ( auto& id : tr->lhcbIDs() ) info() << id.lhcbID() << endmsg;
    //}
    return std::make_tuple( std::move( outputTracks ), std::move( clusters ) );
}

//=============================================================================
// Extend track towards smaller z,
// on both sides of the detector as soon as one hit is missed.
//=============================================================================
void PrPixelTracking::extendTrack( std::vector<PrPixelModuleHits>& modulehits, std::array<size_t,71>& hitbuffer) const
{

  // Initially scan every second module (stay on the same side).
  int step = 2;
  // Start two modules behind the last one.
  int next = hitbuffer[3] - step;
  // Count modules without hits found.
  unsigned nbMissed = 0;
  unsigned foundHits =4;

  while ( next >= 0 ) {
    //// Attempt to add new hits from this module with given tolerances
    //info() << "normal call: " << next << endmsg;
    const size_t h3 = bestHit( modulehits, next, foundHits, hitbuffer );

    if ( h3 < 999999999 ) {
      //info() << "adding hit" << endmsg;
      hitbuffer[++foundHits] =  next ;
      hitbuffer[++foundHits] =  h3;

      // Reset missed hit counter.
      nbMissed = 0;
      //// Update the pair of hits to be used for extrapolating.
    } else {
      // No hits found.


      if ( step == 2 ) {
        //info() << "miss but calling againg with: " << next + 1 << endmsg;
        // Look on the other side.
        const size_t h3 = bestHit( modulehits, next + 1, foundHits, hitbuffer );

        if ( h3 <  999999999) {
          //info() << "adding hit" << endmsg;
          hitbuffer[++foundHits] =  next + 1 ;
          hitbuffer[++foundHits] =  h3;
        } else {
          //info() << "missed again" << endmsg;
          nbMissed += step;
        }
        // Switch to scanning every module (left and right).
        step = 1;
      } else {
        //info() << "standard miss: " << nbMissed<< endmsg;
        ++nbMissed;
      }
    }
    if ( m_maxMissed < nbMissed ) break;
    next -= step;
  }
  hitbuffer[0] = foundHits;
}

//=========================================================================
//  Search starting with a pair of consecutive modules.
//=========================================================================
void PrPixelTracking::searchByPair( std::vector<PrPixelModuleHits>& modulehits,
                                    LHCb::Tracks& outputTracks ) const
{

  // Get the range of modules to start search on,
  // starting with the one at largest Z.
  const size_t lastModule = m_hitManager->lastModule();
  const size_t firstModule = m_hitManager->firstModule() + 4;

  // cache 3-hit tracks to only fit if the hits are really unused after the pattern reco is done
  std::vector<size_t> ThreeHitVec;
  ThreeHitVec.reserve(600);
  PrPixelTrack FitTrack = PrPixelTrack(35);
  std::array<size_t, 71> hitbuffer{};

  for (size_t sens0 = lastModule; firstModule <= sens0; --sens0) {
    // Pick-up the "paired" module one station backwards
    const size_t sens1 = sens0 - 2;
    const float z0 = m_hitManager->module(sens0)->z();
    const float z1 = m_hitManager->module(sens1)->z();
    const float dz = z0 - z1;
    // Calculate the search window from the slope limits.
    const float dxMax = m_maxXSlope * fabs(dz);
    const float dyMax = m_maxYSlope * fabs(dz);
    // Loop over hits in the first module (larger Z) in the pair.
    auto& m0hits = modulehits[sens0];
    auto& m1hits = modulehits[sens1];
    auto first1 = 0;

    //info() << "Sensors: " << sens0 << " " << sens1 << endmsg;
    for (size_t hit0 = 0; hit0 < m0hits.size(); ++hit0) {
      // Skip hits already assigned to tracks.
      //info() << "hit0: " << m0hits.getX(hit0)<< " " << m0hits.getY(hit0)<< " " << m0hits.IsUsed(hit0) << endmsg;
      if (m0hits.IsUsed(hit0)) continue;
      const float x0 = m0hits.getX(hit0);
      const float y0 = m0hits.getY(hit0);
      // Calculate x-pos. limits on the other module.
      const float xMin = x0 - dxMax;
      const float xMax = x0 + dxMax;

      // Loop over hits in the second module (smaller Z) in the pair.
      for (size_t hit1 = first1; hit1 < m1hits.size(); ++hit1) {
        const float x1 = m1hits.getX(hit1);
        const float y1 = m1hits.getY(hit1);
        // Skip hits below the X-pos. limit.
        //info() << "hit1: " << m1hits.getX(hit1)<< " " << m1hits.getY(hit1)<< " " << m1hits.IsUsed(hit1) << endmsg;
        if (x1 < xMin) {
          first1 = hit1 + 1;
          continue;
        }
        // Stop search when above the X-pos. limit.
        if (x1 > xMax) break;
        // Skip hits already assigned to tracks.
        if (m1hits.IsUsed(hit1)) continue;
        // Skip hits out of Y-pos. limit.
        if (fabs(y1 - y0) > dyMax) continue;

        // Make a seed track out of these two hits.
        // Extend the seed track towards smaller Z.
        hitbuffer[1] = sens0;
        hitbuffer[2] = hit0;
        hitbuffer[3] = sens1;
        hitbuffer[4] = hit1;
        //info() << "extendTrack" << endmsg;
        extendTrack(modulehits, hitbuffer);

        if ( hitbuffer[0] < 6 ) continue;
        if ( hitbuffer[0] == 6 ) {
          ThreeHitVec.insert(ThreeHitVec.end(), hitbuffer.begin()+1, hitbuffer.begin() + 1 + hitbuffer[0]);
          //info() << "=== Store 3 - track " << endmsg;
          continue;
        }
        unsigned unUsed = 0;
        for ( size_t idx = 1; idx < 1 + hitbuffer[0]; idx += 2 )
          if ( !modulehits[hitbuffer[idx]].IsUsed( hitbuffer[idx+1] ) ) ++unUsed;
        if ( 2 * unUsed < hitbuffer[0] * m_fractionUnused.value() ) continue;

        //for (const auto idx : hitbuffer) IsUsedVec.set( idx );
        FitTrack.fill(modulehits, hitbuffer);
        FitTrack.fit();
        makeLHCbTracks(FitTrack, hitbuffer, modulehits, outputTracks);
        //info() << "=== Store track nhits " << hitbuffer[0]/2 << endmsg;
        //printTrack(FitTrack);

        //info() << "BREAKING " << endmsg;
        break;
      } // hit1
    } //hit0
  } // sensor loop
  //info() << "starting 3 hit loop " << ThreeHitVec.size()/6 << endmsg;
  for(size_t i = 0; i < ThreeHitVec.size(); i+=6){
    const size_t i0 = ThreeHitVec[i];
    const size_t i1 = ThreeHitVec[i + 2];
    const size_t i2 = ThreeHitVec[i + 4];
    PrPixelModuleHits& mod0 = modulehits[i0];
    PrPixelModuleHits& mod1 = modulehits[i1];
    PrPixelModuleHits& mod2 = modulehits[i2];
    const size_t h0 = ThreeHitVec[i + 1];
    const size_t h1 = ThreeHitVec[i + 3];
    const size_t h2 = ThreeHitVec[i + 5];
    if( mod0.IsUsed(h0) || mod1.IsUsed(h1) || mod2.IsUsed(h2)) continue;
    FitTrack.fill(mod0, mod1, mod2, h0, h1 ,h2);
    FitTrack.fit();
    if ( FitTrack.chi2() > m_maxChi2Short.value()) continue;
    hitbuffer[0] = 6;
    for(size_t j = 1; j<7; ++j) hitbuffer[j] = ThreeHitVec[i - 1 + j];
    makeLHCbTracks(FitTrack, hitbuffer, modulehits, outputTracks);
    //makeLHCbTracks(FitTrack, {6,i0,h0,i1,h1,i2,h2}, modulehits, outputTracks);
  }
}

//=========================================================================
// Convert the local tracks to LHCb tracks
//=========================================================================
void PrPixelTracking::makeLHCbTracks( PrPixelTrack& track, const std::array<size_t, 71>& hitbuffer,
                                      std::vector<PrPixelModuleHits>& modulehits, LHCb::Tracks& outputTracks ) const
{


  // Create a new LHCb track.
  LHCb::Track *newTrack = new LHCb::Track(outputTracks.size());
  newTrack->setType(LHCb::Track::Velo);
  newTrack->setHistory(LHCb::Track::PatFastVelo);
  newTrack->setPatRecStatus(LHCb::Track::PatRecIDs);

  // Sort the hits back by decreasing z.
  // FIXME there used to be a sort here but it seems to be redundant?
  //auto& x = track.hitsX();
  //auto& y = track.hitsY();
  //auto& z = track.hitsZ();
  //auto& i = track.hitsID();
  //auto zipped = view::zip( x,y,z,i);
  //auto comp   = []( const std::tuple<float, float, float, LHCb::LHCbID>& h1,
                  //const std::tuple<float, float, float, LHCb::LHCbID>& h2 ) {
    //return std::get<2>( h2 ) < std::get<2>( h1 );
  //};
  //sort(zipped, comp);

  //std::sort(track.hits().begZin(), track.hits().end(),
            //[](SOA::PrPixelHit::pointer a, SOA::PrPixelHit::pointer b)
            //{return b->z() < a->z();});

  // Loop over the hits and add their LHCbIDs to the LHCb track.
  for ( size_t idx = 1; idx < 1 + hitbuffer[0]; idx += 2 ){
    //info() << "ID check: " << IDVec[idx].lhcbID() << endmsg;
    newTrack->addToLhcbIDs(modulehits[hitbuffer[idx]].getID(hitbuffer[idx+1]));
  }

  // Decide if this is a forward or backward track.
  // Calculate z where the track passes closest to the beam.
  const float zBeam = track.zBeam();
  // Define backward as z closest to beam downstream of hits.
  const bool backward = zBeam > track.hitsZ().front();
  newTrack->setFlag(LHCb::Track::Backward, backward);

  // Get the state at zBeam from the straight line fit.
  LHCb::State state;
  state.setLocation(LHCb::State::ClosestToBeam);
  state.setState(track.state(zBeam));
  state.setCovariance(track.covariance(zBeam));

  // Parameters for kalmanfit scattering. calibrated on MC, shamelessly
  // hardcoded:
  const float tx = state.tx();
  const float ty = state.ty();
  const float scat2 = 1e-8 + 7e-6 * (tx * tx + ty * ty);

  // The logic is a bit messy in the following, so I hope we got all cases
  // right
  if (m_stateClosestToBeamKalmanFit ||
      m_addStateFirstLastMeasurementKalmanFit) {
    // Run a K-filter with scattering to improve IP resolution
    LHCb::State upstreamstate;
    track.fitKalman(upstreamstate, backward ? 1 : -1, scat2);
    // Add this state as state at first measurement if requested
    if (m_addStateFirstLastMeasurementKalmanFit) {
      upstreamstate.setLocation(LHCb::State::FirstMeasurement);
      newTrack->addToStates(upstreamstate);
    }
    // Transport the state to the closestToBeam position
    if (m_stateClosestToBeamKalmanFit) {
      upstreamstate.setLocation(LHCb::State::ClosestToBeam);
      upstreamstate.linearTransportTo(zBeam);
      newTrack->addToStates(upstreamstate);
    }
  }
  if (!m_stateClosestToBeamKalmanFit) {
    newTrack->addToStates(state);
  }

  // Set state at last measurement, if requested
  if ((!backward && m_stateEndVeloKalmanFit) ||
      m_addStateFirstLastMeasurementKalmanFit) {
    LHCb::State downstreamstate;
    track.fitKalman(downstreamstate, backward ? -1 : +1, scat2);
    if (m_addStateFirstLastMeasurementKalmanFit) {
      downstreamstate.setLocation(LHCb::State::LastMeasurement);
      newTrack->addToStates(downstreamstate);
    }
    if (m_stateEndVeloKalmanFit) {
      state = downstreamstate;
    }
  }

  // Add state at end of velo
  if (!backward) {
    state.setLocation(LHCb::State::EndVelo);
    state.linearTransportTo(StateParameters::ZEndVelo);
    newTrack->addToStates(state);
  }

  // Set the chi2/dof
  newTrack->setNDoF(hitbuffer[0] - 4);
  newTrack->setChi2PerDoF(track.chi2());
  // Add the LHCb track to the list.
  outputTracks.insert(newTrack);

}

//=========================================================================
// Add hits from the specified module to the track
//=========================================================================
size_t PrPixelTracking::bestHit( std::vector<PrPixelModuleHits>& modulehits, const size_t next,
                                                  const unsigned foundHits, std::array<size_t, 71>& hitbuffer ) const
{
  PrPixelModuleHits& m0 = modulehits[hitbuffer[foundHits - 3]];
  PrPixelModuleHits& m1 = modulehits[hitbuffer[foundHits - 1]];
  PrPixelModuleHits& m2 = modulehits[next];

  if (m2.empty()) return 999999999;

  const size_t hit0 = hitbuffer[foundHits - 2];
  const size_t hit1 = hitbuffer[foundHits];

  const float x0 = m0.getX(hit0);
  const float z0 = m0.getZ(hit0);
  const float x1 = m1.getX(hit1);
  const float z1 = m1.getZ(hit1);

  //info() << "BestHit start: " << x0 << " " << m0.getY(hit0) << " " << z0 << " " << x1 << " " << m1.getY(hit1) << " " << z1 << endmsg;

  const float td = 1.0 / (z1 - z0);
  const float txn = (x1 - x0);
  const float tx = txn * td;

  // Extrapolate to the z-position of the module
  const float module_z = m_hitManager->module(next)->z();
  const float xGuess = x0 + tx * (module_z - z0);

  // If the first hit is already below this limit we can stop here.
  if (m2.lastHitX() < xGuess - m_extraTol) return 999999999;
  if (m2.firstHitX() > xGuess + m_extraTol) return 999999999;

  const float y0 = m0.getY(hit0);
  const float y1 = m1.getY(hit1);
  const float tyn = (y1 - y0);
  const float ty = tyn * td;

  // Do a binary search through the hits.
  size_t hit_start{0};
  size_t step(m2.size());
  while (2 < step) {  // quick skip of hits that are above the X-limit
    step /= 2;
    if ( m2.getX(hit_start + step) < xGuess-m_extraTol) hit_start += step;
  }

  //info() << "start x at : " << m2.getX(hit_start) << endmsg;
  // Find the hit that matches best.

#ifdef DEBUG_HISTO
  unsigned int nFound = 0;
#endif

  float bestScatter = m_maxScatterSq;
  size_t bestHit = 999999999;
  for (size_t i = hit_start; i < m2.size(); ++i) {

    const float hit_x = m2.getX(i);
    const float hit_z = m2.getZ(i);

    //info() <<  hit_x << endmsg;

    const float dz = hit_z - z0;
    const float xPred = x0 + tx * dz;

#ifdef DEBUG_HISTO
    plot((hit.x() - xPred) / m_extraTol, "HitExtraErrPerTol",
         "Hit X extrapolation error / tolerance", -4.0, +4.0, 400);
#endif

    // If x-position is above prediction + tolerance, keep looking.
    if (hit_x + m_extraTol < xPred) continue;
    //info() <<  hit_x << endmsg;
    // If x-position is below prediction - tolerance, stop the search.
    if (hit_x - m_extraTol > xPred) break;
    //info() <<  hit_x << endmsg;

    const float hit_y = m2.getY(i);
    const float yPred = y0 + ty * dz;
    const float dy = yPred - hit_y;
    // Skip hits outside the y-position tolerance.
    if (fabs(dy) > m_extraTol) continue;
    //info() <<  hit_x << endmsg;
    const float scatterDenom = 1.0 / (hit_z - z1);
    const float dx = xPred - hit_x;
    const float scatterNum = (dx * dx) + (dy * dy);
    const float scatter = scatterNum * scatterDenom * scatterDenom;
    if (scatter < bestScatter) {
      //info() <<  hit_x << endmsg;
      bestHit = i;
      bestScatter = scatter;
    }
#ifdef DEBUG_HISTO
    if (scatter < m_maxScatterSq) ++nFound;
    plot(sqrt(scatter), "HitScatter", "hit scatter [rad]", 0.0, 0.5, 500);
    plot2D(dx, dy, "Hit_dXdY",
           "Difference between hit and prediction in x and y [mm]", -1, 1, -1,
           1, 500, 500);
#endif
  }
#ifdef DEBUG_HISTO
  plot(nFound, "HitExtraCount",
       "Number of hits within the extrapolation window with chi2 within limits"
       0.0, 10.0, 10);
#endif
  //if ( bestHit < 999999999 ) info() << "at end of BestHit: " << m2.getX( bestHit ) << endmsg;
  return bestHit;
}

//=========================================================================
// Print all hits on a track.
//=========================================================================
void PrPixelTracking::printTrack(PrPixelTrack& track) const {
  for (size_t hit=0; hit < track.size(); ++hit) {
    info() << format(" x%8.3f y%8.3f z%8.2f",
                     track.hitsX()[hit], track.hitsY()[hit], track.hitsZ()[hit]);//, track.hitsID()[hit].lhcbID());
    info() << endmsg;
  }
}

#ifdef DEBUG_HISTO
//=========================================================================
// countHits
//=========================================================================
std::tuple<unsigned int, unsigned int>
PrPixelTracking::countHits(std::vector<PrPixelModuleHits>& modulehits) const {
  unsigned int n = 0;
  unsigned int nUsed = 0;
  for (const auto& modulehit: modulehits) {
    for (const auto& hit: modulehit.hits()) {
      ++n;
      if (hit.isUsed()) {
        ++nUsed;
      }
    }
  }
  return std::make_tuple(n, nUsed);
}

//=========================================================================
// monitor
//=========================================================================
StatusCode PrPixelTracking::monitor(std::vector<PrPixelModuleHits>& modulehits,
                                    const LHCb::Tracks& tracks,
                                    const LHCb::VPClusters& clusters) const {
  const unsigned int firstModule = m_hitManager->firstModule();
  const unsigned int lastModule = m_hitManager->lastModule();
  for (unsigned int i = firstModule; i < lastModule; ++i) {
    for (const auto& hit : modulehits[i].hits()) {
      const float x = hit.x();
      const float y = hit.y();
      const float z = hit.z();
      const float r = sqrt(x * x + y * y);
      if (!hit.isUsed()) {
        plot3D(x, y, z, "UnusedHits3D", "Distribution of Unused Hits",
               -50., 50., -50., 50., -500., 800., 100, 100, 200);
        plot2D(r, z, "UnusedHitsRZ", "Distribution of Unused Hits",
               0., 60., -500., 800., 100, 100);
        plot2D(x, y, "UnusedHitsXY", "Distribution of Unused Hits",
               -50., 50., -50., 50., 100, 100);
      }
      plot3D(x, y, z, "Hits3D", "Distribution of Hits",
             -50., 50., -50., 50., -500., 800., 100, 100, 200);
      plot2D(r, z, "HitsRZ", "Distribution of Hits",
             0., 60., -500., 800., 100, 100);
      plot2D(x, y, "HitsXY", "Distribution of Hits",
             -50., 50., -50., 50., 100, 100);
    }
  }

  // count nb hits and nb used hits
  const std::tuple<unsigned int, unsigned int> nbhits = countHits(modulehits);
  plot(std::get<0>(nbhits), "HitsPerEvent", "Number of hits per event", 0.0, 8000.0, 80);
  if (std::get<0>(nbhits) > 0) {
    plot(100. * std::get<1>(nbhits) / std::get<0>(nbhits), "PercentUsedHitsPerEvent",
         "Percentage of hits assigned to tracks", 0.0, 100.0, 100);
  }

  unsigned int nFwd = 0;
  unsigned int nBwd = 0;
  for (const auto& track : tracks) {
    const bool bwd = track->checkFlag(LHCb::Track::Backward);
    const unsigned int nHitsPerTrack = track->lhcbIDs().size();
    const float chi2 = track->chi2PerDoF();
    const float eta = track->pseudoRapidity();
    const float phi = track->phi() / Gaudi::Units::degree;
    if (bwd) {
      ++nBwd;
      plot(nHitsPerTrack, "BwdHitsPerTrack",
           "Number of hits per backward track", 0.5, 40.5, 40);
      plot(chi2, "BwdChi2", "Chi2/DoF of backward tracks", 0., 10., 50);
      plot(eta, "BwdEta", "Pseudorapidity of backward tracks", 1., 6., 50);
      plot(phi, "BwdPhi", "Phi-angle of backward tracks", -180., 180., 60);
      plot2D(eta, nHitsPerTrack, "BwdHitsPerTrackVsEta",
             "Hits/track vs pseudorapidity of backward tracks",
             1., 6., 0.5, 15.5, 50, 15);
      plot2D(eta, chi2, "BwdChi2VsEta",
             "Chi2/DoF vs pseudorapidity of backward tracks",
             1., 6., 0., 10., 50, 20);
      plot2D(nHitsPerTrack, chi2, "BwdChi2VsHitsPerTrack",
             "Chi2/DoF vs hits/backward track", 0.5, 15.5, 0., 10., 15, 20);
    } else {
      ++nFwd;
      plot(nHitsPerTrack, "FwdHitsPerTrack",
           "Number of hits per forward track", 0.5, 40.5, 40);
      plot(chi2, "FwdChi2", "Chi2/DoF of forward tracks", 0., 10., 50);
      plot(eta, "FwdEta", "Pseudorapidity of forward tracks", 1., 6., 50);
      plot(phi, "FwdPhi", "Phi-angle of forward tracks", -180., 180., 60);
      plot2D(eta, nHitsPerTrack, "FwdHitsPerTrackVsEta",
             "Hits/track vs pseudorapidity of forward tracks",
             1., 6., 0.5, 15.5, 50, 15);
      plot2D(eta, chi2, "FwdChi2VsEta",
             "Chi2/DoF vs pseudorapidity of forward tracks",
             1., 6., 0., 10., 50, 20);
      plot2D(nHitsPerTrack, chi2, "FwdChi2VsHitsPerTrack",
             "Chi2/DoF vs hits/forward track", 0.5, 15.5, 0., 10., 15, 20);
    }
    // Calculate radius at first and last hit
    // (assume that hits are sorted by module)
    const LHCb::VPChannelID id0 = track->lhcbIDs().front().vpID();
    const LHCb::VPChannelID id1 = track->lhcbIDs().back().vpID();
    const LHCb::VPCluster* cluster0 = clusters.object(id0);
    if (!cluster0) continue;
    const LHCb::VPCluster* cluster1 = clusters.object(id1);
    if (!cluster1) continue;
    const float x0 = cluster0->x();
    const float y0 = cluster0->y();
    const float r0 = sqrt(x0 * x0 + y0 * y0);
    const float x1 = cluster1->x();
    const float y1 = cluster1->y();
    const float r1 = sqrt(x1 * x1 + y1 * y1);
    const float minR = r0 > r1 ? r1 : r0;
    const float maxR = r0 > r1 ? r0 : r1;
    plot(minR, "MinHitRadius", "Smallest hit radius [mm]", 0., 50., 100);
    plot(maxR, "MaxHitRadius", "Largest hit radius [mm]", 0., 50., 100);
  }
  plot(nFwd, "FwdTracksPerEvent", "Number of forward tracks per event",
       0., 400., 40);
  plot(nBwd, "BwdTracksPerEvent", "Number of backward tracks per event",
       0., 400., 40);
  return StatusCode::SUCCESS;
}
#endif
