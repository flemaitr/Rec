#ifndef PRPIXELTRACKING_H
#define PRPIXELTRACKING_H 1

 //#define DEBUG_HISTO // fill some histograms while the algorithm runs.

// Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/Transformer.h"

// LHCb
#include "Event/RawEvent.h"

// Local
#include "PrPixelHitManager.h"
#include "PrPixelTrack.h"

#include <boost/dynamic_bitset.hpp>

/** @class PrPixelTracking PrPixelTracking.h
 *  This is the main tracking for the Velo Pixel upgrade
 *
 *  @author Olivier Callot
 *  @author Sebastien Ponce
 */

 class PrPixelTracking
     : public Gaudi::Functional::MultiTransformer<std::tuple<LHCb::Tracks, LHCb::VPLightClusters>( const LHCb::RawEvent& )
#ifdef DEBUG_HISTO
                                                      ,
                                                  Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>
#endif
                                                  >{

 public:
  /// Standard constructor
  PrPixelTracking(const std::string &name, ISvcLocator *pSvcLocator);

  /// Algorithm initialization
  StatusCode initialize() override;

  /// Algorithm execution
  std::tuple<LHCb::Tracks, LHCb::VPLightClusters> operator()(const LHCb::RawEvent&) const override;

 private:

  /// Extrapolate a seed track and try to add further hits.
   void extendTrack( std::vector<PrPixelModuleHits>& modulehits, std::array<size_t, 71>& hitbuffer) const;

   /// Search for tracks starting from pair of hits on adjacent sensors
   void searchByPair( std::vector<PrPixelModuleHits>& modulehits, LHCb::Tracks& outputTracks ) const;

   /// Produce LHCb::Track list understandable to other LHCb applications.
   void makeLHCbTracks( PrPixelTrack& track, const std::array<size_t, 71>& hitbuffer,
                        std::vector<PrPixelModuleHits>& modulehits, LHCb::Tracks& outputTracks ) const;

   /// Try to add a matching hit on a given module.
   size_t bestHit( std::vector<PrPixelModuleHits>& modulehits, const size_t next,
                                    const unsigned foundHits, std::array<size_t, 71>& hitbuffer ) const;

   void printTrack( PrPixelTrack& track ) const;

   /// count hits for monitoring
   std::tuple<unsigned int, unsigned int> countHits( std::vector<PrPixelModuleHits>& modulehits ) const;

   /// monitoring function
   StatusCode monitor( std::vector<PrPixelModuleHits>& modulehits, const LHCb::Tracks& tracks,
                       const LHCb::VPLightClusters& clusters ) const;

   /// Hit manager tool
   ToolHandle<PrPixelHitManager> m_hitManager{"PrPixelHitManager", this};

   /// Properties
   Gaudi::Property<float> m_maxXSlope{this, "MaxXSlope", 0.400, "X Slope limit for seed pairs"};
   Gaudi::Property<float> m_maxYSlope{this, "MaxYSlope", 0.400, "Y Slope limit for seed pairs"};
   Gaudi::Property<float> m_extraTol{this, "ExtraTol", 0.6 * Gaudi::Units::mm, "Tolerance window when adding hits"};
   Gaudi::Property<unsigned int> m_maxMissed{this, "MaxMissed", 3,
                                             "Number of modules without a hit after which to stop extrapolation"};
   Gaudi::Property<float> m_maxScatter{this, "MaxScatter", 0.004, "Acceptance criteria for adding new hits"};
   Gaudi::Property<float> m_maxChi2Short{this, "MaxChi2Short", 20.0,
                                         "Acceptance criteria for track candidates : Max. chi2 for 3-hit tracks"};
   Gaudi::Property<float> m_fractionUnused{this, "FractionUnused", 0.5,
                                           "Acceptance criteria for track candidates : Min. fraction of unused hits"};
   Gaudi::Property<bool> m_stateClosestToBeamKalmanFit{this, "ClosestToBeamStateKalmanFit", true,
                                                       "Parameter for Kalman fit"};
   Gaudi::Property<bool> m_stateEndVeloKalmanFit{this, "EndVeloStateKalmanFit", false, "Parameter for Kalman fit"};
   Gaudi::Property<bool> m_addStateFirstLastMeasurementKalmanFit{this, "AddFirstLastMeasurementStatesKalmanFit", false,
                                                                 "Parameter for Kalman fit"};
   Gaudi::Property<unsigned int> m_maxClusterSize{
       this, "MaxClusterSize", VP::NPixelsPerSensor,
       "Maximum clusters size (no effect when running on lite cluster banks)"};
   Gaudi::Property<bool> m_trigger{this, "Trigger", false, "Are we running in the trigger?"};
   Gaudi::Property<bool> m_storecluster{this, "StoreClusters", true, "Do we want to store the clusters ?"};

   float m_maxScatterSq = 0.0;
};
#endif  // PRPIXELTRACKING_H
