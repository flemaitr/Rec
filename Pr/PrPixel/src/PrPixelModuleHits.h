#ifndef PRPIXELMODULEHITS_H
#define PRPIXELMODULEHITS_H 1

// Local
#include "PrPixelHit.h"

// SOA
#include <AlignedAllocator.h>
#include <boost/dynamic_bitset.hpp>

/** @class PrPixelModule PrPixelModule.h
 *  Class to hold hits for one VP module
 *
 *  @author Sebastien Ponce
 */
namespace Pr
{
  namespace Pixel
  {
    namespace Detail
    {
      template <typename T>
      //using aligned_vector = std::vector<T, AlignedAllocator<T, 64>>;
      using aligned_vector = std::vector<T>;



      //https://stackoverflow.com/questions/17074324/how-can-i-sort-two-vectors-in-the-same-way-with-criteria-that-uses-only-one-of/17074810#17074810
      template <typename T, typename Compare>
      std::vector<std::size_t> sort_permutation(
          aligned_vector<T>& vec,
          Compare compare)
      {
          std::vector<std::size_t> p(vec.size());
          std::iota(p.begin(), p.end(), 0);
          std::stable_sort(p.begin(), p.end(),
              [&](std::size_t i, std::size_t j){ return compare(vec[i], vec[j]); });
          return p;
      }

      template <typename T>
      void apply_permutation_in_place(
          aligned_vector<T>& vec,
          const std::vector<std::size_t>& p)
      {
          std::vector<bool> done(vec.size());
          for (std::size_t i = 0; i < vec.size(); ++i)
          {
              if (done[i])
              {
                  continue;
              }
              done[i] = true;
              std::size_t prev_j = i;
              std::size_t j = p[i];
              while (i != j)
              {
                  std::swap(vec[prev_j], vec[j]);
                  done[j] = true;
                  prev_j = j;
                  j = p[j];
              }
          }
      }


    }
  }
}
using namespace Pr::Pixel::Detail;

struct PrPixelModuleHits {
public:
  inline bool empty() const noexcept { return m_hitsX.empty(); }
  inline size_t size() const noexcept { return m_hitsX.size(); }
  inline float lastHitX() const { return m_hitsX.back(); }
  inline float firstHitX() const { return m_hitsX.front(); }
  inline bool IsUsed(const size_t idx) const { return m_IsUsedVec.test(idx); }
  inline void SetUsed(const size_t idx) { m_IsUsedVec.set(idx); }
  inline void createIsUsedVec() { m_IsUsedVec = boost::dynamic_bitset<>(m_hitsX.size()); }

  inline float getX(size_t idx) const noexcept { return m_hitsX[idx]; }
  inline float getY(size_t idx) const noexcept { return m_hitsY[idx]; }
  inline float getZ(size_t idx) const noexcept { return m_hitsZ[idx]; }
  inline LHCb::LHCbID getID(size_t idx) const noexcept { return m_hitsID[idx]; }

  aligned_vector<float>& getXVec() { return m_hitsX; }
  aligned_vector<float>& getYVec() { return m_hitsY; }
  aligned_vector<float>& getZVec() { return m_hitsZ; }
  aligned_vector<LHCb::LHCbID>& getIDVec() { return m_hitsID; }
  boost::dynamic_bitset<>& getIsUsedVec() { return m_IsUsedVec; }

  // Assume binary resolution of hit position. This is the weight.
  static constexpr float wxerr() noexcept { return 62.98366573f ;}//std::sqrt( 12.0 ) / ( 0.055 ); }
  static constexpr float wyerr() noexcept { return 62.98366573f; }
  static constexpr float wx() noexcept { return wxerr() * wxerr(); }
  static constexpr float wy() noexcept { return wyerr() * wyerr(); }

  aligned_vector<float> m_hitsX;
  aligned_vector<float> m_hitsY;
  aligned_vector<float> m_hitsZ;
  aligned_vector<LHCb::LHCbID> m_hitsID;
  boost::dynamic_bitset<> m_IsUsedVec;
};

#endif // PRPIXELMODULEHITS_H
