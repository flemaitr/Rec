CMAKE_MINIMUM_REQUIRED(VERSION 2.8.5)

#---------------------------------------------------------------
# Load macros and functions for Gaudi-based projects
find_package(GaudiProject)
#---------------------------------------------------------------

include_directories(SOAContainer/include)

set( CMAKE_EXPORT_COMPILE_COMMANDS ON )
# Declare project name and version
gaudi_project(Rec v23r0
              USE Lbcom v22r0
              DATA TCK/HltTCK
                   ChargedProtoANNPIDParam VERSION v1r*
                   TMVAWeights)
