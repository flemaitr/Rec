//-----------------------------------------------------------------------------
/** @file OTHitCreator.h
 *
 *  Header file for class : Tf::OTHitCreator
 *
 *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
 *  @date   2007-06-01
 */
//-----------------------------------------------------------------------------

#ifndef TFKERNEL_OTHitCreator_H
#define TFKERNEL_OTHitCreator_H 1

#include "boost/optional.hpp"

#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/IIncidentListener.h"

#include "GaudiAlg/GaudiTool.h"

#include "TfKernel/IOTHitCreator.h"
#include "TfKernel/OTHit.h"

#include "OTDAQ/IOTRawBankDecoder.h"

namespace LHCb{
  class OTChannelID;
}

class DeOTDetector;

namespace Tf
{

  // forward declaration of the class that is holding the hit data
  namespace HitCreatorGeom {
    class OTModule ;
    class OTDetector ;
  }

  /** @class OTHitCreator OTHitCreator.h
   *
   *  Implementation of Tf::IOTHitCreator.
   *
   *  Creates the Tf::OTHit objects for the Tf tracking framework
   *
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-06-01
   */

  class OTHitCreator final: public extends<GaudiTool, IOTHitCreator,IIncidentListener>
  {

  public:

    /// constructer
    OTHitCreator(const std::string& type,
                 const std::string& name,
                 const IInterface* parent);

    /// initialisation
    StatusCode initialize() override;
    /// finalisation
    StatusCode finalize() override;

    /// incident service handle
    void handle( const Incident& incident ) override;
 private:
    /// update manager handle
    StatusCode updateGeometry() ;
 public:
    // RestUsed flag for all OT hits
    void resetUsedFlagOfHits() override;

    // Load all the OT hits
    OTHitRange hits() const override;

    // Load the hits for a given region of interest
    OTHitRange hits(const TStationID iStation,
                    const TLayerID iLayer,
                    const OTRegionID iRegion) const override;

    // Load the hits for a given region of interest
    OTHitRange hitsLocalXRange(const TStationID iStation,
		       const TLayerID iLayer,
		       const OTRegionID iRegion,
		       const double xmin,
		       const double xmax) const override;

    // Retrieve the OTRegion for a certain region ID. The region
    const OTRegion* region(const TStationID iStation,
                           const TLayerID iLayer,
                           const OTRegionID iRegion) const override;


    // Create a single OTHit from an lhcbid
    OTHit hit( const LHCb::OTChannelID id ) const override;

  public:
    double tmin() const { return m_tmin ; }
    double tmax() const { return m_tmax ; }
    bool   rejectOutOfTime() const { return m_rejectOutOfTime ; }
    const IOTRawBankDecoder* decoder() const { return &(*m_otdecoder) ; }
    const HitCreatorGeom::OTModule* module( const LHCb::OTChannelID id ) const ;
    // return a pointer to a custom rt relation if drift times are not to be
    // used and null otherwise
    const OTDet::RtRelation* getRtRelation() const;

  private:
    ToolHandle<IOTRawBankDecoder> m_otdecoder { "OTRawBankDecoder" };

    Gaudi::Property<float> m_tmin { this, "TMin", -8*Gaudi::Units::ns };
    Gaudi::Property<float> m_tmax { this, "TMax", 56*Gaudi::Units::ns };
    Gaudi::Property<float> m_forceDriftRadius { this, "ForceDriftRadius", 0. * Gaudi::Units::mm };
    Gaudi::Property<float> m_forceResolution { this,  "ForceResolution",  5. * Gaudi::Units::mm / std::sqrt(12.) };
    Gaudi::Property<bool> m_rejectOutOfTime { this, "RejectOutOfTime", false };
    Gaudi::Property<bool> m_noDriftTimes { this, "NoDriftTimes", false };

    const DeOTDetector* m_otdetector = nullptr;
    std::unique_ptr<HitCreatorGeom::OTDetector> m_detectordata ;
    boost::optional<OTDet::RtRelation> m_rtrel;
  };
}


#endif // TFKERNEL_OTHitCreator_H

