#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/IIncidentSvc.h"

#include "TfKernel/DefaultVeloRHitManager.h"


//-----------------------------------------------------------------------------
// Implementation file for class : DefaultVeloRHitManager
//
// 2007-07-04 : Kurt Rinnert <kurt.rinnert@cern.ch>
//-----------------------------------------------------------------------------
namespace Tf {

DECLARE_COMPONENT( DefaultVeloRHitManager )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DefaultVeloRHitManager::DefaultVeloRHitManager(const std::string& type,
    const std::string& name,
    const IInterface* parent)
  : DefaultVeloHitManager<DeVeloRType,VeloRHit,4>(type, name, parent)
{
  declareInterface<DefaultVeloRHitManager>(this);
}


} // namespace Tf
