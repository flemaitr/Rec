#ifndef TFTOOLS_IOTHITCLEANER_H
#define TFTOOLS_IOTHITCLEANER_H 1

// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/SystemOfUnits.h"

// From Tf
#include "TfKernel/OTHit.h"


namespace Tf
{

  /** @class IOTHitCleaner IOTHitCleaner.h TfKernel/IOTHitCleaner.h
   *
   *  Interface to tool that 'cleans' ranges of OTHits
   *
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-07-20
   */
  struct IOTHitCleaner : extend_interfaces<IAlgTool>
  {
    /// Return the interface ID
    DeclareInterfaceID(IOTHitCleaner,2,0);

    /** Clean the given range of OT hits
     *  @param[in]  input The range of OT hits to clean
     *  @param[out] output The selected hits
     *  @return The number of removed hits
     */
    template<class INPUTDATA >
    inline OTHits cleanHits( const INPUTDATA & input ) const
    {
      return cleanHits ( input.begin(), input.end() );
    }

    /** Clean the given range of hits
     *  @param[in] begin Iterator to the start of a range of OT hits
     *  @param[in] end   Iterator to the start of a range of OT hits
     *  @param[out] output The selected hits
     *  @return The number of removed hits
     */
    virtual OTHits cleanHits( const OTHits::const_iterator begin,
                              const OTHits::const_iterator end ) const = 0;
  };

}

#endif // TFTOOLS_IOTHITCLEANER_H
