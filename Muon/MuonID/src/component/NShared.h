#ifndef NSHARED_H
#define NSHARED_H 1

// Include files

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "INShared.h"            // Interface


#include "Event/Track.h"
#include "Event/MuonPID.h"
#include "Event/MuonCoord.h"

/** @class NShared NShared.h
 *
 *
 *  @author Xabier Cid Vidal
 *  @date   2011-03-17
 */
class NShared : public extends<GaudiTool, INShared> {
public:
  /// Standard constructor
  NShared( const std::string& type,
           const std::string& name,
           const IInterface* parent);

  StatusCode getNShared(const LHCb::Track& muTrack, int& nshared) override;
  void fillNShared() override;

protected:

  // add info for ttracks?
  Gaudi::Property<bool> m_useTtrack {this, "useTtrack", false, "add info for ttracks?"};

  Gaudi::Property<std::string> m_TracksPath {this, "TrackLocation", LHCb::TrackLocation::Default};

  /// TES path to output the MuonPIDs to
  Gaudi::Property<std::string> m_MuonPIDsPath 
    {this, "MuonIDLocation", LHCb::MuonPIDLocation::Default, 
    "Destination of MuonPID"};

  /// TES path to output the Track PIDs to
  std::string m_MuonTracksPath;

  bool compareHits(const LHCb::Track& muTrack1,const LHCb::Track& muTrack2);
  LHCb::MuonPID* mupidFromTrack(const LHCb::Track& mother) const;

  //from ChargedProtoParticleAddMuonInfo.h
  StatusCode mapMuonPIDs();

  typedef std::map<const LHCb::Track *, LHCb::MuonPID *> TrackToMuonPID;
  TrackToMuonPID m_muonMap;

};
#endif // NSHARED_H
