#include "MuonChi2MatchTool.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonID/CommonMuonHit.h"
#include "Event/Track.h"

#include "GaudiKernel/SystemOfUnits.h"
#include <TMatrixF.h>
#include <vector>
#include <algorithm>
#include <numeric>

#define ISQRT12 0.2886751345948129

/** @class MuonChi2MatchTool MuonChi2MatchTool.h component/MuonChi2MatchTool.h
 *
 *
 *  @author Violetta Cogoni, Marianna Fontana, Rolf Oldeman, ported here by Giacomo Graziani
 *  @date   2015-11-11
 */

// convention for station counters to avoid confusions:
//   S from  0 to nStations (real station number obtained from condDB)
//   s from  0 to nSt       (hardcoded to 4) for vectors defined for M2-M5
//   iO from 0 to m_ord     (number of matched stations on which the algorithm is run)


MuonChi2MatchTool::MuonChi2MatchTool( const std::string& type,
                                const std::string& name,
                                const IInterface* parent )
  : GaudiTool ( type, name , parent )
{
  declareInterface<IMuonMatchTool>(this);
}

StatusCode MuonChi2MatchTool::initialize() {
  const StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  // initialize geometry
  m_mudet=getDet<DeMuonDetector>(DeMuonLocation::Default);
  nStations = m_mudet->stations();
  nRegions = m_mudet->regions();

  m_myFirstStation = (nStations == 5) ? 1 : 0;  // upgrade-wise

//  const float isqrt12 = 1. / std::sqrt(12);
  for (unsigned S = m_myFirstStation; S < nStations; ++S) {
     for (unsigned int R=0; R!=nRegions; ++R) {
       m_sigmapadX[S][R] = m_mudet->getPadSizeX(S, R) * ISQRT12;
       m_sigmapadY[S][R] = m_mudet->getPadSizeY(S, R) * ISQRT12;
     }
  }

  // Define radiation lenghts z/X0 wrt z position
  // TODO: remove hardcoding of these numbers
  //                           z pos   z/X0
  std::vector<std::pair<float, float> > mcs;
  mcs.emplace_back(12800.*Gaudi::Units::mm,28.);  // ECAL + SPD + PS
  mcs.emplace_back(14300.*Gaudi::Units::mm,53.);  // HCAL
  mcs.emplace_back(15800.*Gaudi::Units::mm,47.5); // M23 filter
  mcs.emplace_back(17100.*Gaudi::Units::mm,47.5); // M34 filter
  mcs.emplace_back(18300.*Gaudi::Units::mm,47.5); // M45 filter
  
  const auto mudet = m_mudet;
  for (unsigned i = m_myFirstStation; i < nStations; ++i) {
    for (unsigned j = m_myFirstStation; j <= i; ++j) {
	m_scattercache[i * (i + 1) / 2 + j] = std::accumulate(
	  mcs.begin(), mcs.begin() + (j + (m_myFirstStation ? 1 : 2)), 0.f,
	  [i, j, mudet] (float sum, const std::pair<float, float>& mcs) {
	    return sum + (mudet->getStationZ(i) - mcs.first) *
	      (mudet->getStationZ(j) - mcs.first) *
	      13.6 * Gaudi::Units::MeV * 13.6 * Gaudi::Units::MeV * mcs.second;
          });
    }
  }  

  // vectors used for the algorithm
  m_maxcomb =pow(m_maxnhitinFOI, nSt);
  m_nhit.resize(nSt);
  m_trkExtrap.resize(nSt);

  // vectors to store the output
  m_hitOnStation.resize(nStations);
  m_bestmatchedHits.resize(nStations);

  return StatusCode::SUCCESS;
}

// invert matrix by hand
std::array<std::array<float,4>,4> MuonChi2MatchTool::invert(MuonChi2MatchTool::MuonCandCovariance cov) const{

   unsigned int dim = cov.size();
   float deter;
   std::array<std::array<float,4>,4> minor, invCov;

   if (dim == 2){
     minor[0][0] = cov(1,1);
     minor[0][1] = cov(1,0);
     minor[1][0] = minor[0][1];
     minor[1][1] = cov(0,0);
     deter = std::abs(cov(0,0)*cov(1,1) - cov(1,0)*cov(0,1));
     invCov[0][0] = minor[0][0]/deter;
     invCov[0][1] = -minor[0][1]/deter;
     invCov[1][0] = -minor[1][0]/deter;
     invCov[1][1] = minor[1][1]/deter;
   }
   else if (dim == 3){
    minor[0][0] = cov(1,1)*cov(2,2)-cov(1,2)*cov(2,1);
    minor[0][1] = cov(1,0)*cov(2,2)-cov(1,2)*cov(2,0);
    minor[0][2] = cov(1,0)*cov(2,1)-cov(1,1)*cov(2,0);
    minor[1][0] = minor[0][1];
    minor[1][1] = cov(0,0)*cov(2,2)-cov(0,2)*cov(2,0);
    minor[1][2] = cov(0,0)*cov(2,1)-cov(0,1)*cov(2,0);
    minor[2][0] = minor[0][2];
    minor[2][1] = minor[1][2];
    minor[2][2] = cov(0,0)*cov(1,1)-cov(0,1)*cov(1,0);
    deter = std::abs(cov(0,0)*minor[0][0] - cov(0,1)*minor[0][1] + cov(0,2)*minor[0][2]);
    invCov[0][0] = minor[0][0]/deter;
    invCov[0][1] = -minor[0][1]/deter;
    invCov[0][2] = minor[0][2]/deter;
    invCov[1][0] = -minor[1][0]/deter;
    invCov[1][1] = minor[1][1]/deter;
    invCov[1][2] = -minor[1][2]/deter;
    invCov[2][0] = minor[2][0]/deter;
    invCov[2][1] = -minor[2][1]/deter;
    invCov[2][2] = minor[2][2]/deter;
   }
   else if (dim == 4){
    minor[0][0] = cov(1,1)*cov(2,2)*cov(3,3)+cov(2,1)*cov(3,2)*cov(1,3)+cov(1,2)*cov(2,3)*cov(3,1)
                 -cov(1,3)*cov(2,2)*cov(3,1)-cov(1,2)*cov(2,1)*cov(3,3)-cov(2,3)*cov(3,2)*cov(1,1);
    minor[0][1] = cov(1,0)*cov(2,2)*cov(3,3)+cov(2,0)*cov(3,2)*cov(1,3)+cov(1,2)*cov(2,3)*cov(3,0)
                 -cov(1,3)*cov(2,2)*cov(3,0)-cov(1,2)*cov(2,0)*cov(3,3)-cov(2,3)*cov(3,2)*cov(1,0);
    minor[0][2] = cov(1,0)*cov(2,1)*cov(3,3)+cov(2,0)*cov(3,1)*cov(1,3)+cov(1,1)*cov(2,3)*cov(3,0)
                 -cov(1,3)*cov(2,1)*cov(3,0)-cov(1,1)*cov(2,0)*cov(3,3)-cov(2,3)*cov(3,1)*cov(1,0);
    minor[0][3] = cov(1,0)*cov(2,1)*cov(3,2)+cov(2,0)*cov(3,1)*cov(1,2)+cov(1,1)*cov(2,2)*cov(3,0)
                 -cov(1,2)*cov(2,1)*cov(3,0)-cov(1,1)*cov(2,0)*cov(3,2)-cov(2,2)*cov(3,1)*cov(1,0);
    minor[1][0] = minor[0][1];
    minor[1][1] = cov(0,0)*cov(2,2)*cov(3,3)+cov(2,0)*cov(3,2)*cov(0,3)+cov(0,2)*cov(2,3)*cov(3,0)
                 -cov(0,3)*cov(2,2)*cov(3,0)-cov(0,2)*cov(2,0)*cov(3,3)-cov(2,3)*cov(3,2)*cov(0,0);
    minor[1][2] = cov(0,0)*cov(2,1)*cov(3,3)+cov(2,0)*cov(3,1)*cov(0,3)+cov(0,1)*cov(2,3)*cov(3,0)
                 -cov(0,3)*cov(2,1)*cov(3,0)-cov(0,1)*cov(2,0)*cov(3,3)-cov(2,3)*cov(3,1)*cov(0,0);
    minor[1][3] = cov(0,0)*cov(2,1)*cov(3,2)+cov(2,0)*cov(3,1)*cov(0,2)+cov(0,1)*cov(2,2)*cov(3,0)
                 -cov(0,2)*cov(2,1)*cov(3,0)-cov(0,1)*cov(2,0)*cov(3,2)-cov(2,2)*cov(3,1)*cov(0,0);
    minor[2][0] = minor[0][2];
    minor[2][1] = minor[1][2];
    minor[2][2] = cov(0,0)*cov(1,1)*cov(3,3)+cov(1,0)*cov(3,1)*cov(0,3)+cov(0,1)*cov(1,3)*cov(3,0)
                 -cov(0,3)*cov(1,1)*cov(3,0)-cov(0,1)*cov(1,0)*cov(3,3)-cov(1,3)*cov(3,1)*cov(0,0);
    minor[2][3] = cov(0,0)*cov(1,1)*cov(3,2)+cov(1,0)*cov(3,1)*cov(0,2)+cov(0,1)*cov(1,2)*cov(3,0)
                 -cov(0,2)*cov(1,1)*cov(3,0)-cov(0,1)*cov(1,0)*cov(3,2)-cov(1,2)*cov(3,1)*cov(0,0);
    minor[3][0] = minor[0][3];
    minor[3][1] = minor[1][3];
    minor[3][2] = minor[2][3];
    minor[3][3] = cov(0,0)*cov(1,1)*cov(2,2)+cov(1,0)*cov(2,1)*cov(0,2)+cov(0,1)*cov(1,2)*cov(2,0)
                 -cov(0,2)*cov(1,1)*cov(2,0)-cov(0,1)*cov(1,0)*cov(2,2)-cov(1,2)*cov(2,1)*cov(0,0);
    deter = std::abs(cov(0,0)*minor[0][0] - cov(0,1)*minor[0][1] + cov(0,2)*minor[0][2] - cov(0,3)*minor[0][3]);
    invCov[0][0] = minor[0][0]/deter;
    invCov[0][1] = -minor[0][1]/deter;
    invCov[0][2] = minor[0][2]/deter;
    invCov[0][3] = -minor[0][3]/deter;
    invCov[1][0] = -minor[1][0]/deter;
    invCov[1][1] = minor[1][1]/deter;
    invCov[1][2] = -minor[1][2]/deter;
    invCov[1][3] = minor[1][3]/deter;
    invCov[2][0] = minor[2][0]/deter;
    invCov[2][1] = -minor[2][1]/deter;
    invCov[2][2] = minor[2][2]/deter;
    invCov[2][3] = -minor[2][3]/deter;
    invCov[3][0] = -minor[3][0]/deter;
    invCov[3][1] = minor[3][1]/deter;
    invCov[3][2] = -minor[3][2]/deter;
    invCov[3][3] = minor[3][3]/deter;
  }
  return invCov;
}

// Calculate Chi2
float MuonChi2MatchTool::calcChi2(const MuonCandidate& cand, float p,
	const SigmaPadCache& sigmapadX, const SigmaPadCache& sigmapadY) const
{
  // build covariance matrices
  MuonCandCovariance covX(cand, p, m_scattercache, sigmapadX);
  MuonCandCovariance covY(cand, p, m_scattercache, sigmapadY);

  // try to get rid of Cholesky decomposition from ROOT libraries
  std::array<std::array<float,4>,4> invCovX = invert(covX);
  std::array<std::array<float,4>,4> invCovY = invert(covY);

  // take the inverse of covariances
//  ROOT::Math::CholeskyDecompGenDim<double> decompX(covX.size(), &covX(0, 0));
//  if (!decompX) return std::numeric_limits<double>::max();
//  ROOT::Math::CholeskyDecompGenDim<double> decompY(covY.size(), &covY(0, 0));
//  if (!decompY) return std::numeric_limits<double>::max();
//  decompX.Invert(&covX(0, 0));
//  decompY.Invert(&covY(0, 0));
    // compute chi2 from brute force
  float chisqbf = 0.;
  for (unsigned i = 0; i < cand.size(); ++i) {
    for (unsigned j = 0; j < i; ++j) {
      chisqbf += 2 * (cand.dx(i) * invCovX[i][j] * cand.dx(j) +
                      cand.dy(i) * invCovY[i][j] * cand.dy(j));
    }
    chisqbf += cand.dx(i) * invCovX[i][i] * cand.dx(i) +
               cand.dy(i) * invCovY[i][i] * cand.dy(i);
  }
  return chisqbf;  

// calculate chi^2: d^T Var d
//  double chisq = 0.;
//  for (unsigned i = 0; i < cand.size(); ++i) {
//    for (unsigned j = 0; j < i; ++j) {
//      chisq += 2 * (cand.dx(i) * covX(i, j) * cand.dx(j) +
//	  cand.dy(i) * covY(i, j) * cand.dy(j));
//    }
//    chisq += cand.dx(i) * covX(i, i) * cand.dx(i) +
//      cand.dy(i) * covY(i, i) * cand.dy(i);
//  }
  
//  return chisq;
}

void MuonChi2MatchTool::getResidual(const TrackMuMatch& match)
{
  const CommonMuonHit* muhit = std::get<0>(match);
  float matchSigma = std::get<1>(match);
  if(matchSigma > m_InputMaxSigmaMatch) return;
  int s = muhit->station() - m_myFirstStation; // index for our M2-M5 vectors
  if(s < 0 || s > (nSt-1)) return; // do not use M1
  if(m_OnlyCrossedHits && muhit->uncrossed()) return;
  if(m_hitOnStation[muhit->station()] == false) m_missedStations--;
  m_hitOnStation[muhit->station()] = true;
  if (msgLevel(MSG::DEBUG)) debug() << "MuonChi2MatchTool found match in M"<<muhit->station()+1<<" uncrossed? "<<muhit->uncrossed()<<endmsg;
  m_nhit[s]++;

  float extraX = std::get<2>(match);
  float extraY = std::get<3>(match);
  m_trkExtrap[s] = std::make_pair(extraX, extraY);
  m_dhit[s].push_back(std::make_pair(muhit,
	pow(extraX - muhit->x(),2) +  pow(extraY - muhit->y(),2) ) );
}

MuonChi2MatchTool::MuonCandidates MuonChi2MatchTool::buildCandidates() const
{
  std::vector<MuonCandidate> cands;
  cands.reserve(m_maxcomb);

  std::array<unsigned, 5> max={0};
  std::array<unsigned, 5> sta={0};
  unsigned dimmax = 0;
  for (unsigned i = 0; i < m_ord; ++i) {
    const auto station = m_matchedStationIndex[i];
    const auto n = std::min(unsigned(m_dhit[station].size()), unsigned(m_maxnhitinFOI));
    if (!n) continue;
    sta[dimmax] = station;
    max[dimmax++] = n;
  }
  unsigned comb = 0;
  for (MultiIndex<5> idx(dimmax, max); idx && comb < m_maxcomb; ++idx) {
    std::array<const CommonMuonHit*, 5> hits;
    std::array<float, 5> xextrap, yextrap;
    for (unsigned dim = 0; dim < idx.size(); ++dim) {
      hits[dim] = m_dhit[sta[dim]][idx[dim]].first;
      xextrap[dim] = m_trkExtrap[sta[dim]].first;
      yextrap[dim] = m_trkExtrap[sta[dim]].second;
    }
    cands.emplace_back(idx.size(), hits, xextrap, yextrap);
    ++comb;
  }

  return cands;
}

// =============== MAIN FUNCTION ===============
StatusCode MuonChi2MatchTool::run(const LHCb::Track* track,
    std::vector<TrackMuMatch>* bestMatches,
    std::vector<TrackMuMatch>* spareMatches)
{
  m_lasttrack = track;
  m_lastMatchesBest = bestMatches;
  m_lastMatchesSpare = spareMatches;
  m_ord=0;
  m_chisq_min=-1.;
  m_bestcomb=-1;
  m_matchedHits.clear();
  for (unsigned int S=0; S!=nStations ; ++S) {
    m_hitOnStation[S]=false;
    m_bestmatchedHits[S]=NULL;
  }

  if(!bestMatches)  return  StatusCode::SUCCESS;
  if(bestMatches->size() < 2) return  StatusCode::SUCCESS; // require at least 2 reasonable matches

  m_dhit.clear();
  m_dhit.resize(nSt);

  for (unsigned int s=0; s!=nSt; ++s) m_nhit[s]=0;
  m_missedStations=nSt;

  std::vector<TrackMuMatch>::iterator ih;
  if(bestMatches) {
    //  for_each(std::begin(bestMatches), std::end(bestMatches), [&](const TrackMuMatch *ih){
    //     getResidual(ih);
    //     } );
    for (ih=bestMatches->begin(); ih != bestMatches->end(); ++ih) {
      getResidual(*ih);
    }
  }

  if(m_missedStations>0 && spareMatches) { // also look among low quality muon hits if nothing better was found
    //for_each(std::begin(spareMatches), std::end(spareMatches), [&](const TrackMuMatch *ih){
    for (ih=spareMatches->begin(); ih != spareMatches->end(); ++ih) {
      if(false == m_hitOnStation[(std::get<0>(*ih))->station()]) getResidual(*ih);
      //    } );
    }
  }

  if(m_missedStations>2) return StatusCode::SUCCESS; // require at least 2 matched stations

  // Sort distsq from closest to farthest hits
  for(unsigned int s=0;s!=nSt;++s){
    if(m_nhit[s]>0)
      std::sort(m_dhit[s].begin(), m_dhit[s].end(), [](const std::pair<const CommonMuonHit*,float> &left, const std::pair<const CommonMuonHit*,float> &right) {
	  return left.second < right.second;
	  });
  }

  m_ord=nSt-m_missedStations; // m_ord is the number of matched station (order of our covariance matrix)
  m_matchedStationIndex.resize(m_ord); // map dimension to station

  std::vector<unsigned int> c(m_ord); // vector with running indexes for the combinations
  for (unsigned int s = 0, iO = 0; s!=nSt; ++s) {
    if(m_nhit[s] > 0){ // use this station
      m_nhit[s]=std::min(m_maxnhitinFOI.value(), m_nhit[s]);
      m_matchedStationIndex[iO]=s;
      c[iO] =0;
      iO++;
    }
  }

  // build all candidates, and calculate the chi^2 for each
  auto cands = buildCandidates();
  if (msgLevel(MSG::VERBOSE)) verbose() << "Found "<< cands.size() <<" combinations"<<endmsg;
  std::vector<float> chi2s;
  chi2s.reserve(cands.size());
  const auto p = track->p();
  std::transform(cands.begin(), cands.end(), std::back_inserter(chi2s),
  	[this, p] (const MuonCandidate& c) { return calcChi2(c, p, m_sigmapadX, m_sigmapadY); });

  // find smallest chi^2 combination
  unsigned minidx = std::min_element(chi2s.begin(), chi2s.end()) - chi2s.begin();
  const auto& bestcomb = cands[minidx];
  const auto bestchi2 = chi2s[minidx];
  // store the best CommonMuonHits
  if (msgLevel(MSG::VERBOSE)) verbose() << "MuonChi2MatchTool best comb is " << minidx <<endmsg;
  for (unsigned iO=0; iO!=m_ord; ++iO) {
    const CommonMuonHit* bestHit = bestcomb.hit(iO);
    m_matchedHits.push_back(bestHit);
    m_bestmatchedHits[bestHit->station()] = bestHit;
  }
  if (msgLevel(MSG::DEBUG)) debug() << "MuonChi2MatchTool output: chi2="<< bestchi2 <<" with ndof="<<bestcomb.size()*2<<endmsg;
  m_chisq_min = bestchi2;
  m_ord = bestcomb.size();
  return StatusCode::SUCCESS;
}

void MuonChi2MatchTool::getListofCommonMuonHits(CommonConstMuonHits& matchedMuonHits, int station, bool) {
  if (station<0) {
    matchedMuonHits=m_matchedHits;
  } else {
    matchedMuonHits.clear();
    if(station<(int)nStations && m_bestmatchedHits[station])
      matchedMuonHits.push_back(m_bestmatchedHits[station]);
  }
}

void MuonChi2MatchTool::getListofMuonTiles(std::vector<LHCb::MuonTileID>& matchedTiles, int station, bool) {
  matchedTiles.clear();
  if(station>-1 && station<(int)nStations && m_bestmatchedHits[station]) {
    matchedTiles.push_back(m_bestmatchedHits[station]->tile());
  } else {
    std::transform(m_matchedHits.begin(),m_matchedHits.end(),
                   std::back_inserter(matchedTiles),
                   [](const CommonMuonHit* hit) { return hit->tile(); } );
  }
}

float MuonChi2MatchTool::getChisquare(unsigned int& ndof) const {
  ndof= 2*m_ord;
  return m_chisq_min;
}

IMuonMatchTool::MuonMatchType MuonChi2MatchTool::getMatch(int station) const {
  if(!m_hitOnStation[station]) return IMuonMatchTool::NoMatch;
  if(m_bestmatchedHits[station]->uncrossed()) return IMuonMatchTool::Uncrossed;
  return IMuonMatchTool::Good;
}

float MuonChi2MatchTool::getMatchSigma(unsigned int station) const {
  // retrieve the original match distance in sigma for this station
  if (msgLevel(MSG::VERBOSE)) 
    verbose() << "requested MatchSigma for M"<<station+1<<" lastTrack is "<<m_lasttrack<<" match is "<<m_hitOnStation[station]<<endmsg;
  float out=999.;
  if(!m_hitOnStation[station]) return out;
  if(m_lasttrack) {
    std::vector< TrackMuMatch > * mvector= nullptr;
    if(m_lastMatchesBest) mvector = m_lastMatchesBest;
    if(!mvector) {
      if(m_lastMatchesSpare) mvector = m_lastMatchesSpare;
    }
    if(mvector) {
      auto im = std::find_if( mvector->begin(), mvector->end(),
                              [&](const TrackMuMatch& tmm) {
            return std::get<0>(tmm) == m_bestmatchedHits[station];
                              } );
      if (im!=mvector->end()) out = std::get<1>(*im);
    }
  }
  return out;
}

DECLARE_COMPONENT( MuonChi2MatchTool )
