#ifndef PREPAREMUONHITS_H
#define PREPAREMUONHITS_H

#include <vector>

#include "Event/MuonCoord.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/IIncidentListener.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonID/MuonHitHandler.h"
#include "MuonID/CommonMuonHit.h"
#include "MuonID/CommonMuonStation.h"

namespace {
   const std::vector<std::vector<double>> defaultRegions =
      {{{-3940, -1920, -960, -480, 480, 960, 1920, 3940},
        {-4900, -1200, -600, 600, 1200, 4900},
        {-5280, -1292, -644, 644, 1292, 5280},
        {-5656, -1392, -696, 696, 1392, 5656},
        {-6052, -1488, -744, 744, 1488, 6052}}};
}

/** @class Preparemuonhits Preparemuonhits.h
 *  Used to be CommonMuonHitManager.
 *
 *  @author Roel Aaij
 *  @author Kevin Dungs
 *  @date   2015-01-03
 */
class PrepareMuonHits final
   : public Gaudi::Functional::Transformer<MuonHitHandler(const LHCb::MuonCoords& coords)> {
public:

   PrepareMuonHits(const std::string& name, ISvcLocator* pSvcLocator);

   MuonHitHandler operator()(const LHCb::MuonCoords& coords) const override;

   StatusCode initialize() override;

private:

   // X region boundaries
   // Define the regions, the last ones are increased a little to make sure
   // everything is caught.
   Gaudi::Property<std::vector<std::vector<double>>> m_xRegions
   {this, "XRegions", defaultRegions};

   // Data members
   DeMuonDetector* m_muonDet = nullptr;
   size_t m_nStations;

};
#endif  // PREPAREMUONHITS_H
