#ifndef COMPONENT_MUONFAKECLUSTERING_H
#define COMPONENT_MUONFAKECLUSTERING_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "MuonInterfaces/IMuonClusterRec.h"            // Interface
#include "GaudiKernel/IIncidentListener.h"

/** @class MuonFakeClustering MuonFakeClustering.h component/MuonFakeClustering.h
 *
 *produce fake clusters by just copying the list of MuonLogPads to MuonHits
 *  @author Giacomo GRAZIANI
 *  @date   2009-10-15
 */
class MuonFakeClustering : public extends<GaudiTool, IMuonClusterRec, IIncidentListener> {
public:
  /// Standard constructor
  MuonFakeClustering( const std::string& type,
                      const std::string& name,
                      const IInterface* parent);

  StatusCode initialize() override;
  void handle ( const Incident& incident ) override;
  const std::vector<MuonHit>& clusters(const std::vector<MuonLogPad>& pads,
                                        bool force=false) override;


private:
  void clear();
  DeMuonDetector* m_muonDetector = nullptr;
  IMuonFastPosTool* m_posTool = nullptr;

  Gaudi::Property<std::string> m_posToolName {this, "PosTool", "MuonDetPosTool"};

  std::vector<MuonHit> m_clusters;
  bool m_clustersDone = false;
};
#endif // COMPONENT_MUONFAKECLUSTERING_H
