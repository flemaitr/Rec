#ifndef COMPONENT_MUONTRACKMOMREC_H 
#define COMPONENT_MUONTRACKMOMREC_H 1

// Include files
#include <vector>
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "MuonInterfaces/IMuonTrackMomRec.h"            // Interface

struct IBIntegrator;
class DeMuonDetector;

/** @class MuonTrackMomRec MuonTrackMomRec.h component/MuonTrackMomRec.h
 *    
 *
 *  @author Giacomo Graziani
 *  @date   2010-02-10
 *  
 *  tool to compute momentum for standalone muon track
 *  adapted from the TrackPtKick tool by M. Needham
 *
 *
 */
class MuonTrackMomRec final : public extends<GaudiTool, IMuonTrackMomRec, IIncidentListener> {
public: 
  /// Standard constructor
  MuonTrackMomRec( const std::string& type, 
                   const std::string& name,
                   const IInterface* parent);

  // from GaudiTool
  StatusCode initialize() override;     
  
  // from IMuonTrackMomRec
  std::unique_ptr<LHCb::Track> recMomentum(MuonTrack& track) const override;
  double getBdl() const override {return m_bdlX;}
  double getZcenter() const override {return m_zCenter;}

  // from IIncidentListener
  void handle ( const Incident& incident ) override;   

private:
  
  Gaudi::Property<std::vector<float>> m_ParabolicCorrection 
    {this, "ParabolicCorrection", {1.04,0.14}};
  
  Gaudi::Property<std::vector<float>> m_resParams 
    {this, "m_resParams", {0.015,0.29}};

  Gaudi::Property<float> m_Constant {
    this, "ConstantCorrection", 0., "In MeV"};

  StatusCode initBdl();

  IBIntegrator* m_bIntegrator = nullptr; // magnetic field tool
  DeMuonDetector* m_muonDetector = nullptr;
  double m_zCenter = 0; // Bx field center position in z
  double m_bdlX = 0;    // integrated Bx field
  int m_FieldPolarity = 0;

};
#endif // COMPONENT_MUONTRACKMOMREC_H
