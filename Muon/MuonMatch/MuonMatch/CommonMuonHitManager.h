#ifndef COMMONMUONHITMANAGER_H_
#define COMMONMUONHITMANAGER_H_

#include <array>
#include <vector>

#include "Event/MuonCoord.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "MuonDet/DeMuonDetector.h"

#include "MuonID/CommonMuonHit.h"
#include "MuonID/CommonMuonStation.h"

namespace {
   const std::vector<std::vector<double>> defaultRegions =
      {{{-3940, -1920, -960, -480, 480, 960, 1920, 3940},
        {-4900, -1200, -600, 600, 1200, 4900},
        {-5280, -1292, -644, 644, 1292, 5280},
        {-5656, -1392, -696, 696, 1392, 5656},
        {-6052, -1488, -744, 744, 1488, 6052}}};
}

static const InterfaceID IID_CommonMuonHitManager("CommonMuonHitManager", 1, 0);

/** @class CommonMuonHitManager CommonMuonHitManager.h
 *  Muon hit manager for Hlt1 and Offline.
 *
 *  Used to be Hlt/Hlt1Muons/Hlt1MuonHitManager.
 *
 *  @author Roel Aaij
 *  @author Kevin Dungs
 *  @date   2015-01-03
 */
class CommonMuonHitManager : public GaudiTool, public IIncidentListener {
 public:
  static const InterfaceID& interfaceID() { return IID_CommonMuonHitManager; }

  CommonMuonHitManager(const std::string& type, const std::string& name,
                       const IInterface* parent);

  ~CommonMuonHitManager() override = default;

  StatusCode initialize() override;

  void handle(const Incident& incident) override;

  virtual CommonMuonHitRange hits(double xmin, unsigned int station,
                                  unsigned int region);
  virtual CommonMuonHitRange hits(double xmin, double xmax,
                                  unsigned int station, unsigned int region);
 
  virtual unsigned int nRegions(unsigned int station) const;

  virtual const CommonMuonStation& station(unsigned int id) const;

 private:
  // Properties
  Gaudi::Property<std::string> m_coordLocation{this, "MuonCoordLocation",
                                               LHCb::MuonCoordLocation::MuonCoords};

  // X region boundaries
  // Define the regions, the last ones are increased a little to make sure
  // everything is caught.
  Gaudi::Property<std::vector<std::vector<double>>> m_xRegions
  {this, "XRegions", defaultRegions};
   
  // Data members
  DeMuonDetector* m_muonDet = nullptr;
   
  std::vector<CommonMuonStation> m_stations;
  std::bitset<5> m_prepared;

  std::array<std::vector<const LHCb::MuonCoord*>, 5> m_coords;
  bool m_loaded;

  // Functions
  void prepareHits(unsigned int station);

  void loadCoords();
   
};
#endif  // COMMONMUONHITMANAGER_H_
