// STL
#include <algorithm>

// from Gaudi
#include "GaudiKernel/boost_allocator.h"

// from MuonID
#include "MuonID/CommonMuonHit.h"

// local
#include "MuonMatchVelo.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonMatchVelo
//
// 2010-12-02 : Roel Aaij
// 2017-10-05 : Miguel Ramos Pernas
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( MuonMatchVelo )

//=============================================================================
//
void MuonMatchVelo::addHits( Candidate& seed ) const
{
  // First hit is in magnet
  const double zMagnet = seed.hits.at(0).z();
  const double xMagnet = seed.hits.at(0).x();

  unsigned int nMissed = 0;
  for ( unsigned int i = 1; i < order.size() && nMissed <= m_maxMissed; ++i ) {
    
    // Find candidate hits
    unsigned int s = order[i];

    // Get the station we're looking at.
    const CommonMuonStation& station = m_hitManager->station(s);
    double zStation = station.z();

    // Calculate window in x and y for this station
    double yMuon = 0., yRange = 0;
    seed.yStraight(zStation, yMuon, yRange);
    yRange = m_yWindow;

    const double yMin = yMuon - yRange;
    const double yMax = yMuon + yRange;

    const double xMuon = (zStation - zMagnet)*seed.fitatt->slope + xMagnet;
    const double xRange = m_xWindow;

    const double xMin = xMuon - xRange;
    const double xMax = xMuon + xRange;

    // Look for the closest hit inside the search window
    boost::optional<CommonMuonHit> closest;
    double minDist2 = 0;

    for ( unsigned int r = 0; r < station.nRegions(); ++r ) {
      
      if ( !station.overlaps(r, xMin, xMax, yMin, yMax) ) continue;
      
      for ( const auto& hit : m_hitManager->hits(xMin, xMax,  s, r) ) {
	
	if ( hit.y() > yMax || hit.y() < yMin ) continue;

	auto dx = xMuon - hit.x();
	auto dy = yMuon - hit.y();
	double dist2 = dx*dx + dy*dy;
	if ( !closest || dist2 < minDist2 ) {
	  closest.reset(hit);
	  minDist2 = dist2;
	}
      }
    }

    if ( closest )
      seed.hits.emplace_back(*closest);
    else
      ++nMissed;
  }
  // If a new candidate is good, fit it.
  if ( nMissed <= m_maxMissed ) fitCandidate(seed);
}

//=============================================================================
//
Candidates MuonMatchVelo::findSeeds( const LHCb::Track& seed,
				     const unsigned int seedStation ) const
{
  // Make a Candidate from the track
  Candidate veloSeed{&seed};

  // forward extrapolation, make seed point
  double zMagnet = m_zb + m_za * veloSeed.tx2();
  double xMagnet = 0., errXMagnet = 0.;
  veloSeed.xStraight(zMagnet, xMagnet, errXMagnet);
  double yMagnet = 0., errYMagnet = 0.;
  veloSeed.yStraight(zMagnet, yMagnet, errYMagnet);
  
  LHCb::MuonTileID id;
  CommonMuonHit magnetHit(id, xMagnet, errXMagnet, yMagnet, errYMagnet, zMagnet, 0., false);
  
  const CommonMuonStation& station = m_hitManager->station(seedStation);
  double zStation = station.z();

  double dz = (zStation - zMagnet);
  // double sign = ( veloSeed.tx() > 0) - ( veloSeed.tx() < 0 );
  double tanMin = this->tanMin(veloSeed);
  double xMin   = xMagnet + dz*tanMin - m_xWindow;
  double tanMax = this->tanMax(veloSeed);
  double xMax   = xMagnet + dz*tanMax + m_xWindow;

  // Calculate window in y
  double yMuon = 0., yRange = 0;
  veloSeed.yStraight(zStation, yMuon, yRange);
  yRange += m_yWindow;

  double yMin = yMuon - yRange;
  double yMax = yMuon + yRange;

  // Make vector of candidates
  Candidates seeds;

  for ( unsigned int r = 0; r < station.nRegions(); ++r )
    if ( station.overlaps(r, xMin, xMax, yMin, yMax) ) {

      // Get hits, and add seed hits to container
      for ( const auto& hit : m_hitManager->hits(xMin, xMax, seedStation, r) ) {
      
	if ( hit.y() > yMax || hit.y() < yMin ) continue;
      
	seeds.emplace_back(veloSeed);
	auto& seed = seeds.back();
	
	seed.hits.emplace_back(magnetHit);
	seed.hits.emplace_back(hit);
	
	seed.fitatt->slope = (hit.x() - xMagnet)/(hit.z() - zMagnet);
	seed.fitatt->p = momentum(seed.fitatt->slope - seed.state->tx());
      }
    }
  
  return std::move(seeds);
}

//=============================================================================
//
void MuonMatchVelo::fitCandidate( Candidate& candidate ) const
{
  const CommonMuonHits& hits = candidate.hits;

  double sW = 0., sZ = 0., sX = 0.;

  for ( const auto& hit : hits ) {
    
    const double dx = hit.dx();
    const double w = 4./(dx*dx);
    
    sW += w;
    
    sZ += hit.z()*w;
    sX += hit.x()*w;
  }
  double zow = sZ/sW;

  double stmp2 = 0.;
  double b = 0.;
  for ( const auto& hit : hits ) {
    
    const double err = hit.dx()/2.;
    const double tmp = (hit.z() - zow)/err;
    
    stmp2 += tmp*tmp;
    
    b += tmp*hit.x()/err;
  }

  b /= stmp2;
  double a = (sX - sZ*b)/sW;
  // double errA = sqrt((1. + sZ*sZ/(sW*stmp2))/sW);
  // double errB = sqrt(1./stmp2);

  double chi2 = 0.;
  for ( const auto& hit : hits ) {
    
    const double err = hit.dx()/2.;
    const double tmp = (hit.x() - a - b*hit.z())/err;
    
    chi2 += tmp*tmp;
  }

  double p = momentum(b - candidate.state->tx());
  double ndof = hits.size() - 2;
  
  candidate.fitatt.reset(CandidateFitAtt{b, p, chi2/ndof});
}

//=============================================================================
//
void MuonMatchVelo::match( const LHCb::Track& seed, LHCb::Track::Vector& tracks ) const
{

  auto seeds = this->extractSeeds(seed);

  if ( !m_setQOverP ) {
    // In this case, we only care whether a good seed exists for the specified track
    if ( std::any_of( std::begin(seeds), std::end(seeds), [=](const Candidate& c) {
	  return c.fitatt->chi2ndof < m_maxChi2DoFX ;
        }) ) {
      // There is a good enough candidate, put the seed into the output
      // unmodified.
      std::unique_ptr<LHCb::Track> out{seed.clone()};
      out->addToAncestors(seed);
      tracks.push_back(out.release());
    }
  }
  else {
    //TODO: instead of the best, shouldn't we take everything below m_maxChiDoFX??
    auto best =
      std::min_element( std::begin(seeds), std::end(seeds),
			[]( const Candidate& lhs, const Candidate& rhs ) {
			  return lhs.fitatt->chi2ndof < rhs.fitatt->chi2ndof;
			} );

    const double bchi2ndof = best->fitatt->chi2ndof;
    const double slope = best->fitatt->slope;
    
    if ( best != std::end(seeds) && bchi2ndof < m_maxChi2DoFX ) {

      auto& bst = best->state;
      
      std::unique_ptr<LHCb::Track> out{seed.clone()};
      
      out->addToAncestors(seed);
      out->addInfo(35, slope - bst->tx());
      
      const double down = m_fieldSvc->isDown() ? -1 : +1;
      const double q = down*((slope < bst->tx()) - (slope > bst->tx()));
      
      auto state = out->stateAt(LHCb::State::EndVelo);
	    
      state->setQOverP(q/best->fitatt->p);
      
      tracks.push_back(out.release());
    }
  }
  
}
