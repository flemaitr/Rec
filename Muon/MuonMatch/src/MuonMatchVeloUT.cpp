// Boost
#include <boost/range/iterator_range_core.hpp>

// from Gaudi
#include "GaudiKernel/boost_allocator.h"

// from MuonID
#include "MuonID/CommonMuonHit.h"

// from MuonMatch
#include "MuonMatch/Parsers.h"

// local
#include "MuonMatchVeloUT.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonMatchVeloUT
//
// 2010-12-02 : Roel Aaij, Vasileios Syropoulos
// 2017-10-05 : Miguel Ramos Pernas
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( MuonMatchVeloUT )

// Some typedefs
namespace {
  using getHit = std::pair<double, double>
    (MuonMatchVeloUT::*)( const CommonMuonHit&, bool ) const;
}

//=============================================================================
//
void MuonMatchVeloUT::addHits( Candidate& seed ) const
{
  // First hit is in magnet
  const double zMagnet = seed.hits.at(0).z();
  const double xMagnet = seed.hits.at(0).x();
  
  unsigned int nmissed = 0;
  for ( unsigned int i = 1; i < order.size() && nmissed <= m_maxMissed; ++i ) {
    
    // Find candidate hits
    unsigned int s = order[i];

    // Get the station we are looking at.
    const CommonMuonStation& station = m_hitManager->station(s);
    const double zStation = station.z();

    double xw = 0., yw = 0.;
    std::tie(xw, yw) = m_window.value().at(s);

    // Calculate window in x and y for this station
    double yMuon = 0., yRange = 0;
    seed.yStraight(zStation, yMuon, yRange);
    yRange = yw;
    
    const double yMin = yMuon - yRange;
    const double yMax = yMuon + yRange;

    const double slope = seed.fitatt->slope;

    const double xMuon = (zStation - zMagnet)*slope + xMagnet;
    const double xRange = xw;

    const double xMin = xMuon - xRange;
    const double xMax = xMuon + xRange;

    // Look for the closest hit inside the search window
    boost::optional<CommonMuonHit> closest;
    double minDist2 = 0;
    double x = 0., dx = 0., y = 0., dy = 0.;

    for ( unsigned int r = 0; r < station.nRegions(); ++r ) {
      
      if ( !station.overlaps(r, xMin, xMax, yMin, yMax) ) continue;
      
      for ( const auto& hit : m_hitManager->hits(xMin, xMax, s, r) ) {
	
        std::tie(y, dy) = hity(hit, true);
        if ( y > yMax || y < yMin ) continue;

        std::tie(x, dx) = hitx(hit, true);
        seed.yStraight(hit.z(), yMuon, yRange);
	
        auto resx = (xMagnet + (hit.z() - zMagnet)*slope) - hit.x();
        auto resy = yMuon - hit.y();
        double dist2 = (resx*resx + resy*resy)/(4*dx*dx + 4*dy*dy);
        if ( !closest || dist2 < minDist2 ) {
          closest.reset(hit);
          minDist2 = dist2;
        }
      }
    }

    if ( closest )
      seed.hits.emplace_back(*closest);
    else
      ++nmissed;
  }

  // If a new candidate is good, fit it.
  if ( nmissed <= m_maxMissed ) fitCandidate(seed);
}

//=============================================================================
// In y, if we assume the velo extrapolation is correct the calculate the
// residuals and chi^2 can be calculated with respect to that. This is NOT the
// final chi2 of the candidate.
double MuonMatchVeloUT::calcChi2y( const Candidate& candidate ) const {

  double chi2 = 0;
  double yExt = 0, err = 0, yh = 0, dy = 0;
    
  // Skip the magnet hit. It's approximated error can only add noise
  for ( const auto& hit : boost::make_iterator_range(std::begin(candidate.hits) + 1,
						     std::end(candidate.hits)) ) {
    if ( hit.tile().key() == 0 ) continue;
    
    candidate.yStraight(hit.z(), yExt, err);
    std::tie(yh, dy) = hity(hit, true);

    const double c = (yh - yExt)/dy;
    
    chi2 += c*c;
  }
    
  return chi2;
}

//=============================================================================
//
Candidates MuonMatchVeloUT::findSeeds( const LHCb::Track& seed,
				       const unsigned int seedStation ) const
{
  // Make a Candidate from the track
  Candidate veloSeed{&seed};

  // Initialize vector of candidates
  Candidates seeds;

  // Extrapolation from TT to M3 station - Same extrapolation as in the VeloMuonMatch.
  const CommonMuonStation& station = m_hitManager->station(seedStation);
  const double zStation = station.z();

  // parametrization of the z-position of the magnet’s focal plane as a function of
  // the direction of the velo track ty^2
  const double zMagnet = m_z0 + m_z1*veloSeed.ty2();
  double xMagnet = 0., errXMagnet = 0.;
  veloSeed.xStraight(zMagnet, xMagnet, errXMagnet);

  double dz = (zStation - zMagnet);
  double xMin = 0.;
  double xMax = 0.;

  double xw = 0., yw = 0.;
  std::tie(xw, yw) = m_window.value().at(seedStation);

  if ( seed.hasUT() ) {
    
    //Use VeloTT info to get the charge of the seed.
    double tan = 0;
    double xM3 = 0., errXM3 = 0.;
    
    veloSeed.xStraight(zStation, xM3, errXM3);
    
    const int mag = m_fieldSvc->isDown() ? -1 : +1;
    const int charge = seed.charge();
    
    if (charge*mag < 0 )
      tan = this->tanMax(veloSeed);
    else
      tan = this->tanMin(veloSeed);
    
    xMin = xMagnet + dz*tan;
    xMax = xM3 ;
    if (xMin > xMax)
      std::swap(xMin, xMax);
    
    xMin -= xw;
    xMax += xw;
  }
  else {
    // If can not estimate the charge, do as in old VeloMuonMatch
    const double tanMin = this->tanMin(veloSeed);
    xMin = xMagnet + dz*tanMin - xw;
    const double tanMax = this->tanMax(veloSeed);
    xMax = xMagnet + dz*tanMax + xw;
  }

  // Calculate window in y. In y I just extrapolate in a straight line.
  double yMuon = 0., yRange = 0;
  veloSeed.yStraight(zStation, yMuon, yRange);
  yRange += yw;

  const double yMin = yMuon - yRange;
  const double yMax = yMuon + yRange;

  // Momentum-dependent correction on zMagnet as a function of 1/p in 1/GeV
  auto correct = [](double p_inv, const CorrectMap& cor, CorrectMap::key_type key,
                    double def) -> double {
    
    if (!cor.count(key)) return def;
    
    double r = 0.;
    
    auto coeffs = cor.at(key);
    for (size_t i = 0; i < coeffs.size(); ++i)
      r += coeffs[i]*pow(p_inv, i);
    
    return r;
  };

  // Get hits inside window, and make M3 seeds for a given velo seed
  for ( unsigned int r = 0; r < station.nRegions(); ++r )
    if ( station.overlaps(r, xMin, xMax, yMin, yMax) ) {
    
      for (const auto& hit : m_hitManager->hits(xMin, xMax, seedStation, r) ) {
      
	if ( hit.y() > yMax || hit.y() < yMin ) continue;
      
	seeds.emplace_back(veloSeed);
	Candidate& seed = seeds.back();

	// Initial slope and momentum estimate
	auto zm    = zMagnet;
	auto slope = (hit.x() - xMagnet)/(hit.z() - zm);
	auto p     = momentum(slope - seed.state->tx());

	// Momentum-dependent correction on zMagnet as a function of p in GeV
	zm += m_c0 + m_c1*Gaudi::Units::GeV/p;

	double xMagnet = 0., errXMagnet = 0.;
	seed.xStraight(zm, xMagnet, errXMagnet);
	double yMagnet = 0., errYMagnet = 0.;
	seed.yStraight(zm, yMagnet, errYMagnet);

	// Update momentum estimate of seed
	auto& fitatt = seed.fitatt;
	fitatt->slope = (hit.x() - xMagnet)/(hit.z() - zm);
	fitatt->p = momentum(fitatt->slope - seed.state->tx());
      
	// Filter seeds with too high p_T
	if ( fitatt->p*seed.sinTrack() > m_maxPt )
	  seeds.pop_back();
	else {
	  LHCb::MuonTileID id;
	  // Set the X and Y errors such that the error correction is a straigt multiplication
	  // to get the desired resolution.
	  // Calculate the error on the hit in the magnet from a parameterisation obtained from MC
	  // as a function of the first momentum estimate based on the single seed hit.
	  auto dx = 0.5*correct(Gaudi::Units::GeV/fitatt->p, m_errCorrect.value(), "mx", 1.);
	  auto dy = 0.5*correct(Gaudi::Units::GeV/fitatt->p, m_errCorrect.value(), "my", 1.);
	  
	  CommonMuonHit magnet_hit(id, xMagnet, dx, yMagnet, dy, zm, 0., false);
	  
	  seed.hits.emplace_back(magnet_hit);
	  seed.hits.emplace_back(hit);
	}
      }
    }

  return std::move(seeds);
}

//=============================================================================
//
CandidateFitResult MuonMatchVeloUT::fit( const getHit hitFun,
					 const CommonMuonHit& magnetHit,
					 const CommonMuonHits& hits ) const {
  
  auto info = [this, &hitFun](const CommonMuonHit& hit)
    { return (this->*hitFun)(hit, true); };
  
  // Calculate some sums
  double sw = 0., sz = 0., sc = 0.;
  for ( const auto& hit : hits ) {
      
    auto c = info(hit);
    double w = 1./(c.second*c.second);
      
    sw += w;
    sz += (hit.z() - magnetHit.z())*w;
    sc += c.first*w;
  }
  double zow = sz/sw;

  // Calculate the estimate for the slope
  double st = 0.;
  double b = 0.;
  for ( const auto& hit : hits ) {
      
    auto c = info(hit);
    double tmp = (hit.z() - magnetHit.z() - zow)/c.second;
      
    st += tmp*tmp;
    b += tmp*c.first/c.second;
  }
  b /= st;
  double a = (sc - sz*b)/sw;

  // Calculate the chi^2
  double chi2 = 0.;
  for ( const auto& hit : hits ) {
      
    auto c = info(hit);
    double tmp = (c.first - a - b*(hit.z() - magnetHit.z()))/c.second;
    
    chi2 += tmp*tmp;
  }

  return {a, b, chi2, hits.size() - 2};
}

//=============================================================================
//
void MuonMatchVeloUT::fitCandidate( Candidate& candidate ) const
{
  const CommonMuonHits& hits = candidate.hits;
  auto magnetHit = hits[0];
  
  // Do the fit in X and also fit Y, or get the chi^2 from the extrapolation.
  auto fitX = fit(&MuonMatchVeloUT::hitx, magnetHit, hits);
  CandidateFitResult fitY;
  if ( m_fitY )
    fitY = fit(&MuonMatchVeloUT::hity, magnetHit, hits);
  else
    fitY = CandidateFitResult{ magnetHit.y(),
			       candidate.state->ty(),
			       calcChi2y(candidate),
			       hits.size() };
  
  // Total chi^2
  double slope = fitX.slope;
  double p     = momentum(slope - candidate.state->tx());
  double chi2  = fitX.chi2 + fitY.chi2;
  double ndof  = fitX.ndof + fitY.ndof;
  
  candidate.fitatt.reset(CandidateFitAtt{slope, p, chi2/ndof});
  
  // Some plots for monitoring purposes
  if ( produceHistos() ) {

    for ( const auto& hit : candidate.hits ) {
      
      std::string title = (hit.tile().key() == 0) ? "magnet" :
	std::string{"M"} + std::to_string(hit.station() + 1);
      
      plotResidual(&MuonMatchVeloUT::hitx, hit, magnetHit, fitX, title + "_x");
      plotResidual(&MuonMatchVeloUT::hity, hit, magnetHit, fitY, title + "_y");
    }
  }
}

//=============================================================================
//
StatusCode MuonMatchVeloUT::initialize()
{
  
  MuonMatchBase::initialize();

  // Check if properties containing corrections are correct.
  std::map<std::string, size_t> cor{{"x", 4}, {"y", 4}, {"mx", 3}, {"my", 3}};
  
  for ( const auto& e : cor) {
   
    const auto& cv = m_errCorrect.value();
    const auto& fs = cv.at(e.first).size(); 

    if ( !cv.count(e.first) )
      return Error(std::string{"No entry "} + e.first + " in ErrorCorrection.",
		   StatusCode::FAILURE);

    else if ( fs != e.second )
      return Error(std::string{"Correction entry "} + e.first + " has size "
		   + std::to_string(fs) + ", while it should be "
		   + std::to_string(e.second),
		   StatusCode::FAILURE);
  }

  // Print the coeffients
  auto printCor = [this](CorrectMap::key_type key) {
    
    info() << key << ": ";
    
    for (auto e : m_errCorrect.value()[key])
      info() << e << " ";
  };
  info() << "Corrections  to  errors: "; printCor("x"); printCor("y"); info() << endmsg;
  info() << "Coefficients for errors: "; printCor("mx"); printCor("my"); info() << endmsg;

  // Initialize the detector
  m_det = getDet<DeMuonDetector>("/dd/Structure/LHCb/DownstreamRegion/Muon");

  // Fill local arrays of pad sizes and region sizes
  m_padSizeX.resize(nStations*nRegions);
  m_padSizeY.resize(nStations*nRegions);

  for ( unsigned int s = 0; s < nStations; ++s )
    for ( unsigned int r = 0; r < nRegions; ++r ) {
      m_padSizeX[s*nRegions + r] = m_det->getPadSizeX(s, r);
      m_padSizeY[s*nRegions + r] = m_det->getPadSizeY(s, r);
    }

  return StatusCode::SUCCESS;
}

//=============================================================================
//
void MuonMatchVeloUT::match( const LHCb::Track& seed, LHCb::Track::Vector& tracks ) const
{

  auto seeds = this->extractSeeds(seed);

  // In this case, we only care whether a good seed exists for the specified track
  if ( !m_setQOverP ) {
    
    if ( std::any_of( std::begin(seeds), std::end(seeds),
		      [=]( const Candidate& c ) {
			return c.fitatt && c.fitatt->chi2ndof < m_maxChi2DoFX;
		      }) ) {
      // There is a good enough candidate, put the seed into the output unmodified.
      std::unique_ptr<LHCb::Track> out{seed.clone()};
      out->addToAncestors(seed);
      tracks.push_back(out.release());
    }
  }
  else {
    
    auto last = std::partition(std::begin(seeds), std::end(seeds),
			       []( const Candidate& c) { return c.fitatt; });
    
    auto best = std::min_element( std::begin(seeds), last,
                                  []( const Candidate& lhs, const Candidate& rhs ) {
				    return lhs.fitatt->chi2ndof < rhs.fitatt->chi2ndof;
                                  } );
    
    const double bchi2ndof = best->fitatt->chi2ndof;
    const double slope = best->fitatt->slope;
    
    if ( best != last && bchi2ndof < m_maxChi2DoFX ) {
      
      auto& bst = best->state;

      std::unique_ptr<LHCb::Track> out{seed.clone()};
      out->addToAncestors(seed);
      out->addInfo(35, slope - bst->tx());
      
      double down = m_fieldSvc->isDown() ? -1 : +1;
      double q = down*((slope < bst->tx()) - (slope > bst->tx()));
      
      for ( const auto& state : out->states() )
        state->setQOverP(q/best->fitatt->p);
      
      tracks.push_back(out.release());
    }
  }
  
}

//=============================================================================
//
void MuonMatchVeloUT::plotResidual( const getHit hitFun,
				    const CommonMuonHit& hit,
				    const CommonMuonHit& magnetHit,
				    const CandidateFitResult& info,
				    std::string title ) const {
  
  double c = 0., e = 0.;
      
  for (auto entry : {std::make_pair(false, ""), std::make_pair(true, "_corrected")}) {
    
    std::tie(c, e) = (this->*hitFun)(hit, entry.first);
    auto res = c - (info.a0 + info.slope*(hit.z() - magnetHit.z()));
    auto suf = entry.second;
    
    plot(res, title + "_residual" + suf, -500, 500, 100);
    plot(e, title + "_error" + suf, -500, 500, 100);
    plot(res/e, title + "_pull" + suf, -5, 5, 100);
  }
}
