// local
#include "MuonMatchBase.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonMatchBase
//
// 2017-10-05 : Miguel Ramos Pernas
//-----------------------------------------------------------------------------

const std::array<unsigned int,4> order{{ 2u, 3u, 4u, 1u }};

//=============================================================================
//
MuonMatchBase::MuonMatchBase( const std::string& name, ISvcLocator *pSvcLocator ) :
  Transformer(name, pSvcLocator,
	      KeyValue{"SeedInput", LHCb::TrackLocation::Seed},
	      KeyValue{"MatchOutput", LHCb::TrackLocation::Match})
{
}

//=============================================================================
//
StatusCode MuonMatchBase::initialize() {

  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc;

  if ( m_doTiming ) {
    
    m_timerTool = tool<ISequencerTimerTool>("SequencerTimerTool");
    m_timerTool->increaseIndent();
    m_toolTime = m_timerTool->addTimer("Internal timer for a MuonMatch transformer");
    m_timerTool->decreaseIndent();
  }

  m_hitManager = tool<CommonMuonHitManager>("CommonMuonHitManager");
  m_fieldSvc   = svc<ILHCbMagnetSvc>("MagneticFieldSvc", true);

  return sc;
}

//=============================================================================
//
Candidates MuonMatchBase::extractSeeds( const LHCb::Track& seed ) const {

  // Get seeds
  auto seeds = this->findSeeds(seed, order[0]);

  // Add hits to seeds
  for ( Candidate& c : seeds ) addHits(c);

  // Create histograms
  if ( produceHistos() ) {
    plot(seeds.size(), "NSeedHits", -0.5, 50.5, 51);
    for ( const auto& c : seeds )
      if ( c.fitatt )
	plot(c.fitatt->chi2ndof, "Chi2DoFX", 0, 100, 100);
  }

  return std::move(seeds);
}

//=============================================================================
//
LHCb::Tracks MuonMatchBase::operator() ( const LHCb::Tracks& seeds ) const {
  
  if ( m_doTiming ) m_timerTool->start(m_toolTime);

  LHCb::Tracks matched;
  
  counter("#seeds") += seeds.size();
  
  for ( const auto& seed : seeds ) {
    
    LHCb::Track::Vector tmp;
    
    this->match(*seed, tmp);
    
    if ( !matched.empty() )
      for ( auto &track : tmp )
	matched.insert(track);
  }

  counter("#matched") += matched.size();
  
  if ( m_doTiming ) m_timerTool->stop(m_toolTime);
  
  return std::move(matched);
}
