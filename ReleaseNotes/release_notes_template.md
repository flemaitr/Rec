{% set used = [] -%}
{% macro section(labels) -%}
{% for mr in select_mrs(merge_requests, labels, used) %}
- {{mr.title}}, !{{mr.iid}} (@{{mr.author.username}}) {{find_tasks(mr)}}  
  {{mr.description|mdindent(2)}}
{% endfor %}
{%- endmacro %}

{{date}} {{project}} {{version}}
===

This version uses Lbcom v22r0, LHCb v44r0, Gaudi v29r3 and LCG_93 with ROOT 6.12.06
<p>
This version is released on `2018-patches` branch.

Built relative to Rec v22r1, with the following changes:

### New features
{{ section(['new feature']) }}

### Enhancements
{{ section(['enhancement']) }}

### Bug fixes
{{ section(['bug fix']) }}

### Code modernisations and cleanups
{{ section(['cleanup', 'modernisation']) }}

### Monitoring changes
{{ section(['monitoring']) }}

### Changes to tests
{{ section(['testing']) }}

### Other
{{ section([[]]) }}
