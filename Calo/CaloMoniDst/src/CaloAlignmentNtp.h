#ifndef CALOALIGNEMENTNTP_H 
#define CALOALIGNEMENTNTP_H 1

// Include files
// from Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "Event/ODIN.h" 
#include "Event/ProtoParticle.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloInterfaces/ICaloHypo2Calo.h"
#include "CaloInterfaces/ICounterLevel.h"
// #include "CaloInterfaces/ICaloHypoEstimator.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "CaloUtils/CaloMomentum.h"
#include "CaloUtils/ICaloElectron.h"
#include "TrackInterfaces/ITrackExtrapolator.h"


// List of Consumers dependencies
namespace {
  using Vertices = LHCb::RecVertices;
  using ODIN = LHCb::ODIN;
  using Digits = LHCb::CaloDigits;
  using Tracks = LHCb::Tracks;
  using Protos = LHCb::ProtoParticles;
}

//==============================================================================

/** @class CaloAlignmentNtp CaloAlignmentNtp.h
 *  
 *
 *  @author Olivier Deschamps
 *  @date   2009-12-11
 */

class CaloAlignmentNtp final
: public Gaudi::Functional::Consumer<void(const Vertices&, const ODIN&, const Digits&, const Tracks&, const Protos&),
    Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>>
{
public: 
  /// Standard constructor
  CaloAlignmentNtp( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  void operator()(const Vertices&, const ODIN&, const Digits&, const Tracks&, const Protos&) const override;

  /// C++11 non-copyable idiom
  CaloAlignmentNtp() = delete;
  CaloAlignmentNtp( const CaloAlignmentNtp& ) = delete;
  CaloAlignmentNtp &operator=( const CaloAlignmentNtp& ) = delete;

private:
  bool acceptTrack(const LHCb::Track*) const;
  bool hypoProcessing(const LHCb::CaloHypo* hypo) const;
  bool inArea(const LHCb::CaloHypo* hypo) const;

  // Tools
  DeCalorimeter* m_calo = nullptr;
  std::string m_vertLoc;

  ToolHandle<ICounterLevel>      m_counterStat  { "CounterLevel"};
  ToolHandle<ICaloElectron>      m_caloElectron { "CaloElectron"                            , this };
  ToolHandle<IEventTimeDecoder>  m_odin         { "OdinTimeDecoder/OdinDecoder"             , this };
  ToolHandle<ICaloHypo2Calo>     m_toSpd        { "CaloHypo2Calo/CaloHypo2Spd"              , this };
  ToolHandle<ICaloHypo2Calo>     m_toPrs        { "CaloHypo2Calo/CaloHypo2Prs"              , this };
  ToolHandle<ITrackExtrapolator> m_extrapolator { "TrackRungeKuttaExtrapolator/Extrapolator", this };

  // Flags -- init
  Gaudi::Property<bool> m_usePV3D { this, "UsePV3D", false};
  Gaudi::Property<std::vector<int>> m_tracks { this, "TrackTypes", { LHCb::Track::Types::Long }};

  // Flags -- global cuts
  Gaudi::Property<std::pair<double, double>> m_nSpd { this, "SpdMult"  , { 0. , 250.    }};
  Gaudi::Property<std::pair<double, double>> m_nTrk { this, "nTracks"  , { 0. , 350.    }};
  Gaudi::Property<std::pair<double, double>> m_nVtx { this, "nVertices", { -1., 999999. }};
  Gaudi::Property<bool> m_inArea {this, "inAreaAcc", true};

  // Flags -- hypo cuts
  Gaudi::Property<std::pair<double, double>> m_e   { this, "EFilter"  , { 0.  , 99999999. }};
  Gaudi::Property<std::pair<double, double>> m_et  { this, "EtFilter" , { 150., 999999.   }};
  Gaudi::Property<std::pair<double, double>> m_prs { this, "PrsFilter", { 10. , 1024     }};
  Gaudi::Property<std::pair<double, double>> m_spd { this, "SpdFilter", { .5  , 9999.    }};

  // Flags
  Gaudi::Property<std::pair<double, double>> m_eop    { this, "EoPFilter"          , { 0.7       , 1.3}};
  Gaudi::Property<std::pair<double, double>> m_dlle   { this, "DLLeFilter"         , { -9999999. , 9999999.}};
  Gaudi::Property<std::pair<double, double>> m_rdlle  { this, "RichDLLeFilter"     , { 0.        , 99999.}};
  Gaudi::Property<std::pair<double, double>> m_bMatch { this, "BremMatchFilter"    , { -99999999., 99999999.}};
  Gaudi::Property<std::pair<double, double>> m_eMatch { this, "ElectronMatchFilter", { -999999.  , 999999.}};
  Gaudi::Property<std::pair<double, double>> m_cMatch { this, "ClusterMatchFilter" , { -999999.  , 999999.}};
  Gaudi::Property<std::pair<double, double>> m_mas    { this, "MassFilter"         , { -99999.   , 99999.}};
  Gaudi::Property<std::pair<double, double>> m_dist   { this, "BremEleDistFilter"  , { 4.        , 99999.}};

  Gaudi::Property<bool>  m_pairing { this, "EmlectronPairing"    , true};
  Gaudi::Property<float> m_min     { this, "DeltaMin"            , -150.};
  Gaudi::Property<float> m_max     { this, "DeltaMax"            , +150.};
  Gaudi::Property<int>   m_thBin   { this, "ThetaBin"            , 14};
  Gaudi::Property<int>   m_bin     { this, "DeltaBin"            , 150};
  Gaudi::Property<bool>  m_brem    { this, "ElectronWithBremOnly", false};
  Gaudi::Property<float> m_r       { this, "SshapeRange"         , 0.7};
  Gaudi::Property<int>   m_b       { this, "SshapeBin"           , 50};
  
  // Flags -- outputs
  Gaudi::Property<bool> m_histo  { this, "Histo"  , true};
  Gaudi::Property<bool> m_tuple  { this, "Tuple"  , true};
  Gaudi::Property<bool> m_profil { this, "Profile", true};
  
};
#endif // CALOALIGNEMENTNTP_H
