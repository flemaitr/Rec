#include "CaloEFlowBase.h"
#include "GaudiAlg/Consumer.h"
#include "Event/ODIN.h"

/*
  Class CaloEFlowAlg

  Refactored from the original implementation to allow migration to Gaudi::Functional,
  such that this class's computation doesn't rely on MC objects.

  Author: Chitsanu Khurewathanakul
  Data  : 2016-10-29
 */

class CaloEFlowAlg final
: public Gaudi::Functional::Consumer<void(const Input&, const LHCb::ODIN&),
    Gaudi::Functional::Traits::BaseClass_t<CaloEFlowBase>>
{
public:
  void operator()(const Input&, const LHCb::ODIN&) const override;

  CaloEFlowAlg(const std::string &name, ISvcLocator *pSvcLocator);

private:
  Gaudi::Property<bool> m_simulation              { this, "Simulation"             , true};
  Gaudi::Property<bool> m_ignoreTAE               { this, "IgnoreTAE"              , true};
  Gaudi::Property<bool> m_ignoreNonBeamCrossing   { this, "IgnoreNonBeamCrossing"  , true};
  Gaudi::Property<bool> m_ignoreNonPhysicsTrigger { this, "IgnoreNonPhysicsTrigger", true};
};

//==============================================================================

DECLARE_COMPONENT( CaloEFlowAlg )

//==============================================================================

CaloEFlowAlg::CaloEFlowAlg( const std::string &name, ISvcLocator *pSvcLocator )
  : Consumer( name, pSvcLocator, {
      KeyValue{ "Input"      , "" },
      KeyValue{ "InputODIN"  , LHCb::ODINLocation::Default },
  })
{
  // Configure Input, based on detData()
  updateHandleLocation( *this, "Input", digitLocation());
}

//==============================================================================
// Main execution
//==============================================================================

void CaloEFlowAlg::operator()(const Input& digits, const LHCb::ODIN& evt) const {

  // In simulated sample, it bypasses this check
  if(!m_simulation) {
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
      debug() << "Event: " << evt.eventNumber() << " Run: " << evt.runNumber() << endmsg;

    const LHCb::ODIN::BXTypes bxtype = evt.bunchCrossingType();
    const unsigned int tae           = evt.timeAlignmentEventWindow();
    const unsigned int trigger       = evt.triggerType();

    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
      debug() << " TAE " << tae
              << " BXType " << bxtype
              << " trigger type " << trigger
              << endmsg;

    if (tae!=0 && m_ignoreTAE) {
      if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
        debug() << " TAE WINDOW SET TO " << tae << " WILL SKIP THE EVENT " << endmsg;
      return;
    }
    if (bxtype!=3 && m_ignoreNonBeamCrossing) {
      //3 =  BeamCrossing
      if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
        debug() << " BEAM CROSSING TYPE IS " << bxtype << " WILL SKIP THE EVENT " << endmsg;
      return;
    }
    if (trigger!=1 && m_ignoreNonPhysicsTrigger){
      //1 = PhysicsTrigger
      if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
        debug() << " TRIGGER TYPE IS " << trigger << " WILL SKIP THE EVENT " << endmsg;
      return;
    }
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
      debug() << " TAE " << tae
              << " BXType " << bxtype
              << " trigger type " << trigger
              << endmsg;
  }

  // produce histos ?
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
    debug() << " Producing histo " << produceHistos() << endmsg;
  if ( !produceHistos() ) return;

  // get input data
  if ( digits.empty() ){
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
      debug() << "Empty digit found at " << inputLocation() << endmsg;
    return;
  }

  // Main execution in the CaloEFlowBase
  process_digits(digits);
}

//==============================================================================
