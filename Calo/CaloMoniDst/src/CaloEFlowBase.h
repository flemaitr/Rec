#ifndef CALOENERGYFLOWMONITOR_H 
#define CALOENERGYFLOWMONITOR_H 1

// Includes
#include "Event/CaloDigit.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloMoniAlg.h"

// =============================================================================

/** @class CaloEFlowAlg CaloEFlowAlg.h
 *  
 *
 *  The algorithm for dedicated "EnergyFlow" monitoring of "CaloDigits" containers.
 *  The algorithm produces the following histograms:
 *   1. CaloDigit multiplicity
 *   2. CaloDigit ocupancy 2D plot per area
 *   3. CaloDigit energy and transverse energy 2D plot per area
 *  The same set of histograms, but with cut on Et (or E), is produced if specified
 *
 *  Histograms reside in the directory @p /stat/"Name" , where
 *  @p "Name" is the name of the algorithm
 *
 *  @see   CaloMoniAlg
 *  @see GaudiHistoAlg
 *  @see GaudiAlgorithm
 *  @see      Algorithm
 *  @see     IAlgorithm
 *
 *  @author Aurelien Martens
 *  @date   2009-04-08
 */

namespace {
  using Input = LHCb::CaloDigit::Container;
}

class CaloEFlowBase: public CaloMoniAlg {

public:
  using CaloMoniAlg::CaloMoniAlg;
  StatusCode initialize() override;

protected:
  DeCalorimeter *m_calo = nullptr;

  // Helper method to determine input location from detector name
  std::string digitLocation() const {
    if(detData()     == "Ecal" ){ return LHCb::CaloDigitLocation::Ecal; }
    else if(detData()== "Hcal" ){ return LHCb::CaloDigitLocation::Hcal; }
    else if(detData()== "Prs"  ){ return LHCb::CaloDigitLocation::Prs; }
    else if(detData()== "Spd"  ){ return LHCb::CaloDigitLocation::Spd; }    
    return "";
  }

  std::string deCaloLocation() const {
    if      ( "Ecal" == detData() ) { return DeCalorimeterLocation::Ecal; }  
    else if ( "Hcal" == detData() ) { return DeCalorimeterLocation::Hcal; }
    else if ( "Prs"  == detData() ) { return DeCalorimeterLocation::Prs; }  
    else if ( "Spd"  == detData() ) { return DeCalorimeterLocation::Spd; }  
    return "";
  }

  // Main computation to be shared by subclass
  void process_digits(const Input&) const;

private:
  Gaudi::Property<float> m_eFilterMin   { this, "EnergyFilterMin", -999};
  Gaudi::Property<float> m_etFilterMin  { this, "EtFilterMin"    , -999};
  Gaudi::Property<float> m_eFilterMax   { this, "EnergyFilterMax", -999};
  Gaudi::Property<float> m_etFilterMax  { this, "EtFilterMax"    , -999};
  Gaudi::Property<int>   m_ADCFilterMin { this, "ADCFilterMin"   , -999};
  Gaudi::Property<int>   m_ADCFilterMax { this, "ADCFilterMax"   , -999};

};
#endif // CALOENERGYFLOWMONITOR_H

