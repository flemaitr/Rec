// ===========================================================================
#ifndef CALORECO_CALOCLUSTERCOVARIANCEALG_H
#define CALORECO_CALOCLUSTERCOVARIANCEALG_H 1
/// ===========================================================================
// Include files
// from STL
#include <string>
#include "GaudiAlg/GaudiAlgorithm.h"
#include "CaloInterfaces/ICounterLevel.h"

struct ICaloClusterTool   ;
class SubClusterSelectorTool ;

/** @class CaloClusterCovarianceAlg CaloClusterCovarianceAlg.h
 *
 *   Simple algorithm for evaluation of covariance matrix
 *   for CaloCluster object
 *
 *  @author Vanya BElyaev Ivan Belyaev
 *  @date   04/07/2001
 */

class CaloClusterCovarianceAlg : public GaudiAlgorithm
{
public:

  /** Standard constructor
   *  @param   name          algorith name
   *  @param   pSvcLocator   pointer to Service Locator
   */
  CaloClusterCovarianceAlg
  ( const std::string& name  ,
    ISvcLocator*       pSvcLocator );
  
  /** standard initialization method
   *  @see GaudiAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode initialize() override;

  /** standard execution method
   *  @see GaudiAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode execute() override;

  /** standard finalization method
   *  @see GaudiAlgorithm
   *  @see     Algorithm
   *  @see    IAlgorithm
   *  @return status code
   */
  StatusCode finalize() override;

protected:

  inline ICaloClusterTool*       cov    () const { return m_cov    ; }
  inline ICaloClusterTool*       spread () const { return m_spread ; }
  inline SubClusterSelectorTool* tagger () const { return m_tagger ; }


private:

  /// default constructor is private
  CaloClusterCovarianceAlg();

  /** copy constructor is private
   *  @param copy object to be copied
   */
  CaloClusterCovarianceAlg
  ( const  CaloClusterCovarianceAlg& copy );

  /** assignement operator is private
   *  @param copy object to be copied
   */
  CaloClusterCovarianceAlg& operator=
  ( const  CaloClusterCovarianceAlg& copy );

private:

  bool                    m_copy = false ;  /// copy flag

  // tool used for covariance matrix calculation
  Gaudi::Property<std::string> m_covType {this, "CovarianceType", "ClusterCovarianceMatrixTool"};
  Gaudi::Property<std::string> m_covName {this, "CovarianceName"};
  ICaloClusterTool*       m_cov    = nullptr ; ///< tool
  SubClusterSelectorTool* m_tagger = nullptr ;

  // tool used for cluster spread estimation
  Gaudi::Property<std::string> m_spreadType {this, "SpreadType", "ClusterSpreadTool"};
  Gaudi::Property<std::string> m_spreadName {this, "SpreadName"};
  ICaloClusterTool*       m_spread = nullptr ; ///< tool

  Gaudi::Property<std::string> m_outputData {this, "OutputData"};
  Gaudi::Property<std::string> m_inputData  {this, "InputData"};
  Gaudi::Property<std::string> m_tagName    {this, "TaggerName"};

  // following properties are inherited by the selector tool when defined:
  Gaudi::Property<std::vector<std::string>> m_taggerE {this, "EnergyTags"};
  Gaudi::Property<std::vector<std::string>> m_taggerP {this, "PositionTags"};

  // collection of known cluster shapes
  std::map<std::string , std::string> m_clusterShapes;
  std::map<std::string,std::vector<double> > m_covParams;
  ICounterLevel* counterStat = nullptr;
  };

#endif // CALORECO_CALOCLUSTERCOVARIANCEALG_H
