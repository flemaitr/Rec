#ifndef CALOCLUSTERIZATIONTOOL_H
#define CALOCLUSRERIZATIONTOOL_H 1
// ============================================================================
#include <string>
#include <iostream>
#include <string>
#include <iostream>
#include "GaudiAlg/GaudiTool.h"
#include "CaloKernel/CaloVector.h"
#include "CaloInterfaces/ICaloClusterization.h"
#include "CaloInterfaces/ICounterLevel.h"
#include "CaloDet/DeCalorimeter.h"
#include "CelAutoTaggedCell.h"
#include "CaloUtils/CellSelector.h"

/** @class CaloClusterizationTool CaloClusterizationTool.h
 *
 *
 *  @author Victor Egorychev
 *  @date   2008-04-03
 */
class CaloClusterizationTool : public extends<GaudiTool,ICaloClusterization> {

public:
  /// container to tagged  cells with sequential access
  typedef std::vector<CelAutoTaggedCell*> SeqVector;
  /// container to tagged  cells with direct (by CaloCellID key)  access
  typedef CaloVector<CelAutoTaggedCell*>  DirVector ;


  /// Standard constructor
  CaloClusterizationTool( const std::string& type,
                 const std::string& name,
                 const IInterface* parent);

  StatusCode clusterize
  ( std::vector<LHCb::CaloCluster*>&      clusters   ,
    const LHCb::CaloDigits*               hits       ,
    const DeCalorimeter*                  detector   ,
    const std::vector<LHCb::CaloCellID>&  seeds      ,
    const unsigned int                    level      ) override
  {
    return _clusterize ( clusters , *hits , detector , seeds , level ) ;
  }

  // ==========================================================================

  StatusCode clusterize
  ( std::vector<LHCb::CaloCluster*>&      clusters   ,
    const CaloVector<LHCb::CaloDigit*>&   hits       ,
    const DeCalorimeter*                  detector   ,
    const std::vector<LHCb::CaloCellID>&  seeds      ,
    const unsigned int                    level      ) override
  {
    return _clusterize ( clusters , hits , detector , seeds , level ) ;
  }

  // ==========================================================================
  StatusCode clusterize
  ( std::vector<LHCb::CaloCluster*>&      clusters   ,
    const LHCb::CaloDigits*               hits       ,
    const DeCalorimeter*                  detector   ,
    const LHCb::CaloCellID&               seed       ,
    const unsigned int                    level      ) override
  {
    return clusterize ( clusters , hits , detector ,
                        std::vector<LHCb::CaloCellID>(1,seed) , level ) ;
  };
  // ==========================================================================
  StatusCode clusterize
  ( std::vector<LHCb::CaloCluster*>&      clusters   ,
    const LHCb::CaloDigits*               hits       ,
    const DeCalorimeter*                  detector   ) override
  {
    const unsigned int level = 0;
    std::vector<LHCb::CaloCellID>  seeds;
    seeds.clear();
    return clusterize ( clusters , hits , detector, seeds, level) ;
  } ;
  unsigned int iterations() override {return m_pass;};
  StatusCode initialize() override;

protected:

  template<class TYPE> StatusCode _clusterize
  ( std::vector<LHCb::CaloCluster*>&      clusters   ,
    const TYPE&                           data       ,
    const DeCalorimeter*                  detector   ,
    const std::vector<LHCb::CaloCellID>&  seeds      ,
    const unsigned int                    level      )  ;

  inline bool isLocMax
  ( const LHCb::CaloDigit*     digit ,
    const DirVector&     hits  ,
    const DeCalorimeter* det ) ;

  inline void appliRulesTagger
  ( CelAutoTaggedCell*   taggedCell,
    DirVector&           taggedCellsDirect,
    const DeCalorimeter* detector );

  inline StatusCode setEXYCluster( LHCb::CaloCluster*         cluster,const DeCalorimeter* detector );

private:

  bool m_withET = false;
  double m_ETcut =  -10. * Gaudi::Units::GeV;
  bool m_release = false;
  std::string m_usedE;
  std::string m_usedP;
  CellSelector m_cellSelectorE;
  CellSelector m_cellSelectorP;
  unsigned int m_pass = 0;
  unsigned int m_passMax = 10;
  ICounterLevel* counterStat = nullptr;
  };
#endif // CALOCLUSTERIZATIONTOOL_H
