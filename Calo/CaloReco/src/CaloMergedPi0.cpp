// ============================================================================
// ============================================================================
#include <numeric>
#include <algorithm>
#include <cmath>
#include "Event/CaloDataFunctor.h"
#include "CaloDet/DeCalorimeter.h"
#include "Event/CellID.h"
#include "Event/CaloHypo.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "Kernel/CaloCellID.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "CaloMergedPi0.h"
#include "CaloUtils/CaloMomentum.h"
// ============================================================================
/** @file CaloMergedPi0.cpp
 *
 *  Implementation file for class : CaloMergedPi0
 *
 *  @author Olivier Deschamps
 *  @date 05/05/2014
 *
 *  New implementation of CaloMergedPi0 algorithm
 *
 */
// ============================================================================

DECLARE_COMPONENT( CaloMergedPi0 )

  CaloMergedPi0::CaloMergedPi0( const std::string& name    ,
                                      ISvcLocator*       svcloc  ): GaudiAlgorithm ( name , svcloc )
{
  // following properties are be inherited by the covariance tool
  declareProperty( "CovarianceParameters" , m_covParams    ) ; // KEEP IT UNSET ! INITIAL VALUE WOULD BYPASS DB ACCESS

  // default context-dependent locations
  m_clusters  = LHCb::CaloAlgUtils::CaloClusterLocation ( "Ecal"     , context()    );  // input : neutral CaloCluster's
  m_mergedPi0s = LHCb::CaloAlgUtils::CaloHypoLocation   ("MergedPi0s" , context()   );  // output : mergedPi0 CaloHypo's
  m_splitPhotons  = LHCb::CaloAlgUtils::CaloHypoLocation("SplitPhotons", context() );  // output : splitPhoton CaloHypo's
  m_splitClusters = LHCb::CaloAlgUtils::CaloSplitClusterLocation(context()         );  // output : splitCluster CaloClusters
}

bool CaloMergedPi0::isNeighbor(LHCb::CaloCellID id0 , LHCb::CaloCellID id1){
  if( id0 == LHCb::CaloCellID() || id1 == LHCb::CaloCellID() )return false;
  if( abs(int(id0.row()) - int(id1.row()) ) > 1 )return false;
  if( abs(int(id0.col()) - int(id1.col()) ) > 1 )return false;
  return true;
}

// ============================================================================

StatusCode CaloMergedPi0::initialize(){
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug()<< "==> Initialise" << endmsg;
  StatusCode sc = GaudiAlgorithm::initialize();
  if( sc.isFailure() )return Error("Could not initialize the base class!",sc);

  // Always skip negative-energy clusters :
  if(m_minET<0.)m_minET=0.;

  if(m_createClusterOnly)info() << "Producing SplitClusters only" << endmsg;

  // get detectorElement
  m_detector = getDet<DeCalorimeter>( m_det ) ;
  if( !m_detector ){ return Error("could not locate calorimeter '"+m_det+"'");}

  //==== get tools

  // - main tool :
  m_oTool=tool<ICaloShowerOverlapTool>("CaloShowerOverlapTool","SplitPhotonShowerOverlap",this);

  // - cluster  tools
  m_cov     = tool<ICaloClusterTool>      ( "ClusterCovarianceMatrixTool" , "EcalCovariance"     , this ) ;
  m_spread  = tool<ICaloClusterTool>      ( "ClusterSpreadTool"           , "EcalSpread"         , this ) ;
  m_tagger  = tool<SubClusterSelectorTool>( "SubClusterSelectorTool"      , "EcalClusterTag"     , this );

  // - hypo tools
  for ( std::vector<std::string>::const_iterator it = m_photonTools.begin() ;m_photonTools.end() != it ; ++it ){
    ICaloHypoTool* t = tool<ICaloHypoTool>( *it , this );
    if( 0 == t ) { return StatusCode::FAILURE ; }
    m_gTools.push_back( t ) ;
  }

  for ( std::vector<std::string>::const_iterator it = m_pi0Tools.begin() ;m_pi0Tools.end() != it ; ++it ){
    ICaloHypoTool* t = tool<ICaloHypoTool>( *it , this );
    if( 0 == t ) { return StatusCode::FAILURE ; }
    m_pTools.push_back( t ) ;
  }
  counterStat = tool<ICounterLevel>("CounterLevel");
  return sc;
}

StatusCode CaloMergedPi0::finalize(){
  m_pTools.clear() ;
  m_gTools.clear() ;
  return GaudiAlgorithm::finalize();
}

StatusCode CaloMergedPi0::execute(){

  using namespace  LHCb::CaloDataFunctor;
  typedef LHCb::CaloClusters Clusters;
  typedef Clusters::iterator Iterator;

  //======== load input :
  Clusters* clusters = getIfExists<Clusters>( m_clusters );
  if( NULL == clusters )return Warning("No cluster input container : no merged Pi0 !", StatusCode::SUCCESS);


  //======== create cluster output :
  //- split clusters (check it does not exist first)


  LHCb::CaloClusters* splitclusters = new LHCb::CaloClusters();
  int level = msgLevel();     // store outputLevel
  try{
    setProperty("OutputLevel", MSG::ALWAYS ).ignore();// suppress FATAL message
    put(splitclusters, m_splitClusters );
  } catch(GaudiException &exc ) {
    setProperty("OutputLevel", level ).ignore();      // reset outputLevel before issuing a warning...
    Warning("Existing SplitCluster container at "+ m_splitClusters + " found -> will replace",StatusCode::SUCCESS,1).ignore();
    delete splitclusters;
    splitclusters=get<LHCb::CaloClusters>( m_splitClusters );
    splitclusters->clear();
  }
  setProperty("OutputLevel", level ).ignore();


  //- pi0s & SPlitPhotons
  LHCb::CaloHypos* pi0s = new LHCb::CaloHypos();
  LHCb::CaloHypos* phots = new LHCb::CaloHypos();

  // put on TES when requested
  if(!m_createClusterOnly){
    put( pi0s , m_mergedPi0s );
    put( phots , m_splitPhotons);
  }


  //- load SPD container :
  LHCb::CaloDigits* spds = getIfExists<LHCb::CaloDigits>( LHCb::CaloDigitLocation::Spd );

  // - setup the estimator of cluster transverse energy
  LHCb::CaloDataFunctor::EnergyTransverse<const DeCalorimeter*> eT ( m_detector ) ;

    // define entry status'

  LHCb::CaloDigitStatus::Status used   =
    LHCb::CaloDigitStatus::UseForEnergy  |
    LHCb::CaloDigitStatus::UseForPosition |
    LHCb::CaloDigitStatus::UseForCovariance  ;
  LHCb::CaloDigitStatus::Status seed   = LHCb::CaloDigitStatus::SeedCell |
    LHCb::CaloDigitStatus::LocalMaximum | used ;

  // ============ loop over all clusters ==============
  for( Iterator icluster = clusters->begin() ; clusters->end() != icluster ; ++icluster ){
    LHCb::CaloCluster* cluster = *icluster ;
    if( 0 == cluster )                { continue ; }
    if ( 0 < m_etCut &&  m_etCut > eT( cluster ) ) { continue ; }

    // -- remove small clusters :
    if( 1 >=  cluster->entries().size() )continue;

    // -- locate cluster Seed
    const LHCb::CaloCluster::Digits::iterator iSeed =
      clusterLocateDigit( cluster->entries().begin() , cluster->entries().end  () , LHCb::CaloDigitStatus::SeedCell     );
    LHCb::CaloDigit* dig1 = iSeed->digit() ;
    if( 0 == dig1) continue ;
    LHCb::CaloCellID  seed1 = dig1->cellID() ;

    double seede  = dig1->e();

    // -- get spd hit in front of seed1
    int spd1 = 0 ;
    const LHCb::CaloDigit* spddigit1 = (spds == NULL) ? NULL : spds->object( dig1->key() );
    if( NULL != spddigit1 )spd1 = (spddigit1->e() > 0.) ? 1 : 0 ;

    // -- locate seed2
    double sube   = 0. ; // 2nd seed should must have a positive energy !
    LHCb::CaloDigit*  dig2  = nullptr;
    for( LHCb::CaloCluster::Digits::iterator it =cluster->entries().begin() ; cluster->entries().end() != it ; ++it ){
      LHCb::CaloDigit* dig = it->digit() ;
      if( !dig ) { continue ; }
      LHCb::CaloCellID seed  = dig->cellID() ;
      double ecel = dig->e()*it->fraction();
      if (ecel > sube && ecel < seede && isNeighbor( seed1, seed) && !(seed==seed1)){
        //if (ecel > sube && ecel < seede && isNeighbor( seed1, seed) ){
        sube=ecel;
        dig2=dig;
      }
    }

    if ( !dig2 ){
      if(counterStat->isQuiet())counter("Cluster without 2nd seed found") += 1;
      continue ;
    }

    LHCb::CaloCellID  seed2 = dig2->cellID() ;
    // -- get spd hit in front of seed2
    const LHCb::CaloDigit* spddigit2 = ( spds  ? spds->object( dig2->key() ) : nullptr );
    int spd2 = 0 ;
    if( spddigit2 ) { spd2 = (spddigit2->e() > 0.) ? 1 : 0 ; }


    // -- create and fill sub-cluster
    auto cl1 = std::make_unique<LHCb::CaloCluster>();
    cl1->setSeed( seed1 );
    cl1->setType( LHCb::CaloCluster::Area3x3 );
    auto cl2 = std::make_unique<LHCb::CaloCluster>();
    cl2->setSeed( seed2 );
    cl2->setType( LHCb::CaloCluster::Area3x3 );

    for( const auto& it2 : cluster->entries() ) {
      const LHCb::CaloDigit* dig = it2.digit() ;
      if( !dig ) continue ;
      const LHCb::CaloCellID  id = dig->cellID() ;
      double fraction = it2.fraction();

      // -- tag 3x3 area for energy and position
      if ( isNeighbor( seed1, id ) ){
        LHCb::CaloDigitStatus::Status status = ( seed1 == id ) ? seed : used;
        // set initial weights
        double weight1 = fraction;
        if( seed2 == id )weight1=0.;
        else if( seed1 == id )weight1=fraction;
        else if (isNeighbor(seed2,id)) weight1=dig1->e() / (dig1->e() + dig2->e())*fraction;
        cl1->entries().emplace_back( dig , status, weight1 );
      }
      if ( isNeighbor( seed2, id ) ){
        LHCb::CaloDigitStatus::Status status = ( seed2 == id ) ? seed : used;
        //set initial weights
        double weight2 = fraction;
        if( seed1 == id )weight2=0.;
        else if(seed2 == id)weight2=fraction;
        else if (isNeighbor(seed1,id))weight2=dig2->e() / (dig1->e() + dig2->e())*fraction;
        cl2->entries().emplace_back( dig , status,weight2 );
      }
    }


    // --  apply position tagger (possibly replacing the 3x3 already set)
    // --  needed to apply hypo S/L-corrections with correct parameters internally
    // --  keep the 3x3 energy tag for the time being (to be applied after the overlap subtraction)
    StatusCode sc;
    sc = m_tagger->tagPosition(  cl1.get()  ) ;
    sc = m_tagger->tagPosition(  cl2.get()  ) ;
    if( sc.isFailure() )Warning("SplitCluster tagging failed - keep the initial 3x3 tagging").ignore();

    // == apply the mergedPi0 tool : subtract shower overlap
    m_oTool->process(cl1.get(),cl2.get(), spd1*10+spd2, m_iter,true); // 'true' means the initial entries weight is propagated
    if( LHCb::CaloMomentum(cl1.get()).pt() <= m_minET || LHCb::CaloMomentum(cl2.get()).pt() <= m_minET ){ // skip negative energy "clusters"
      continue;
    }

    // == prepare outputs :

    // apply loose mass window : (TODO ??)

    // == APPLY CLUSTER TOOLS : Energy tagger,  covariance & spread (position tagger already applied):
      if  (m_tagger  -> tagEnergy    ( cl1.get() ).isFailure()  )if (counterStat->isQuiet() )counter("Fails to tag(E) cluster (1)")+=1;
      if  (m_tagger  -> tagEnergy    ( cl2.get() ).isFailure()  )if (counterStat->isQuiet() )counter("Fails to tag(E) cluster (2)")+=1;
      if  (m_cov   -> process( cl1.get() ).isFailure()  )if (counterStat->isQuiet() )counter("Fails to set covariance (1)")+=1;
      if  (m_cov   -> process( cl2.get() ).isFailure()  )if (counterStat->isQuiet() )counter("Fails to set covariance (2)")+=1;
      if  (m_spread-> process( cl1.get() ).isFailure()  )if (counterStat->isQuiet() )counter("Fails to set spread (1)")    +=1;
      if  (m_spread-> process( cl2 .get()).isFailure()  )if (counterStat->isQuiet() )counter("Fails to set spread (2)")    +=1;

    // == insert splitClusters into their container
    auto clu1 = cl1.get(); splitclusters->insert( cl1.release() ) ;
    auto clu2 = cl2.get(); splitclusters->insert( cl2.release() ) ;
    // == create CaloHypos if needed
    if (!m_createClusterOnly) {
      // new CaloHypos for splitPhotons
      auto g1   = std::make_unique<LHCb::CaloHypo>() ;
      g1 -> setHypothesis( LHCb::CaloHypo::PhotonFromMergedPi0 ) ;
      g1 -> addToClusters( cluster )                ;
      g1 -> addToClusters( clu1    )                ;
      g1 -> setPosition( std::make_unique< LHCb::CaloPosition>( clu1->position()) );

      auto g2   = std::make_unique<LHCb::CaloHypo>() ;
      g2 -> setHypothesis( LHCb::CaloHypo::PhotonFromMergedPi0 ) ;
      g2 -> addToClusters( cluster )                ;
      g2 -> addToClusters( clu2    )                ;
      g2 -> setPosition( std::make_unique<LHCb::CaloPosition>( clu2->position() ) );

      // new CaloHypo for mergedPi0
      auto pi0 = std::make_unique<LHCb::CaloHypo>();
      pi0 -> setHypothesis( LHCb::CaloHypo::Pi0Merged ) ;
      pi0 -> addToClusters( cluster );
      pi0 -> addToHypos ( g2.get() );
      pi0 -> addToHypos ( g1.get() );

      //--  Apply hypo tools : E/S/L-corrections
      int i = 0;
      for( ICaloHypoTool* t : m_gTools ) {
        i++;
        if( UNLIKELY( msgLevel(MSG::DEBUG) ) )debug() << " apply SplitPhoton tool " << i << "/" << m_gTools.size() << endmsg;
        if( !t )  continue;
        auto sc = (*t) ( g1.get() ) ;
        if( sc.isFailure() ) Error("Error from 'Tool' for g1 " , sc ).ignore() ;
        sc      = (*t) ( g2.get() ) ;
        if( sc.isFailure() )Error("Error from 'Tool' for g2 " , sc ).ignore() ;
      }

      i = 0;
      for( ICaloHypoTool* t : m_pTools ) {
        i++;
        if( UNLIKELY( msgLevel(MSG::DEBUG) ) )debug() << " apply MergedPi0 tool " << i << "/" << m_pTools.size() << endmsg;
        if( !t ) { continue; }
        auto sc = (*t) ( pi0.get() ) ;
        if( sc.isFailure() )Error("Error from 'Tool' for pi0 " , sc ).ignore() ;
      }


      // skip negative energy CaloHypos
      if( LHCb::CaloMomentum(g1.get()).pt() >= m_minET && LHCb::CaloMomentum(g2.get()).pt() >= m_minET ){
        if ( m_verbose ) info() << " >> MergedPi0 hypo Mass : "  << LHCb::CaloMomentum(pi0.get()).mass() << endmsg;
        phots ->insert( g1.release()  ) ;
        phots ->insert( g2.release()  ) ;
        pi0s -> insert( pi0.release() ) ;
      }
    }
  }

  // ====================== //
  if(counterStat->isQuiet()&&!m_createClusterOnly)counter ( m_clusters + "=>" + m_mergedPi0s )   += pi0s   -> size() ;
  if(counterStat->isQuiet()&&!m_createClusterOnly)counter ( m_clusters + "=>" + m_splitPhotons)  += phots  -> size() ;
  if(counterStat->isQuiet())counter ( m_clusters + "=>" + m_splitClusters) += splitclusters -> size() ;

  // delete (empty) container* if not on TES
  StatusCode sc = StatusCode::SUCCESS;
  if(m_createClusterOnly){
    if( 0 != pi0s->size() || 0 != phots->size() ){
      sc = Error( "Container* to be deleted are not empty", StatusCode::FAILURE);
    }
    delete pi0s;
    delete phots;
  }
  return sc;
}
