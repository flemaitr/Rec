// Include files
#include "CaloEnergyForTrack.h"

// ============================================================================
/** @class SpdEnergyForTrack
 *  The concrete preconfigured insatnce for CaloEnergyForTrack tool
 *  along the track line
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================

class SpdEnergyForTrack final : public CaloEnergyForTrack {
 public:
  SpdEnergyForTrack(const std::string& type, const std::string& name,
                    const IInterface* parent)
      : CaloEnergyForTrack(type, name, parent) {
    _setProperty("DataAddress", LHCb::CaloDigitLocation::Spd);
    _setProperty("Tolerance", "2");  /// 2 * Gaudi::Units::mm
    _setProperty("Calorimeter", DeCalorimeterLocation::Spd);
  };
};

// ============================================================================

DECLARE_COMPONENT( SpdEnergyForTrack )
