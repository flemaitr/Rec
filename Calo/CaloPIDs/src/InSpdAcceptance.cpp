// Include files
#include "InCaloAcceptance.h"

// ============================================================================
/** @class InSpdAcceptance
 *  The precofigured instance of InCaloAcceptance Tool
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
// ============================================================================

struct InSpdAcceptance final : InCaloAcceptance {
  /// standard constructor
  InSpdAcceptance(const std::string& type, const std::string& name,
                  const IInterface* parent)
      : InCaloAcceptance(type, name, parent) {
    _setProperty("Calorimeter", DeCalorimeterLocation::Spd);
    _setProperty("UseFiducial", "false");
    _setProperty("Tolerance", "1");  /// 1 * Gaudi::Units::mm
  };

  /// C++11 non-copyable idiom
  InSpdAcceptance() = delete;
  InSpdAcceptance(const InSpdAcceptance&) = delete;
  InSpdAcceptance& operator=(const InSpdAcceptance&) = delete;
};

// ============================================================================

DECLARE_COMPONENT( InSpdAcceptance )

// ============================================================================
