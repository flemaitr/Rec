// Include files
#include "CaloTrackMatchAlg.h"
#include "ToString.h"

// ============================================================================
/** @file
 *  Implementation file for class CaloTrackMatchAlg
 *  @date 2006-06-16
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
/// Standard protected constructor
// ============================================================================

CaloTrackMatchAlg::CaloTrackMatchAlg(const std::string& name, ISvcLocator* pSvc)
    : CaloTrackAlg(name, pSvc) {
  declareProperty( "Tool", m_matcher );
  // context-dependent default track container
  m_tracks = LHCb::CaloAlgUtils::TrackLocations( context() ) ;
}

// ============================================================================
/// standard algorithm itinialization
// ============================================================================

StatusCode CaloTrackMatchAlg::initialize()
{
  StatusCode sc = CaloTrackAlg::initialize();
  if ( sc.isFailure() ) { return sc ; }
  //
  if ( m_tracks .empty() ) { Warning ( "empty 'Tracks'-list"   ).ignore() ; }
  if ( m_calos  .empty() ) { Warning ( "empty 'Calos'-list"    ).ignore() ; }
  if ( m_output .empty() ) { Warning ( "empty 'Output'-value"  ).ignore() ; }
  if ( m_matcher.empty() ) { Warning ( "empty 'Tool'-value"    ).ignore() ; }
  if ( m_filter .empty() ) { Warning ( "empty 'Filter'-value"  ).ignore() ; }

  // Retrieve tool  
  sc = m_matcher.retrieve();
  if (sc.isFailure())
    return sc;

  return StatusCode::SUCCESS;
}

// ============================================================================
