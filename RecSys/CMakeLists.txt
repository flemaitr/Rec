################################################################################
# Package: RecSys
################################################################################
gaudi_subdir(RecSys v23r0)

gaudi_depends_on_subdirs(Calo/CaloMoniDst
                         Calo/CaloPIDs
                         Calo/CaloReco
                         Calo/CaloTools
                         Hlt/HltMonitors
                         Muon/MuonID
			 Muon/MuonInterfaces
                         Muon/MuonPIDChecker
                         Muon/MuonTools
                         Muon/MuonTrackAlign
                         Muon/MuonTrackMonitor
                         Muon/MuonTrackRec
                         Pr/PrAlgorithms
                         Pr/PrFitParams
                         Pr/PrKernel
                         Pr/PrMCTools
                         Pr/PrPixel
                         Pr/PrUtils
                         Pr/PrVeloUT
			 Rec/ChargedProtoANNPID
                         Rec/GlobalReco
                         Rec/LumiAlgs
                         Rec/RecAlgs
                         Rec/RecConf
                         Rec/RecInterfaces
                         Rich/RichRecUtils
              		 Rich/RichFutureRecBase
                         Rich/RichFutureRecEvent
                         Rich/RichFutureRecPixelAlgorithms
                         Rich/RichFutureRecTrackAlgorithms
                         Rich/RichFutureRecPhotonAlgorithms
                         Rich/RichFutureRecAlgorithms
                         Rich/RichFutureGlobalPID
                         Rich/RichFutureRecSys
                         Rich/RichFutureRecMonitors
                         Rich/RichFutureRecCheckers
                         Rich/RichRecTests
                         Tf/FastPV
                         Tf/FastVelo
                         Tf/PatAlgorithms
			 Tf/PatKernel
                         Tf/PatVelo
                         Tf/PatVeloTT
                         Tf/TfKernel
                         Tf/TfTools
                         Tf/TrackSys
                         Tf/TsaAlgorithms
                         Tf/TsaKernel
                         Tr/PatChecker
                         Tr/PatFitParams
                         Tr/PatPV
                         Tr/TrackAssociators
                         Tr/TrackCheckers
                         Tr/TrackExtrapolators
                         Tr/TrackFitEvent
                         Tr/TrackFitter
                         Tr/TrackIdealPR
                         Tr/TrackInterfaces
			 Tr/TrackKernel
                         Tr/TrackMCTools
                         Tr/TrackMonitors
                         Tr/TrackProjectors
                         Tr/TrackTools
                         Tr/TrackUtils
                         Tr/TrackVectorFit
                         Velo/VeloRecMonitors
                         # Extra packages only for v19r2
                         # LHCb (not in v40r1)
                         Event/TrackEvent
                         )


gaudi_add_test(QMTest QMTEST)
